mod generated_model;
use clap;
use generated_model::*;

use std::fs::File;
use std::io::prelude::*;

use indicatif::ProgressBar;
use rand::prelude::*;
use rand_distr::{Distribution, Poisson};
use std::time::{Duration, Instant};

use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[clap(short, long)]
    until: f64,
    #[clap(short, long)]
    num_obs: usize,

    #[clap(short, long)]
    tau: Option<f64>,
}

pub enum StepResult {
    AllGood,
    NoReactionPossible,
    //StructuralChange { idx_of_reaction: usize },
    OnlyObservation,
}

impl generated_model::GeneratedModel {
    #[cfg(feature = "ssa")]
    fn step(&mut self, io_step: f64) -> StepResult {
        if self.propensity_sum <= 0.0 {
            self.update_all_propensities();
        }
        if self.propensity_sum <= 0.0 {
            return StepResult::NoReactionPossible;
        }
        let distr = rand_distr::Exp::new(self.propensity_sum).unwrap();
        let timestep = distr.sample(&mut self.rng);
        if timestep + self.time > self.last_io + io_step {
            let old_time = self.time;
            self.time = self.last_io + io_step;
            self.last_io = self.time;
            self.make_observation();
            self.time = old_time;
        }
        self.time += timestep;

        let propensity_threshold = self.rng.gen_range(0.0, self.propensity_sum);
        let idx = match self
            .propensities
            .iter()
            .scan(0.0, |sum, p| {
                *sum += p;
                Some(*sum)
            })
            .enumerate()
            .find(|(_idx, sum)| *sum > propensity_threshold)
        {
            None => panic!("reaction finding problem"),
            Some((idx, _)) => idx,
        };

        //println!("{} -> {}", self.time, idx);

        self.fire_once_by_idx(idx);

        return StepResult::AllGood;
    }

    #[cfg(feature = "tau")]
    pub fn step_tau(&mut self, mut planned_step_size: f64, io_step_size: f64) -> StepResult {
        if planned_step_size >= io_step_size {
            planned_step_size = io_step_size / 2.;
        }

        let do_observe = self.time + planned_step_size > self.last_io + io_step_size;
        if do_observe {
            assert!(planned_step_size > 0.);
            let stepsize_in = planned_step_size;
            planned_step_size = self.last_io + io_step_size - self.time;
            if planned_step_size <= 0. {
                self.make_observation();
                self.last_io = self.time;
                return StepResult::OnlyObservation;
            }
            assert!(planned_step_size > 0.);
            assert!(planned_step_size <= stepsize_in);
        }

        let fireings: Vec<usize> = self
            .propensities
            .clone()
            .into_iter()
            .map(|prop| {
                if prop > 0. {
                    let lambda = prop * planned_step_size;
                    //println!("{} = {} * {}",lambda,prop,planned_step_size);

                    if lambda < 1e-12 {
                        return 0;
                    }

                    if lambda == 0. {
                        return 0;
                    }
                    let dstr = Poisson::new(lambda);
                    //println!("Poisson at prop={}, stepsize={}",prop,step_size);
                    if dstr.is_ok() {
                        let res: u64 = dstr.unwrap().sample(&mut self.rng);
                        let res = res as usize;
                        //println!(" -> {}",res);
                        res
                    } else {
                        panic!(
                            "Poisson error at prop={}, stepsize={}",
                            prop, planned_step_size
                        );
                    }
                } else {
                    0
                }
            })
            .collect();
        let fireings: [usize; GeneratedModel::NUM_REAC] = fireings.try_into().unwrap();
        let mut tmp_new_counts = self.species_amounts.to_array();
        let old_glob_vals = self.global_vals.clone();
        let total_num_firings = fireings.iter().sum::<usize>();
        //println!("tau steps: {}",fireings.iter().sum::<u64>());
        /*for (idx, n) in fireings.iter().enumerate().filter(|(_idx, n)| **n > 0) {
            self.fire_reaction_tau(idx, *n as usize, &mut tmp_counts);
        }*/
        self.fire_all_tau(&fireings, &mut tmp_new_counts);

        //println!("{:?}{:?}",fireings,tmp_counts);
        let all_values_positive = tmp_new_counts.iter().all(|k| *k >= 0);
        //let mut new_counts: Vec<_> = tmp_counts.into_iter().map(|k| k as usize).collect();
        let old_countsn = self.species_amounts.to_array();
        self.species_amounts = SpeciesAmounts::from_array(&tmp_new_counts);
        self.update_all_propensities();
        let all_still_possible = fireings
            .iter()
            .enumerate()
            .filter(|(_idx, n)| **n > 0)
            .all(|(idx, _)| self.propensities[idx] > 0.);
        let mut detailed_check = true;
        if !all_still_possible {
            detailed_check = total_num_firings <= 1;
        }
        if all_values_positive && (all_still_possible || detailed_check) {
            // Accept
            self.reaction_counts
                .iter_mut()
                .zip(fireings.iter())
                .for_each(|(count, add)| *count += *add as usize);
            //self.metrics.real_steps += 1;
            self.time += planned_step_size;
            //println!("STEPS: {}",total_num_firings);
            if do_observe {
                self.last_io = self.time;
                self.make_observation();
            }
            if total_num_firings > 1 {
                //self.metrics.virtual_steps += total_num_firings as usize - 1;
            }
            StepResult::AllGood
        } else {
            //println!("deeper!");
            //self.metrics.steps_where_tau_too_large += 1;
            self.species_amounts = SpeciesAmounts::from_array(&old_countsn);
            self.global_vals = old_glob_vals;
            self.step_tau(planned_step_size / 2., io_step_size);
            self.step_tau(planned_step_size / 2., io_step_size);
            StepResult::AllGood
        }

        /*if self.metrics.real_steps > 100 {
            panic!("abort");
        }*/
    }
}
fn main() {
    let args = Args::parse();
    let io_step = args.until / args.num_obs as f64;

    println!("SETTING {:?}!", args);

    let mut model = GeneratedModel::new();
    let pb = ProgressBar::new(1000);
    let mut num_stebs: usize = 0;
    let start = Instant::now();
    while model.time < args.until {
        let step_res: StepResult;
        #[cfg(feature = "tau")]
        {
            step_res = model.step_tau(
                args.tau.expect("Feature set to tau, but no tau given!"),
                io_step,
            );
        }
        #[cfg(feature = "ssa")]
        {
            step_res = model.step(io_step);
        }
        /*match args.tau {
            None => {step_res = model.step(io_step);},
            Some(t) => {
                if cfg!(tau){
                    #[cfg(feature = "tau")]{
                        step_res = model.step_tau(t, io_step);
                    }
                } else {
                    panic!("unreachable?? tau is set to {}",cfg!(tau));
                }
                //return unreachable!();
            },
        };*/
        match step_res {
            StepResult::AllGood => {}
            StepResult::NoReactionPossible => model.time += io_step / 2.,
            StepResult::OnlyObservation => model.time += io_step / 4.,
        }
        if num_stebs % 100000 == 0 {
            pb.set_position((model.time / args.until * 1000.) as u64);
        }
        num_stebs += 1;
    }
    pb.finish();
    let time = start.elapsed().as_secs_f64();
    println!(
        "{} steps in {} seconds\n{} steps per second",
        num_stebs,
        time,
        num_stebs as f64 / time
    );
    let mut file = File::create("output_code_gen.csv").unwrap();
    write!(file, "{}", GeneratedModel::observation_first_line());
    write!(file, "{}", model.observation);
    println!("{:?}", model.reaction_counts);
}
