import subprocess
import pandas as pd
import tabulate
import tqdm
import matplotlib.pyplot as plt
import time
import numpy as np


models = ["hybrid","dyn","static"]

def bench(model,runtime):
    if model =="hybrid":
        subprocess.check_output("cargo build --release --features hybrid_att",shell=True)
    else:
        subprocess.check_output("cargo build --release",shell=True)
    print("model: ",model)
    t = time.perf_counter()
    dyn_flag = ""
    if model =="dyn":
        dyn_flag = "--dynamic"
    subprocess.check_output(f"target/release/ml_rules_3_term benchmarkmodels/network_{model}.mlr --simulate-until {runtime}[s] --output-rule-counts {dyn_flag}",shell=True)
    runtime = time.perf_counter() - t
    df = pd.read_csv("output_rule_counts.csv")
    num_reacs = df.iloc[-1].drop("time[s]").sum()
    return (runtime,num_reacs)

measure_time = 8
def gather_data():
    dfs = []
    for v in range(5):
        for m in models:
            (t_init,num_initial) = bench(m,1e-8)
            run_target = 20
            t_run = 0
            num_run = 0
            #while(t_run < 5 or t_run < 5 * t_init or num_initial *100 > num_run):
            #print(f"{t_run} < 5 or {t_run} < 5 * {t_init} or {num_initial} * 100 > {num_run}")
            (t_run,num_run) = bench(m,run_target)
            while(t_run < 0.8*measure_time or t_run > 1.2*measure_time):
                run_target = run_target*measure_time / (t_run+0.1)
                (t_run,num_run) = bench(m,run_target)
                print(f" ---- {t_run} not good -> new run target {run_target}")

            #    run_target *=2
            r=  {"model":m,"init":t_init,"event_rate": (num_run-num_initial)/(t_run - t_init), "t_run": t_run, "t_end" :run_target}
            dfs.append(r)
        df = pd.DataFrame(dfs)
        df.to_csv("Hybrid_benchmark.csv")
        print(df.groupby("model").mean().to_markdown())


gather_data()