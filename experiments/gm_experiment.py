import numpy as np
import subprocess
import pandas as pd
import matplotlib.pyplot as plt
import os
import string
import random
import multiprocessing as mp
import tqdm
import matplotlib.ticker as mtick
import scipy.stats as stats

def exec(config):
    prefix =  ''.join(random.choice(string.ascii_lowercase) for i in range(10))
    cmd = f"./target/release/ml_rules_3_term benchmarkmodels/GM.mlr --output-prefix={prefix} --simulate-until 96[h] --output-csv --dynamic-silent --set-constant temp={config['temp']} --set-constant initial_GM=20 --set-constant initial_GM_length={config['length']} --num-observation=1000 --set-constant initial_GM_weight={config['weight']} --set-constant ox_sat={config['ox_sat']}"
    #print(cmd)
    subprocess.run(cmd,check=True,stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL,shell=True)
    df = pd.read_csv(f"{prefix}.csv")
    os.remove(f"{prefix}.csv")
    return df

def get_data2():
    configs = []
    temp = 4.0
    weights = np.linspace(150,4500,9)
    #for weight in weights:
    for temp in np.linspace(2,17,20):
        #for (temp,length,weight) in [(2.0,445,890),(2.0,560 ,1790),(6.0,560,1740),(6.0, 385 , 570)]:
        for ox_sat in np.linspace(20,30,num=20):
            length = 500
            weight = 3000
            configs.append({"temp":temp,"weight": weight,"ox_sat":ox_sat,"length":length})
    configs = configs *1500
    subprocess.check_output("cargo build --release",shell=True)
    pool = mp.Pool(mp.cpu_count())
    results = list(tqdm.tqdm(pool.imap_unordered(exec,configs),total=len(configs)))
    results = pd.concat(results)
    results.to_csv("GM_data2.csv")

def get_data():
    #results = pd.DataFrame()
    configs = []
    for (temp,length,weight) in [(2.0,445,890),(2.0,560 ,1790),(6.0,560,1740),(6.0, 385 , 570)]:
            for ox_sat in np.linspace(20,30,num=40):
                configs.append({"temp":temp,"weight": weight,"ox_sat":ox_sat,"length":length})
    configs = configs *3000
    subprocess.check_output("cargo build --release",shell=True)
    pool = mp.Pool(mp.cpu_count())
    results = list(tqdm.tqdm(pool.imap_unordered(exec,configs),total=len(configs)))
    results = pd.concat(results)
    results.to_csv("GM_data.csv")
def plot_data():
    df = pd.read_csv("GM_data.csv")
    df = df.sort_values("ox_sat")
    print(df.columns)
    for gm_weight in set(df['initial_GM_weight']):
        for temp in set(df['temp']):
            print("gm weight, ",gm_weight, " && temp = ",temp)
            sub_df = df[(df['time[s]'] == 9600) & (df['initial_GM_weight'] == gm_weight) & (df['temp'] == temp)]
            sub_df_mean = sub_df.groupby("ox_sat").mean()
            sub_df_std = sub_df.groupby("ox_sat").sem()
            if (len(sub_df_mean) > 0):
                plt.plot(sub_df_mean.index,1 - sub_df_mean["##GM/initial_GM"],label=f"weight={gm_weight}, temp={temp}")
                #plt.errorbar(sub_df_mean.index,1 - sub_df_mean["/"],yerr=sub_df_std["/"],capsize=4,label=f"weight={gm_weight}, temp={temp}")
    plt.legend()
    plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1))
    plt.gca().xaxis.set_major_formatter(mtick.PercentFormatter(xmax=100))
    plt.ylabel("GM mortality")
    plt.xlabel("Oxygen saturation")
    plt.savefig("gm_plot.pdf")
    plt.savefig("gm_plot.svg")
    plt.show()

def plot_data2():
    df = pd.read_csv("GM_data2.csv")
    df = df.sort_values("time[s]")
    print(df.columns)
    for gm_weight in set(df['initial_GM_weight']):
        for temp in set(df['temp']):
            for ox_sat in set(df["ox_sat"]):
                print("gm weight, ",gm_weight, " && temp = ",temp)
                sub_df = df[(df['ox_sat'] == ox_sat) & (df['initial_GM_weight'] == gm_weight) & (df['temp'] == temp)]
                sub_df_mean = sub_df.groupby("time[s]").mean()
                sub_df_std = sub_df.groupby("time[s]").sem()
                if (len(sub_df_mean) > 0):
                    label = f"weight={gm_weight}, temp={temp}, ox={ox_sat}"
                    if ox_sat != min(df['ox_sat']):
                        label = None
                    plt.plot(sub_df_mean.index/(60*60*24),1 - sub_df_mean["##GM/initial_GM"],label=label)
                    #plt.errorbar(sub_df_mean.index,1 - sub_df_mean["/"],yerr=sub_df_std["/"],capsize=4,label=f"weight={gm_weight}, temp={temp}")
    plt.legend()
    plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1))
    #plt.gca().xaxis.set_major_formatter(mtick.PercentFormatter(xmax=100))
    plt.ylabel("GM mortality")
    plt.xlabel("time")
    #plt.savefig("gm_plot.pdf")
    #plt.savefig("gm_plot.svg")
    plt.show()

get_data2()
#plot_data2()
