import numpy as np
import os
import pandas as pd
import subprocess
import tqdm
import time

import matplotlib.pyplot as plt

apporx_values = ["0.2","0.1","0.05","0.02","0.01"]

results_exact = []
results_approx = [[] for _ in range( len(apporx_values))]

target_time = "10"

reps = 30
timings = np.zeros(len(apporx_values) + 1)

for _ in tqdm.tqdm(range(reps)):
    start = time.time()
    subprocess.check_output("cargo run --release -- exponential.mlr --simulate "+target_time+" 100",shell=True)
    timings[-1] += time.time() - start
    data = pd.read_csv("output.csv")

    results_exact.append(data.filter(regex="A__*").sum(axis=1))

    for (n,apx) in enumerate(apporx_values):
        start = time.time()
        subprocess.check_output("cargo run --release -- exponential.mlr --simulate "+target_time+" 100 --approximate "+str(apx),shell=True)
        timings[n] += time.time() - start
        data = pd.read_csv("output.csv")
        results_approx[n].append(data.filter(regex="A__*").sum(axis=1))

timings = timings[-1]/timings

print("approximation:",apporx_values)
print("speedub: ",timings)


df_exact = np.transpose(np.array(results_exact))


plt.semilogy(data["time[s]"],df_exact.sum(axis=1),label="exact")
#plt.xlim(xmin=float(target_time)/2.)
for (n,apx) in enumerate(apporx_values):
    df_approx = np.transpose(np.array(results_approx[n]))
    plt.semilogy(data["time[s]"],df_approx.sum(axis=1),"--",label="approx "+str(float(apx)*100.)+"%")
plt.legend()
plt.savefig("errors.pdf")
plt.savefig("errors.svg")
plt.show()





