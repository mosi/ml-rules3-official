import pandas as pd
import glob

#df = pd.DataFrame()
dfs = []
for (n,f) in enumerate(glob.glob("not_split/output*.csv")):
    mdf = pd.read_csv(f)
    mdf["run"] =n
    dfs.append(mdf)
df = pd.concat(dfs)
print(df)
df.to_csv("output_merged_not_split.csv")