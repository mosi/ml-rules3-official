/* (Streamlined) mRNA Delivery model by Ligon et al. 2014 */

/* rate constants */
const kA = 0.03 [1/h];
const kW = 1e20 [1/h];
const kE = 0.8 [1/h];
const kL = 0.065 [1/h];
const dE = 0.65 [1/h];
const kU = 1e6 [1/h];
const dL = 1e-6 [1/h];
const dM = 0.062 [1/h];
const kTL = 170 [1/h];
const kM = 5.5 [1/h];
const dG = 0.056 [1/h];

/* other constants */
const iT = 1 [h];

/* species */
L();
P();
System();
mRNA();
GFPu();
GFP();
Cell();

/* Initial solution */
Initial: System{10 L{350 mRNA} + 1 Cell{}};

/* attachment */
System{L:l{?solL} + Cell{?solC} + ?solS} -> System{Cell{?solC} + P{L{?solL}} + ?solS} @@ kA*#l;

/* wash*/
System{L:l{?solL} + ?solS} ->  System{?solS} @ if time() > iT then kW;

/* endocytose */
System{Cell{?solCell}+P:p{?solP}+?solS} -> System{Cell{P{?solP} + ?solCell}+?solS} @ kE;

/* lyse */
System{Cell{P{?solP}+?solC}+?solS} -> System{Cell{?solP+?solC}+?solS} @ kL;

/* unpack */
System{Cell{L:l{?solL}+?solC}+?solS} -> System{Cell{?solL + ?solC}+?solS} @ kU;