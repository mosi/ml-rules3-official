extern crate core;

use askama::Template;
use std::{fs, io};

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use chrono::{DateTime, Local};
use clap::Parser;
use log::*;
use ml_rules_3_core::expansion::static_expansion::FlatModel;

use ml_rules_3_core::simulator::simulate_fast::{OutputOptions, SimulationFast};
use rayon::prelude::*;
use sha2::{Digest, Sha512};

use ml_rules_3_core::running::{read_model_from_file_and_expand, run_in_term};
use std::process::Command;

fn calculate_hash(t: &String) -> u8 {
    let mut s = Sha512::new();
    //s.update(&t);
    Digest::update(&mut s, &t);
    s.finalize()[0]
}

fn do_code_gen(m2: FlatModel, sim_param: Option<(f64, usize)>, tau: Option<f64>) {
    let model = SimulationFast::new(&m2, None, false, None).unwrap();
    //println!("{}", model.render().unwrap());
    let _existing_hash = {
        let f = File::open("generated_sim/src/generated_model.rs");
        let mut buffer = String::new();
        match f {
            Ok(mut fil) => {
                fil.read_to_string(&mut buffer).unwrap();
            }
            Err(_) => buffer = "nothing to see here".to_string(),
        }
        calculate_hash(&buffer)
    };
    {
        let mut file = File::create("generated_sim/src/generated_model.rs").unwrap();
        let proposed_model = model.render().unwrap();
        //panic!("{} {}",existing_hash,calculate_hash(&proposed_model));
        //if existing_hash != calculate_hash(&proposed_model) {
        //    println!("Rewrite model");
        writeln!(file, "{}", proposed_model).unwrap();
        //} else {println!("Reuse model");}
    }

    match sim_param {
        None => {}
        Some((until, step)) => {
            let status = Command::new("cargo")
                .args(&[
                    "build",
                    "--release",
                    "--manifest-path=generated_sim/Cargo.toml",
                    "--features",
                    match tau {
                        Some(_) => "tau",
                        None => "ssa",
                    },
                ])
                .status()
                .expect("compile failed to execute");

            if status.success() {
                match tau {
                    None => Command::new("generated_sim/target/release/generated_sim")
                        .arg(format!("--until={}", until))
                        .arg(format!("--num-obs={}", step))
                        .status()
                        .unwrap(),
                    Some(tau) => Command::new("generated_sim/target/release/generated_sim")
                        .arg(format!("--until={}", until))
                        .arg(format!("--num-obs={}", step))
                        .arg(format!("--tau={}", tau))
                        .status()
                        .unwrap(),
                };
            } else {
                panic!("Could not Compile successfully.")
            }
        }
    }
    return;
}

fn swap_braces_and_brackets_in_file(input_path: &str) -> io::Result<()> {
    // Read the contents of the input file.
    let mut input_file = fs::File::open(input_path)?;
    let mut input_contents = String::new();
    input_file.read_to_string(&mut input_contents)?;

    // Replace '{' with '[' and '}' with ']'.
    let swapped_contents = input_contents
        .replace('{', "__square_start___")
        .replace('}', "__square_end___")
        .replace("[", "__curly_start___")
        .replace("]", "__curly_stop___")
        .replace("__square_start___", "[")
        .replace("__square_end___", "]")
        .replace("__curly_start___", "{")
        .replace("__curly_stop___", "}");
    // Write the modified contents to the output file.
    let mut output_file = fs::File::create(input_path)?;
    output_file.write_all(swapped_contents.as_bytes())?;

    Ok(())
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// file of the model
    input_file: String,

    /// Simulation end
    #[arg(long)]
    simulate_until: Option<String>,

    /// Number of simulation observations
    #[arg(long, default_value_t = 10)]
    num_observation: usize,

    /// allow dynamic restructuring/nesting
    #[arg(long)]
    dynamic: bool,

    /// allow dynamic restructuring/nesting, but prints no output
    #[arg(long)]
    dynamic_silent: bool,

    /// This debug function prints at every step and change, the structure and all reactions that are fired
    #[arg(long)]
    print_every_step: bool,

    /// Do use a cache file. This is usefull for some performance measures.
    #[arg(long)]
    cache_file: bool,

    /// Tau leaping step
    #[arg(long)]
    tau_step: Option<f64>,

    /// Approximation grade
    #[arg(long)]
    approx: Option<f64>,

    /// generate optimized code for not dynamic models
    #[arg(long)]
    code_gen: bool,

    /// Number of parallel replications to be executed (in parallel)
    #[arg(long, default_value_t = 1)]
    num_reps: usize,

    /// Maximum number of "Number of steps used for expansion
    #[arg(long, default_value_t = 100)]
    expansion_steps: usize,

    /// Output prefix
    #[arg(long)]
    output_prefix: Option<String>,

    /// give a json file output
    #[arg(long)]
    output_json: bool,

    /// give a json zip file output
    #[arg(long)]
    output_json_zip: bool,

    /// give a flat table output
    #[arg(long)]
    output_flat_table: bool,

    /// give a basic csv counts
    #[arg(long)]
    output_csv: bool,

    /// give rule counts
    #[arg(long)]
    output_rule_counts: bool,

    /// give latex version of input model
    #[arg(long)]
    output_latex: bool,

    /// generate simple plots of the observations
    #[arg(long)]
    output_plot: bool,

    /// advanced option to generate new performance templates
    #[arg(long)]
    no_new_templates: bool,

    /// switch parenthesis from [] and {} to {} and []
    #[arg(long)]
    switch_parenthesis: bool,

    /// set a constant to a different value
    #[arg(long)]
    set_constant: Vec<String>,
    /// (experimental) seed for simulation
    #[arg(long)]
    seed: Option<u64>,
}

fn write_file(filename: &String, outname: &Option<String>) {
    // Read the contents of the input file
    let input_path = Path::new(filename);
    let mut input_file = File::open(input_path).unwrap();

    let mut input_contents = String::new();
    input_file.read_to_string(&mut input_contents).unwrap();

    // Get the current date and time
    let current_datetime: DateTime<Local> = Local::now();

    // Format the date and time as a string
    let formatted_datetime = current_datetime.format("%Y-%m-%d %H:%M:%S");

    // Create the new content with the current date and time at the top
    let new_content = format!(
        "// Date and Time: {}\n{}",
        formatted_datetime, input_contents
    );
    let outfile = outname.as_ref().unwrap_or(&"output".to_string()).to_owned() + "_saved_model.mlr";
    // Create or open the output file and write the new content
    let output_path = Path::new(&outfile);
    let mut output_file = File::create(output_path).unwrap();

    output_file.write_all(new_content.as_bytes()).unwrap();
}

fn main() {
    env_logger::init();
    info!("Starting ML-Rules 3");
    let cli: Cli = Cli::parse(); /*App::new("mlr expander")
                                 .version("0.1")
                                 .author("Till Köster")
                                 .about("Expanding ML-Rules models into reaction networks (and some basic execution functionality")
                                 .arg(Arg::with_name("INPUT FILE")
                                     .help("Sets the input file to read the model from (.mlr)")
                                     .required(true)
                                     .index(1))
                                 .arg(Arg::with_name("simulate").long("simulate").help("Runs a basic(slow) Simulation. Until is the model time to run to in seconds. IO STEPS specifies the number of outputs.").value_names(&["UNTIL","IO STEP"]))
                                 .arg(Arg::with_name("keep-parameters").long("keep-parameters").help("Keep the parameters (defined with param instead of const) for the expanded model."))
                                 .arg(Arg::with_name("approximate").long("approximate").help("allow for a small deviation of the rate calculations").takes_value(true).default_value("0.01"))

                                 .arg(Arg::with_name("big-flat-table-output").long("big-flat-table-output").help("write a bit csv table with all flat attributes").takes_value(false))
                                 .get_matches();*/
    if cli.switch_parenthesis {
        swap_braces_and_brackets_in_file(&*cli.input_file).expect("IO problem");
    }

    let output = OutputOptions {
        json: cli.output_json,
        compressed_json: cli.output_json_zip,
        flat_table: cli.output_flat_table,
        rule_counts: cli.output_rule_counts,
        plot: cli.output_plot,
        regular_csv: cli.output_csv,
        latex: cli.output_latex,
        debug_outputs: false,
    };
    //let filename = cli.input_file;

    let m2 = read_model_from_file_and_expand(
        &cli.input_file,
        cli.dynamic || cli.dynamic_silent,
        cli.cache_file,
        cli.expansion_steps,
        &output,
        &cli.set_constant,
    );
    write_file(&cli.input_file, &cli.output_prefix);
    let m2 = match m2 {
        None => return,
        Some(x) => x,
    };

    let sim_param = match &cli.simulate_until {
        None => None,
        Some(x) => {
            let until = x.clone();
            let until = ml_rules_3_core::input::semantic_conversion::string_to_seconds(&until)
                .expect("Could not parse endtime");
            //let until: f64 = until.parse().expect("the UNTIL value is invalid. It should be a floatingpoint number (e.g. 1., 1e7, 0.123, ...)");
            println!("calculated {until} seconds of runtime");
            let step = cli.num_observation;
            if step < 5 {
                panic!("Choose a larger number of outputs (at least 5)");
            }
            Some((until, step))
        }
    };
    if cli.code_gen {
        do_code_gen(m2, sim_param, cli.tau_step);
        return;
    }
    match sim_param {
        None => {}
        Some((until, step)) => {
            /*let tau: Option<f64> = match cli.values_of("tau") {
                None => None,
                Some(mut y) => Some(y.next().unwrap().parse().unwrap()),
            };*/

            if cli.tau_step.is_some() && cli.approx.is_some() {
                panic!("You can not use Tau and approx!");
            }

            assert!(cli.num_reps > 0);
            if cli.num_reps == 1 {
                /*let o_name = match cli.output_prefix {
                    None => None,
                    Some(mut x) => Some(x.next().unwrap().parse().unwrap()),
                };*/
                run_in_term(
                    m2.clone(),
                    until,
                    cli.tau_step,
                    step,
                    cli.approx,
                    !cli.dynamic_silent,
                    cli.print_every_step,
                    cli.output_prefix,
                    cli.num_reps != 1,
                    &output,
                    !cli.no_new_templates,
                    cli.seed,
                );
            } else {
                (0..cli.num_reps).into_par_iter().for_each(|rep_num| {
                    let mut o_name = cli.output_prefix.clone();

                    if cli.num_reps != 1 {
                        o_name = Some(match &cli.output_prefix {
                            Some(x) => format!("{x}_{rep_num}"),
                            None => format!("output_{}", rep_num),
                        });
                    }
                    run_in_term(
                        m2.clone(),
                        until,
                        cli.tau_step,
                        step,
                        cli.approx,
                        !cli.dynamic_silent,
                        cli.print_every_step,
                        o_name,
                        cli.num_reps != 1,
                        &output,
                        cli.no_new_templates,
                        cli.seed,
                    );
                });
            }
            /*{
                model: m2,
                target_time: until,
                step_size: until / step as f64,
                output_file_name: "output.csv".to_string(),
                reaction_count_file_name: None,
            };*/
            /*println!("{:?}", sim.observation_values);
            println!("glob: {:?}", sim.obs_global_vals_names);
            println!("glob: {:?}", sim.global_values);
            println!("{:?}", sim.obs_times);*/
        }
    }
}

/*#[cfg(test)]
mod tests {
    use crate::{do_code_gen, read_model_from_file_and_expand, run_in_term};
    const EXPANSION_STEPS: usize = 100;
    #[test]
    #[ignore]
    fn testmodel() {
        let m =
            read_model_from_file_and_expand("testmodel.mlr", false, false, false, EXPANSION_STEPS)
                .unwrap();
        /*run_in_term(
            m, 2., None, 30, None, false, false, None, false, false, false, false,
        );*/
    }

    #[test]
    #[ignore]
    fn testmodel_pp() {
        let m = read_model_from_file_and_expand("pp.mlr", false, false, false, EXPANSION_STEPS)
            .unwrap();
        /* run_in_term(
            m, 2.0e-6, None, 30, None, false, false, None, false, false, false, false,
        );*/
    }

    #[test]
    #[ignore]
    fn testmodel_tau() {
        let m = read_model_from_file_and_expand("pp.mlr", false, false, false, EXPANSION_STEPS)
            .unwrap();
        /*run_in_term(
            m,
            2.0e-6,
            Some(1e-10),
            30,
            None,
            false,
            false,
            None,
            false,
            false,
            false,
            false,
        );*/
    }

    #[test]
    #[ignore]
    fn testmodel_pp_code_gen() {
        let m = read_model_from_file_and_expand("pp.mlr", false, false, false, EXPANSION_STEPS)
            .unwrap();
        do_code_gen(m, Some((2e-6, 10)), None);
        //run_in_term(m,2.0e-6,None,30,None,false);
    }
}
*/
