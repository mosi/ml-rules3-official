use crate::buildin_functions::BuildinFunction;
use crate::expansion::static_expansion::{EnumeratedSpecies, OptimizedString, SpeciesAmt};
use crate::input::structures::{
    FloatOp, PrimitiveTypes, RateExpr, RateExprTerm, SpeciesLeftIdentifier,
};
use crate::simulator::evaluation::IndexedRateExprTerm::StarAttRefComp;
use crate::simulator::simulate_fast::to_uindex;
use itertools::izip;
use std::collections::{BTreeSet, HashMap};

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct MultiResultAssociation {
    pub species_idx: usize,
    pub variation_idx: usize,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OneMultiResult {
    pub matches_to_get_to_value: Vec<MultiResultAssociation>,
    pub value: PrimitiveTypes,
}

pub fn is_submatch(a: &Vec<MultiResultAssociation>, b: &Vec<MultiResultAssociation>) -> bool {
    for aa in a.iter() {
        if !b.contains(aa) {
            return false;
        }
    }
    return true;
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct MultiResults {
    pub what: Vec<SpeciesLeftIdentifier>,
    pub results: Vec<OneMultiResult>,
}
impl MultiResults {
    pub fn debug_is_complete(&self) {
        for r in self.results.iter() {
            debug_assert_eq!(r.matches_to_get_to_value.len(), self.what.len());
        }
    }
}

impl MultiResults {
    pub fn to_string(&self, string_conversion: &HashMap<String, OptimizedString>) -> String {
        let mut s = "".to_string();
        for k in self.results.iter() {
            s += &*format!("{} for ", k.value.to_string(string_conversion));
            for (a, b) in izip!(&self.what, &k.matches_to_get_to_value) {
                s += &*format!(" {:?}/{}.{}", a, b.species_idx, b.variation_idx);
            }
        }
        s
    }
}

pub fn merge_multi_results(
    a_matches: &Vec<MultiResultAssociation>,
    a_what: &Vec<SpeciesLeftIdentifier>,
    b_matches: &Vec<MultiResultAssociation>,
    b_what: &Vec<SpeciesLeftIdentifier>,
    merged_whats: &Vec<SpeciesLeftIdentifier>,
) -> Option<Vec<MultiResultAssociation>> {
    debug_assert_eq!(a_what.len(), a_matches.len());
    debug_assert_eq!(b_what.len(), b_matches.len());
    let result: Vec<_> = merged_whats
        .iter()
        .map(|w| {
            let a_idx = a_what.iter().position(|k| k == w);
            let b_idx = b_what.iter().position(|k| k == w);
            match (a_idx, b_idx) {
                (None, None) => {
                    println!("{:?} & {:?} \nto {:?}", a_what, b_what, merged_whats);
                    unreachable!()
                }
                (Some(a), None) => Some(a_matches[a].clone()),
                (None, Some(b)) => Some(b_matches[b].clone()),
                (Some(a), Some(b)) => {
                    if a_matches[a] == b_matches[b] {
                        Some(a_matches[a].clone())
                    } else {
                        None
                    }
                }
            }
        })
        .collect();
    for k in result.iter() {
        if k.is_none() {
            return None;
        }
    }
    Some(result.into_iter().filter_map(|k| k).collect())
}

#[derive(Debug, PartialEq, Clone)]
pub enum EvaluationResult {
    Single(PrimitiveTypes),
    //Multi( HashMap<SpeciesLeftIdentifier,Vec<MultiResult>>),
    #[cfg(feature = "hybrid_att")]
    Multi(MultiResults),
}
impl EvaluationResult {
    pub fn single(&self) -> Option<&PrimitiveTypes> {
        match self {
            EvaluationResult::Single(x) => Some(x),
            #[cfg(feature = "hybrid_att")]
            EvaluationResult::Multi(_) => None,
        }
    }
}

#[derive(Clone, Debug)]
pub enum IndexedEnumeratedRate {
    _MassActionConstant(f64),
    _MassActionExpression(IndexedRateExprTerm),
    _ComplexExpression(IndexedRateExprTerm),
}

/*#[derive(Clone, Debug)]
pub enum IndexedRateExprTerm {
    Single(IndexedRateExprTerm),
    Multi(IndexedRateExprTerm, FloatOp, IndexedRateExprTerm),
}*/

impl IndexedRateExprTerm {
    pub fn from_expr(
        rate: &RateExpr,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        converte_left: &HashMap<SpeciesLeftIdentifier, usize>,
        species_counts: &Vec<SpeciesAmt>, //converte_globals: &HashMap<EnumeratedGlobalValue, usize>,
    ) -> IndexedRateExprTerm {
        match rate {
            RateExpr::Single(l) => {
                IndexedRateExprTerm::from(l, converse_map, converte_left, species_counts)
            }
            RateExpr::Double(l, op, r) => IndexedRateExprTerm::Expr(Box::new((
                IndexedRateExprTerm::from(l, converse_map, converte_left, species_counts),
                *op,
                IndexedRateExprTerm::from(r, converse_map, converte_left, species_counts),
            ))),
        }
    }
    /*pub fn get_involved(&self, species2rule: &mut BTreeSet<usize>, use_time: &mut bool) {
        match self {
            IndexedRateExprTerm::Single(x) => x.get_involved(species2rule, use_time),
            IndexedRateExprTerm::Multi(l, _, r) => {
                l.get_involved(species2rule, use_time);
                r.get_involved(species2rule, use_time)
            }
        }
    }*/
}

#[derive(Clone, Debug)]
pub struct IndexedAttributeRefReg {
    pub name: OptimizedString,
    pub idx_of_species: usize,
    pub id_on_left: SpeciesLeftIdentifier, // to determine correlation between Attributes on evaluation
}

#[derive(Clone, Debug)]
pub struct IndexedAttributeRefComp {
    pub name: OptimizedString,
    pub idx_of_species: usize,
}

#[derive(Clone, Debug)]
pub struct IndexedCondition {
    pub guard: IndexedRateExprTerm,
    pub(crate) result_true: IndexedRateExprTerm,
    pub result_false: Option<IndexedRateExprTerm>,
}

#[derive(Clone, Debug)]
pub struct LeftSideAmtGuard {
    pub idices: Vec<usize>,
    pub idices_replication_num: Vec<usize>,
    pub idices_left_names: Vec<SpeciesLeftIdentifier>, // only needed for starr attribute
    pub is_mass_action: bool,
    pub inner: Box<IndexedRateExprTerm>,
    pub has_starr_attribut_amounts: bool,
}

#[derive(Clone, Debug)]
pub enum IndexedRateExprTerm {
    Primitive(PrimitiveTypes),

    LeftSideAmtGuard(LeftSideAmtGuard),
    StarAttRefComp(IndexedAttributeRefComp),
    Condition(Box<IndexedCondition>),
    CountFromLeftSimple(usize),
    CountFromLeftCpx(usize, SpeciesLeftIdentifier),
    CountGlobally(usize),
    SpecificCountFromGlobal(Vec<usize>),
    SpecifCountFromGlobalConstraint(Vec<(usize, Option<IndexedRateExprTerm>)>),
    BuildinFunction(BuildinFunction, Vec<IndexedRateExprTerm>),
    Expr(Box<(IndexedRateExprTerm, FloatOp, IndexedRateExprTerm)>),

    StarAttRefReg(IndexedAttributeRefReg),
}

impl IndexedRateExprTerm {
    fn from(
        rate: &RateExprTerm,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        converte_left: &HashMap<SpeciesLeftIdentifier, usize>,
        species_counts: &Vec<SpeciesAmt>, //converte_globals: &HashMap<EnumeratedGlobalValue, usize>,
    ) -> IndexedRateExprTerm {
        match rate {
            RateExprTerm::Primitive(t) => IndexedRateExprTerm::Primitive(t.clone()),
            RateExprTerm::ExpandedAttRef(x) => unreachable!("Att ref {:?}", x),
            RateExprTerm::StarAttRef(x) => {
                let idx = converte_left[&x.species_id];
                match species_counts[idx] {
                    SpeciesAmt::NumOnly(_) => unreachable!(),
                    SpeciesAmt::CompartmentWithStar(_, _) => {
                        StarAttRefComp(IndexedAttributeRefComp {
                            name: x.att_name.clone(),
                            idx_of_species: idx,
                        })
                    }
                    #[cfg(feature = "hybrid_att")]
                    SpeciesAmt::NotExpanded(_) => {
                        IndexedRateExprTerm::StarAttRefReg(IndexedAttributeRefReg {
                            name: x.att_name.clone(),
                            idx_of_species: idx,
                            id_on_left: x.species_id.clone(),
                        })
                    }
                }
                ////////i
            }
            /*IndexedRateExprTerm::GlobAttRef(
                *converte_globals
                    .get(&glob_val_from_left_ident(
                        &x.species_id,
                        converse_map,
                        converte_left,
                        &x.att_name,
                    ))
                    .unwrap(),
            ),*/
            RateExprTerm::Expr(x) => {
                match x.as_ref() {
                    RateExpr::Single(inner_exp) => {
                        IndexedRateExprTerm::from(
                            inner_exp,
                            converse_map,
                            converte_left,
                            species_counts, //converte_globals,
                        )
                    }
                    RateExpr::Double(_, _, _) => {
                        IndexedRateExprTerm::from_expr(
                            &(*x),
                            converse_map,
                            converte_left,
                            species_counts, //converte_globals,
                        )
                    }
                }
            }
            RateExprTerm::SpecCount(x) => {
                let idx = converte_left[&x.idx_on_left];
                match species_counts[idx] {
                    SpeciesAmt::NumOnly(_) => IndexedRateExprTerm::CountFromLeftSimple(idx),
                    SpeciesAmt::CompartmentWithStar(_, _) => {
                        IndexedRateExprTerm::CountFromLeftSimple(idx)
                    }
                    #[cfg(feature = "hybrid_att")]
                    SpeciesAmt::NotExpanded(_) => {
                        IndexedRateExprTerm::CountFromLeftCpx(idx, x.idx_on_left.clone())
                    }
                }
            }
            RateExprTerm::GlobalCount(gce) => {
                gce.to_indexed(converse_map, converte_left, species_counts)
                /*match gce {
                    GlobalCountEnum::GenericUnspecified(_) => {
                        unreachable!()
                    }
                    GlobalCountEnum::SpecificEnumerated(y) => {
                        IndexedRateExprTerm::SpecificCountFromGlobal(
                            y.iter()
                                .map(|v| converse_map.get(v).unwrap())
                                .cloned()
                                .collect(),
                        )
                    }
                }*/
                /*let mut res = vec![];
                if y.definition.len() != 1 {
                    todo!("not implemented for Multi names? Crazy interaction possible?");
                }
                for x in y.definition.iter() {
                    match x {
                        CountingElement::_SpecificNameFromLeft(_) => todo!(),
                        CountingElement::GeneralDescription(_name, constaints) => {
                            if !constaints.is_empty() {
                                todo!("Constaints on global counts not implemeted");
                            }
                            res.extend(
                                converse_map
                                    .iter()
                                    .filter(|(s, _)| s.species_name == y.name_in_model_code)
                                    .map(|(_, n)| *n),
                            )
                        }
                    }
                }
                IndexedRateExprTerm::SpecificCount(res)*/
            }
            RateExprTerm::Condition(cond) => {
                IndexedRateExprTerm::Condition(Box::new(IndexedCondition {
                    guard: IndexedRateExprTerm::from_expr(
                        &cond.guard,
                        converse_map,
                        converte_left,
                        species_counts,
                    ),
                    result_true: IndexedRateExprTerm::from_expr(
                        &cond.result_true,
                        converse_map,
                        converte_left,
                        species_counts,
                    ),
                    result_false: match &cond.result_false {
                        None => None,
                        Some(k) => Some(IndexedRateExprTerm::from_expr(
                            &k,
                            converse_map,
                            converte_left,
                            species_counts,
                        )),
                    },
                }))
            }
            RateExprTerm::BuildinFunction(bf) => IndexedRateExprTerm::BuildinFunction(
                bf.function,
                bf.params
                    .iter()
                    .map(|k| {
                        IndexedRateExprTerm::from_expr(
                            k,
                            converse_map,
                            converte_left,
                            species_counts,
                        )
                    })
                    .collect(),
            ),
        }
    }

    pub fn get_involved(&self, species2rule: &mut BTreeSet<usize>, use_time: &mut bool) {
        match self {
            IndexedRateExprTerm::Primitive(_) => {}
            IndexedRateExprTerm::Expr(y) => {
                let (l, _, r) = y.as_ref();
                l.get_involved(species2rule, use_time);
                r.get_involved(species2rule, use_time);
            }
            IndexedRateExprTerm::CountGlobally(y) => {
                species2rule.insert(to_uindex(y));
            }
            IndexedRateExprTerm::SpecificCountFromGlobal(y) => {
                species2rule.extend(y.iter().map(|k| to_uindex(k)));
            }
            IndexedRateExprTerm::StarAttRefReg(x) => {
                species2rule.insert(to_uindex(&x.idx_of_species));
            }
            StarAttRefComp(x) => {
                species2rule.insert(to_uindex(&x.idx_of_species));
            }
            IndexedRateExprTerm::Condition(k) => {
                k.guard.get_involved(species2rule, use_time);
                k.result_true.get_involved(species2rule, use_time);
                match &k.result_false {
                    None => {}
                    Some(p) => {
                        p.get_involved(species2rule, use_time);
                    }
                }
            }
            IndexedRateExprTerm::CountFromLeftCpx(y, _) => {
                species2rule.insert(to_uindex(y));
            }
            IndexedRateExprTerm::CountFromLeftSimple(y) => {
                species2rule.insert(to_uindex(y));
            }

            IndexedRateExprTerm::LeftSideAmtGuard(y) => {
                species2rule.extend(y.idices.iter());
                y.inner.get_involved(species2rule, use_time);
            }
            IndexedRateExprTerm::BuildinFunction(f, params) => {
                for p in params.iter() {
                    p.get_involved(species2rule, use_time);
                }
                match f {
                    BuildinFunction::Pi => {}
                    BuildinFunction::Euler => {}
                    BuildinFunction::Sin => {}
                    BuildinFunction::Cos => {}
                    BuildinFunction::Exp => {}
                    BuildinFunction::Round => {}
                    BuildinFunction::Ceil => {}
                    BuildinFunction::Floor => {}
                    BuildinFunction::RandomGauss => {}
                    BuildinFunction::RandomUniform => {}
                    BuildinFunction::Time => *use_time = true,
                    BuildinFunction::Abs => {}
                    BuildinFunction::AbsOrZero => {}
                    BuildinFunction::RandomPoisson => {}
                }
            }
            IndexedRateExprTerm::SpecifCountFromGlobalConstraint(x) => {
                for (idx, rate) in x.iter() {
                    species2rule.insert(*idx);
                    match rate {
                        None => {}
                        Some(x) => {
                            x.get_involved(species2rule, use_time);
                        }
                    }
                }
            }
        };
    }
}
