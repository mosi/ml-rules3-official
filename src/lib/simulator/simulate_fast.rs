use crate::expansion::static_expansion::*;
use crate::input::structures::{
    FloatOp, NonNumericalDimension, PrimitiveTypes, Species, SpeciesLeftIdentifier, SpeciesName,
    SpeciesNameConverter,
};
use crate::output::{
    OutputAttribute, OutputSpecies, OutputValue, ReactionStateOutput, SimOutput, SimOutputJson,
};
use askama::Template;
use fixedbitset::FixedBitSet;
use float_pretty_print::PrettyPrintFloat;
use hashbag::HashBag;
#[cfg(feature = "hybrid_att")]
use itertools::izip;
use itertools::Itertools;
use more_asserts::*;
#[cfg(feature = "hybrid_att")]
use rand::distributions::WeightedIndex;
use rand::prelude::*;
use rand_distr::Distribution;
//use rayon::prelude::*;
use core::default::Default;
use std::cmp::Ordering;
use std::collections::{BTreeSet, HashMap};
use std::convert::TryFrom;
use std::iter;
use std::mem::swap;

use crate::simulator::evaluation::*;
use crate::simulator::perf_templates_generated::{get_performance_template, RateOptimization};

//use rayon::prelude::*;

#[derive(Debug, Copy, Clone)]
pub struct Metrics {
    pub step_function_calls: usize,
    pub regular_steps: usize,
    pub virtual_steps: usize,
    pub steps_where_tau_too_large: usize,
    pub steps_where_approximated: usize,
    pub num_rate_updates: usize,
}
impl Metrics {
    pub fn output(&self, duration_in_sec: f64) {
        println!(
            "Computed {} steps in {:.6} seconds",
            self.regular_steps,
            PrettyPrintFloat(duration_in_sec)
        );
        println!(
            "throughput {:.6} steps per second",
            PrettyPrintFloat(self.regular_steps as f64 / duration_in_sec)
        );
        if self.virtual_steps > 0 {
            println!("TAU-leaping performed");
            println!(
                "  Approximated {} steps for {:.5}x speedup",
                self.virtual_steps,
                PrettyPrintFloat(self.virtual_steps as f64 / self.step_function_calls as f64)
            );
            println!(
                "  tau throughput {:.7} steps per second",
                PrettyPrintFloat(
                    (self.step_function_calls + self.virtual_steps) as f64 / duration_in_sec
                )
            );
            println!(
                "  tau step faild (step to large) for {:.5}%",
                PrettyPrintFloat(
                    self.steps_where_tau_too_large as f64 / self.step_function_calls as f64 * 100.
                )
            );
        }
    }
}

const USE_SLOW_FIRST_REACTION_METHOD: bool = false;
pub type Uindex = usize;
pub fn to_uindex(i: &usize) -> Uindex {
    match Uindex::try_from(*i) {
        Ok(y) => y,
        Err(_k) => {
            panic!("Could not convert index! 32 bit are not enough for {}", i)
        }
    }
}

#[derive(Clone)]
pub struct EnumeratedIndexedReaction {
    pub species_in_idx: Vec<Uindex>,
    pub species_in_names: Vec<SpeciesLeftIdentifier>,
    pub could_have_multi_result: bool,
    //pub species_in_amt: Vec<Uindex>,
    pub species_out: Vec<(Option<IndexedRateExprTerm>, Uindex)>,
    pub reaction_number_of_prototype: usize,
    invokes_structural_change: bool,
    //guards: Vec<IndexedLogicalConstraint>,
    //global_val_changes: Vec<(usize, IndexedRateExprTerm)>,
    rate: IndexedRateExprTerm,
    out_attribute_values: Vec<Vec<(OptimizedString, IndexedRateExprTerm)>>,
    dependend_reactions: Option<Vec<Uindex>>,
    dependend_propensities_in_triee: Vec<usize>,
    count: usize,
    rate_optimization: RateOptimization,
    max_count: Option<usize>,
}
impl EnumeratedIndexedReaction {
    fn _compute_tree_updates_slow(&self) -> Vec<usize> {
        assert!(self.dependend_propensities_in_triee.is_empty());

        // assert is sorted:
        debug_assert!(self
            .dependend_reactions
            .as_ref()
            .unwrap()
            .windows(2)
            .all(|slc| slc[0] <= slc[1]));
        if self.dependend_reactions.as_ref().unwrap().is_empty() {
            return vec![];
        }

        let mut r = vec![];
        let mut tree_stack = BTreeSet::new(); //BinaryHeap::new();
        let mut iter = self
            .dependend_reactions
            .as_ref()
            .unwrap()
            .iter()
            .rev()
            .peekable();
        while iter.peek().is_some() || !tree_stack.is_empty() {
            match (iter.peek(), tree_stack.last()) {
                (None, None) => {
                    unreachable!()
                }
                (Some(n), None) => {
                    if **n != 0 {
                        tree_stack.insert((**n - 1) / 2);
                    }
                    if r.last().unwrap_or(&usize::MAX) != *n {
                        r.push(**n);
                    }
                    iter.next().unwrap();
                }
                (None, Some(n)) => {
                    let n = n.clone();
                    if n != 0 {
                        tree_stack.insert((n - 1) / 2);
                    }
                    if *r.last().unwrap_or(&usize::MAX) != n {
                        r.push(n);
                    }
                    tree_stack.pop_last().unwrap();
                }
                (Some(n), Some(t)) => {
                    if *n < t {
                        let t = t.clone();
                        if t != 0 {
                            tree_stack.insert((t - 1) / 2);
                        }
                        if *r.last().unwrap_or(&usize::MAX) != t {
                            r.push(t);
                        }
                        tree_stack.pop_last().unwrap();
                    } else if *n > t {
                        if **n != 0 {
                            tree_stack.insert((**n - 1) / 2);
                        }
                        if r.last().unwrap_or(&usize::MAX) != *n {
                            r.push(**n);
                        }
                        iter.next().unwrap();
                    } else {
                        debug_assert_eq!(*n, t);
                        if **n != 0 {
                            tree_stack.insert((**n - 1) / 2);
                        }
                        if r.last().unwrap_or(&usize::MAX) != *n {
                            r.push(**n);
                        }
                        iter.next().unwrap();
                        tree_stack.pop_last();
                    }
                }
            }
        }
        debug_assert_eq!(self.compute_tree_updates(), r);
        return r;
    }
    fn compute_tree_updates(&self) -> Vec<usize> {
        assert!(self.dependend_propensities_in_triee.is_empty());

        // assert is sorted:
        debug_assert!(self
            .dependend_reactions
            .as_ref()
            .unwrap()
            .windows(2)
            .all(|slc| slc[0] <= slc[1]));
        if self.dependend_reactions.as_ref().unwrap().is_empty() {
            return vec![];
        }

        let max_size = self.dependend_reactions.as_ref().unwrap().last().unwrap();
        let mut tree_indices_to_update = FixedBitSet::with_capacity(*max_size + 1);
        debug_assert_eq!(tree_indices_to_update.count_ones(..), 0);

        for idx in self.dependend_reactions.as_ref().unwrap().iter() {
            let mut test_idx: usize = *idx;
            while !tree_indices_to_update[test_idx] {
                tree_indices_to_update.set(test_idx, true);
                if test_idx == 0 {
                    break;
                }
                test_idx = (test_idx - 1) / 2;
            }
        }
        let mut r: Vec<usize> = tree_indices_to_update.ones().collect(); //.sort();
        r.reverse();
        if cfg!(debug_assertions) {
            for idx in 0..r.len() {
                if idx < r.len() - 1 {
                    assert!(r[idx] > r[idx + 1]);
                }
            }
        }
        r
    }

    /*fn _merge_conditions(
        a: Vec<IndexedRateExprTerm>,
        b: Vec<IndexedRateExprTerm>,
    ) -> IndexedRateExprTerm {
        assert!(a.len() + b.len() > 0);
        a.into_iter()
            .chain(b.into_iter())
            .reduce(|v1, v2| {
                IndexedRateExprTerm::Expr(Box::new(IndexedRateExprTerm::Multi(
                    v1,
                    FloatOp::Mul,
                    v2,
                )))
            })
            .unwrap()
    }*/
    /*fn _guards_to_expressions(
        reac: &EnumeratedReaction,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        conv_left: &HashMap<SpeciesLeftIdentifier, usize>,
    ) -> Vec<IndexedRateExprTerm> {
        reac.guard
            .iter()
            .map(|v| {
                IndexedRateExprTerm::Condition(Box::new(IndexedCondition {
                    guard: IndexedRateExprTerm {
                        left: IndexedRateExprTerm::Expr(Box::new(IndexedRateExprTerm::from(
                            &v.left,
                            converse_map,
                            conv_left,
                        ))),
                        op: Some((
                            FloatOp::Comparison(v.op),
                            IndexedRateExprTerm::Expr(Box::new(IndexedRateExprTerm::from(
                                &v.right,
                                converse_map,
                                conv_left,
                            ))),
                        )),
                    },
                    result_true: IndexedRateExprTerm {
                        left: IndexedRateExprTerm::Primitive(PrimitiveTypes::Integer(1)),
                        op: None,
                    },
                    result_false: None,
                }))
            })
            .collect()
    }*/
    fn get_rate_opt(
        reac: &EnumeratedReaction,
        _converse_map: &HashMap<EnumeratedSpecies, usize>,
        _conv_left: &HashMap<SpeciesLeftIdentifier, usize>,
        _spec_counts: &Vec<SpeciesAmt>, //species_in_idx: &Vec<Uindex>,
        //species_in_amt: &Vec<Uindex>,
        rate: &IndexedRateExprTerm,
        generate_new: bool,
        all_species: &Vec<SpeciesAmt>,
    ) -> RateOptimization {
        if reac.max_count.is_some() {
            return RateOptimization::None;
        }
        get_performance_template(rate, generate_new, all_species)
    }
    fn combine_rate(
        reac: &EnumeratedReaction,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        conv_left: &HashMap<SpeciesLeftIdentifier, usize>,
        spec_counts: &Vec<SpeciesAmt>, //species_in_idx: &Vec<Uindex>,
                                       //species_in_amt: &Vec<Uindex>,
    ) -> IndexedRateExprTerm {
        let inner = match &reac.rate {
            EnumeratedRate::MassActionConstant(k) => {
                IndexedRateExprTerm::Primitive(PrimitiveTypes::Float(*k))
            }

            EnumeratedRate::MassActionExpression(y) => {
                IndexedRateExprTerm::from_expr(y, converse_map, conv_left, spec_counts)
            }
            EnumeratedRate::ComplexExpression(p) => {
                IndexedRateExprTerm::from_expr(p, converse_map, conv_left, spec_counts)
            }
        };
        let inner_w_guard = reac.guards_from_left_side.iter().fold(inner, |old, new| {
            IndexedRateExprTerm::Condition(Box::new(IndexedCondition {
                guard: IndexedRateExprTerm::from_expr(new, converse_map, conv_left, spec_counts),

                result_true: old,
                result_false: None,
            }))
        });
        let inner_w_guard = Box::new(inner_w_guard);
        /*println!(
            "REAC: {} in -> {:?}",
            reac.reaction_number_of_prototype, reac.species_in
        );*/
        let spec_in_fixed_order: Vec<_> = reac.species_in.iter().collect();
        let idices_replication_num: Vec<usize> = spec_in_fixed_order
            .iter()
            .enumerate()
            .map(|(num, (_left_id, spec_enum))| {
                spec_in_fixed_order
                    .iter()
                    .take(num + 1)
                    .filter(|(_search_left, search_enumerated)| spec_enum == search_enumerated)
                    .count()
            })
            .collect();
        //println!("+{:?}", spec_in_fixed_order);
        //println!("---{:?}", idices_replication_num);
        let mut idices_left_names: Vec<_> = spec_in_fixed_order
            .iter()
            .map(|(a, _)| (*a).clone())
            .collect();
        let has_starr_attribut_amounts =
            reac.species_in
                .values()
                .any(|p| match &spec_counts[converse_map[p]] {
                    SpeciesAmt::NumOnly(_) => false,
                    #[cfg(feature = "hybrid_att")]
                    SpeciesAmt::NotExpanded(_) => true,
                    // TODO is this correct? Do i not need special care for compartments with star?
                    SpeciesAmt::CompartmentWithStar(_, _s) => false, //s.names.len() > 0,
                });
        if !has_starr_attribut_amounts {
            idices_left_names.clear()
        }
        match &reac.rate {
            EnumeratedRate::MassActionConstant(_const_in) => {
                IndexedRateExprTerm::LeftSideAmtGuard(LeftSideAmtGuard {
                    idices: spec_in_fixed_order
                        .iter()
                        .map(|(k, _)| conv_left[k])
                        .collect(),
                    idices_replication_num,
                    idices_left_names,
                    is_mass_action: true,
                    inner: inner_w_guard,
                    has_starr_attribut_amounts,
                })
            }

            EnumeratedRate::MassActionExpression(_y) => {
                IndexedRateExprTerm::LeftSideAmtGuard(LeftSideAmtGuard {
                    idices: reac.species_in.iter().map(|(k, _)| conv_left[k]).collect(),
                    idices_replication_num,
                    is_mass_action: true,
                    idices_left_names,

                    inner: inner_w_guard,
                    has_starr_attribut_amounts,
                })
            }

            EnumeratedRate::ComplexExpression(_p) => {
                IndexedRateExprTerm::LeftSideAmtGuard(LeftSideAmtGuard {
                    idices: reac.species_in.iter().map(|(k, _)| conv_left[k]).collect(),
                    idices_replication_num,
                    idices_left_names,

                    is_mass_action: false,
                    inner: inner_w_guard,
                    has_starr_attribut_amounts,
                })
            }
        }
    }
    fn shrink(&mut self) {
        self.species_in_idx.shrink_to_fit();
        //self.species_in_amt.shrink_to_fit();
        self.species_out.shrink_to_fit();
        //self.global_val_changes.shrink_to_fit();
        match &mut self.dependend_reactions {
            None => {}
            Some(x) => x.shrink_to_fit(),
        }
    }
    fn from(
        reac: &EnumeratedReaction,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        spec_counts: &Vec<SpeciesAmt>,
        generate_new_templates: bool, //converte_globals: &HashMap<EnumeratedGlobalValue, usize>,
    ) -> EnumeratedIndexedReaction {
        //assert!(reac.global_val_changes.is_empty());
        //assert!(reac.species_in.len() > 0);
        let species_in_names: Vec<_> = reac.species_in.iter().map(|(a, _)| a.clone()).collect();
        let species_in_idx: Vec<_> = species_in_names
            .iter()
            .map(|v| converse_map.get(&reac.species_in[v]).unwrap())
            .map(|k| to_uindex(k))
            .collect();
        assert_eq!(species_in_idx.len(), reac.species_in.len());
        //let spec_in_unique: HashSet<_> = spec_in.iter().cloned().collect();

        //let spec_in_unique: Vec<Uindex> = spec_in_unique.into_iter().collect();
        let conv_left: HashMap<SpeciesLeftIdentifier, usize> = reac
            .species_in
            .iter()
            .map(|(k, v)| (k.clone(), *converse_map.get(v).unwrap()))
            .collect();
        /*let species_in_amt = spec_in_unique
        .iter()
        .map(|v| spec_in.iter().filter(|o| *o == v).count())
        .map(|k| to_uindex(&k))
        .collect();*/
        let rate = Self::combine_rate(reac, converse_map, &conv_left, &spec_counts);
        let mut res = EnumeratedIndexedReaction {
            rate_optimization: Self::get_rate_opt(
                reac,
                converse_map,
                &conv_left,
                &spec_counts,
                &rate,
                generate_new_templates,
                spec_counts,
            ),
            rate,
            dependend_reactions: None,
            dependend_propensities_in_triee: vec![],
            could_have_multi_result: species_in_idx.iter().any(|k| match spec_counts[*k] {
                SpeciesAmt::NumOnly(_) => false,
                SpeciesAmt::CompartmentWithStar(_, _) => false,
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(_) => true,
            }),
            //species_in_amt,
            species_in_idx,
            species_in_names,
            species_out: reac
                .species_out
                .iter()
                .map(|(amt, v)| (amt, converse_map.get(v).unwrap()))
                .map(|(amt, k)| {
                    (
                        match amt {
                            None => None,
                            Some(amt) => Some(IndexedRateExprTerm::from_expr(
                                amt,
                                converse_map,
                                &conv_left,
                                spec_counts,
                            )),
                        },
                        to_uindex(&k),
                    )
                })
                .collect(),
            reaction_number_of_prototype: reac.reaction_number_of_prototype,
            out_attribute_values: {
                assert_eq!(
                    reac.species_out.len(),
                    reac.star_attribute_expressions.len()
                );
                reac.star_attribute_expressions
                    .iter()
                    .map(|k| {
                        k.iter()
                            .cloned()
                            .map(|(name, val)| {
                                (
                                    name,
                                    match val {
                                        PrimitiveOrExpr::Primitive(prim) => {
                                            IndexedRateExprTerm::Primitive(prim)
                                        }
                                        PrimitiveOrExpr::Expr(expr) => {
                                            IndexedRateExprTerm::from_expr(
                                                &expr,
                                                converse_map,
                                                &conv_left,
                                                spec_counts,
                                            )
                                        }
                                    },
                                )
                            })
                            .collect()
                    })
                    .collect()
            },
            invokes_structural_change: reac.invokes_structural_changes,
            count: 0,
            max_count: reac.max_count,
        };
        res.shrink();
        res
    }

    fn get_involved(&self) -> InvolvedInReaction {
        let mut spec: BTreeSet<_> = Default::default();
        let mut time_dep = false;

        //spec.extend(self.species_in_idx.iter());
        self.rate.get_involved(&mut spec, &mut time_dep);
        let left: HashBag<_> = self.species_in_idx.iter().cloned().collect();
        assert_eq!(self.species_out.len(), self.out_attribute_values.len());
        let mut changed: BTreeSet<usize> = self
            .species_out
            .iter()
            .map(|k| k.1)
            .zip(self.out_attribute_values.iter().cloned())
            .filter(|(_n, a)| !a.is_empty())
            .map(|(a, _b)| a)
            .collect();
        let right_potentially_unchanged: HashBag<_> = self
            .species_out
            .iter()
            .map(|k| k.1)
            .zip(self.out_attribute_values.iter().cloned())
            .filter(|(_n, a)| a.is_empty())
            .map(|(a, _b)| a)
            .collect();
        for (lidx, lamt) in left.set_iter() {
            match right_potentially_unchanged.get(lidx) {
                None => {
                    changed.insert(*lidx);
                }
                Some((_, r_amt)) => {
                    if r_amt != lamt {
                        changed.insert(*lidx);
                    }
                }
            }
        }
        for (ridx, ramt) in right_potentially_unchanged {
            match left.get(&ridx) {
                None => {
                    changed.insert(ridx);
                }
                Some((_, l_amt)) => {
                    if ramt != l_amt {
                        changed.insert(ridx);
                    }
                }
            }
        }
        //let on_left_and_not_unchanged = self.species_out.iter().enumerate().filter(|(nth,i)| !self.species_in_idx.contains(i) || !self.out_attribute_values)
        InvolvedInReaction {
            changing_species: changed.into_iter().collect(),
            /*changing_globals: self
            .global_val_changes
            .iter()
            .map(|(v, _)| v)
            .cloned()
            .collect(),*/
            time_dependend: time_dep,
            rate_species: spec,
            is_structure_changing: self.invokes_structural_change,
        }
    }
}

#[derive(Debug, Clone)]
struct InvolvedInReaction {
    changing_species: BTreeSet<Uindex>,
    //changing_globals: HashSet<usize>,
    time_dependend: bool,
    rate_species: BTreeSet<Uindex>,
    is_structure_changing: bool,
}

#[derive(Clone)]
enum SpecialTimeType {
    Observation,
    TimeDepRate,
    Endtime,
}
#[derive(Clone)]
pub struct SpecialTimePoint {
    time: f64,
    typ: SpecialTimeType,
}

impl PartialEq<Self> for SpecialTimePoint {
    fn eq(&self, other: &Self) -> bool {
        self.time.eq(&other.time)
    }
}

impl PartialOrd for SpecialTimePoint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.time.partial_cmp(&other.time)
    }
}

#[derive(Template, Clone)]
#[template(path = "code_gen.txt")]
pub struct SimulationFast {
    //pub model: EnumaratedModel,
    //pub target_time: f64,
    //pub step_size: f64,
    pub special_times: Vec<SpecialTimePoint>,
    pub reactions: Vec<EnumeratedIndexedReaction>,
    involved_in_reactions: Vec<InvolvedInReaction>,
    species_modif_by_rules: Vec<Vec<Uindex>>,
    pub species_counts: Vec<SpeciesAmt>,
    species_counts_as_used_for_propensitie: Vec<usize>, // used for approximative
    pub species_lookup: HashMap<EnumeratedSpecies, usize>,
    //pub global_value_lookup: HashMap<EnumeratedGlobalValue, usize>,
    pub observation_queries: Vec<(String, IndexedRateExprTerm)>,
    pub observation_values: Vec<HashMap<String, PrimitiveTypes>>,
    pub state_jsons: Vec<SimOutputJson>,
    pub observation_compartment_attributes: Vec<HashMap<String, PrimitiveTypes>>,
    pub obs_reaction_counts: Vec<Vec<usize>>,
    pub species_name_conversion: SpeciesNameConverter,
    pub species_names: Vec<String>,
    pub species_definitions: HashMap<SpeciesName, Species>,
    pub reaction_names: Vec<String>,
    pub reaction_counts: Vec<usize>,
    pub string_conversion: HashMap<String, OptimizedString>,

    //pub global_values: Vec<PrimitiveTypes>,
    //pub obs_global_vals_names: Vec<String>,
    time_dependend_reactions: Vec<Uindex>,
    pub last_time_dependend_update: f64,

    pub time: f64,
    pub propensities: Vec<f64>,
    pub propensities_calculated: Vec<EvaluationResult>,
    //propensity_sum: f64,
    propensitiy_tree: Vec<f64>,
    pub metrics: Metrics,
    //pub stepcount: usize,
    //pub steps_where_approximated: usize,
    pub reaction_idx_sum: usize,
    count_for_next_sort: usize,

    pub(crate) rng: SmallRng,
    pub(crate) last_io: f64,
    pub obs_times: Vec<f64>,
}

impl Default for SimulationFast {
    fn default() -> Self {
        SimulationFast {
            special_times: vec![],
            reactions: vec![],
            involved_in_reactions: vec![],
            species_modif_by_rules: vec![],
            species_counts: vec![],
            species_counts_as_used_for_propensitie: vec![],
            species_lookup: Default::default(),
            observation_queries: vec![],
            observation_values: vec![],
            state_jsons: vec![],
            observation_compartment_attributes: vec![],
            obs_reaction_counts: vec![],
            species_name_conversion: SpeciesNameConverter {
                map: Default::default(),
            },
            species_names: vec![],
            species_definitions: Default::default(),
            reaction_names: vec![],
            reaction_counts: vec![],
            string_conversion: Default::default(),
            time_dependend_reactions: vec![],
            last_time_dependend_update: 0.0,
            time: 0.0,
            propensities: vec![],
            propensities_calculated: vec![],
            propensitiy_tree: vec![],
            metrics: Metrics {
                step_function_calls: 0,
                regular_steps: 0,
                virtual_steps: 0,
                steps_where_tau_too_large: 0,
                steps_where_approximated: 0,
                num_rate_updates: 0,
            },
            reaction_idx_sum: 0,
            count_for_next_sort: 0,
            rng: SmallRng::from_seed(Default::default()),
            last_io: 0.0,
            obs_times: vec![],
        }
    }
}

impl SimulationFast {
    pub fn print_tree_flat(&self) {
        println!("{}", self.generate_output(false).to_string());
        //println!("{:?}", self.propensities);
        //println!("{:?}", self.reactions[3].rate);
    }

    pub fn get_number_of_possible_reactions(&self) -> usize {
        self.propensities.len()
    }
    /*fn set_propensity(&mut self, idx: Uindex, value: f64) {
        let idx = idx as usize;
        self.propensities[idx] = value;
        //self.tmp_reaction_to_update.push(idx);
        //self.update_tree(idx);
        /* let old_prop_sum = self.propensity_sum;
        let old_propensity = self.propensities[idx];
        self.propensity_sum -= self.propensities[idx];
        self.propensities[idx] = value;
        self.propensity_sum += self.propensities[idx];
        self.update_tree(idx, self.propensities[idx] - old_propensity);
        if old_prop_sum > self.propensity_sum * 100. {
            self.rebuild_tree();
        }*/
    }*/
    fn _check_tree(&mut self) {
        let mut test_tree = vec![0.0; self.propensitiy_tree.len()];
        swap(&mut test_tree, &mut self.propensitiy_tree);
        self.rebuild_tree();
        for (a, b) in test_tree.iter().zip(self.propensitiy_tree.iter()) {
            assert_eq!(*a, *b);
        }
        swap(&mut test_tree, &mut self.propensitiy_tree);
    }
    fn rebuild_tree(&mut self) {
        assert!(!USE_SLOW_FIRST_REACTION_METHOD);
        /*if USE_SLOW_FIRST_REACTION_METHOD {
            self.propensity_sum = self.propensities.iter().sum();
            return;
        }*/
        let mut sum = 0.;
        for (idx, p) in self.propensities.iter().enumerate().rev() {
            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;
            if right_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[idx] =
                    self.propensitiy_tree[right_child] + self.propensitiy_tree[left_child];
            } else if left_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[idx] = self.propensitiy_tree[left_child];
            } else {
                self.propensitiy_tree[idx] = 0.;
            }
            sum += p;
            self.propensitiy_tree[idx] += p;
        }
    }
    fn _update_tree(&mut self, mut idx: usize) {
        loop {
            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;

            if right_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[idx] =
                    self.propensitiy_tree[right_child] + self.propensitiy_tree[left_child];
            } else if left_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[idx] = self.propensitiy_tree[left_child];
            } else {
                self.propensitiy_tree[idx] = 0.;
            }
            self.propensitiy_tree[idx] += self.propensities[idx];
            if idx == 0 {
                break;
            }
            idx = (idx - 1) / 2; // parent node
        }
    }
    fn select_from_threshold(&mut self, first_try: bool, rand_val: f64) -> Option<usize> {
        let sum;
        match self.propensitiy_tree.iter().next() {
            None => return None,
            Some(x) => {
                if *x <= 0. {
                    return None;
                }
                sum = x;
            }
        }
        debug_assert_lt!(rand_val, 1.);

        let mut thrshold = rand_val * sum;
        if thrshold >= f64::MAX {
            match self
                .propensities
                .iter()
                .enumerate()
                .filter(|(_, v)| **v == f64::MAX)
                .map(|(idx, _)| idx)
                .choose(&mut self.rng)
            {
                None => {
                    panic!("Infinite rate sum?")
                }
                Some(x) => return Some(x),
            }
        }
        let mut idx = 0;
        loop {
            let y = match self.propensities.get(idx) {
                None => break,
                Some(&y) => y,
            };
            if y >= thrshold {
                return Some(idx);
            } else {
                thrshold -= y;
            }

            let left_child = idx * 2 + 1;
            let right_child = idx * 2 + 2;

            match self.propensitiy_tree.get(left_child) {
                None => break,
                Some(&y) => {
                    if y >= thrshold {
                        idx = left_child;
                    } else {
                        thrshold -= y;
                        idx = right_child;
                    }
                }
            }
        }

        if first_try {
            //self.rebuild_tree();
            return self.select_from_threshold(false, rand_val);
        }
        if self.propensitiy_tree.len() < 100 {
            println!("Internal reaction selection error at {}! Tree problem {}!\n Tree {:?}\n{:?} \n  rand_val={}", idx, self.propensitiy_tree.len(), self.propensitiy_tree, self.propensities, rand_val);
        }
        println!(
            "Internal reaction selection error at {}! Tree problem {}!\n  rand_val={}",
            idx,
            self.propensitiy_tree.len(),
            rand_val
        );
        let rand_val = self.rng.gen_range(0.0..1.);
        return self.select_from_threshold(false, rand_val);
    }
    fn _get_next_reaction_idx_slow(&mut self) -> Option<usize> {
        let propsum: f64 = self.propensities.iter().sum();
        let propensity_threshold = self.rng.gen_range(0.0..propsum);
        match self
            .propensities
            .iter()
            .scan(0.0, |sum, p| {
                *sum += p;
                Some(*sum)
            })
            .enumerate()
            .find(|(_idx, sum)| *sum > propensity_threshold)
        {
            None => None,
            Some((idx, _)) => Some(idx),
        }
    }
    fn get_next_reaction_idx(&mut self) -> Option<usize> {
        /*if ((self.metrics.real_steps % (10000 * self.propensitiy_tree.len())) == 0)
            || (self.propensity_sum < 0.)
        {
            self.rebuild_tree();
        }*/
        /*if USE_SLOW_FIRST_REACTION_METHOD {
            match self.get_next_reaction_idx_slow() {
                Some(x) => return Some(x),
                None => {
                    self.rebuild_tree();
                    return self.get_next_reaction_idx_slow();
                }
            }
        }*/

        let rand_val = self.rng.gen_range(0.0..1.);
        self.select_from_threshold(false, rand_val)
    }

    /*pub fn _seed(&mut self) {
        self.rng = SmallRng::from_entropy();
    }*/
    /*fn write_header(&self) {
        let mut file = match File::create(&self.output_file_name) {
            Err(why) => panic!("couldn't open {}: {}", self.output_file_name, why),
            Ok(file) => file,
        };
        write!(file, "Time,").unwrap();
        match &self.model.observations {
            None => self
                .model
                .species
                .iter()
                .for_each(|v| write!(file, "{},", v.string_name()).unwrap()),
            Some(obs) => obs
                .iter()
                .for_each(|(v, _)| write!(file, "{},", v).unwrap()),
        }
        self.model
            .global_values
            .iter()
            .for_each(|(v, _)| write!(file, "{},", v.string_name()).unwrap())
    }*/

    pub fn build_json_from_current_state(&self) -> SimOutputJson {
        SimOutputJson {
            time: self.time,
            species: self.generate_output(false).species,
        }
    }

    pub fn make_observations(&mut self, options: &OutputOptions) {
        if !options.any_traj_output() {
            return;
        }
        self.obs_times.push(self.time);

        if options.flat_table || options.json || options.compressed_json {
            self.state_jsons.push(self.build_json_from_current_state());
        }
        //println!("{:?}", self.observation_queries);

        self.observation_values.push(
            self.observation_queries
                .iter()
                .map(|(n, v)| (n.clone(), self.eval_term(v).single().unwrap().clone()))
                .collect(),
        );
        /*self.observation_values.push(
            self.species_names
                .iter()
                .cloned()
                //chain(self.obs_global_vals_names.iter().cloned())
                .zip(
                    self.species_counts
                        .iter()
                        .map(|v| PrimitiveTypes::Integer(v.total_amt() as isize))
                        /*.chain(self.global_values.iter().cloned())*/,
                )
                .collect(),
        );
        self.observation_compartment_attributes.push(
            self.species_lookup
                .iter()
                .filter(|(a, _)| a.my_id.is_some())
                .map(|(a, _k)| {
                    a.expanded_attribute_values
                        .iter()
                        .map(|(n, v)| {
                            (
                                (a.my_id.as_ref().unwrap().clone().to_string()
                                    + "_"
                                    + &*n.get_original_string(&self.string_conversion)),
                                v.clone(),
                            )
                        })
                        .collect::<Vec<_>>()
                    //.cloned()
                })
                .flatten()
                .collect(),
        );*/
        //println!("{:?} {:?}",self.species_names,self.obs_global_vals_names);
        //println!("{:?}",self.observation_values.last().unwrap());
        assert_eq!(self.species_names.len(), self.species_counts.len());

        {
            let obs = &mut self.obs_reaction_counts;
            let counts = &self.reaction_counts;
            counts.iter().enumerate().for_each(|(n, v)| obs[n].push(*v));
        }
        /*self.model
        .global_values
        .iter()
        .for_each(|(v, _)| write!(file, "{},", global_values.get(v).unwrap()).unwrap())*/
    }

    /*fn get_propensities(
        &self,
        spec_counts: &HashMap<EnumeratedSpecies, usize>,
        global_vals: &HashMap<EnumeratedGlobalValue, PrimitiveTypes>,
        time: f64,
    ) -> Vec<f64> {
        self.model
            .reactions
            .iter()
            .map(|v| v.calc_propensity(spec_counts, global_vals, time))
            .collect()
    }*/

    fn find_depend_reactions_for(&mut self, reaction_idx: usize) {
        let mut dependent_on_this = FixedBitSet::with_capacity(self.reactions.len()); //Vec::new();
        assert!(self.reactions[reaction_idx].dependend_reactions.is_none());

        if !self.involved_in_reactions[reaction_idx].is_structure_changing {
            dependent_on_this.clear();

            if self.reactions[reaction_idx].max_count.is_some() {
                // If there is a max count, all identical reactions need to be updated on fire
                let my_proto = self.reactions[reaction_idx].reaction_number_of_prototype;
                for i in 0..self.reactions.len() {
                    if self.reactions[reaction_idx].reaction_number_of_prototype == my_proto {
                        println!("Identical dep");
                        dependent_on_this.insert(i);
                    }
                }
            }

            for x in self.involved_in_reactions[reaction_idx]
                .changing_species
                .iter()
            {
                for v in self.species_modif_by_rules[*x as usize].iter() {
                    dependent_on_this.insert(*v);
                }
            }

            dependent_on_this.insert(reaction_idx); // mostly not needed, but for @@ reaction that reduces amount, to ensure it won't get negative

            self.reactions[reaction_idx].dependend_reactions =
                Some(dependent_on_this.ones().collect());
            self.reactions[reaction_idx].dependend_propensities_in_triee =
                self.reactions[reaction_idx].compute_tree_updates();
        } else {
            self.reactions[reaction_idx].dependend_reactions = Some(vec![]);
        }
        self.reactions[reaction_idx].shrink();
    }

    fn find_dependent_reactions_prep_phase(&mut self) {
        self.involved_in_reactions = self.reactions.iter().map(|v| v.get_involved()).collect();
        {
            self.species_modif_by_rules = vec![Vec::new(); self.species_counts.len()];

            for (n, d) in self.involved_in_reactions.iter().enumerate() {
                let n = to_uindex(&n);

                for v in d.rate_species.iter() {
                    self.species_modif_by_rules[*v as usize].push(n);
                }
                if d.time_dependend {
                    self.time_dependend_reactions.push(n);
                }
            }

            /*for n in 0..self.involved_in_reactions.len() {
                //self.involved_in_reactions.iter().enumerate() {
                //let n = to_uindex(&n);

                for v in self.involved_in_reactions[n].rate_species.iter() {
                    self.species_modif_by_rules[*v as usize].push(n);
                }
                if self.involved_in_reactions[n].time_dependend {
                    self.time_dependend_reactions.push(n);
                }
            }*/
            if cfg!(debug_assertions) {
                self.species_modif_by_rules.iter_mut().for_each(|k| {
                    k.sort();
                    let old_len = k.len();
                    k.dedup();
                    assert_eq!(old_len, k.len())
                });
            }
        }
    }

    pub fn reseed(&mut self) {
        self.rng = SmallRng::from_entropy();
    }

    pub fn new(
        m: &FlatModel,
        endtime_num_obs: Option<(f64, usize)>,
        generate_new_templates: bool,
        rng_seed: Option<u64>,
    ) -> Result<SimulationFast, String> {
        let species_lookup: HashMap<EnumeratedSpecies, usize> = m
            .species
            .iter()
            .enumerate()
            .map(|(a, b)| (b.clone(), a))
            .collect();
        assert_eq!(species_lookup.len(), m.species.len());

        let mut species_counts: Vec<SpeciesAmt> = m
            .species
            .iter()
            .map(|k| {
                let species = &m.model.species[&k.species_name];
                if species.not_expanding_attributes.is_empty() {
                    SpeciesAmt::NumOnly(0)
                } else if species.is_compartment {
                    SpeciesAmt::CompartmentWithStar(
                        false,
                        StarAttributeValues {
                            names: species.not_expanding_attributes.keys().cloned().collect(),
                            values: species
                                .not_expanding_attributes
                                .values()
                                .map(|k| match k.dim.non_numerical {
                                    NonNumericalDimension::Nothing => {
                                        PrimitiveTypes::Float(f64::MAX)
                                    }
                                    NonNumericalDimension::String => {
                                        PrimitiveTypes::String(OptimizedString::dummy())
                                    }
                                    NonNumericalDimension::Bool => PrimitiveTypes::Boolean(false),
                                    NonNumericalDimension::Integer => {
                                        PrimitiveTypes::Integer(i64::MIN)
                                    }
                                    NonNumericalDimension::Enumeration(_) => {
                                        unimplemented!("Star attributes can not be enumerations!")
                                    }
                                })
                                .collect(),
                        },
                    )
                } else {
                    #[cfg(feature = "hybrid_att")]
                    {
                        SpeciesAmt::NotExpanded(vec![])
                    }
                    #[cfg(not(feature = "hybrid_att"))]
                    unreachable!()
                }
            })
            .collect();
        assert!(m.initial_species.len() <= species_counts.len());
        for (s, count) in m.initial_species.iter() {
            //println!("I'm loking for {}",s.string_name());
            species_counts[*species_lookup.get(s).expect(&format!(
                "A species you told me existed (because of amount) doesn't! {:?}",
                s
            ))] = count.clone();
        }

        /*let species_counts_as_used_for_propensitie;
        if acceptable_error == 0.0 {
            species_counts_as_used_for_propensitie = vec![];
        } else if acceptable_error > 0. {
            if acceptable_error > 0.2 {
                panic!("Accetable error set too large")
            }
            species_counts_as_used_for_propensitie = species_counts.clone();
        } else {
            panic!("Negative accetable error not possible")
        }*/

        let mut r = SimulationFast {
            special_times: match endtime_num_obs {
                None => {
                    vec![]
                }
                Some((t, n)) => {
                    assert_ge!(n, 1);
                    let num_time_updates = n.max(1000);
                    (0..n)
                        .map(|counter| SpecialTimePoint {
                            time: counter as f64 * t / (n as f64),
                            typ: SpecialTimeType::Observation,
                        })
                        .chain((0..num_time_updates).map(|counter| SpecialTimePoint {
                            time: counter as f64 * t / (num_time_updates as f64),
                            typ: SpecialTimeType::TimeDepRate,
                        }))
                        .chain(iter::once(SpecialTimePoint {
                            time: t,
                            typ: SpecialTimeType::Endtime,
                        }))
                        .sorted_by(|a, b| a.time.partial_cmp(&b.time).unwrap())
                        .rev()
                        .collect()
                }
            },
            count_for_next_sort: std::cmp::max(m.reactions.len() / 3, 1000),
            reactions: m
                .reactions
                .iter()
                //.par_iter()
                .map(|v| {
                    EnumeratedIndexedReaction::from(
                        v,
                        &species_lookup, /*&global_value_lookup*/
                        &species_counts,
                        generate_new_templates,
                    )
                })
                .collect(),
            observation_queries: m
                .observations
                .iter()
                .map(|(n, obs)| {
                    (
                        n.clone(),
                        IndexedRateExprTerm::from_expr(
                            obs,
                            &species_lookup,
                            &HashMap::new(),
                            &species_counts,
                        ),
                    )
                })
                .collect(),
            species_counts,
            species_counts_as_used_for_propensitie: vec![], // todo
            species_lookup,
            //global_value_lookup,
            observation_values: vec![],
            state_jsons: vec![],
            observation_compartment_attributes: vec![],
            obs_reaction_counts: vec![vec![]; m.model.transitions.len()],
            //dead_observations_to_keep: 0,
            /*observation_names: m
            .species
            .iter()
            .map(|s| s.string_name(&m.species))
            .chain(m.global_values.iter().map(|(a, _)| a.string_name()))
            .collect()*/
            species_name_conversion: m.model.species_name_conversion.clone(),
            species_names: m
                .species
                .iter()
                .map(|k| {
                    k.string_name(
                        &m.species,
                        &m.model.species_name_conversion,
                        &m.model.string_conversion,
                    )
                })
                .collect(),
            species_definitions: m.model.species.clone(),
            reaction_names: m
                .model
                .transitions
                .iter()
                .map(|t| t.original_string.clone())
                .collect(),
            reaction_counts: vec![0; m.model.transitions.len()],
            //global_values: m.global_values.iter().map(|(_v, a)| a.clone()).collect(),
            /*obs_global_vals_names: m
            .global_values
            .iter()
            .map(|(a, _)| a.string_name())
            .collect(),*/
            string_conversion: m.model.string_conversion.clone(),
            time_dependend_reactions: vec![],
            last_time_dependend_update: 0.0,
            time: 0.0,
            propensities: vec![0.; m.reactions.len()],
            propensities_calculated: (0..m.reactions.len())
                .map(|_| EvaluationResult::Single(PrimitiveTypes::Boolean(false)))
                .collect(),
            propensitiy_tree: vec![0.; m.reactions.len()],
            metrics: Metrics {
                step_function_calls: 0,
                regular_steps: 0,
                virtual_steps: 0,
                steps_where_tau_too_large: 0,
                steps_where_approximated: 0,
                num_rate_updates: 0,
            },
            rng: match rng_seed {
                None => SmallRng::from_entropy(),
                Some(x) => SmallRng::seed_from_u64(x),
            },
            last_io: 0.0,
            obs_times: vec![],
            //model: m,
            reaction_idx_sum: 0,
            involved_in_reactions: vec![],
            species_modif_by_rules: vec![],
        };
        r.find_dependent_reactions_prep_phase();
        r.update_all_propensities()?;
        Ok(r)
    }

    /*pub fn eval_term_code_gen(&self, expr: &IndexedRateExprTerm) -> String {
        match expr {
            IndexedRateExprTerm::Single(x) => self.eval_term_code_gen(&x),
            IndexedRateExprTerm::Multi(left, op, right) => {
                let right_s = self.eval_term_code_gen(right);
                let left_s = self.eval_term_code_gen(left);
                let right = self.eval_term(right);
                let left = self.eval_term(left);
                match op {
                    FloatOp::Div => {
                        return format!("({} as f64) {} ({} as f64)", left_s, op, right_s)
                    }
                    _ => {}
                }
                match (left.single().unwrap(), right.single().unwrap()) {
                    (PrimitiveTypes::Float(_), PrimitiveTypes::Float(_)) => {
                        format!("({} {} {})", left_s, op, right_s)
                    }
                    (PrimitiveTypes::Integer(_), PrimitiveTypes::Integer(_)) => {
                        format!("({} {} {})", left_s, op, right_s)
                    }
                    (PrimitiveTypes::Float(_), PrimitiveTypes::Integer(_)) => {
                        format!("({} {} {} as f64)", left_s, op, right_s)
                    }
                    (PrimitiveTypes::Integer(_), PrimitiveTypes::Float(_)) => {
                        format!("({} as f64 {} {})", left_s, op, right_s)
                    }
                    (_, _) => {
                        unimplemented!()
                    }
                }
                //op.apply(&left, &right).unwrap()
            }
        }
    }*/
    fn merge_evaluation_result_for_float_op(
        op: &FloatOp,
        left: EvaluationResult,
        right: EvaluationResult,
    ) -> EvaluationResult {
        Self::merge_evaluation_result(
            |a, b| match op.apply(a, b) {
                Some(a) => a,
                None => {
                    panic!("Failed to evaluate {:?} {} {:?}", a, op, b)
                }
            },
            left,
            right,
        )
    }

    #[cfg(not(feature = "hybrid_att"))]
    fn merge_evaluation_result<F: Fn(&PrimitiveTypes, &PrimitiveTypes) -> PrimitiveTypes>(
        op: F,
        left: EvaluationResult,
        right: EvaluationResult,
    ) -> EvaluationResult {
        match (left, right) {
            (EvaluationResult::Single(l), EvaluationResult::Single(r)) => {
                EvaluationResult::Single(op(&l, &r))
            }
        }
    }
    #[cfg(feature = "hybrid_att")]
    fn merge_evaluation_result<F: Fn(&PrimitiveTypes, &PrimitiveTypes) -> PrimitiveTypes>(
        op: F,
        left: EvaluationResult,
        right: EvaluationResult,
    ) -> EvaluationResult {
        match (left, right) {
            (EvaluationResult::Single(l), EvaluationResult::Single(r)) => {
                EvaluationResult::Single(op(&l, &r))
            }
            (EvaluationResult::Multi(l), EvaluationResult::Single(r)) => {
                EvaluationResult::Multi(MultiResults {
                    what: l.what,
                    results: l
                        .results
                        .into_iter()
                        .map(|mut k| {
                            k.value = op(&k.value, &r);
                            k
                        })
                        .collect(),
                })
            }
            (EvaluationResult::Single(l), EvaluationResult::Multi(r)) => {
                EvaluationResult::Multi(MultiResults {
                    what: r.what,
                    results: r
                        .results
                        .into_iter()
                        .map(|mut k| {
                            k.value = op(&l, &k.value);
                            k
                        })
                        .collect(),
                })
            }
            (EvaluationResult::Multi(l), EvaluationResult::Multi(r)) => {
                l.debug_is_complete();
                r.debug_is_complete();
                //println!("MERGING \n{l} &&&\n{r}\n---");
                let merged_what: Vec<_> = l
                    .what
                    .iter()
                    .chain(r.what.iter())
                    .sorted()
                    .dedup()
                    .cloned()
                    .collect();
                let mut results: Vec<_> = itertools::iproduct!(l.results.iter(), r.results.iter())
                    .filter_map(|(lr, rr)| {
                        Some((
                            merge_multi_results(
                                &lr.matches_to_get_to_value,
                                &l.what,
                                &rr.matches_to_get_to_value,
                                &r.what,
                                &merged_what,
                            )?,
                            &lr.value,
                            &rr.value,
                        ))
                    })
                    .map(|(k, lv, rv)| OneMultiResult {
                        matches_to_get_to_value: k,
                        value: { op(lv, rv) },
                    })
                    .collect();

                // TODO there is a lot of redundant work, every match is found twice
                results.sort_by(|a, b| a.matches_to_get_to_value.cmp(&b.matches_to_get_to_value));
                results.dedup_by(|a, b| a.matches_to_get_to_value.eq(&b.matches_to_get_to_value));
                let r = MultiResults {
                    what: merged_what,
                    results: results,
                };

                //println!("MERGED TO {r}");
                EvaluationResult::Multi(r)
                //unimplemented!()
            }
        }
    }

    /*pub fn eval_term(&self, expr: &IndexedRateExprTerm) -> EvaluationResult {
        match expr {
            IndexedRateExprTerm::Single(x) => self.eval_term(x),
            IndexedRateExprTerm::Multi(left, op, right) => {
                let right = self.eval_term(right);
                let left = self.eval_term(left);
                SimulationFast::merge_evaluation_result_for_float_op(op, left, right)
            }
        }
    }*/
    pub fn write_code_gen_global(&self, _idx: &usize) -> String {
        unimplemented!() //self.code_gen_print_primitive(&self.global_values[*idx])
    }
    pub fn code_gen_print_primitive(&self, prm: &PrimitiveTypes) -> String {
        match prm {
            PrimitiveTypes::String(_) => {
                unimplemented!()
            }
            PrimitiveTypes::Boolean(x) => format!("{}", x),
            PrimitiveTypes::Float(f) => format!("{:e}", f),
            PrimitiveTypes::Integer(i) => format!("{}", i),
        }
    }
    pub fn eval_term_code_gen(&self, expr: &IndexedRateExprTerm) -> String {
        match expr {
            IndexedRateExprTerm::Primitive(p) => self.code_gen_print_primitive(p),

            IndexedRateExprTerm::Expr(_x) => unimplemented!(), //self.eval_term_code_gen(x),
            IndexedRateExprTerm::CountGlobally(idx) => {
                format!("self.species_amounts.{}", self.species_names[*idx])
            }
            IndexedRateExprTerm::SpecificCountFromGlobal(idices) => {
                if idices.is_empty() {
                    panic!("Empty global count!");
                    //return format!(" 0 /* unclear what the global count referes to */");
                }
                let s: String = Itertools::intersperse(
                    idices
                        .iter()
                        .map(|v| format!("self.species_amounts.{}", self.species_names[*v])),
                    format!(" + "),
                )
                .collect();
                format!("( {} )", s)
            }

            IndexedRateExprTerm::Condition(_) => {
                unimplemented!()
            }
            IndexedRateExprTerm::CountFromLeftCpx(_, _) => {
                unimplemented!()
            }
            IndexedRateExprTerm::CountFromLeftSimple(_) => {
                unimplemented!()
            }

            IndexedRateExprTerm::LeftSideAmtGuard(_) => {
                unimplemented!()
            }
            IndexedRateExprTerm::StarAttRefReg(_) => {
                unimplemented!()
            }
            IndexedRateExprTerm::StarAttRefComp(_) => {
                unimplemented!()
            }
            IndexedRateExprTerm::BuildinFunction(_, _) => {
                unimplemented!()
            }
            IndexedRateExprTerm::SpecifCountFromGlobalConstraint(_) => {
                unimplemented!()
            }
        }
    }

    fn attribute_to_output(
        &self,
        att_name: &OptimizedString,
        att_val: &PrimitiveTypes,
        species_name: &SpeciesName,
        string_conversion: &HashMap<String, OptimizedString>,
    ) -> (String, OutputAttribute) {
        let s = &self.species_definitions[species_name];
        let att_unit = s.dim_from_attribute_name(att_name).unwrap();
        (
            att_name.get_original_string(&self.string_conversion),
            OutputAttribute {
                value_si: OutputValue::from_prim(att_val, string_conversion),
                unit_str_prefactor: att_unit.dim_str_prefak.clone(),
            },
        )
    }

    fn generate_output_for_one(
        &self,
        espec: &EnumeratedSpecies,
        string_conversion: &HashMap<String, OptimizedString>,
    ) -> Vec<OutputSpecies> {
        let compartment_id = match &espec.my_id {
            None => None,
            Some(k) => {
                assert!(k.number >= 0);
                Some(k.number as usize)
            }
        };
        match &self.species_counts[self.species_lookup[espec]] {
            SpeciesAmt::NumOnly(k) => {
                if *k > 0 {
                    vec![OutputSpecies {
                        species_name: self
                            .species_name_conversion
                            .get_rev(&espec.species_name)
                            .clone(),
                        attributes: espec
                            .expanded_attribute_values
                            .iter()
                            .sorted_by_key(|(k, _)| k)
                            .map(|(att_name, att_val)| {
                                self.attribute_to_output(
                                    att_name,
                                    att_val,
                                    &espec.species_name,
                                    string_conversion,
                                )
                            })
                            .collect(),
                        children: vec![],
                        compartment_id,
                        amt: *k,
                    }]
                } else {
                    vec![]
                }
            }
            #[cfg(feature = "hybrid_att")]
            SpeciesAmt::NotExpanded(y) => y
                .iter()
                .filter(|(amt, _)| *amt > 0)
                .map(|(amt, xtra_atts)| OutputSpecies {
                    species_name: self
                        .species_name_conversion
                        .get_rev(&espec.species_name)
                        .clone(),
                    attributes: espec
                        .expanded_attribute_values
                        .iter()
                        .map(|(a, b)| (a, b))
                        .chain(
                            xtra_atts
                                .names
                                .iter()
                                .zip(xtra_atts.values.iter())
                                .map(|(a, b)| (a, b)),
                        )
                        .map(|(name, val)| {
                            self.attribute_to_output(
                                name,
                                val,
                                &espec.species_name,
                                string_conversion,
                            )
                        })
                        //                        .map(|(a, b)| (a.clone(), b.clone()))
                        //.cloned()
                        .collect(),
                    children: vec![],
                    compartment_id,
                    amt: *amt,
                })
                //.map(|mut k| k.attributes.sort_by_key(|(a, _)| a))
                .collect(),
            SpeciesAmt::CompartmentWithStar(is_present, stars) => {
                if !is_present {
                    return vec![];
                }
                vec![OutputSpecies {
                    species_name: self
                        .species_name_conversion
                        .get_rev(&espec.species_name)
                        .clone(),
                    attributes: espec
                        .expanded_attribute_values
                        .iter()
                        .map(|(a, b)| (a, b))
                        .chain(
                            stars
                                .names
                                .iter()
                                .zip(stars.values.iter())
                                .map(|(a, b)| (a, b)),
                        )
                        //.map(|(a, b)| (a.clone(), b.clone()))
                        //.cloned()
                        .map(|(name, val)| {
                            self.attribute_to_output(
                                name,
                                val,
                                &espec.species_name,
                                string_conversion,
                            )
                        })
                        .collect(),
                    children: vec![],
                    compartment_id,
                    amt: 1,
                }]
            }
        }
    }
    fn rekursive_output_species(
        &self,
        idx: usize,
        children_at_idx: &Vec<Vec<usize>>,
        idx2enumerated: &HashMap<usize, &EnumeratedSpecies>,
        string_conversion: &HashMap<String, OptimizedString>,
    ) -> Vec<OutputSpecies> {
        let enumerated: &EnumeratedSpecies = idx2enumerated[&idx];
        let mut output = self.generate_output_for_one(enumerated, string_conversion);
        if enumerated.my_id.is_none() {
            assert!(children_at_idx[idx].is_empty());
        } else if output.is_empty() {
            assert!(enumerated.my_id.is_some());
            assert!(children_at_idx[idx].is_empty());
        } else {
            assert!(enumerated.my_id.is_some());
            assert_eq!(output.len(), 1, "{:?}", output);
            for child_idx in children_at_idx[idx].iter() {
                let mut children = self.rekursive_output_species(
                    *child_idx,
                    children_at_idx,
                    idx2enumerated,
                    string_conversion,
                );
                output[0].children.append(&mut children);
            }
        }
        output
    }
    pub fn generate_output(&self, include_propensities: bool) -> SimOutput {
        let mut children_at_idx = vec![vec![]; self.species_counts.len()];
        let idx2enumerated: HashMap<usize, &EnumeratedSpecies> =
            self.species_lookup.iter().map(|(a, b)| (*b, a)).collect();
        let lookup_ids_vec: Vec<(CompartmentID, usize)> = self
            .species_lookup
            .iter()
            .filter_map(|(s, n)| match &s.my_id {
                None => None,
                Some(id) => Some((id.clone(), *n)),
            })
            .filter(|(_, n)| match &self.species_counts[*n] {
                SpeciesAmt::NumOnly(amt) => {
                    assert!(*amt <= 1);
                    *amt == 1
                }
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(k) => !k.is_empty(),
                SpeciesAmt::CompartmentWithStar(is_present, _) => *is_present,
            })
            .collect();
        let lookup_ids: HashMap<_, _> = lookup_ids_vec.iter().cloned().collect();
        assert_eq!(lookup_ids_vec.len(), lookup_ids.len());
        let mut first_layer = vec![];
        for (s, n) in self.species_lookup.iter() {
            match &s.parent_id {
                None => first_layer.push(*n),
                Some(p) => {
                    children_at_idx[lookup_ids[p]].push(*n);
                }
            }
        }
        let mut propensities_reacs = vec![];
        if include_propensities {
            propensities_reacs = self
                .reaction_names
                .iter()
                .map(|n| ReactionStateOutput {
                    name: n.clone(),
                    total_prop: 0.0,
                    copy_propensities: vec![],
                })
                .collect();
            for (prop, n) in self.propensities.iter().zip(
                self.reactions
                    .iter()
                    .map(|k| k.reaction_number_of_prototype),
            ) {
                propensities_reacs[n].copy_propensities.push(*prop);
                propensities_reacs[n].total_prop += *prop;
            }
        }
        //assert!(!first_layer.is_empty());
        SimOutput {
            time: self.time,
            species: first_layer
                .into_iter()
                .map(|k| {
                    self.rekursive_output_species(
                        k,
                        &children_at_idx,
                        &idx2enumerated,
                        &self.string_conversion,
                    )
                    .into_iter()
                })
                .flatten()
                .collect(),
            propensities: propensities_reacs,
        }
    }

    pub fn eval_term(&self, expr: &IndexedRateExprTerm) -> EvaluationResult {
        //println!("Evaluating {:?}", expr);
        let r = match expr {
            IndexedRateExprTerm::Primitive(p) => EvaluationResult::Single(p.clone()),
            IndexedRateExprTerm::Expr(x) => {
                let (left, op, right) = x.as_ref();
                let right = self.eval_term(&right);
                let left = self.eval_term(&left);
                SimulationFast::merge_evaluation_result_for_float_op(&op, left, right)
            }
            IndexedRateExprTerm::CountGlobally(_idx) => {
                panic!("should not be used");
                /*match &self.species_counts[*idx] {
                    SpeciesAmt::NumOnly(k) => {
                        EvaluationResult::Single(PrimitiveTypes::Integer(*k as isize))
                    }
                    SpeciesAmt::NotExpanded(multi) => EvaluationResult::Multi(MultiResults {
                        what: vec![],
                        results: multi
                            .iter()
                            .enumerate()
                            .map(|(variation_idx, (amt, _))| OneMultiResult {
                                matches_to_get_to_value: vec![MultiResultAssociation {
                                    species_idx: *idx,
                                    variation_idx: variation_idx,
                                }],
                                value: PrimitiveTypes::Integer(*amt as isize),
                            })
                            .collect(),
                    }),
                }*/
                //PrimitiveTypes::Integer(self.species_counts[*idx] as isize)
            }
            #[allow(unreachable_code)]
            IndexedRateExprTerm::SpecificCountFromGlobal(idices) => {
                EvaluationResult::Single(PrimitiveTypes::Integer(
                    idices
                        .iter()
                        .map(|v| self.species_counts[*v].total_amt() as i64) //self.species_counts[*v] as isize)
                        .sum(),
                ))
            }
            IndexedRateExprTerm::CountFromLeftSimple(idx) => match &self.species_counts[*idx] {
                SpeciesAmt::NumOnly(k) => {
                    EvaluationResult::Single(PrimitiveTypes::Integer(*k as i64))
                }
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(_multi) => unreachable!(),
                SpeciesAmt::CompartmentWithStar(is_present, _) => {
                    EvaluationResult::Single(PrimitiveTypes::Integer(if *is_present {
                        1
                    } else {
                        0
                    }))
                }
            },
            IndexedRateExprTerm::CountFromLeftCpx(idx, _leftid) => {
                let r = match &self.species_counts[*idx] {
                    SpeciesAmt::NumOnly(k) => {
                        EvaluationResult::Single(PrimitiveTypes::Integer(*k as i64))
                    }
                    #[cfg(feature = "hybrid_att")]
                    SpeciesAmt::NotExpanded(multi) => EvaluationResult::Multi(MultiResults {
                        what: vec![_leftid.clone()],
                        results: multi
                            .iter()
                            .enumerate()
                            .map(|(variation_idx, (amt, _))| OneMultiResult {
                                matches_to_get_to_value: vec![MultiResultAssociation {
                                    species_idx: *idx,
                                    variation_idx: variation_idx,
                                }],
                                value: PrimitiveTypes::Integer(*amt as i64),
                            })
                            .collect(),
                    }),
                    SpeciesAmt::CompartmentWithStar(is_present, _) => {
                        EvaluationResult::Single(PrimitiveTypes::Integer(if *is_present {
                            1
                        } else {
                            0
                        }))
                    }
                };
                //println!("Counting {leftid:?} -> {r:?}");
                r
            }
            IndexedRateExprTerm::StarAttRefComp(y) => {
                match &self.species_counts[y.idx_of_species] {
                    SpeciesAmt::NumOnly(_) => {
                        unreachable!()
                    }
                    SpeciesAmt::CompartmentWithStar(is_present, stars) => {
                        assert!(is_present);
                        EvaluationResult::Single(stars.get_by_str(&y.name).unwrap().clone())
                    }
                    #[cfg(feature = "hybrid_att")]
                    SpeciesAmt::NotExpanded(_) => {
                        unreachable!()
                    }
                }
            }
            IndexedRateExprTerm::StarAttRefReg(y) => match &self.species_counts[y.idx_of_species] {
                SpeciesAmt::NumOnly(_) => {
                    unreachable!()
                }
                SpeciesAmt::CompartmentWithStar(_is_present, _stars) => {
                    unreachable!();
                }
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(not_expanded_attributes) => {
                    //println!("{:?} for {:?}", y, self.species_counts[y.idx_of_species]);
                    //panic!();
                    EvaluationResult::Multi(MultiResults {
                        what: vec![y.id_on_left.clone()],
                        results: not_expanded_attributes
                            .iter()
                            .map(|(_, vals): &(_, StarAttributeValues)| {
                                vals.get_by_str(&y.name).unwrap()
                            })
                            .enumerate()
                            .map(|(n, val)| OneMultiResult {
                                matches_to_get_to_value: vec![MultiResultAssociation {
                                    species_idx: y.idx_of_species,
                                    variation_idx: n,
                                }],
                                value: val.clone(),
                            })
                            .collect(),
                    })
                }
            },

            IndexedRateExprTerm::Condition(cond) => match self.eval_term(&cond.guard) {
                EvaluationResult::Single(s) => match s {
                    PrimitiveTypes::Boolean(b) => {
                        return if b {
                            self.eval_term(&cond.result_true)
                        } else {
                            match &cond.result_false {
                                None => EvaluationResult::Single(PrimitiveTypes::Integer(0)),
                                Some(k) => self.eval_term(&k),
                            }
                        }
                    }
                    _ => unreachable!(),
                },
                #[cfg(feature = "hybrid_att")]
                EvaluationResult::Multi(guard_result) => {
                    //assert!(cond.result_false.is_none());
                    let second_result = self.eval_term(&cond.result_true);
                    let result_false = match &cond.result_false {
                        None => PrimitiveTypes::Integer(0),
                        Some(l) => match self.eval_term(l) {
                            EvaluationResult::Single(l) => l,
                            EvaluationResult::Multi(_) => {
                                unimplemented!("The else branch in a condition can not use hybrid star attributes yet");
                            }
                        },
                    };
                    let r = Self::merge_evaluation_result(
                        |g, v| match g {
                            PrimitiveTypes::Boolean(b) => {
                                if *b {
                                    //println!("guard is good! returning {:?}", v);
                                    v.clone()
                                } else {
                                    //println!("guard is not good! {:?}", g);
                                    result_false.clone()
                                }
                            }
                            _ => unreachable!(),
                        },
                        EvaluationResult::Multi(guard_result),
                        second_result,
                    );
                    match &r {
                        EvaluationResult::Single(_) => {
                            unreachable!()
                        }
                        EvaluationResult::Multi(_m) => {
                            //println!("EVAL RESULT {m}")
                        }
                    }
                    r
                }
            },
            #[cfg(feature = "hybrid_att")]
            IndexedRateExprTerm::LeftSideAmtGuard(gg) => self.eval_left_amt_guard(gg),

            #[cfg(not(feature = "hybrid_att"))]
            IndexedRateExprTerm::LeftSideAmtGuard(gg) => self.eval_left_amt_guard_no_starr(gg),
            IndexedRateExprTerm::BuildinFunction(f, params) => {
                let params: Vec<_> = params.iter().map(|k| self.eval_term(k)).collect();
                if params.iter().any(|k| k.single().is_none()) {
                    #[cfg(feature = "hybrid_att")]
                    {
                        if params.len() > 1 {
                            unimplemented!("Not implemented to have star value in function with more than 1 parameter");
                        }
                        let mut r = match params.into_iter().next().unwrap() {
                            EvaluationResult::Single(_) => {
                                unreachable!()
                            }
                            EvaluationResult::Multi(x) => x,
                        };
                        r.results.iter_mut().for_each(|k| {
                            k.value = f.eval(
                                &vec![k.value.clone()],
                                true,
                                Some(self.last_time_dependend_update),
                            )
                        });

                        EvaluationResult::Multi(r)
                    }
                    #[cfg(not(feature = "hybrid_att"))]
                    unreachable!()
                } else {
                    EvaluationResult::Single(
                        f.eval(
                            &params
                                .into_iter()
                                .map(|k| match k {
                                    EvaluationResult::Single(x) => x,
                                    #[cfg(feature = "hybrid_att")]
                                    EvaluationResult::Multi(_) => {
                                        unreachable!()
                                    }
                                })
                                .collect(),
                            true,
                            Some(self.last_time_dependend_update),
                        ),
                    )
                }
            }
            IndexedRateExprTerm::SpecifCountFromGlobalConstraint(data) => {
                EvaluationResult::Single(PrimitiveTypes::Integer(
                    data.iter()
                        .filter(|(_, expr)| match expr {
                            None => true,
                            Some(x) => match self.eval_term(x) {
                                EvaluationResult::Single(x) => x.as_bool(),
                                #[cfg(feature = "hybrid_att")]
                                EvaluationResult::Multi(_) => {
                                    unimplemented!("Star not implemented in this global count")
                                }
                            },
                        })
                        .map(|(idx, _)| self.species_counts[*idx].total_amt())
                        .sum::<usize>() as i64,
                ))
            }
        };
        //println!("### {:?} -> {:?}", expr, r);
        r
    }
    fn eval_left_amt_guard_no_starr(&self, gg: &LeftSideAmtGuard) -> EvaluationResult {
        //println!("--{:#?}", gg);
        let mut amounts_iter = gg.idices.iter().map(|k| &self.species_counts[*k]);
        debug_assert!(!gg.has_starr_attribut_amounts);
        if gg.is_mass_action {
            let flat_amts = amounts_iter.map(|k| k.total_amt());
            let mut prefak = 1.;
            for (fa, needed) in flat_amts.zip(gg.idices_replication_num.iter()) {
                if fa < *needed {
                    return EvaluationResult::Single(PrimitiveTypes::Integer(0));
                }
                assert!(*needed > 0);
                prefak *= (fa - (needed - 1)) as f64;
            }
            match self.eval_term(&&gg.inner) {
                EvaluationResult::Single(k) => EvaluationResult::Single(
                    FloatOp::Mul
                        .apply(&PrimitiveTypes::Float(prefak), &k)
                        .unwrap(),
                ),
                #[cfg(feature = "hybrid_att")]
                EvaluationResult::Multi(mut multi) => {
                    multi.results.iter_mut().for_each(|k| {
                        k.value = FloatOp::Mul
                            .apply(&PrimitiveTypes::Float(prefak), &k.value)
                            .unwrap()
                    });
                    EvaluationResult::Multi(multi)
                }
            }
        } else {
            let flat_amts = amounts_iter.clone().map(|k| k.total_amt());
            for (fa, needed) in flat_amts.zip(gg.idices_replication_num.iter()) {
                if fa < *needed {
                    return EvaluationResult::Single(PrimitiveTypes::Integer(0));
                }
                assert!(*needed > 0);
            }
            if amounts_iter.any(|p| !p.is_not_zero()) {
                EvaluationResult::Single(PrimitiveTypes::Integer(0))
            } else {
                self.eval_term(&&gg.inner)
            }
        }
    }

    #[cfg(feature = "hybrid_att")]
    fn eval_left_amt_guard(&self, gg: &LeftSideAmtGuard) -> EvaluationResult {
        if !gg.has_starr_attribut_amounts {
            return self.eval_left_amt_guard_no_starr(gg);
        }
        if gg.idices.is_empty() {
            return self.eval_term(&gg.inner);
        }
        debug_assert_eq!(gg.idices.len(), gg.idices_left_names.len());
        debug_assert_eq!(gg.idices.len(), gg.idices_replication_num.len());
        let multis = izip!(
            gg.idices.iter(),
            gg.idices_left_names.iter(),
            gg.idices_replication_num.iter()
        )
        .map(
            |(idx, left_name, repamt_needed)| match &self.species_counts[*idx] {
                SpeciesAmt::NumOnly(n) => {
                    if *n >= *repamt_needed {
                        if gg.is_mass_action {
                            Some(EvaluationResult::Single(PrimitiveTypes::Float(
                                (n - (repamt_needed - 1)) as f64,
                            )))
                        } else {
                            Some(EvaluationResult::Single(PrimitiveTypes::Integer(1)))
                        }
                    } else {
                        None //EvaluationResult::Single(PrimitiveTypes::Integer(0))
                    }
                }
                SpeciesAmt::CompartmentWithStar(is_present, _) => {
                    //assert_eq!(*repamt_needed, 1);
                    //todo fix repamount needed to avoid always folse cases for compartment amounts
                    if !is_present || (*repamt_needed != 1) {
                        None //EvaluationResult::Single(PrimitiveTypes::Integer(0))
                    } else {
                        Some(EvaluationResult::Single(PrimitiveTypes::Integer(1)))
                    }
                }
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(multi) => {
                    /* TODO there is a bug here for a case of two not expanded species of the same name. I'll create a guard for that separatly */
                    let multi_result = MultiResults {
                        what: vec![left_name.clone()],
                        results: multi
                            .iter()
                            .enumerate()
                            .map(|(variation_idx, (amt, _))| OneMultiResult {
                                matches_to_get_to_value: vec![MultiResultAssociation {
                                    species_idx: *idx,
                                    variation_idx: variation_idx,
                                }],
                                value: if *amt < *repamt_needed {
                                    PrimitiveTypes::Integer(0)
                                } else {
                                    if gg.is_mass_action {
                                        PrimitiveTypes::Float((*amt - (repamt_needed - 1)) as f64)
                                    } else {
                                        PrimitiveTypes::Integer(1)
                                    }
                                },
                            })
                            .collect(),
                    };
                    multi_result.debug_is_complete();
                    Some(EvaluationResult::Multi(multi_result))
                }
            },
        );
        let prefak = multis
            .reduce(|a, b| match (a, b) {
                (Some(a), Some(b)) => Some(Self::merge_evaluation_result_for_float_op(
                    &FloatOp::Mul,
                    a,
                    b,
                )),
                (_, _) => None,
            })
            .unwrap();
        match prefak {
            None => EvaluationResult::Single(PrimitiveTypes::Integer(0)),
            Some(prefak) => Self::merge_evaluation_result_for_float_op(
                &FloatOp::Mul,
                prefak,
                self.eval_term(&gg.inner),
            ),
        }
    }
    #[cfg(feature = "hybrid_att")]
    fn get_sum_multiplied_by_counts(&self, vals: MultiResults) -> f64 {
        vals.results
            .iter()
            .map(|k| {
                k.value.clone().as_float().unwrap()
                /*   * k.matches_to_get_to_value
                .iter()
                .map(|p| {
                    self.species_counts[p.species_idx]
                        .to_not_expanded()
                        .unwrap()[p.variation_idx]
                        .0 as f64
                })
                .product::<f64>()*/
            })
            .sum::<f64>()
    }

    // Returns none if hard not allowed to fire
    fn eval_rate_expression_and_guard_of(
        &self,
        idx: usize,
        //allow_fail: bool,
    ) -> Option<EvaluationResult> {
        //let idx = idx s usize;
        let reac = &self.reactions[idx];
        //println!("RATE: {:?}", reac.rate);
        match reac.max_count {
            None => {}
            Some(x) => {
                debug_assert_eq!(self.reaction_counts.len(), self.reaction_names.len());
                if x == self.reaction_counts[reac.reaction_number_of_prototype] {
                    //reac.count {
                    println!(
                        "Max reaction count reachead! for {} of {}",
                        idx,
                        self.reactions.len()
                    );
                    return None;
                } else {
                    assert!(x > reac.count)
                }
            }
        }
        Some(self.eval_term(&reac.rate))
    }

    pub fn get_propensity_of(&self, idx: usize, _allow_fail: bool) -> Result<f64, String> {
        //let idx = idx as usize;
        let reac = &self.reactions[idx];
        //        println!("{:?}", reac.rate);

        let r1 = self.evaluate_performance_template(&reac.rate_optimization);
        if !cfg!(debug_assertions) {
            match r1 {
                None => {}
                Some(k) => {
                    if k >= 0. && k.is_finite() {
                        return Ok(k);
                    }
                }
            }
        }

        //println!("Spec in : {:?}",reac.species_in_idx);
        let eval_rate = match self.eval_rate_expression_and_guard_of(idx) {
            None => {
                assert!(r1.is_none());
                return Ok(0.);
            }
            Some(x) => x,
        };

        let ret = match eval_rate {
            EvaluationResult::Single(k) => k.as_float().unwrap(),
            #[cfg(feature = "hybrid_att")]
            EvaluationResult::Multi(m) => self.get_sum_multiplied_by_counts(m),
        };
        if !ret.is_finite() {
            return Err(format!("Rate is not a normal number!!!!\n This is a model error!\nRULE: {}\nCalculated propensity:{}\n\n",self.reaction_names[reac.reaction_number_of_prototype],ret));
            //return 0.;
        }
        if ret < 0. {
            /*if allow_fail {
                return -1.;
            }*/
            //error!("\nERROR:");
            //self.print_tree_flat();
            return Err(format!(
                "Negative Rate!!!! This is a model error!\nRULE: {}\nCalculated propensity:{}\n\n",
                self.reaction_names[reac.reaction_number_of_prototype], ret
            ));
            //error!("Whole rate expr: {:?}", reac.rate);
            /*if self.species_counts.len() < 100 {
                error!("species Counts: {:?}", self.species_counts);
            }*/
            //println!("globs: {:?}", self.global_values);

            //panic!();
            //return 0.;
        }
        match r1 {
            None => {}
            Some(r1) => {
                assert_ge!(
                    r1 * 1.0001,
                    ret,
                    "{:?}\n{:?}\n{:?}",
                    self.reaction_names[reac.reaction_number_of_prototype],
                    reac.rate_optimization,
                    reac.rate,
                );
                assert_le!(
                    r1 * 0.9999,
                    ret,
                    "{:?}\n{:?}\n{:?}",
                    self.reaction_names[reac.reaction_number_of_prototype],
                    reac.rate_optimization,
                    reac.rate,
                );
            }
        }
        Ok(ret)
    }

    pub fn update_all_propensities_max_count(&mut self) -> Result<(), String> {
        /*swap(&mut tmp, &mut self.time_dependend_reactions);
        for i in tmp.iter() {
            self.propensities[*i] = self.get_propensity_of(*i, false)?;
        }
        swap(&mut tmp, &mut self.time_dependend_reactions);*/
        let mut any_max_count = false;
        for i in 0..self.reactions.len() {
            if self.reactions[i].max_count.is_some() {
                self.propensities[i] = self.get_propensity_of(i, false)?;
                any_max_count = true;
            }
        }
        if any_max_count {
            self.rebuild_tree();
        }
        Ok(())
    }
    pub fn update_all_propensities_time_dep(&mut self) -> Result<(), String> {
        assert_ge!(self.time, self.last_time_dependend_update);
        self.last_time_dependend_update = self.time;
        let mut tmp = vec![];
        swap(&mut tmp, &mut self.time_dependend_reactions);
        for i in tmp.iter() {
            self.propensities[*i] = self.get_propensity_of(*i, false)?;
        }
        swap(&mut tmp, &mut self.time_dependend_reactions);
        self.rebuild_tree();
        Ok(())
    }
    fn _check_all_propensities(&self) {
        for i in 0..to_uindex(&self.reactions.len()) {
            let a = self.propensities[i];
            let b = self.get_propensity_of(i, false).unwrap();
            let eps_fak = 1e-4;
            if (a > b * (1. + eps_fak)) || (a < b * (1. - eps_fak)) {
                if self.time_dependend_reactions.contains(&i) {
                    println!(
                        "This reaction is known to be time dependent! {} , {}",
                        self.time, self.last_time_dependend_update
                    );
                }
                println!("Rate is: {:?}", self.reactions[i].rate);
                if self.reactions[i].max_count.is_some() {
                    println!(
                        "Count is {}",
                        self.reaction_counts[self.reactions[i].reaction_number_of_prototype]
                    );
                }
                panic!(
                    "{a} (assumed) != {b} (should) for reaction {}",
                    self.reaction_names[self.reactions[i].reaction_number_of_prototype]
                );
            }
        }
        //self.propensity_sum = self.propensities.iter().sum();
    }
    fn _update_all_propensities_for_tau(&mut self, _rebuild_tree: bool, _allow_fail: bool) {
        unimplemented!();
        /*if self.species_counts_as_used_for_propensitie.len() > 0 {
            self.species_counts_as_used_for_propensitie =
                self.species_counts.iter().cloned().collect();
        }
        for i in 0..to_uindex(&self.reactions.len()) {
            //self.set_propensity(i, self.get_propensity_of(i));
            self.propensities[i] = self.get_propensity_of(i, allow_fail);
        }
        if rebuild_tree {
            self.rebuild_tree();
        }
        return;*/
    }
    fn update_all_propensities(&mut self) -> Result<(), String> {
        assert_le!(self.last_time_dependend_update, self.time);
        self.last_time_dependend_update = self.time;
        if self.species_counts_as_used_for_propensitie.len() > 0 {
            unimplemented!()
            /* self.species_counts_as_used_for_propensitie =
            self.species_counts.iter().cloned().collect();*/
        }
        for i in 0..to_uindex(&self.reactions.len()) {
            //self.set_propensity(i, self.get_propensity_of(i));
            self.propensities[i] = self.get_propensity_of(i, false)?;
        }
        self.rebuild_tree();
        return Ok(());
    }

    /*fn write_guard_code_gen(&self, g: IndexedLogicalConstraint) -> String{
        match
    }*/

    #[allow(unreachable_code, unused_variables, unused)]
    fn write_reaction_exec_tau_to_code_gen(&self, idx: &usize) -> String {
        let mut otput = String::new();
        let reac = &self.reactions[*idx];
        match reac.max_count {
            None => {}
            Some(_y) => {
                unimplemented!("Max counts do not work with code generation")
            }
        }
        assert!(!self.reactions[*idx].invokes_structural_change);
        unimplemented!();
        /*reac.species_in_idx
        .iter()
        .zip(unimplemented!() /*reac.species_in_amt.iter()*/)
        .for_each(|(idx_s, amt)| {
            otput += format!(
                "reactants_state[{}] -= {} * num_fire as isize;\n",
                idx_s, *amt
            )
            .as_str()
        });*/
        reac.species_out.iter().for_each(|v| {
            otput += format!("reactants_state[{}] += num_fire as isize;\n", v.1).as_str()
        });
        /*for (val_idx, term) in reac.global_val_changes.iter() {
            otput += format!(
                "self.global_vals.{} = ({}) as f64;",
                self.obs_global_vals_names[*val_idx],
                self.eval_term_code_gen(term)
            )
            .as_str();
        }*/

        otput
    }

    fn write_reaction_exec_to_code_gen(&self, idx: &usize) -> String {
        let mut otput = String::new();
        /*
        self.reaction_counts[self.reactions[idx_of_reaction].reaction_number_of_prototype] += 1;
        self.reactions[idx_of_reaction].count += 1;
        */
        let reac = &self.reactions[*idx];
        assert!(!self.reactions[*idx].invokes_structural_change);
        reac.species_in_idx
            .iter()
            //.zip(reac.species_in_amt.iter())
            .for_each(|idx_s /*, amt*/| {
                otput += format!(
                    "self.species_amounts.{} -= 1;\n",
                    self.species_names[*idx_s as usize]
                )
                .as_str()
            });
        reac.species_out.iter().for_each(|v| {
            otput += format!("self.species_amounts.{} += 1;\n", self.species_names[v.1]).as_str()
        });
        /*for (val_idx, term) in reac.global_val_changes.iter() {
            otput += format!(
                "self.global_vals.{} = ({}) as f64;",
                self.obs_global_vals_names[*val_idx],
                self.eval_term_code_gen(term)
            )
            .as_str();
        }*/
        otput += "\n\n //update dependent reactions\n";
        if reac.dependend_reactions.as_ref().unwrap().len() + 1 >= self.reactions.len() {
            otput += "self.update_all_propensities()\n";
        } else {
            let use_delta =
                reac.dependend_reactions.as_ref().unwrap().len() < self.reactions.len() / 5;
            if use_delta {
                otput += format!(
                    "for idx in {:?} {{ \n\t\tself.propensity_sum -= self.propensities[idx]}}\n;",
                    reac.dependend_reactions
                )
                .as_str()
            }
            for i in reac.dependend_reactions.as_ref().unwrap().iter() {
                otput += format!(
                    "self.propensities[{}] = self.get_propropensity_for_{}();\n",
                    i, i
                )
                .as_str();
            }
            if use_delta {
                otput += format!(
                    "for idx in {:?} {{ \n\t\tself.propensity_sum += self.propensities[idx]}}\n;",
                    reac.dependend_reactions
                )
                .as_str()
            } else {
                otput += "self.propensity_sum = self.propensities.iter().sum();";
            }
        }

        otput
    }

    #[allow(unreachable_code)]
    fn write_reaction_rate_to_code_gen(&self, idx: &usize) -> String {
        let mut outpt = String::new();
        let _reac = &self.reactions[*idx];
        outpt += "let species_faktor = 1.";
        unimplemented!(); /*
                          for (s, amt) in reac.species_in_idx.iter().zip(reac.species_in_amt.iter()) {
                              for n in 0..*amt {
                                  if n == 0 {
                                      outpt += format!(" * self.species_amounts.{} as f64", self.species_names[*s])
                                          .as_str();
                                  } else {
                                      outpt += format!(
                                          " * (self.species_amounts.{} - {}) as f64",
                                          self.species_names[*s], n
                                      )
                                      .as_str();
                                  }
                              }
                          }*/
        outpt += ";\nif species_faktor == 0. { return 0.};\n;let calculated_rate = ";
        unimplemented!();
        /* match &reac.rate {
            /*IndexedEnumeratedRate::MassActionConstant(k) => {
                outpt += format!("{} as f64 * species_faktor as f64", k).as_str()
            }
            IndexedEnumeratedRate::MassActionExpression(expr) => {
                outpt += format!(
                    "({}) as f64 * species_faktor as f64",
                    self.eval_term_code_gen(expr)
                )
                .as_str()
            }

            IndexedEnumeratedRate::ComplexExpression(expr) => {
                outpt += format!("({}) as f64", self.eval_term_code_gen(expr)).as_str()
            }*/
        };*/
        outpt += ";\n if calculated_rate > 0. { return calculated_rate;} else {panic!(\"RATE CALC ERROR\")}";
        /*

        l
        if !ret.is_finite() {
            panic!("Rate is not a normal number!!!!\n This is a model error!\nRULE: {}\nCalculated propensity:{}\n\n",self.reaction_names[reac.reaction_number_of_prototype],ret);
            //return 0.;
        }
        if ret < 0. {
            println!(
                "Negative Rate!!!! This is a model error!\nRULE: {}\nCalculated propensity:{}\n\n",
                self.reaction_names[reac.reaction_number_of_prototype], ret
            );
            return 0.;
        }
        ret*/
        outpt
    }
    #[allow(unreachable_code)]
    fn sort_reactions(&mut self) {
        //todo
        return; /* TODO Sorting is needed for time dependent idx and flat model */
        #[allow(unreachable_code)]
        let mut all_reacs = vec![
            EnumeratedIndexedReaction {
                species_in_idx: vec![],
                //species_in_amt: vec![],
                species_in_names: vec![],
                could_have_multi_result: true,
                species_out: vec![],
                reaction_number_of_prototype: 0,
                invokes_structural_change: false,
                //global_val_changes: vec![],
                rate: IndexedRateExprTerm::Primitive(PrimitiveTypes::Integer(-1)),
                out_attribute_values: vec![],
                dependend_reactions: None,
                dependend_propensities_in_triee: vec![],
                count: 0,
                rate_optimization: RateOptimization::None,
                max_count: None
            };
            self.reactions.len()
        ];
        swap(&mut self.reactions, &mut all_reacs);

        let mut histos: Vec<usize> = (0..all_reacs.len()).collect();
        histos.sort_unstable_by_key(|idx| std::cmp::Reverse(all_reacs[*idx].count));

        let mut indices = vec![0; histos.len()];
        for i in histos.iter() {
            indices[histos[*i]] = *i;
        }
        all_reacs
            .iter_mut() /*par_iter_mut()*/
            .for_each(|reac| {
                reac.dependend_reactions
                    .as_mut()
                    .unwrap() // todo might not exist yet
                    .iter_mut()
                    .for_each(|i| *i = to_uindex(&indices[*i as usize]));
                reac.dependend_propensities_in_triee = reac.compute_tree_updates();
            });
        /*for i in indices.iter() {
            print!("{}->{},", i, all_reacs[*i].count);
        }
        println!();*/

        for (i, reac) in indices.into_iter().zip(all_reacs.into_iter()) {
            self.reactions[i] = reac;
        }
        match self.update_all_propensities() {
            Ok(_) => {}
            Err(i) => {
                unimplemented!("{}", i)
            }
        }
        //self.debug_print_counts();
    }

    fn _fire_reaction_tau(
        &mut self,
        idx_of_reaction: usize,
        number_of_fireings: usize,
        tmp_counts: &mut Vec<isize>,
    ) -> FireResult {
        /*self.reaction_counts[self.reactions[idx_of_reaction].reaction_number_of_prototype] +=
        number_of_fireings;*/

        self.reactions[idx_of_reaction].count += number_of_fireings;
        let reac = &self.reactions[idx_of_reaction];

        // if we have a structural change: Abort execution and don't change system
        assert!(!self.reactions[idx_of_reaction].invokes_structural_change);
        assert!(self.species_counts_as_used_for_propensitie.is_empty());

        reac.species_in_idx
            .iter()
            //.zip(reac.species_in_amt.iter())
            .for_each(|idx| tmp_counts[*idx as usize] -= (1 * number_of_fireings) as isize);
        reac.species_out
            .iter()
            .for_each(|v| tmp_counts[v.1 as usize] += number_of_fireings as isize);

        /*if !self.reactions[idx_of_reaction]
            .global_val_changes
            .is_empty()
        {
            let changes: Vec<_> = self.reactions[idx_of_reaction]
                .global_val_changes
                .iter()
                .map(|(idx, c)| (idx, self.eval_term(c)))
                .collect();
            for (idx, val) in changes {
                self.global_values[*idx] = val;
            }
        }*/
        FireResult::Good
    }

    fn choose_fireing_match(&mut self, idx_of_reaction: usize) -> Option<MultiResults> {
        if !self.reactions[idx_of_reaction].could_have_multi_result {
            return None;
        }
        // weighted choice from result
        let rate = self
            .eval_rate_expression_and_guard_of(idx_of_reaction)
            .unwrap();

        if cfg!(debug_assertions) {
            let rate2 = self
                .eval_rate_expression_and_guard_of(idx_of_reaction)
                .unwrap();
            assert_eq!(rate, rate2);
        }

        match rate {
            EvaluationResult::Single(_) => {
                //println!("Single only");
                None
            }
            #[cfg(feature = "hybrid_att")]
            EvaluationResult::Multi(mut x) => {
                //println!("------------\n{x}");
                assert!(x.results.len() > 0);
                if x.results.len() == 1 {
                    //println!("RATE: {:?}", self.reactions[idx_of_reaction].rate);
                    //println!("SINGLE result: {x}");
                    assert_ge!(x.what.len(), 1);
                    Some(x)
                } else {
                    let distr: WeightedIndex<f64>;
                    {
                        distr = WeightedIndex::new(
                            x.results
                                .iter()
                                .map(|k| k.value.clone().as_float().unwrap()),
                        )
                        .unwrap();
                    }
                    let choosen_idx = distr.sample(&mut self.rng);
                    //println!("CHOOOOOOOS {choosen_idx}");
                    x.results.swap(choosen_idx, 0);
                    x.results.resize_with(1, || unreachable!());
                    //println!("{x}");
                    //println!("------\n");
                    /*assert_ge!(
                        x.what.len(),
                        self.reactions[idx_of_reaction].species_in_idx.len()
                    );*/
                    Some(x)
                }
            }
        }
    }

    fn fire_reaction(&mut self, idx_of_reaction: usize) -> Result<FireResult, String> {
        self.reaction_counts[self.reactions[idx_of_reaction].reaction_number_of_prototype] += 1;
        //println!("Fire: {}", self.reactions[idx_of_reaction].count);
        self.reactions[idx_of_reaction].count += 1;

        // if we have a structural change: Abort execution and don't change system
        if self.reactions[idx_of_reaction].invokes_structural_change {
            return Ok(FireResult::InvokeStructureChange);
        }
        #[cfg(feature = "hybrid_att")]
        let choosen_att_match: Option<MultiResults> = self.choose_fireing_match(idx_of_reaction);
        #[cfg(feature = "hybrid_att")]
        let mut lookup: Option<Vec<(usize, usize)>> = match &choosen_att_match {
            Some(x) => Some(
                x.results[0]
                    .matches_to_get_to_value
                    .iter()
                    .map(|k| (k.species_idx, k.variation_idx))
                    .collect(),
            ),
            None => None,
        };
        let reac = &self.reactions[idx_of_reaction];

        let amts: Option<Vec<usize>> = if reac.species_out.iter().all(|(amt, _)| amt.is_none()) {
            None
        } else {
            Some(
                reac.species_out
                    .iter()
                    .map(|(amt, _)| match amt {
                        None => Ok(1),
                        Some(x) => match self.eval_term(x) {
                            EvaluationResult::Single(x) => match x.as_int() {
                                None => {
                                    return Err("Non integer amt".to_string());
                                }
                                Some(x) => {
                                    if x < 0 {
                                        return Err("Negative amt".to_string());
                                    }
                                    Ok(x as usize)
                                }
                            },
                            #[cfg(feature = "hybrid_att")]
                            _ => {
                                return Err("Star attribute in amt not implemented".to_string());
                            }
                        },
                    })
                    .collect::<Result<Vec<_>, String>>()?,
            )
        };
        //println!("FIRE {:?}",reac);

        /*println!(
            "choosen fire result {}",
            choosen_att_match.as_ref().unwrap()
        );*/
        //
        //println!("Out vals = {:?}", reac.out_attribute_values);
        let all_new_values: Vec<StarAttributeValues> = reac
            .out_attribute_values
            .iter()
            .map(|assignments| {
                StarAttributeValues {
                    names: assignments.iter().map(|(k, _)| k.clone()).collect(),
                    values: assignments
                        .iter()
                        .map(|(_, expr)| match self.eval_term(expr) {
                            EvaluationResult::Single(k) => k,
                            #[cfg(feature = "hybrid_att")]
                            EvaluationResult::Multi(attribute_assignments) => {
                                //println!("expr:{:?}\nFound: {:?}", expr, attribute_assignments);
                                assert!(!attribute_assignments.results.is_empty());
                                //println!("Rate expr: {:?}", reac.rate);
                                //println!("Choosen: {:?}", choosen_att_match.as_ref().unwrap());
                                debug_assert!(
                                    attribute_assignments.what.len()
                                        <= choosen_att_match.as_ref().unwrap().what.len()
                                );
                                //println!("\n\n Have {:?}\nshould: {:?}", m.results, choosen_att_match);
                                for k in attribute_assignments.results {
                                    if is_submatch(
                                        &k.matches_to_get_to_value,
                                        &choosen_att_match.as_ref().unwrap().results[0]
                                            .matches_to_get_to_value,
                                    ) {
                                        return k.value;
                                    }
                                }
                                panic!("Could not find correct match! Internal error.");
                            }
                        })
                        .collect(),
                }
                .sort()
            })
            .collect();
        for i in all_new_values.iter() {
            debug_assert!(i.is_sorted());
        }
        let counts = &mut self.species_counts;
        //println!("{:?} -> {:?} {:?}", reac.species_in_idx, lookup, counts);
        #[cfg(feature = "hybrid_att")]
        let mut removed_multi_vals: Vec<(usize, usize)> = vec![];
        reac.species_in_idx
            .iter()
            .for_each(|idx| match &mut counts[*idx as usize] {
                SpeciesAmt::NumOnly(x) => {
                    //println!("reduce {idx} to {x}");

                    assert!(*x >= 1);
                    *x -= 1;
                }
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(y) => {
                    assert!(!y.is_empty());

                    // need to choose which to diminish
                    let variation;
                    if y.len() == 1 {
                        variation = 0;
                    } else {
                        debug_assert_eq!(choosen_att_match.as_ref().unwrap().results.len(), 1);
                        //println!("{:?}", choosen_att_match);
                        //assert_eq!(choosen_att_match.as_ref().unwrap().what.len(),1);
                        match &mut lookup {
                            Some(look) => {
                                assert_le!(look.len(), y.len());
                                let (sub_idx, (_, v)) = // todo there might be an issue here with always picking the first match
                                    look.iter().find_position(|(a, _)| a == idx).unwrap();
                                variation = *v;
                                look.remove(sub_idx);
                                //variation = *look.get(idx).unwrap();
                                //look.remove(idx);
                            }
                            None => unreachable!(),
                        }

                        /*variation = choosen_att_match.as_ref().expect("Could be unimplemented, because I was not expecting multiple options for star attributes here. To implement amt check.").results[0]
                        .matches_to_get_to_value
                        .iter()
                        .find(|k| k.species_idx == *idx)
                        .unwrap()
                        .variation_idx;*/
                    }
                    assert_lt!(variation, y.len());
                    //println!("Using variation {variation}");
                    //assert_eq!(*amt, 1);
                    assert!(y[variation].0 > 0);
                    if y[variation].0 == 1 {
                        removed_multi_vals.push((*idx, variation));
                        //y.remove(variation);
                    } else {
                        y[variation].0 -= 1; // amt must be 1
                        assert!(y[variation].0 > 0);
                    }
                    for (variat, _) in y.iter() {
                        assert!(*variat > 0);
                    }
                }
                SpeciesAmt::CompartmentWithStar(is_present, _) => {
                    assert!(*is_present);
                    *is_present = false;
                }
            });

        #[cfg(feature = "hybrid_att")]
        {
            removed_multi_vals.sort();
            removed_multi_vals.dedup();
            for (s_idx, var_idx) in removed_multi_vals.iter().rev() {
                match &mut counts[*s_idx] {
                    #[cfg(feature = "hybrid_att")]
                    SpeciesAmt::NotExpanded(s) => {
                        s.remove(*var_idx);
                    }
                    _ => unreachable!(),
                }
            }
        }
        //println!("FIRE out {:?}", reac.species_out);
        for (i, ((_amt, out_idx), new_val)) in reac
            .species_out
            .iter()
            .zip(all_new_values.into_iter())
            .enumerate()
        {
            let amt = match &amts {
                None => 1,
                Some(x) => x[i],
            };
            let counts = &mut self.species_counts;

            match &mut counts[*out_idx] {
                SpeciesAmt::NumOnly(x) => {
                    assert!(
                        new_val.names.is_empty(),
                        "We have only {:?}, but expect to assign {:?}",
                        x,
                        new_val
                    );
                    *x += amt
                }
                SpeciesAmt::CompartmentWithStar(is_present, stars) => {
                    assert_eq!(amt, 1);
                    assert!(!*is_present);
                    *is_present = true;
                    *stars = new_val;
                }
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(y) => {
                    assert_eq!(amt, 1);
                    debug_assert!(new_val.is_sorted());
                    match y.iter_mut().find(|(mut _n, k)| *k == new_val) {
                        None => {
                            y.push((1, new_val));
                        }
                        Some((old_n, _)) => {
                            *old_n += 1;
                        }
                    }
                }
            }
        }

        /*for k in self.species_counts.iter() {
            match k {
                SpeciesAmt::NumOnly(_) => {}
                SpeciesAmt::NotExpanded(l) => {
                    for (p, _) in l.iter() {
                        assert!(*p > 0);
                    }
                }
            }
        }*/

        /*if !self.reactions[idx_of_reaction]
            .global_val_changes
            .is_empty()
        {
            let changes: Vec<_> = self.reactions[idx_of_reaction]
                .global_val_changes
                .iter()
                .map(|(idx, c)| (idx, self.eval_term(c)))
                .collect();
            for (idx, val) in changes {
                self.global_values[*idx] = val;
            }
        }*/

        // for approximative version skip rate update
        if !self.species_counts_as_used_for_propensitie.is_empty()
        /*&& self.reactions[idx_of_reaction]
        .global_val_changes
        .is_empty()*/
        {
            unimplemented!()
            /*let mut all_changed_species_indices =
                reac.species_in_idx.iter().chain(reac.species_out.iter());
            if all_changed_species_indices.all(|idx| {
                let b = self.species_counts[*idx] as f64;
                let a = self.species_counts_as_used_for_propensitie[*idx] as f64;
                (a - b).abs() / (b + 1e-5) < self.acceptable_error
            }) {
                self.metrics.steps_where_approximated += 1;
                return FireResult::Good;
            } else {
                let mut tmp = vec![];
                std::mem::swap(&mut tmp, &mut self.species_counts_as_used_for_propensitie);
                self.reactions[idx_of_reaction]
                    .species_in_idx
                    .iter()
                    .chain(self.reactions[idx_of_reaction].species_out.iter())
                    .for_each(|idx| tmp[*idx] = self.species_counts[*idx]);
                std::mem::swap(&mut tmp, &mut self.species_counts_as_used_for_propensitie);
            }*/
        }

        match self.reactions[idx_of_reaction].dependend_reactions {
            None => self.find_depend_reactions_for(idx_of_reaction),
            Some(_) => {}
        }

        //self.update_all_propensities();
        self.metrics.num_rate_updates += self.reactions[idx_of_reaction]
            .dependend_reactions
            .as_ref()
            .unwrap()
            .len();

        for idx in self.reactions[idx_of_reaction]
            .dependend_reactions
            .as_ref()
            .unwrap()
            .iter()
        {
            self.propensities[*idx] = match self.get_propensity_of(*idx, false) {
                Ok(l) => l,
                Err(i) => {
                    return Err(i);
                }
            }
        }

        self.update_tree_for_fired_reaction(idx_of_reaction);

        Ok(FireResult::Good)
    }
    fn update_tree_for_fired_reaction(&mut self, idx_of_reaction: usize) {
        assert!(self.reactions[idx_of_reaction]
            .dependend_reactions
            .is_some());
        for idx in self.reactions[idx_of_reaction]
            .dependend_propensities_in_triee
            .iter()
        {
            let left_child = *idx * 2 + 1;
            let right_child = *idx * 2 + 2;
            if right_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[*idx] =
                    self.propensitiy_tree[right_child] + self.propensitiy_tree[left_child];
            } else if left_child < self.propensitiy_tree.len() {
                self.propensitiy_tree[*idx] = self.propensitiy_tree[left_child];
            } else {
                self.propensitiy_tree[*idx] = 0.;
            }
            self.propensitiy_tree[*idx] += self.propensities[*idx];
        }
    }

    pub fn _debug_print_counts(&self) {
        for r in self.reactions.iter() {
            print!("{},", r.count);
        }
        println!();
    }

    pub fn step_tau(
        &mut self,
        _planned_step_size: f64,
        _io_step_size: f64,
        _print_reaction_fired: bool,
    ) -> StepResult {
        unimplemented!()
        /*
        if planned_step_size >= io_step_size {
            planned_step_size = io_step_size / 2.;
        }
        //println!("STEPSIZE {}",step_size);
        if self.metrics.step_function_calls <= 1 {
            self.update_all_propensities_for_tau(false, false);
        }
        if self.metrics.step_function_calls == 0 {
            assert!(self.time <= 0.);
            self.make_observations();
            self.last_io = self.time;
            self.metrics.step_function_calls = 1;
        }
        let sum = match self.propensitiy_tree.first() {
            None => return StepResult::NoReactionPossible,
            Some(x) => *x,
        };
        if sum <= 0.0 {
            self.time = self.last_io + io_step_size;
            self.last_io = self.time;
            self.update_all_propensities_time_dep();
            self.make_observations();
            self.metrics.step_function_calls += 1;
            return StepResult::NoReactionPossible;
        }
        let do_observe = self.time + planned_step_size > self.last_io + io_step_size;
        if do_observe {
            assert!(planned_step_size > 0.);
            let stepsize_in = planned_step_size;
            planned_step_size = self.last_io + io_step_size - self.time;
            if planned_step_size <= 0. {
                self.make_observations();
                self.last_io = self.time;
                return StepResult::OnlyObservation;
            }
            assert!(planned_step_size > 0.);
            assert!(planned_step_size <= stepsize_in);
        }

        let mut fireings: Vec<u64> = self
            .propensities
            .clone()
            .into_iter()
            .map(|prop| {
                if prop > 0. {
                    let lambda = prop * planned_step_size;
                    //println!("{} = {} * {}",lambda,prop,planned_step_size);

                    if lambda < 1e-12 {
                        return 0;
                    }

                    if lambda == 0. {
                        return 0;
                    }
                    let dstr = Poisson::new(lambda);
                    //println!("Poisson at prop={}, stepsize={}",prop,step_size);
                    if dstr.is_ok() {
                        let res = dstr.unwrap().sample(&mut self.rng);
                        if res > 3135946490245806. {
                            panic!("Excessivly large rate for tau leaping! rate is {}", res);
                            //println!("{prop}*{planned_step_size} = {} -> {}", lambda, res);
                        }
                        res as u64
                    } else {
                        panic!(
                            "Poisson error at prop={}, stepsize={}",
                            prop, planned_step_size
                        );
                    }
                } else {
                    0
                }
            })
            .collect();

        // ensure no more than the maximum allowed number of execs
        fireings.iter_mut().enumerate().for_each(|(idx, firi)| {
            match self.reactions[idx].max_count {
                None => {}
                Some(mx) => {
                    if *firi + self.reactions[idx].count as u64 > mx as u64 {
                        *firi = self.reactions[idx].count as u64 - mx as u64
                    }
                }
            }
        });
        let mut tmp_counts: Vec<isize> = self.species_counts.iter().map(|k| *k as isize).collect();
        //let old_glob_vals = self.global_values.clone();
        //println!("{:?}", fireings);
        let total_num_firings = fireings.iter().sum::<u64>();
        //println!("tau steps: {}",fireings.iter().sum::<u64>());
        let mut num_structural_change = 0;
        let mut idx_of_structural_change = None;
        for (idx, n) in fireings.iter().enumerate().filter(|(_idx, n)| **n > 0) {
            if self.reactions[idx].invokes_structural_change {
                num_structural_change += n;
                idx_of_structural_change = Some(idx);
            } else {
                self.fire_reaction_tau(idx, *n as usize, &mut tmp_counts);
            }
        }

        //println!("{:?}{:?}", fireings, tmp_counts);
        let all_values_positive = tmp_counts.iter().all(|k| *k >= 0);
        let mut new_counts: Vec<_> = tmp_counts.into_iter().map(|k| k as usize).collect();
        swap(&mut self.species_counts, &mut new_counts);
        self.update_all_propensities_for_tau(false, true);
        let all_still_possible = fireings
            .iter()
            .enumerate()
            .filter(|(_idx, n)| **n > 0)
            .all(|(idx, _)| self.propensities[idx] > 0.);
        let mut detailed_check = true;
        if !all_still_possible {
            detailed_check = total_num_firings <= 1;
        }
        if all_values_positive
            && (all_still_possible || detailed_check)
            && num_structural_change <= 1
        {
            // Accept

            if print_reaction_fired {
                for (num, idx) in fireings.iter().enumerate().filter(|(_idx, n)| **n > 0) {
                    println!(
                        " * {} x Fire reaction: {} *",
                        num,
                        self.reaction_names
                            [self.reactions[*idx as usize].reaction_number_of_prototype]
                    );
                }
            }

            self.reaction_counts
                .iter_mut()
                .zip(fireings.iter())
                .for_each(|(count, add)| *count += *add as usize);
            self.metrics.step_function_calls += 1;
            self.time += planned_step_size;
            //println!("STEPS: {}",total_num_firings);
            if do_observe {
                self.last_io = self.time;
                self.make_observations();
            }
            if total_num_firings > 1 {
                self.metrics.virtual_steps += total_num_firings as usize - 1;
            }
            if num_structural_change == 1 {
                StepResult::StructuralChange {
                    idx_of_reaction: idx_of_structural_change.unwrap(),
                }
            } else {
                StepResult::AllGood
            }
        } else {
            //println!("deeper!");
            self.metrics.steps_where_tau_too_large += 1;
            swap(&mut self.species_counts, &mut new_counts);
            //self.global_values = old_glob_vals;
            match self.step_tau(planned_step_size / 2., io_step_size, print_reaction_fired) {
                StepResult::AllGood => {
                    self.step_tau(planned_step_size / 2., io_step_size, print_reaction_fired)
                }
                x => x,
            }
        }

        /*if self.metrics.real_steps > 100 {
            panic!("abort");
        }*/

        */
    }

    pub fn step(&mut self, print_reaction_fired: bool) -> Result<StepResult, String> {
        let timestep;
        if self.propensities.is_empty() {
            if self.special_times.is_empty() {
                return Ok(StepResult::NoReactionPossible);
            } else {
                timestep = self.special_times.last().unwrap().time;
            }
        } else {
            if self.metrics.step_function_calls <= 0 {
                self.update_all_propensities()?;
            }
            self.metrics.step_function_calls += 1;

            if self.metrics.step_function_calls == self.count_for_next_sort {
                self.sort_reactions();
                self.count_for_next_sort *= 4;
            }

            if cfg!(debug_assertions) {
                self._check_all_propensities();
            }
            /*if (self.metrics.real_steps % std::cmp::min(1000, self.reactions.len()) == 0)
                || self.propensity_sum < 0.
            {
                self.propensity_sum = self.propensities.iter().sum();
            }*/

            /*let check_all_values: bool = false;
            // TODO move to smarter propensity tree from ml3
            if check_all_values {
                let old_propensities = self.propensities.clone();
                let old_propensity_sum = self.propensitiy_tree[0];
                self.update_all_propensities();
                for (old, new) in old_propensities.iter().zip(self.propensities.iter()) {
                    if (*old < *new * 0.9999) || (*old > *new * 1.0001) {
                        panic!(
                            "Propensity deviates as {} != {}, at t={}, step {}",
                            old, new, self.time, self.metrics.real_steps
                        );
                    }
                }
                if (old_propensity_sum >= 1.00001 * self.propensitiy_tree[0])
                    || (old_propensity_sum < 0.99999 * self.propensitiy_tree[0])
                {
                    panic!(
                        "Propensity sum deviation! {} != {}",
                        old_propensity_sum, self.propensitiy_tree[0]
                    );
                }
            }*/
            if self.propensitiy_tree[0] <= 0.0 {
                self.update_all_propensities()?;
            }

            if self.propensitiy_tree[0] <= 0.0 {
                //println!("No reaction possible!");
                //self.write_output(&time, &species_counts, &global_attribute_values);
                match self.special_times.last() {
                    None => {
                        return Ok(StepResult::NoReactionPossible);
                    }
                    Some(k) => {
                        timestep = k.time;
                    }
                }
            } else {
                let distr = rand_distr::Exp::new(self.propensitiy_tree[0]).unwrap();
                timestep = distr.sample(&mut self.rng);
            }
        }

        match self.special_times.last() {
            None => {}
            Some(k) => {
                if k.time <= self.time + timestep {
                    assert_ge!(k.time, self.time);
                    self.time = k.time;
                    let back = self.special_times.pop().unwrap();
                    return match back.typ {
                        SpecialTimeType::Observation => {
                            //self.make_observations();
                            Ok(StepResult::OnlyObservation)
                        }
                        SpecialTimeType::TimeDepRate => {
                            self.update_all_propensities_time_dep()?;
                            Ok(StepResult::OnlyTimeDependendUpdate)
                        }
                        SpecialTimeType::Endtime => {
                            //self.make_observations();
                            Ok(StepResult::Completed)
                        }
                    };
                }
            }
        }

        /*match time_dependent_update_rate {
            None => {}
            Some(h) => {
                if !self.time_dependend_reactions.is_empty() {
                    if self.time + timestep > self.last_time_dependend_update + h {
                        assert_gt!(h, 0.);
                        //assert_ge!(self.last_time_dependend_update + h, self.time);
                        self.time = (self.last_time_dependend_update + h).max(self.time);
                        self.update_all_propensities_time_dep();
                        assert_eq!(self.time, self.last_time_dependend_update);
                    }
                }
            }
        }
        if self.time + timestep > self.last_io + io_step_size {
            assert_le!(self.last_time_dependend_update, self.time);
            while self.last_io < self.time {
                self.last_io += io_step_size;
                self.make_observations();
            }
            assert_le!(self.time, self.last_io);
            self.time = self.last_io;
            self.update_all_propensities_time_dep();
            return StepResult::OnlyObservation;
        } else if self.time == 0. {
            self.update_all_propensities_time_dep();
            self.make_observations();
        }*/
        self.time += timestep;
        assert!(self.propensitiy_tree[0] > 0.);

        let idx_of_reaction = match self.get_next_reaction_idx() {
            None => return Ok(StepResult::NoReactionPossible),
            Some(y) => y,
        };

        if print_reaction_fired {
            println!(
                " * Fire reaction: {} *\n  **propensity was = {}, propensity sum = {}",
                self.reaction_names[self.reactions[idx_of_reaction].reaction_number_of_prototype],
                self.propensities[idx_of_reaction],
                self.propensitiy_tree[0],
            );
        }
        self.reaction_idx_sum += idx_of_reaction;
        self.metrics.regular_steps += 1;

        match self.fire_reaction(idx_of_reaction)? {
            FireResult::Good => {
                //self.check_all_propensities();
                //self.check_tree();
                Ok(StepResult::AllGood)
            }
            FireResult::InvokeStructureChange => {
                let mut variation = self.choose_fireing_match(idx_of_reaction);
                match &mut variation {
                    None => {}
                    Some(multi) => {
                        for (s, idx) in self.reactions[idx_of_reaction]
                            .species_in_names
                            .iter()
                            .zip(self.reactions[idx_of_reaction].species_in_idx.iter())
                        {
                            match self.species_counts[*idx] {
                                SpeciesAmt::NumOnly(_) => {
                                    assert!(!multi.what.contains(s));
                                    multi.what.push(s.clone());
                                    multi.results[0].matches_to_get_to_value.push(
                                        MultiResultAssociation {
                                            species_idx: *idx,
                                            variation_idx: 0,
                                        },
                                    );
                                }
                                #[cfg(feature = "hybrid_att")]
                                SpeciesAmt::NotExpanded(_) => {}
                                SpeciesAmt::CompartmentWithStar(_, _) => {} // could be an issue
                            }
                        }
                        assert_eq!(
                            multi.what.len(),
                            self.reactions[idx_of_reaction].species_in_idx.len()
                        );
                    }
                }
                Ok(StepResult::StructuralChange {
                    idx_of_reaction,
                    variation,
                })
            }
        }
    }
    pub fn variation_to_lookup(
        &self,
        variation: Option<MultiResults>,
        reactions: &Vec<EnumeratedReaction>,
        index_of_reaction: usize,
    ) -> HashMap<SpeciesLeftIdentifier, (EnumeratedSpecies, usize)> {
        let result: HashMap<_, _> = match variation.clone() {
            None => reactions[index_of_reaction]
                .species_in
                .iter()
                .map(|(a, b)| (a.clone(), (b.clone(), 0)))
                .collect(),
            Some(multi) => {
                let mut reverse_lookup: Vec<&EnumeratedSpecies> =
                    vec![self.species_lookup.iter().next().unwrap().0; self.species_lookup.len()];
                for (key, val) in self.species_lookup.iter() {
                    reverse_lookup[*val] = key;
                }
                for (n, k) in reverse_lookup.iter().enumerate() {
                    assert_eq!(self.species_lookup[k], n);
                }
                assert_eq!(multi.results.len(), 1, "should be single result");
                assert_eq!(
                    multi.what.len(),
                    reactions[index_of_reaction].species_in.len(),
                    "Error for {}\nwhat:{:?}\nrate:{:?}",
                    self.reaction_names[reactions[index_of_reaction].reaction_number_of_prototype],
                    multi.what,
                    self.reactions[index_of_reaction].rate
                );
                multi
                    .what
                    .into_iter()
                    .zip(multi.results[0].matches_to_get_to_value.iter())
                    .map(|(a, b)| (a, (reverse_lookup[b.species_idx].clone(), b.variation_idx)))
                    .collect()
            }
        };
        for (l, _) in reactions[index_of_reaction].species_in.iter() {
            debug_assert!(
                result.contains_key(l),
                "Problem: for '{}'\nmissing: {:?}\nLeft: {:?}\nmatch:{:?}\nrate:{:?}",
                self.reaction_names[reactions[index_of_reaction].reaction_number_of_prototype],
                l,
                reactions[index_of_reaction].species_in,
                variation,
                reactions[index_of_reaction].rate
            );
        }
        assert_eq!(result.len(), reactions[index_of_reaction].species_in.len());
        if cfg!(debug_assertions) {
            let mut species_we_need_for_reaction: HashMap<EnumeratedSpecies, Vec<_>> =
                HashMap::new();
            for (a, (b, idx)) in result.iter() {
                match species_we_need_for_reaction.insert(b.clone(), vec![(a.clone(), idx)]) {
                    None => {}
                    Some(y) => {
                        //println!("{:?} -> {:?},\n and {:?} -> {}", b.clone(), y, a, idx);
                        assert!(b.my_id.is_none());
                        species_we_need_for_reaction.insert(
                            b.clone(),
                            y.iter()
                                .cloned()
                                .chain(iter::once((a.clone(), idx)))
                                .collect(),
                        );
                    }
                }
            }
        }
        result
    }
}

pub struct OutputOptions {
    pub json: bool,
    pub compressed_json: bool,
    pub flat_table: bool,
    pub rule_counts: bool,
    pub plot: bool,
    pub regular_csv: bool,
    pub latex: bool,
    pub debug_outputs: bool,
}
impl OutputOptions {
    fn any_traj_output(&self) -> bool {
        self.json
            || self.plot
            || self.compressed_json
            || self.flat_table
            || self.rule_counts
            || self.regular_csv
    }
    pub fn none() -> OutputOptions {
        OutputOptions {
            json: false,
            compressed_json: false,
            flat_table: false,
            rule_counts: false,
            plot: false,
            regular_csv: false,
            latex: false,
            debug_outputs: false,
        }
    }
}

enum FireResult {
    Good,
    InvokeStructureChange,
}

#[derive(Debug)]
pub enum StepResult {
    AllGood,
    NoReactionPossible,
    StructuralChange {
        idx_of_reaction: usize,
        variation: Option<MultiResults>,
    },
    OnlyTimeDependendUpdate,
    OnlyObservation,
    Completed,
}
