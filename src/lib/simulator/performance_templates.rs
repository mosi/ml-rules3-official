use crate::input::structures::{FloatOp, PrimitiveTypes};
use crate::simulator::evaluation::{IndexedCondition, IndexedRateExprTerm, LeftSideAmtGuard};
use std::fs;

use itertools::{izip, Itertools};

use crate::buildin_functions::BuildinFunction;
use crate::expansion::static_expansion::{OptimizedString, SpeciesAmt};
use crate::simulator::perf_templates_generated::TEMPLATE_FILE;
use crate::simulator::simulate_fast::SimulationFast;
use std::sync::Mutex;

static WRITE_MUTEX: Mutex<i32> = Mutex::new(0);

#[derive(Debug)]
pub struct TemplateFail {
    _message: String,
}
impl TemplateFail {
    fn new(s: &str) -> TemplateFail {
        TemplateFail {
            _message: s.to_string(),
        }
    }
    fn new_too_long(n: usize) -> TemplateFail {
        TemplateFail {
            _message: format!("too long {n}"),
        }
    }
}
type TemplateResult<T> = std::result::Result<T, TemplateFail>;
impl SimulationFast {
    pub fn get_star_attribute_val(&self, species_idx: usize, att_idx: usize) -> f64 {
        match &self.species_counts[species_idx] {
            SpeciesAmt::NumOnly(_) => unreachable!(),
            #[cfg(feature = "hybrid_att")]
            SpeciesAmt::NotExpanded(_) => {
                unreachable!()
            }
            SpeciesAmt::CompartmentWithStar(is_present, vals) => {
                debug_assert!(is_present);
                match vals.get_by_str(&OptimizedString::new(att_idx, &"".to_string())) {
                    None => {
                        unreachable!()
                    }
                    Some(k) => match k {
                        PrimitiveTypes::String(_) => {
                            unreachable!()
                        }
                        PrimitiveTypes::Boolean(_) => {
                            unreachable!()
                        }
                        PrimitiveTypes::Float(f) => *f,
                        PrimitiveTypes::Integer(k) => *k as f64,
                    },
                }
            }
        }
    }
}

struct TypeCounters {
    ints: usize,
    floats: usize,
    index_vec: usize,
}
impl TypeCounters {
    fn new() -> TypeCounters {
        TypeCounters {
            ints: 0,
            floats: 0,
            index_vec: 0,
        }
    }
    fn int(&mut self) -> usize {
        self.ints += 1;
        self.ints - 1
    }
    fn floats(&mut self) -> usize {
        self.floats += 1;
        self.floats - 1
    }
    fn index_vec(&mut self) -> usize {
        self.index_vec += 1;
        self.index_vec - 1
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum CodeReturnVal {
    Float,
    PositiveInt,
    UnsignedInt,
    Boolean,
}
impl CodeReturnVal {
    fn zero(&self) -> String {
        match self {
            CodeReturnVal::Float => {
                format!("0.0")
            }
            CodeReturnVal::PositiveInt => {
                format!("0")
            }
            CodeReturnVal::UnsignedInt => {
                format!("0")
            }
            CodeReturnVal::Boolean => {
                unreachable!()
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct OptimizedCodeFinder {
    pub int_vals: Vec<usize>,
    pub float_vals: Vec<f64>,
    pub index_vector: Vec<Vec<usize>>,
    pub inner_expr: Vec<IndexedRateExprTerm>,
    pub expression_str: String,
    pub return_type: CodeReturnVal,
}
impl OptimizedCodeFinder {
    fn _validate(&self, int_count: usize) {
        if self.int_vals.len() != int_count {
            panic!(
                "{} != {} -> {}",
                self.int_vals.len(),
                int_count,
                self.expression_str
            );
        }
    }
    pub fn to_float(mut self) -> OptimizedCodeFinder {
        match self.return_type {
            CodeReturnVal::Float => self,
            CodeReturnVal::PositiveInt => {
                self.return_type = CodeReturnVal::Float;
                self.expression_str = format!("({}) as f64", self.expression_str);
                self
            }
            CodeReturnVal::UnsignedInt => {
                self.return_type = CodeReturnVal::Float;
                self.expression_str = format!("({}) as f64", self.expression_str);
                self
            }
            CodeReturnVal::Boolean => {
                unreachable!()
            }
        }
    }
    pub fn expression_hash(&self) -> String {
        /*use sha3::{Digest, Sha3_256};
        let mut s = Sha3_256::new(); //std::collections::hash_map::DefaultHasher::new();
        s.update(&self.expression_str);
        // self.expression_str.hash(&mut s);
        let result = s.finalize();
        //s.finish()*/
        //use base64::{engine::general_purpose, Engine as _};
        use blake2::{Blake2b512, Digest};
        let mut hasher = Blake2b512::new();
        hasher.update(&self.expression_str);
        let res = hasher.finalize();
        format!("{:x}", res) // general_purpose::STANDARD_NO_PAD.encode(res))
    }
    pub fn gen_type(&self) -> String {
        format!(
            "Opt{}{{ {}}},",
            self.expression_hash(),
            (0..self.int_vals.len())
                .map(|k| format!("int_{k}g : usize"))
                .chain((0..self.float_vals.len()).map(|k| format!("float_{k}g : f64")))
                .chain((0..self.index_vector.len()).map(|k| format!("i_vec_{k}g : Vec<usize>")))
                .join(",")
        )
    }
    pub fn gen_calc(&self) -> String {
        format!(
            "RateOptimization::Opt{}{{ {}}} => Some({}),",
            self.expression_hash(),
            (0..self.int_vals.len())
                .map(|k| format!("int_{k}g"))
                .chain((0..self.float_vals.len()).map(|k| format!("float_{k}g")))
                .chain((0..self.index_vector.len()).map(|k| format!("i_vec_{k}g")))
                .join(","),
            self.expression_str
        )
    }
    pub fn gen_ret(&self) -> String {
        format!(
            "\"{}\" => return RateOptimization::Opt{}{{ {} }},",
            self.expression_hash(),
            self.expression_hash(),
            (0..self.int_vals.len())
                .map(|k| format!("int_{k}g : opti.int_vals[{k}]"))
                .chain(
                    (0..self.float_vals.len())
                        .map(|k| format!("float_{k}g : opti.float_vals[{k}]"))
                )
                .chain(
                    (0..self.index_vector.len())
                        .map(|k| format!("i_vec_{k}g : opti.index_vector[{k}].clone()"))
                )
                .join(",")
        )
    }
    /*fn shift_by(&self, other: &OptimizedCodeFinder) -> String {
        let mut s = self.expression_str.clone();
        //println!("s : {s}");
        for i in (0..self.int_vals.len()).rev() {
            s = s.replace(
                &*format!("int_{i}g"),
                &*format!("int_{}g", i + other.int_vals.len()),
            );
        }
        for i in (0..self.float_vals.len()).rev() {
            s = s.replace(
                &*format!("float_{i}g"),
                &*format!("float_{}g", i + other.float_vals.len()),
            );
        }
        //println!(" -> {s}");
        s
    }*/
    fn from_specific_count_from_global(
        c: &Vec<usize>,
        counts: &mut TypeCounters,
    ) -> OptimizedCodeFinder {
        if c.is_empty() {
            return OptimizedCodeFinder {
                int_vals: vec![],
                float_vals: vec![],
                index_vector: vec![],
                inner_expr: vec![],
                expression_str: "0".to_string(),
                return_type: CodeReturnVal::PositiveInt,
            };
        }
        if c.len() < 4 {
            let r = OptimizedCodeFinder {
                int_vals: c.clone(),
                float_vals: vec![],
                index_vector: vec![],
                inner_expr: vec![],
                expression_str: (0..c.len())
                    .map(|_| format!("self.species_counts[*int_{}g].total_amt()", counts.int()))
                    .join("+"),
                return_type: CodeReturnVal::PositiveInt,
            };
            //r.validate(int_count);
            r
        } else {
            let r = OptimizedCodeFinder {
                int_vals: vec![],
                float_vals: vec![],
                index_vector: vec![c.clone()],
                inner_expr: vec![],
                expression_str: format!(
                    "i_vec_{}g.iter().map(|k|self.species_counts[*k].total_amt()).sum::<usize>()",
                    counts.index_vec()
                ),
                return_type: CodeReturnVal::PositiveInt,
            };
            //r.validate(int_count);
            r
        }
    }
    fn append_rev(mut self, other: OptimizedCodeFinder) -> OptimizedCodeFinder {
        self.int_vals = other
            .int_vals
            .into_iter()
            .chain(self.int_vals.into_iter())
            .collect();
        self.index_vector = other
            .index_vector
            .into_iter()
            .chain(self.index_vector.into_iter())
            .collect();
        self.float_vals = other
            .float_vals
            .into_iter()
            .chain(self.float_vals.into_iter())
            .collect();
        self
    }
    fn append(mut self, mut other: OptimizedCodeFinder) -> OptimizedCodeFinder {
        self.int_vals.append(&mut other.int_vals);
        self.float_vals.append(&mut other.float_vals);
        self.index_vector.append(&mut other.index_vector);
        self
    }
    fn leftsideamtguard(
        lsag: &LeftSideAmtGuard,
        all_species: &Vec<SpeciesAmt>,
        counts: &mut TypeCounters,
    ) -> TemplateResult<OptimizedCodeFinder> {
        if lsag.has_starr_attribut_amounts {
            return Err(TemplateFail::new(
                "Starr attribute amounts in left side guard",
            ));
        }

        let mut inner =
            OptimizedCodeFinder::new_term(lsag.inner.as_ref(), all_species, counts)?.to_float();
        //inner.validate(int_count);

        if lsag.is_mass_action {
            inner = inner.to_float();
        }
        let mut r = OptimizedCodeFinder {
            int_vals: lsag.idices.clone(),
            float_vals: vec![],
            index_vector: vec![],
            inner_expr: vec![],
            expression_str: "".to_string(),
            return_type: inner.return_type.clone(),
        };
        //println!("Sbef {}", inner.expression_str);
        //let mut s = inner.shift_by(&r);
        let mut s = inner.expression_str.clone();
        //println!("Saft {s}");
        let s_amts: Vec<_> = lsag
            .idices
            .iter()
            .map(|_k| format!("self.species_counts[*int_{}g].total_amt()", counts.int()))
            .collect();
        if lsag.is_mass_action {
            for (amt_compute_string, amt) in s_amts.iter().zip(lsag.idices_replication_num.iter()) {
                assert!(*amt > 0);
                if *amt == 1 {
                    s = format!("{s} * {amt_compute_string} as f64",)
                } else {
                    s = format!("{s} * ({amt_compute_string} - {} ) as f64", amt - 1)
                }
            }
        }
        r = r.append_rev(inner.clone());
        for (amt_string, amt_needed) in /*lsag
            .idices*/
            s_amts.iter().zip(lsag.idices_replication_num.iter())
        {
            s = format!(
                "(if {amt_string} < {amt_needed} {{ {} }} else {{ {s} }})",
                inner.return_type.zero()
            );
        }
        r.expression_str = s;
        //r.validate(int_count);
        Ok(r)
    }
    fn from_buildin(
        f: &BuildinFunction,
        _params: &Vec<IndexedRateExprTerm>,
    ) -> TemplateResult<OptimizedCodeFinder> {
        match f {
            BuildinFunction::Pi => Ok(OptimizedCodeFinder {
                int_vals: vec![],
                float_vals: vec![],
                index_vector: vec![],
                inner_expr: vec![],
                expression_str: "std::f64::consts::PI".to_string(),
                return_type: CodeReturnVal::Float,
            }),
            BuildinFunction::Euler => Ok(OptimizedCodeFinder {
                int_vals: vec![],
                float_vals: vec![],
                index_vector: vec![],
                inner_expr: vec![],
                expression_str: "std::f64::consts::E".to_string(),
                return_type: CodeReturnVal::Float,
            }),
            BuildinFunction::Time => Ok(OptimizedCodeFinder {
                int_vals: vec![],
                float_vals: vec![],
                index_vector: vec![],
                inner_expr: vec![],
                expression_str: "self.last_time_dependend_update".to_string(),
                return_type: CodeReturnVal::Float,
            }),
            BuildinFunction::Sin
            | BuildinFunction::Cos
            | BuildinFunction::Exp
            | BuildinFunction::Abs
            | BuildinFunction::AbsOrZero
            | BuildinFunction::Round
            | BuildinFunction::Ceil
            | BuildinFunction::Floor
            | BuildinFunction::RandomGauss
            | BuildinFunction::RandomUniform
            | BuildinFunction::RandomPoisson => {
                Err(TemplateFail::new(&format!("Buildin {:?} not supported", f)))
            }
        }
    }
    fn new_term(
        t: &IndexedRateExprTerm,
        all_species: &Vec<SpeciesAmt>,
        counts: &mut TypeCounters,
    ) -> TemplateResult<OptimizedCodeFinder> {
        let returno = match t {
            IndexedRateExprTerm::Primitive(p) => match p {
                PrimitiveTypes::String(s) => Ok(OptimizedCodeFinder {
                    int_vals: vec![s.name],
                    float_vals: vec![],
                    index_vector: vec![],
                    inner_expr: vec![],
                    expression_str: format!("*int_{}g", { counts.int() }),
                    return_type: CodeReturnVal::PositiveInt,
                }),
                PrimitiveTypes::Boolean(_b) => Err(TemplateFail::new("Boolean")),
                PrimitiveTypes::Float(l) => {
                    if *l == 1.0 {
                        return Ok(OptimizedCodeFinder {
                            int_vals: vec![],
                            float_vals: vec![],
                            index_vector: vec![],
                            inner_expr: vec![],
                            expression_str: "1.0".to_string(),
                            return_type: CodeReturnVal::Float,
                        });
                    }
                    Ok(OptimizedCodeFinder {
                        int_vals: vec![],
                        float_vals: vec![*l],
                        index_vector: vec![],
                        inner_expr: vec![],
                        expression_str: format!("*float_{}g", { counts.floats() }),
                        return_type: CodeReturnVal::Float,
                    })
                }
                PrimitiveTypes::Integer(l) => {
                    if *l == 1 {
                        return Ok(OptimizedCodeFinder {
                            int_vals: vec![],
                            float_vals: vec![],
                            index_vector: vec![],
                            inner_expr: vec![],
                            expression_str: "1".to_string(),
                            return_type: CodeReturnVal::PositiveInt,
                        });
                    }
                    if *l == 2 {
                        return Ok(OptimizedCodeFinder {
                            int_vals: vec![],
                            float_vals: vec![],
                            index_vector: vec![],
                            inner_expr: vec![],
                            expression_str: "2".to_string(),
                            return_type: CodeReturnVal::PositiveInt,
                        });
                    }
                    if *l <= 0 {
                        Ok(OptimizedCodeFinder {
                            int_vals: vec![(-*l) as usize],
                            float_vals: vec![],
                            index_vector: vec![],
                            inner_expr: vec![],
                            expression_str: format!("(-(*int_{}g as isize))", { counts.int() }),
                            return_type: CodeReturnVal::UnsignedInt,
                        })
                    } else {
                        Ok(OptimizedCodeFinder {
                            int_vals: vec![(*l) as usize],
                            float_vals: vec![],
                            index_vector: vec![],
                            inner_expr: vec![],
                            expression_str: format!("*int_{}g", { counts.int() }),
                            return_type: CodeReturnVal::PositiveInt,
                        })
                    }
                }
            },
            IndexedRateExprTerm::LeftSideAmtGuard(lsag) => {
                OptimizedCodeFinder::leftsideamtguard(lsag, all_species, counts)
            }

            IndexedRateExprTerm::Expr(x) => {
                let (l, op, r) = x.as_ref();
                OptimizedCodeFinder::new_expr(l, op, r, all_species, counts)
            }
            IndexedRateExprTerm::Condition(k) => {
                OptimizedCodeFinder::from_condiotion(k.as_ref(), all_species, counts)
            }
            IndexedRateExprTerm::CountFromLeftCpx(_, _) => {
                Err(TemplateFail::new("Complex count from left"))
            } //todo!("CPX Count"),
            IndexedRateExprTerm::CountGlobally(_) => Err(TemplateFail::new("Count globally")), //todo!(),
            IndexedRateExprTerm::SpecificCountFromGlobal(k) => Ok(
                OptimizedCodeFinder::from_specific_count_from_global(k, counts),
            ),
            IndexedRateExprTerm::CountFromLeftSimple(k) => Ok(OptimizedCodeFinder {
                int_vals: vec![*k],
                float_vals: vec![],
                index_vector: vec![],
                inner_expr: vec![],
                expression_str: format!("self.species_counts[*int_{}g].total_amt()", {
                    counts.int()
                }), //"".to_string(),
                return_type: CodeReturnVal::PositiveInt,
            }),
            IndexedRateExprTerm::StarAttRefReg(_) => Err(TemplateFail::new("StarAttRefReg")),
            IndexedRateExprTerm::StarAttRefComp(comp) => match &all_species[comp.idx_of_species] {
                SpeciesAmt::NumOnly(_) => {
                    unreachable!()
                }
                SpeciesAmt::CompartmentWithStar(_is_present, vals) => {
                    //panic!();
                    match vals.get_by_str(&comp.name) {
                        None => {
                            panic!(
                                "not found in {:?} in {:?}\n{:?}",
                                comp.name, vals, all_species
                            )
                        }
                        Some(v) => match v {
                            PrimitiveTypes::String(_) => {
                                Err(TemplateFail::new("Comp w star string"))
                            }
                            PrimitiveTypes::Boolean(_b) => {
                                Err(TemplateFail::new("Comp w star bool"))
                            }
                            PrimitiveTypes::Float(_) | PrimitiveTypes::Integer(_) => {
                                //panic!();
                                Ok(OptimizedCodeFinder {
                                    int_vals: vec![comp.idx_of_species, comp.name.name],
                                    float_vals: vec![],
                                    index_vector: vec![],
                                    inner_expr: vec![],
                                    expression_str: format!(
                                        "self.get_star_attribute_val(*int_{}g,*int_{}g)",
                                        counts.int(),
                                        counts.int()
                                    ),
                                    return_type: CodeReturnVal::Float,
                                })
                            }
                        },
                    }
                }
                #[cfg(feature = "hybrid_att")]
                SpeciesAmt::NotExpanded(_) => {
                    unreachable!()
                }
            },
            /*Ok(OptimizedCodeFinder {
                int_vals: vec![],
                float_vals: vec![],
                expression_str: "self.last_time_dependend_update".to_string(),
                return_type: CodeReturnVal::Float,
            }),*/
            IndexedRateExprTerm::BuildinFunction(f, params) => {
                OptimizedCodeFinder::from_buildin(f, params)
            }
            IndexedRateExprTerm::SpecifCountFromGlobalConstraint(_) => {
                Err(TemplateFail::new("Specific count from global constraint"))
            }
        };
        returno
        /*match returno {
            None => {
                panic!("{:?}", t);
            }
            Some(k) => Some(k),
        }*/
    }
    fn from_condiotion(
        cond: &IndexedCondition,
        all_species: &Vec<SpeciesAmt>,
        counts: &mut TypeCounters,
    ) -> TemplateResult<OptimizedCodeFinder> {
        let mut guard = OptimizedCodeFinder::new_term(&cond.guard, all_species, counts)?;
        let mut result_true =
            OptimizedCodeFinder::new_term(&cond.result_true, all_species, counts)?;
        let mut else_branch = match &cond.result_false {
            None => OptimizedCodeFinder {
                int_vals: vec![],
                float_vals: vec![],
                index_vector: vec![],
                inner_expr: vec![],
                expression_str: result_true.return_type.zero(),
                return_type: result_true.return_type,
            },
            Some(k) => OptimizedCodeFinder::new_term(&k, all_species, counts)?,
        };
        if result_true.return_type != else_branch.return_type {
            result_true = result_true.to_float();
            else_branch = else_branch.to_float();
        }
        guard.return_type = result_true.return_type;

        //result_true.expression_str = result_true.shift_by(&guard);
        //else_branch.expression_str = else_branch.shift_by(&guard);
        //else_branch.expression_str = else_branch.shift_by(&result_true);
        guard.expression_str = format!(
            "(if {} {{ {} }} else {{ {} }})",
            guard.expression_str, result_true.expression_str, else_branch.expression_str
        );
        guard = guard.append(result_true).append(else_branch);
        Ok(guard)
    }

    pub fn gen_new(
        rate: &IndexedRateExprTerm,
        all_species: &Vec<SpeciesAmt>,
    ) -> TemplateResult<OptimizedCodeFinder> {
        let mut counter = TypeCounters::new();
        match OptimizedCodeFinder::new_term(rate, all_species, &mut counter) {
            Ok(x) => {
                assert_eq!(x.int_vals.len(), counter.ints, "{:?}", rate);
                assert_eq!(x.float_vals.len(), counter.floats);
                assert_eq!(x.index_vector.len(), counter.index_vec);
                Ok(x)
            }
            Err(x) => Err(x),
        }
    }
    fn new_expr(
        left: &IndexedRateExprTerm,
        op: &FloatOp,
        right: &IndexedRateExprTerm,
        all_species: &Vec<SpeciesAmt>,
        counts: &mut TypeCounters,
    ) -> TemplateResult<OptimizedCodeFinder> {
        let result = {
            let l = OptimizedCodeFinder::new_term(left, all_species, counts)?;
            let mut r = OptimizedCodeFinder::new_term(right, all_species, counts)?;
            // r.expression_str = r.shift_by(&l);
            let mut l = l.append(r.clone());
            l.expression_str = match op {
                FloatOp::Pow => match r.return_type {
                    CodeReturnVal::Float => {
                        l = l.to_float();
                        r = r.to_float();
                        format!("({}).powf({})", l.expression_str, r.expression_str)
                    }
                    CodeReturnVal::PositiveInt => {
                        l = l.to_float();
                        //r = r.to_float();
                        format!("({}).powi(i32::try_from({}).expect(\"value is to large to be used in exponential!\"))", l.expression_str, r.expression_str)
                    }
                    CodeReturnVal::UnsignedInt => {
                        l = l.to_float();
                        format!("({}).powi({})", l.expression_str, r.expression_str)
                    }
                    CodeReturnVal::Boolean => {
                        unreachable!()
                    }
                },
                FloatOp::Sub => {
                    if l.return_type == CodeReturnVal::PositiveInt
                        && (r.return_type == CodeReturnVal::PositiveInt
                            || r.return_type == CodeReturnVal::UnsignedInt)
                    {
                        // allow negative values to be created
                        l.return_type = CodeReturnVal::UnsignedInt;
                        format!(
                            "(({}) as isize {} ({}) as isize)",
                            l.expression_str, op, r.expression_str
                        )
                    } else {
                        l = l.to_float();
                        r = r.to_float();
                        format!("(({}) {} ({})) ", l.expression_str, op, r.expression_str)
                    }
                }
                _ => {
                    if (l.return_type == r.return_type) && *op != FloatOp::Div {
                        format!("(({}) {} ({}))", l.expression_str, op, r.expression_str)
                    } else {
                        l = l.to_float();
                        r = r.to_float();
                        format!("(({}) {} ({})) ", l.expression_str, op, r.expression_str)
                    }
                }
            };
            match op {
                FloatOp::Add => {}
                FloatOp::Sub => {}
                FloatOp::Div => {}
                FloatOp::Mul => {}
                FloatOp::Pow => {}
                FloatOp::Comparison(_) => l.return_type = CodeReturnVal::Boolean,
            }
            l
        };

        //result.validate(*int_count - int_count_in);
        if result.int_vals.len() + result.float_vals.len() > 26 {
            return Err(TemplateFail::new_too_long(
                result.int_vals.len() + result.float_vals.len(),
            ));
        }
        Ok(result)
    }
}
#[cfg(feature = "performance-templates")]
pub fn replace_in_this_file(hash: String, s_def: String, s_match: String, s_calc: String) {
    //return;
    //use file_lock::FileLock;

    let _l = WRITE_MUTEX.lock().unwrap();

    /*let mut lock = FileLock::lock(
        code_gen_file(),
        true,
        file_lock::FileOptions::new()
            .read(true)
            .write(true)
            //.append(false)
            .create(true),
    )
    .unwrap();*/
    //let mut contents = String::new();
    //fs::read_to_string(&mut contents).unwrap();
    let mut contents = fs::read_to_string(TEMPLATE_FILE).unwrap();
    if contents.contains(&*hash) {
        return;
    }

    //lock.file.seek(SeekFrom::Start(0)).unwrap();

    for (key, to) in izip!(["DEF", "MATCH", "CALC"], [s_def, s_match, s_calc]) {
        let key = format!("/* PLACE TO ADD STRUCT {key} AUTO */");
        contents = contents.replace(&*key.clone(), &*(key + "\n" + &*to));
    }

    //let mut f = File::create(file!()).unwrap();
    fs::write(TEMPLATE_FILE, contents).unwrap();
    //lock.file.write(contents.as_bytes()).unwrap();
}
