use crate::flatten::FlatModel;
use rand::prelude::*;

struct SimulateSlow {}

impl SimulateSlow {
    pub fn new(m: &FlatModel) -> SimulateSlow {
        todo!()
    }
    pub fn step(&mut self, io_step_size: f64, print_reaction_fired: bool) -> StepResult {
        todo!()
    }
}

pub enum StepResult {
    AllGood,
    NoReactionPossible,
    StructuralChange { idx_of_reaction: usize },
    OnlyObservation,
}
