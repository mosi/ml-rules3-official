extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate core;

mod buildin_functions;
pub mod expansion;
pub mod input;
mod output;
pub mod running;
pub mod running_web;
pub mod simulate_fast_code_gen;
pub mod simulator;
#[cfg(test)]
mod test_suite;
mod tests;

#[cfg(test)]
mod simulator_tests {
    use crate::running::{
        test_different_initial, test_output_initial_propensities, test_output_state, test_state_eq,
        TestUntil,
    };
    #[cfg(feature = "hybrid_att")]
    use more_asserts::assert_ge;
    use more_asserts::assert_gt;

    #[cfg(feature = "hybrid_att")]
    use more_asserts::assert_lt;

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_rate() {
        let output = test_output_initial_propensities("testing/a2b_rates.mlr");
        println!("Output: {}", output);
        output.assert_propensity(0, 15.);
        output.assert_propensity(1, 0.);
        output.assert_propensity(2, 10. + 12.);
        output.assert_propensity(3, 10.);
        output.assert_propensity(4, 10.);
        output.assert_propensity(5, 2440.);
        output.assert_propensity(6, 2440.);
    }
    #[test]
    fn a2b_non_lin() {
        let output = test_output_state("testing/non_linear_matching.mlr", TestUntil::Time(1000.));
        println!("Output: {}", output);
        assert_eq!(output.count_species("A"), 0);
        assert_eq!(output.count_species("B"), 100);
        assert_eq!(output.count_species("C"), 0);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_rate_manually() {
        let output = test_output_initial_propensities("testing/a2b_rates_man.mlr");
        println!("Output: {}", output);
        output.assert_propensity(0, 15.);
        output.assert_propensity(1, 0.);
        output.assert_propensity(2, 10. + 12.);
        output.assert_propensity(3, 10.);
        output.assert_propensity(4, 10.);
        output.assert_propensity(5, 2440.);
        output.assert_propensity(6, 2440.);
    }

    #[test]
    #[ignore]
    fn a2b_rate_second() {
        let output = test_output_initial_propensities("testing/a2b_rates_second_order.mlr");
        println!("Output: {}", output);
        output.assert_propensity(0, 15. * 25.);
        output.assert_propensity(1, 25. * 24.);
        output.assert_propensity(2, 10. * 12.);
        output.assert_propensity(3, 10. * 9.);
        output.assert_propensity(4, 10. * 12.);
        output.assert_propensity(5, 10. * 9.);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_rate_second_man() {
        let output = test_output_initial_propensities("testing/a2b_rates_second_order_man.mlr");
        println!("Output: {}", output);
        output.assert_propensity(0, 15. * 25.);
        output.assert_propensity(1, 25. * 24.);
        output.assert_propensity(2, 10. * 12.);
        output.assert_propensity(3, 10. * 9.);
        output.assert_propensity(4, 10. * 12.);
        output.assert_propensity(5, 10. * 9.);
    }

    #[test]
    fn a2b_initial() {
        test_state_eq("testing/a2b.mlr", "1000 A", TestUntil::Initial);
        let output = test_output_state("testing/a2b.mlr", TestUntil::Initial);
        println!("Output: {}", output);
        assert_eq!(output.count_species("A"), 1000);
        assert_eq!(output.count_species("B"), 0);
        //assert_eq!(output["B"],0);
    }

    #[test]
    fn a2b_final() {
        let output = test_output_state("testing/a2b.mlr", TestUntil::Time(10.));
        assert_eq!(output.count_species("A"), 0);
        assert_eq!(output.count_species("B"), 1000);
        assert!(output.species_eq(&test_different_initial("testing/a2b.mlr", "1000 B")));
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_multi_attribute_final() {
        let output = test_output_state("testing/a2b_multi_attributes.mlr", TestUntil::Time(10.));
        assert_eq!(output.count_species("A"), 0);
        assert_eq!(output.count_species("B"), 1000);
    }

    #[test]
    #[should_panic]
    fn a2b_variable_amt() {
        let output = test_output_state("testing/a2b_variable_amt.mlr", TestUntil::Time(100.));
        assert_eq!(output.count_species("A"), 0);
        assert_eq!(output.count_species("B"), 4 * 500);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_multi_attribute_final_assign() {
        let output = test_output_state(
            "testing/a2b_multi_attributes_w_assignments.mlr",
            TestUntil::Time(10.),
        );
        assert_eq!(output.count_species("A"), 0);
        assert_eq!(output.count_species("B"), 1000);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_multi_attribute_final_assign2() {
        let output = test_output_state(
            "testing/a2b_multi_attributes_w_assignments2.mlr",
            TestUntil::Time(10.),
        );
        assert_eq!(output.count_species("A"), 0);
        assert_eq!(output.count_species("B"), 500);
    }

    #[test]
    fn a2b_enum() {
        let output = test_output_state("testing/a2b_enum.mlr", TestUntil::Step(20));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("B"), 10);
        //assert_eq!(output["B"],0);
    }

    #[test]
    fn a2b_cpx_initial() {
        let output = test_output_state("testing/a2b_cpx.mlr", TestUntil::Initial);
        println!("Output: {:}", output);
        assert_eq!(output.count_species("A"), 1000);
        assert_eq!(
            output.count_species_constraint("A", |k| k.attributes["k"].value_si == 100.into()),
            500
        );
        assert_eq!(output.count_species("B"), 0);
        //assert_eq!(output["B"],0);
    }

    #[test]
    fn a2b_cpx_final() {
        let output = test_output_state("testing/a2b_cpx.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("A"), 0);
        assert_eq!(
            output.count_species_constraint("A", |k| k.attributes["k"].value_si == 100.into()),
            0
        );
        assert_eq!(output.count_species("B"), 1000);
        assert_eq!(
            output.count_species_constraint("B", |k| k.attributes["v"].value_si == "test".into()),
            1000
        );
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_cpx_star_initial() {
        let output = test_output_state("testing/a2b_cpx_star.mlr", TestUntil::Initial);
        println!("Output: {:}", output);
        assert_eq!(output.count_species("A1"), 1000);
        assert_eq!(output.count_species("B1"), 1);
        assert_eq!(output.count_species("A2"), 1000);
        assert_eq!(output.count_species("B2"), 1);
        assert_eq!(output.count_species("A3"), 1000);
        assert_eq!(output.count_species("B3"), 1);
        assert_eq!(output.count_species("A4"), 1000);
        assert_eq!(output.count_species("B4"), 1);
        assert_eq!(
            output.count_species_constraint("A1", |k| k.attributes["k"].value_si == 100.into()),
            500
        );
        assert_eq!(
            output.count_species_constraint("A2", |k| k.attributes["k"].value_si == 100.into()),
            500
        );
        assert_eq!(
            output.count_species_constraint("A3", |k| k.attributes["k"].value_si == 100.into()),
            500
        );
        assert_eq!(
            output.count_species_constraint("A4", |k| k.attributes["k"].value_si == 100.into()),
            500
        );
        //assert_eq!(output["B"],0);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_cpx_star_final2() {
        let output = test_output_state("testing/a2b_cpx_star2.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("A1"), 0);
        assert_eq!(
            output.count_species("Cslow") + output.count_species("B1"),
            1000
        );
        assert!(output.count_species("B1") > 990);
        assert!(output.count_species("Cslow") < 10);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_cpx_star_final_2out() {
        let output = test_output_state("testing/a2b_cpx_star_2out.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("A1"), 0);
        assert_eq!(
            output.count_species("B1") + output.count_species("D1"),
            1002
        );
        assert_ge!(output.count_species("D1"), 10);
        assert_lt!(output.count_species("D1"), 500);

        assert_eq!(output.count_species("A2"), 0);
        assert_eq!(
            output.count_species("B2") + output.count_species("D2"),
            1002
        );
        assert_ge!(output.count_species("D2"), 10);
        assert_lt!(output.count_species("D2"), 500);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_cpx_star_final() {
        let output = test_output_state("testing/a2b_cpx_star_no_init.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("A1"), 0);
        assert_eq!(output.count_species("B1"), 1000);
        assert_eq!(output.count_species("A2"), 0);
        assert_eq!(output.count_species("B2"), 1000);
        assert_eq!(output.count_species("A3"), 0);
        assert_eq!(output.count_species("B3"), 1000);
        assert_eq!(output.count_species("A4"), 0);
        assert_eq!(output.count_species("B4"), 1000);
    }

    #[test]
    #[cfg(feature = "hybrid_att")]
    fn a2b_cpx_star_final_init() {
        let output = test_output_state("testing/a2b_cpx_star.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("A1"), 0);
        assert_eq!(output.count_species("B1"), 1001);
        assert_eq!(output.count_species("A2"), 0);
        assert_eq!(output.count_species("B2"), 1001);
        assert_eq!(output.count_species("A3"), 0);
        assert_eq!(output.count_species("B3"), 1001);
        assert_eq!(output.count_species("A4"), 0);
        assert_eq!(output.count_species("B4"), 1001);
    }

    #[test]
    fn comp_a2b() {
        let output = test_output_state("testing/comp_a2b.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("Comp"), 1);
        assert_eq!(output.count_species("B"), 1000);
    }

    #[test]
    fn comp_a2b_dyn() {
        let output = test_output_state("testing/comp_a2b_dyn.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("Comp"), 1);
        assert_eq!(output.count_species("Comp2"), 101);
        assert_eq!(output.count_species("B"), 100);
    }

    #[test]
    fn comp_a2b_dyn_att() {
        let output = test_output_state("testing/comp_a2b_dyn_att.mlr", TestUntil::Time(10.));
        println!("Output: {:}", output);
        assert_eq!(output.count_species("Comp"), 1);
        assert_eq!(output.count_species("Comp2"), 11);
        assert_eq!(output.count_species("B"), 10);
        assert_eq!(
            output.count_species_constraint("Comp", |k| k.attributes["c"].value_si == 10.into()),
            1
        );
    }

    #[test]
    fn dez_err1() {
        let output = test_output_state(
            "testing/error_cases_dez_22/error_1.mlr",
            TestUntil::NoMoreReaction,
        );
        println!("Output: {:}", output);
        assert_gt!(output.count_species("P"), 1);
        //assert_lt!(output.count_species("P"), 39);
        assert_eq!(
            output.count_species_constraint("L", |k| k.attributes["ext"].value_si == 0.into()),
            output.count_species("P")
        );
        assert_eq!(output.count_species("L"), 40);

        let output = test_output_state(
            "testing/error_cases_dez_22/error_1.mlr",
            TestUntil::NoMoreReaction,
        );
        println!("Output: {:}", output);
        assert_eq!(output.count_species("P"), 40);
        assert_eq!(
            output.count_species_constraint("L", |k| k.attributes["ext"].value_si == 0.into()),
            output.count_species("P")
        );
        assert_eq!(output.count_species("L"), 40);
    }

    #[test]
    fn split_err() {
        let output = test_output_state(
            "testing/error_cases_dez_22/split_error.mlr",
            TestUntil::NoMoreReaction,
        );
        assert_eq!(output.count_species("Drp1"), 60);

        let output = test_output_state(
            "testing/error_cases_dez_22/split_error.mlr",
            TestUntil::Step(1),
        );
        assert_eq!(output.count_species("Drp1"), 60);
    }

    #[test]
    fn split_err2() {
        let _output = test_output_state(
            "testing/error_cases_dez_22/split_error2.mlr",
            TestUntil::NoMoreReaction,
        );
        //assert_eq!(output.count_species("Drp1"), 60);

        let output = test_output_state(
            "testing/error_cases_dez_22/split_error2.mlr",
            TestUntil::Step(1),
        );
        assert_eq!(output.count_species("Drp1"), 61);
    }

    #[test]
    fn dez_err1_state() {
        test_state_eq(
            "testing/error_cases_dez_22/error_1.mlr",
            "System{38 L(ext = 1) + P(inside = 0){L(ext = 0)} + Cell + P(inside = 0){L(ext = 0)}}",
            TestUntil::Step(2),
        );
    }

    #[test]
    fn dez_err2() {
        let output = test_output_state(
            "testing/error_cases_dez_22/error_2.mlr",
            TestUntil::NoMoreReaction,
        );
        println!("Output: {:}", output);
    }

    #[test]
    fn dez_err2_state() {
        test_state_eq(
            "testing/error_cases_dez_22/error_2.mlr",
            "Cell{P(inside = 1){L(ext = 0)}} + 39 L(ext = 1)",
            TestUntil::Step(2),
        );
    }

    #[test]
    fn dez_err3() {
        let output = test_output_state(
            "testing/error_cases_dez_22/error_3.mlr",
            TestUntil::Time(6000.),
        );
        println!("Output: {:}", output);
    }

    #[test]
    fn dez_err3b() {
        let output = test_output_state(
            "testing/error_cases_dez_22/error_3b.mlr",
            TestUntil::NoMoreReaction,
        );
        println!("Output: {:}", output);
    }

    #[test]
    fn dez_err3b_time() {
        let output = test_output_state(
            "testing/error_cases_dez_22/error_3b.mlr",
            TestUntil::Time(6000.),
        );
        println!("Output: {:}", output);
    }

    #[test]
    fn dez_err4() {
        let output = test_output_state(
            "testing/error_cases_dez_22/error_4.mlr",
            TestUntil::NoMoreReaction,
        );
        println!("Output: {:}", output);
    }
    #[test]
    fn dez_err4_2() {
        let output = test_output_state(
            "testing/error_cases_dez_22/error_4_2.mlr",
            TestUntil::NoMoreReaction,
        );
        println!("Output: {:}", output);
    }

    #[test]
    fn minicod_attribute_finding() {
        let output = test_output_state(
            "testing/error_cases_dez_22/minicod.mlr",
            TestUntil::Step(100),
        );
        println!("Output: {:}", output);
    }
}
