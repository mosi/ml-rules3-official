use crate::expansion::static_expansion::OptimizedString;
use crate::input::structures::PrimitiveTypes;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::fmt;
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::io::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum OutputValue {
    String(String),
    Boolean(bool),
    Float(f64),
    Integer(i64),
}

impl From<&str> for OutputValue {
    fn from(value: &str) -> Self {
        OutputValue::String(value.to_string())
    }
}

impl From<i64> for OutputValue {
    fn from(value: i64) -> Self {
        OutputValue::Integer(value)
    }
}

impl OutputValue {
    pub fn from_prim(
        val: &PrimitiveTypes,
        string_conversion: &HashMap<String, OptimizedString>,
    ) -> OutputValue {
        match val {
            PrimitiveTypes::String(s) => {
                OutputValue::String(s.get_original_string(string_conversion))
            }
            PrimitiveTypes::Boolean(k) => OutputValue::Boolean(*k),
            PrimitiveTypes::Float(k) => OutputValue::Float(*k),
            PrimitiveTypes::Integer(k) => OutputValue::Integer(*k),
        }
    }
    pub fn as_float_no_int(&self) -> Option<f64> {
        match self {
            OutputValue::Float(f) => Some(*f),
            _ => None,
        }
    }
}

impl Display for OutputValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            OutputValue::String(s) => write!(f, "{}", s),
            OutputValue::Boolean(s) => write!(f, "{}", s),
            OutputValue::Float(s) => write!(f, "{}", s),
            OutputValue::Integer(s) => write!(f, "{}", s),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct OutputAttribute {
    pub(crate) value_si: OutputValue,
    pub(crate) unit_str_prefactor: Option<(String, f64)>,
}

impl fmt::Display for OutputAttribute {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self.unit_str_prefactor {
            None => {
                write!(f, "{}", self.value_si)
            }
            Some((s, fl_p)) => {
                write!(
                    f,
                    "{}[{}]",
                    self.value_si.as_float_no_int().unwrap() / fl_p,
                    s
                )
            }
        }
    }
}

impl PartialEq for OutputAttribute {
    fn eq(&self, _other: &Self) -> bool {
        todo!()
    }
}

impl OutputAttribute {
    fn approx_eq(&self, other: &Self) -> bool {
        match (&self.unit_str_prefactor, &other.unit_str_prefactor) {
            (None, None) => self.value_si.approx_eq(&other.value_si),
            (Some((pref_a, _)), Some((pref_b, _))) => {
                assert_eq!(pref_a, pref_b);
                self.value_si.approx_eq(&other.value_si)
            }
            (_, _) => unreachable!(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct OutputSpecies {
    pub species_name: String,
    pub attributes: BTreeMap<String, OutputAttribute>,
    pub children: Vec<OutputSpecies>,
    pub compartment_id: Option<usize>,
    pub amt: usize,
}

fn float_check(a: &f64, b: &f64) -> bool {
    *a <= *b * (1. + 1e-6) && *b <= *a * (1. + 1e-6)
}

impl OutputValue {
    fn approx_eq(&self, other: &OutputValue) -> bool {
        match (self, other) {
            (OutputValue::Float(a), OutputValue::Float(b)) => float_check(a, b),
            (OutputValue::Integer(a), OutputValue::Float(b)) => float_check(&(*a as f64), b),
            (OutputValue::Float(a), OutputValue::Integer(b)) => float_check(a, &(*b as f64)),
            (a, b) => a == b,
        }
    }
}

impl std::cmp::PartialEq for OutputSpecies {
    fn eq(&self, other: &Self) -> bool {
        if (self.species_name == other.species_name)
            && (self.amt == other.amt)
            && (self.attributes.len() == other.attributes.len())
        {
            for (key, val) in self.attributes.iter() {
                match other.attributes.get(key) {
                    None => {
                        return false;
                    }
                    Some(k) => {
                        if !k.approx_eq(val) {
                            return false;
                        }
                    }
                }
            }
            SimOutput::eq_vecs_rek(&self.children, &other.children)
            //return true;
        } else {
            return false;
        }
    }
}

/*impl OutputSpecies {
    fn assert_eq(&self, other: &OutputSpecies) {
        assert_eq!(self.name, other.name);
        assert_eq!(self.attributes.len(), other.attributes.len());
        for (key, val) in self.attributes.iter() {
            assert_eq!(other.attributes[key], *val);
        }
        assert_eq!(self.amt, other.amt);
        SimOutput::assert_eq_vecs_rek(&self.children, &other.children);
    }
}*/

impl std::fmt::Display for OutputSpecies {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        assert!(self.amt > 0);
        if self.amt > 1 {
            write!(f, "{} ", self.amt)?
        }
        write!(f, "{}", self.species_name)?;
        if !self.attributes.is_empty() {
            write!(
                f,
                "({})",
                self.attributes
                    .iter()
                    .map(|(n, v)| format!("{} = {}", n, v))
                    .join(", ")
            )?
        }
        if !self.children.is_empty() {
            write!(
                f,
                "[{}]",
                self.children.iter().map(|s| format!("{}", s)).join(" + ")
            )?
        }
        write!(f, "")
    }
}

impl OutputSpecies {
    fn count_species_helper<F: Fn(&OutputSpecies) -> bool>(&self, f: &F) -> usize {
        let mut output = 0;
        if f(self) {
            output += self.amt;
        }
        output += self
            .children
            .iter()
            .map(|k| k.count_species_helper(f))
            .sum::<usize>();
        output
    }
}

pub struct ReactionStateOutput {
    pub name: String,
    pub total_prop: f64,
    pub copy_propensities: Vec<f64>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SimOutputJson {
    pub time: f64,
    pub species: Vec<OutputSpecies>,
}

pub struct SimOutput {
    pub time: f64,
    pub species: Vec<OutputSpecies>,
    pub propensities: Vec<ReactionStateOutput>,
}

impl SimOutput {
    pub fn assert_propensity(&self, idx: usize, value: f64) {
        let r = &self.propensities[idx];
        if (r.total_prop > 1.0001 * value) || (r.total_prop < 0.9999 * value) {
            panic!(
                "Propensity of '{}' is {}, but expect {}",
                r.name, r.total_prop, value
            );
        }
    }
    pub fn count_species_constraint<F: Fn(&OutputSpecies) -> bool>(
        &self,
        name: &str,
        f: F,
    ) -> usize {
        self.species
            .iter()
            .map(|k| k.count_species_helper(&|k| k.species_name == name && f(k)))
            .sum()
    }
    pub fn count_species(&self, name: &str) -> usize {
        self.count_species_constraint(name, |_| true)
    }
    fn eq_vecs_rek(a: &Vec<OutputSpecies>, b: &Vec<OutputSpecies>) -> bool {
        if !(a.len() == b.len()) {
            return false;
        }
        let mut used_positions = vec![true; a.len()];
        for aa in a.iter() {
            match b
                .iter()
                .zip(used_positions.iter())
                .position(|(k, still_available)| *still_available && (k == aa))
            {
                None => {
                    let remaining: Vec<_> = b
                        .iter()
                        .zip(used_positions.iter())
                        .filter_map(|(a, b)| if *b { Some(a) } else { None })
                        .collect();
                    println!("Could not find {:?}. Left are {:?}", aa, remaining);
                    return false;
                }
                Some(pos) => {
                    assert!(used_positions[pos]);
                    used_positions[pos] = false;
                }
            }
        }
        assert_eq!(used_positions.len(), a.len());
        return true;
    }
    pub fn species_eq(&self, other: &Self) -> bool {
        Self::eq_vecs_rek(&self.species, &other.species)
    }
}
/*impl std::ops::Index<&str> for SimOutput {
    type Output = usize;

    fn index(&self, index: &str) -> &Self::Output {
        &self.count_species(index)
    }
}*/

impl std::fmt::Display for SimOutput {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "t = {}[s]: [{}]",
            self.time,
            self.species.iter().map(|k| format!("{}", k)).join(" + ")
        )?;
        for k in self.propensities.iter() {
            write!(f, " {}", k.total_prop)?
        }
        write!(f, "")
    }
}

fn unit_for_flat_output(name: &String, val: &OutputAttribute) -> (String, String) {
    match &val.unit_str_prefactor {
        None => (name.clone(), val.value_si.to_string()),
        Some((p_s, p_f)) => (
            format!("{} [{}]", name, p_s),
            format!("{}", val.value_si.as_float_no_int().unwrap() / p_f),
        ),
    }
}

fn get_output_flatt(
    data: &Vec<OutputSpecies>,
    time: f64,
    nesting_str: String,
    string_conversion: &HashMap<String, OptimizedString>,
) -> Vec<HashMap<String, String>> {
    data.iter()
        .map(|l| {
            //let nesting_sub = format!("{}",nesting_str)
            l.attributes
                .iter()
                .map(|(name, val)| unit_for_flat_output(name, val))
                .chain(std::iter::once(("time".to_string(), time.to_string())))
                .chain(std::iter::once(("amount".to_string(), l.amt.to_string())))
                .chain(std::iter::once((
                    "nesting_level".to_string(),
                    nesting_str.clone(),
                )))
                .chain(std::iter::once((
                    "compartment_id".to_string(),
                    match l.compartment_id {
                        None => "".to_string(),
                        Some(k) => format!("{}({})", l.species_name, k).to_string(),
                    },
                )))
                .chain(std::iter::once((
                    "species_name".to_string(),
                    l.species_name.clone(),
                )))
                .collect()
        })
        .chain(
            data.iter()
                .map(|l| {
                    get_output_flatt(
                        &l.children,
                        time,
                        format!(
                            "in {}({}) {}",
                            l.species_name,
                            match l.compartment_id {
                                None => {
                                    usize::MAX
                                }
                                Some(k) => {
                                    k
                                }
                            },
                            nesting_str
                        )
                        .to_string(),
                        string_conversion,
                    )
                    .into_iter()
                })
                .flatten(),
        )
        .collect()
}
pub fn flatten_states_to_csv_file(
    mut file: File,
    data: &Vec<SimOutputJson>,
    string_conversion: &HashMap<String, OptimizedString>,
) {
    let all_data/*: Vec<HashMap<String, String>>*/ = data
        .iter()
        .map(|k| get_output_flatt(&k.species, k.time, "".to_string(), string_conversion))
        .flatten();

    let mut all_keys: BTreeSet<String> = BTreeSet::new();
    for d in all_data.clone() {
        for s in d.keys() {
            all_keys.insert(s.clone());
        }
    }

    assert!(all_keys.remove("time"));
    writeln!(file, "{},{},", "time [s]", all_keys.iter().join(",")).unwrap();
    for vals in all_data {
        writeln!(
            file,
            "{},{},",
            vals["time"],
            all_keys
                .iter()
                .map(|k| match vals.get(k) {
                    None => {
                        format!("").to_string()
                    }
                    Some(l) => {
                        l.clone()
                    }
                })
                .join(",")
        )
        .unwrap();
    }
}
