use crate::expansion::dynamic_expansion_tree::change_structure_and_reexpand_v2;
use crate::expansion::static_expansion::FlatModel;
use crate::input::structures::{LanguageError, Model};
use crate::running::{test_full_output_step, use_hybrid_att, DataForPlot, TestUntil};
use crate::simulator::simulate_fast::*;
use itertools::Itertools;
use log::*;
use serde::{Deserialize, Serialize};
use std::iter;
use std::mem::swap;
use std::panic;
use std::panic::AssertUnwindSafe;
use std::time::Duration;
use web_time::SystemTime;

use crate::input;
#[derive(PartialEq, Serialize, Deserialize, Clone, Debug)]
pub enum MessageOrPlot {
    FinalMessage(String),
    IntermediateMessage(String),
    IntermediatePlot(DataForPlot),
    FinalPlot(DataForPlot),
}
impl MessageOrPlot {
    pub fn is_final(&self) -> bool {
        match self {
            MessageOrPlot::FinalPlot(_) | MessageOrPlot::FinalMessage(_) => true,
            MessageOrPlot::IntermediateMessage(_) | MessageOrPlot::IntermediatePlot(_) => false,
        }
    }
}

pub struct WebExecutor {
    pub fast_sim: SimulationFast,
    m: FlatModel,
    target_time: f64,
    io_prefix: f64,
    plot_unit: String,
}
impl Default for WebExecutor {
    fn default() -> Self {
        WebExecutor {
            fast_sim: Default::default(),
            m: Default::default(),
            target_time: 0.0,
            io_prefix: 0.0,
            plot_unit: "".to_string(),
        }
    }
}

impl WebExecutor {
    pub fn build_mlrules_to_string(input: &str, endtime: &str) -> Result<WebExecutor, String> {
        match panic::catch_unwind(|| build_mlrules_to_string_w_panic(input, endtime)) {
            Ok(a) => match a {
                GeneratedSim::Err(s) => Err(s),
                GeneratedSim::Ok((sem, fm, t, iop, plot_unit)) => Ok(WebExecutor {
                    fast_sim: sem,
                    m: fm,
                    target_time: t,
                    io_prefix: iop,
                    plot_unit,
                }),
            },
            Err(_b) => Err(format!("PANIC")),
        }
    }
    pub fn execute_mlrules_to_string(&mut self) -> MessageOrPlot {
        match panic::catch_unwind(AssertUnwindSafe(|| {
            self.execute_mlrules_to_string_w_panic()
        })) {
            Ok(a) => a,
            Err(_b) => MessageOrPlot::FinalMessage(format!("PANIC")),
        }
    }

    fn execute_mlrules_to_string_w_panic(&mut self) -> MessageOrPlot {
        info!("Req exec");
        let oo = OutputOptions {
            json: false,
            compressed_json: false,
            flat_table: false,
            rule_counts: false,
            plot: true,
            regular_csv: false,
            latex: false,
            debug_outputs: false,
        };
        let chunk_start_time = SystemTime::now();
        let mut my_step_count = 0;
        while self.fast_sim.time < self.target_time {
            my_step_count += 1;
            let step_res = match self.fast_sim.step(false) {
                Ok(i) => i,
                Err(s) => {
                    return MessageOrPlot::FinalMessage(format!(" -- Simulation error --\n{}", s));
                }
            };
            match step_res {
                StepResult::AllGood => {
                    //println!("Good Step")
                }
                StepResult::NoReactionPossible => {
                    //warn!("Early termination, because no more reactions possible!")
                }
                StepResult::StructuralChange {
                    idx_of_reaction: idx,
                    variation,
                } => {
                    self.fast_sim.take_current_state_as_initial(&mut self.m);
                    let lookup =
                        self.fast_sim
                            .variation_to_lookup(variation, &self.m.reactions, idx);
                    let mut m_dummy = FlatModel::default();
                    let mut sim_dummy = SimulationFast::default();
                    swap(&mut self.m, &mut m_dummy);
                    swap(&mut self.fast_sim, &mut sim_dummy);
                    self.m = change_structure_and_reexpand_v2(m_dummy, idx, 20, false, lookup);
                    self.fast_sim = match sim_dummy.reset_to_changed_model(&mut self.m, false) {
                        Ok(f) => f,
                        Err(i) => {
                            return MessageOrPlot::FinalMessage(i);
                        }
                    };
                }
                StepResult::OnlyObservation => {
                    self.fast_sim.make_observations(&oo);
                    //println!("Obs only")
                }
                StepResult::OnlyTimeDependendUpdate => {}
                StepResult::Completed => {
                    self.fast_sim.make_observations(&oo);
                    break;
                }
            }
            if my_step_count % 100 == 0 {
                if SystemTime::now()
                    .duration_since(chunk_start_time)
                    .unwrap_or(Duration::new(0, 0))
                    .as_secs_f64()
                    > 1.
                {
                    break;
                }
            }
        }
        /*let sim_time = SystemTime::now()
        .duration_since(start_time)
        .unwrap_or(Duration::new(0, 0))
        .as_secs_f64();*/
        info!(
            "Sim run completed with ({} steps)",
            /*sim_time,*/ self.fast_sim.metrics.regular_steps
        );
        /*let title = if self.fast_sim.observation_queries.len() == 1 {
            self.fast_sim.observation_queries[0].0.clone()
        } else {
            format!("Simulation Results")
        };*/
        let obs_t: Vec<_> = self
            .fast_sim
            .obs_times
            .iter()
            .map(|k| *k / self.io_prefix)
            .collect();
        //let plot = generate_plot(&fast_sim, title.as_str(), Some(plot_unit));
        let dat = DataForPlot {
            observation_values: self.fast_sim.observation_values.clone(),
            obs_times: obs_t,
            xmax: self.target_time / self.io_prefix,
            xunit: Some(self.plot_unit.clone()),
        };
        if self.fast_sim.time < self.target_time {
            MessageOrPlot::IntermediatePlot(dat)
        } else {
            MessageOrPlot::FinalPlot(dat)
        }
    }
}

pub enum GeneratedSim {
    Err(String),
    Ok((SimulationFast, FlatModel, f64, f64, String)),
}

pub fn execute_mlrules_error_list(input: &str) -> Vec<LanguageError> {
    match panic::catch_unwind(|| execute_mlrules_error_list_internal(input)) {
        Ok(a) => a,
        Err(_b) => {
            println!("Internal panic");
            vec![]
        }
    }
}

fn execute_mlrules_error_list_internal(input: &str) -> Vec<LanguageError> {
    let start_time = SystemTime::now();

    let m = Model::from_string(&input.to_string(), true, &vec![], format!(""), true);
    info!(
        "Computed errors in {}s",
        SystemTime::now()
            .duration_since(start_time)
            .unwrap_or(Duration::new(0, 0))
            .as_secs_f64()
    );
    match m {
        Ok((_m, _)) => {
            vec![]
        }
        Err(errors) => errors,
    }
}

fn build_mlrules_to_string_w_panic(input: &str, endtime_s: &str) -> GeneratedSim {
    info!("Req exec");
    let endtime = match input::semantic_conversion::string_to_seconds(endtime_s) {
        None => return GeneratedSim::Err(format!("Invalid endtime '{}'", endtime_s)),
        Some(x) => x,
    };
    let re = regex::Regex::new(r"\[([^\]]+)\]").unwrap();
    let (plot_unit, io_prefax) = match re.captures(endtime_s) {
        None => return GeneratedSim::Err(format!("Regex error")),
        Some(x) => match x.get(0) {
            None => return GeneratedSim::Err(format!("Regex error (B)")),
            Some(k) => (k.as_str(), {
                let unit = format!("1{}", k.as_str());
                info!("Unit : {unit}");

                match input::semantic_conversion::string_to_seconds(unit.as_str()) {
                    None => return GeneratedSim::Err(format!("Regex error (C)")),
                    Some(x) => x,
                }
            }),
        },
    };
    //let mut outp = "".to_string();
    let m = match Model::from_string(&input.to_string(), true, &vec![], format!(""), false) {
        Ok((m, _)) => {
            info!(
                "Count Lines {}\nReactions: {}, species {}",
                input.split("\n").count(),
                m.transitions.len(),
                m.species.len()
            );
            m
        }
        Err(errors) => {
            return GeneratedSim::Err(
                errors
                    .into_iter()
                    .map(|k| format!("{} {}", k.line, k.what))
                    .join("\n"),
            );
        }
    };

    let target_time = endtime;
    //let start_time = SystemTime::now();

    let mut m = FlatModel::from_model(m);
    m.flatten_a(100, false);
    info!(
        "Flatten success! {} reactions, {} species\n",
        m.reactions.len(),
        m.species.len()
    );
    let fast_sim = SimulationFast::new(&m, Some((target_time, 1000)), false, None);
    let fast_sim = match fast_sim {
        Ok(f) => f,
        Err(i) => {
            return GeneratedSim::Err(i);
        }
    };

    //outp += format!("\n{}\n", fast_sim.generate_output(false).to_string()).as_str();

    if fast_sim.observation_queries.is_empty() {
        return GeneratedSim::Err(format!(
            "No observation specified. found {} reacs",
            fast_sim.reactions.len()
        ));
    }
    GeneratedSim::Ok((fast_sim, m, target_time, io_prefax, plot_unit.to_string()))
}

fn format_csv(input: &str) -> String {
    //return input.to_string();
    // Split into rows
    let rows: Vec<Vec<String>> = input
        .lines()
        .map(|line| {
            line.split(',')
                .map(|cell| cell.trim().to_string())
                .collect()
        })
        .collect();

    if rows.is_empty() {
        return String::new();
    }

    // Find the maximum width for each column
    let max_widths: Vec<usize> = rows[0]
        .iter()
        .enumerate()
        .map(|(i, _)| {
            rows.iter()
                .map(|row| {
                    if let Some(cell) = row.get(i) {
                        cell.len()
                    } else {
                        0
                    }
                })
                .max()
                .unwrap_or(0)
        })
        .collect();

    // Build the formatted string
    let mut result = String::new();
    for row in &rows {
        for (cell, &max_width) in row.iter().zip(max_widths.iter()) {
            // Align cells to the left, padded with spaces to the maximum column width
            result += &format!("{:<width$}, ", cell, width = max_width);
        }
        result = result
            .trim_end_matches(' ')
            .trim_end_matches(',')
            .to_string();
        result += "\n";
    }

    result
}

fn build_info() -> String {
    use build_time::build_time_utc;
    let utc_build_time = build_time_utc!();
    let features: Vec<&str> = option_env!("CARGO_FEATURES")
        .unwrap_or("")
        .split(',')
        .collect();
    let perf = if cfg!(feature = "performance-templates") {
        "performance-templates used"
    } else {
        "no performance templates"
    };
    let build_mode = if cfg!(debug_assertions) {
        "Debug"
    } else {
        "Release"
    };
    let use_hybrid = use_hybrid_att().to_string();

    format!(
        "{}\nUsed Features: {:?} (hybrid: {})\n{}\nBuild Mode: {}",
        utc_build_time, features, use_hybrid, perf, build_mode
    )
}

pub fn gen_benchmark(
    benchmark_name: &String,
    num_reps: usize,
    custom_extra: Option<usize>,
) -> String {
    let endtimes: Vec<f64>;
    let modelstr;
    match benchmark_name.to_lowercase().as_str() {
        "yeast" => {
            let num_samples = 10;
            let longest_time = custom_extra.unwrap_or(2500) as f64 * 60.;
            endtimes = (0..num_samples)
                .map(|k| (k + 1) as f64 / (num_samples) as f64 * longest_time)
                .collect();
            modelstr = include_str!("../../benchmarkmodels/Yeast_Model_MLR3.mlr");
        }
        _ => return format!("Model not found!"),
    };
    let runtimes: Vec<Vec<(f64, usize)>> = iter::repeat_with(|| {
        endtimes
            .iter()
            .map(|k| {
                info!("Bench until {k}");
                let start_time = SystemTime::now();
                let (m, _) = Model::from_string(
                    &modelstr.to_string(),
                    true,
                    &vec![],
                    format!("no file"),
                    false,
                )
                .unwrap();
                let mut m = FlatModel::from_model(m);
                m.flatten_a(100, false);
                let sim = SimulationFast::new(&m, Some((*k, 10)), false, None).unwrap();
                let s2 = test_full_output_step(m, sim, TestUntil::Time(*k), true);
                (
                    SystemTime::now()
                        .duration_since(start_time)
                        .unwrap_or(Duration::new(0, 0))
                        .as_secs_f64(),
                    s2.metrics.regular_steps,
                )
            })
            .collect()
    })
    .take(num_reps)
    .collect();

    build_info()
        + "\n\n"
        + format_csv(
            iter::once(format!(
                "time[s] ,{}",
                (0..num_reps)
                    .map(|k| format!("time{k},steps{k}"))
                    .join(" ,")
            ))
            .chain(endtimes.iter().enumerate().map(|(num, t)| {
                format!(
                    "{} ,{}",
                    t,
                    runtimes
                        .iter()
                        .map(|k| format!("{},{}", k[num].0, k[num].1))
                        .join(" ,")
                )
            }))
            .join("\n")
            .as_str(),
        )
        .as_str()
}
