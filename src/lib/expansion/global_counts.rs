use crate::expansion::static_expansion::*;
use crate::input::structures::*;
use crate::simulator::evaluation::*;
use std::collections::{BTreeMap, HashMap};
pub fn replace_global_counts_in_rate_term(
    all_species: &HashMap<SpeciesName, Vec<EnumeratedSpecies>>,
    all_species_by_idx: &Vec<EnumeratedSpecies>,
    ex: RateExprTerm,
    left_side: Option<&BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>>,
    global_lookup: &HashMap<CompartmentID, usize>,
) -> RateExprTerm {
    match ex {
        RateExprTerm::Expr(x) => {
            RateExprTerm::Expr(Box::new(x.replace_global_counts_in_rate_expr(
                all_species,
                all_species_by_idx,
                left_side,
                global_lookup,
            )))
        }
        RateExprTerm::GlobalCount(GlobalCountEnum::GenericUnspecified(y)) => {
            let to_count =
                y.get_species_to_count(all_species, all_species_by_idx, global_lookup, left_side);
            if to_count.iter().all(|(_, b)| b.is_none()) {
                RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumerated(
                    to_count
                        .into_iter()
                        .map(|(a, b)| {
                            assert!(b.is_none());
                            a
                        })
                        .collect(),
                ))
            } else {
                RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumeratedGuard(to_count))
            }
        }
        RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumeratedGuard(x)) => {
            RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumeratedGuard(x))
        }
        RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumerated(x)) => {
            RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumerated(x))
        }
        //unreachable!("Global species count already expanded?")
        // }
        //RateExprTerm::SpecificCount(y.get_species_to_count(all_species))

        /*_ => return ex,*/
        RateExprTerm::Primitive(_)
        | RateExprTerm::ExpandedAttRef(_)
        | RateExprTerm::StarAttRef(_)
        | RateExprTerm::SpecCount(_) => return ex,
        RateExprTerm::Condition(cond) => RateExprTerm::Condition(Box::new(Condition {
            guard: cond.guard.replace_global_counts_in_rate_expr(
                all_species,
                all_species_by_idx,
                left_side,
                global_lookup,
            ),
            result_true: cond.result_true.replace_global_counts_in_rate_expr(
                all_species,
                all_species_by_idx,
                left_side,
                global_lookup,
            ),
            result_false: match cond.result_false {
                None => None,
                Some(ex) => Some(ex.replace_global_counts_in_rate_expr(
                    all_species,
                    all_species_by_idx,
                    left_side,
                    global_lookup,
                )),
            },
        })),
        RateExprTerm::BuildinFunction(k) => RateExprTerm::BuildinFunction(BuildinFunctionCall {
            function: k.function,
            params: k
                .params
                .into_iter()
                .map(|k| {
                    k.replace_global_counts_in_rate_expr(
                        all_species,
                        all_species_by_idx,
                        left_side,
                        global_lookup,
                    )
                })
                .collect(),
        }),
    }
}
fn any_parent_is_anywhere_in_x(
    possible: &Vec<EnumeratedSpecies>,
    lookiing_at: &EnumeratedSpecies,
    parent_id: &CompartmentID,
    global_lookup: &HashMap<CompartmentID, usize>,
) -> bool {
    match &lookiing_at.parent_id {
        None => false,
        Some(pid) => {
            if pid == parent_id {
                return true;
            } else {
                let k = &possible[global_lookup[pid]]; /*possible
                                                       .iter()
                                                       .find(|k| k.my_id == lookiing_at.parent_id)
                                                       .unwrap();*/
                debug_assert_eq!(k.my_id, lookiing_at.parent_id);
                return any_parent_is_anywhere_in_x(possible, k, parent_id, global_lookup);
            }
        }
    }
}

fn any_parent_is_species(
    possible: &Vec<EnumeratedSpecies>,
    looking_at: &EnumeratedSpecies,
    parent_name: &SpeciesName,
    global_lookup: &HashMap<CompartmentID, usize>,
) -> Option<EnumeratedSpecies> {
    if looking_at.species_name == *parent_name {
        return Some(looking_at.clone());
    }
    match &looking_at.parent_id {
        None => None,
        Some(pid) => {
            let k = &possible[global_lookup[pid]];
            /*let k = possible
            .iter()
            .filter(|k| k.my_id.is_some())
            .find(|k| *k.my_id.as_ref().unwrap() == *pid)
            .unwrap();*/
            return any_parent_is_species(possible, k, parent_name, global_lookup);
        }
    }
}

fn merge_rate_expr_with_and(mut exprs: Vec<RateExpr>) -> RateExpr {
    let x = match exprs.pop() {
        None => RateExpr::Single(RateExprTerm::Primitive(PrimitiveTypes::Boolean(true))),
        Some(x) => x,
    };
    if exprs.is_empty() {
        x
    } else {
        RateExpr::Double(
            RateExprTerm::Expr(Box::new(x)),
            FloatOp::Comparison(LogicalOp::And),
            RateExprTerm::Expr(Box::new(merge_rate_expr_with_and(exprs))),
        )
    }
}

impl GlobalCountEnum {
    pub(crate) fn to_indexed(
        &self,
        converse_map: &HashMap<EnumeratedSpecies, usize>,
        converte_left: &HashMap<SpeciesLeftIdentifier, usize>,
        species_counts: &Vec<SpeciesAmt>,
    ) -> IndexedRateExprTerm {
        let mut reverse: Vec<&EnumeratedSpecies> = if converse_map.is_empty() {
            vec![]
        } else {
            vec![converse_map.iter().next().unwrap().0; converse_map.len()]
        };
        for (v, idx) in converse_map.iter() {
            reverse[*idx] = v;
        }
        let r = match self {
            GlobalCountEnum::GenericUnspecified(_k) => {
                panic!("should no longer be used");
            }
            GlobalCountEnum::SpecificEnumerated(k) => {
                if k.is_empty() {
                    return IndexedRateExprTerm::Primitive(PrimitiveTypes::Integer(0));
                }
                if k.iter().all(|s| {
                    s.my_id.is_some()
                        && species_counts[converse_map[s]].is_not_zero()
                        && s.species_name == k[0].species_name
                }) {
                    let count_in_system = reverse
                        .iter()
                        .filter(|s| s.species_name == k[0].species_name)
                        .count();
                    if count_in_system == k.len() {
                        return IndexedRateExprTerm::Primitive(PrimitiveTypes::Integer(
                            count_in_system as i64,
                        ));
                    }
                }
                IndexedRateExprTerm::SpecificCountFromGlobal(
                    k.iter()
                        .map(|k| match converse_map.get(k) {
                            None => {
                                assert_eq!(converse_map.len(), species_counts.len());
                                panic!("not found {:?}", k)
                            }
                            Some(k) => *k,
                        })
                        .collect(),
                )
            }
            GlobalCountEnum::SpecificEnumeratedGuard(k) => {
                IndexedRateExprTerm::SpecifCountFromGlobalConstraint(
                    k.iter()
                        .map(|(k, guard)| {
                            (
                                converse_map[k],
                                match guard {
                                    None => None,
                                    Some(x) => Some(IndexedRateExprTerm::from_expr(
                                        x,
                                        converse_map,
                                        converte_left,
                                        species_counts,
                                    )),
                                },
                            )
                        })
                        .collect(),
                )
            }
        };
        //println!("{:?} -> {:?}", r, self);
        r
    }
}
impl GenericGlobalCount {
    pub fn get_species_to_count(
        &self,
        possible_by_species: &HashMap<SpeciesName, Vec<EnumeratedSpecies>>, //&Vec<EnumeratedSpecies>,
        possibly_by_idx: &Vec<EnumeratedSpecies>,
        global_lookup: &HashMap<CompartmentID, usize>,
        left_side: Option<&BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>>, //left_side: Option<&Vec<EnumeratedSpecies>>,
    ) -> Vec<(EnumeratedSpecies, Option<RateExpr>)> {
        let tmp = vec![];
        let candidates = possible_by_species
            .get(&self.species_name)
            .unwrap_or(&tmp)
            .iter()
            //.iter()
            //.filter(|s| s.species_name == self.species_name)
            .filter_map(|s| {
                let x = &merge_rate_expr_with_and(self.constraints.clone());

                // Todo -> Give all left
                match x.evaluate_partial_for_left(
                    &AllOrSingleLeft::SingleLeft(s),
                    None,
                    None,
                    false,
                ) {
                    PartialEvaluationExpr::Primitive(PrimitiveTypes::Boolean(true)) => {
                        Some((s, None))
                    }
                    PartialEvaluationExpr::Primitive(PrimitiveTypes::Boolean(false)) => None,
                    PartialEvaluationExpr::Primitive(_op) => unreachable!(),
                    PartialEvaluationExpr::Incomplete(x) => Some((s, Some(x))),
                }
            }); /*.filter(|k|
                self.constraints.iter().all(|constr| let indexed =  Ind)
                );*/
        let result = match &self.inside_of {
            GenericGlobalCountInsideOf::GlobalNone => {
                let candidates: Vec<_> = candidates.collect();
                /*let is_compartment_count = candidates
                .iter()
                .all(|(k, b)| b.is_none() && k.my_id.is_some());*/

                candidates
                    .into_iter()
                    .map(|(k, b)| (k.clone(), b))
                    .collect()
            }
            GenericGlobalCountInsideOf::SpecificNone => {
                unimplemented!()
            }
            GenericGlobalCountInsideOf::SpecificOneLevel(l) => {
                let l_id = match left_side {
                    None => {
                        unreachable!()
                    }
                    Some(lsl) => lsl.get(l).unwrap().my_id.as_ref().unwrap().clone(),
                };
                candidates
                    .filter(|(s, _guard)| match &s.parent_id {
                        None => false,
                        Some(parent_id) => *parent_id == l_id,
                    })
                    .map(|(k, guard)| (k.clone(), guard))
                    .collect()
            }
            GenericGlobalCountInsideOf::SpecificAnyDepth(l) => candidates
                .filter(|(s, _guard)| {
                    any_parent_is_anywhere_in_x(
                        possibly_by_idx,
                        s,
                        left_side.unwrap().get(l).unwrap().my_id.as_ref().unwrap(),
                        global_lookup,
                    )
                })
                .map(|(k, guard)| (k.clone(), guard))
                .collect(),
            GenericGlobalCountInsideOf::GlobalOneLevel(g, constr) => {
                if !constr.is_empty() {
                    unimplemented!("Global counts, currently are not implemented with an constraint on the parent")
                }
                candidates
                    .filter_map(|(k, guard)| match &k.parent_id {
                        None => None,
                        Some(parent_id) => {
                            let k_parent = &possibly_by_idx[global_lookup[parent_id]];
                            if k_parent.species_name != *g {
                                return None;
                            }
                            Some((k.clone(), guard))
                        }
                    })
                    .collect()
            }
            GenericGlobalCountInsideOf::GlobalAnyDepth(parent_name, parent_constr) => {
                if *parent_name == self.species_name {
                    unimplemented!("Nesting in itself not implemented");
                }
                if !parent_constr.is_empty() {
                    unimplemented!("Global counts, currently are not implemented with an constraint on the parent")
                }
                candidates
                    .filter_map(|(candidate, guard)| {
                        match any_parent_is_species(
                            &possibly_by_idx,
                            candidate,
                            parent_name,
                            global_lookup,
                        ) {
                            None => None,
                            Some(_parent) => Some((candidate.clone(), guard)),
                        }
                    })
                    .collect()
            }
        };
        /*match self.inside_of{
            GenericGlobalCountInsideOf::SpecificNone => {}
            GenericGlobalCountInsideOf::GlobalNone => {}
            GenericGlobalCountInsideOf::SpecificOneLevel(_) => {}
            GenericGlobalCountInsideOf::SpecificAnyDepth(_) => {}
            GenericGlobalCountInsideOf::GlobalOneLevel(_, _) => {}
            GenericGlobalCountInsideOf::GlobalAnyDepth(_, _) => {}
        }*/
        return result;
        /*
        if self.definition.len() != 1 {
            unimplemented!("In expression in global count not realized");
        }
        return match &self.definition[0] {
            CountingElement::_SpecificNameFromLeft(_) => {
                unreachable!()
            }
            CountingElement::GeneralDescription(name, constr) => {
                if !constr.is_empty() {
                    unimplemented!("Constraints on global counts are not yet implemented");
                }
                possible
                    .iter()
                    .filter(|p| p.species_name == *name)
                    .cloned()
                    .collect()
            }
        };
        */

        /*
        //println!("Checking for {:?}", self);
        assert!(!self.definition.is_empty());
        let num_of_last = self.definition.len() - 1;
        let mut def: Vec<_> = self
            .definition
            .iter()
            .enumerate()
            .map(|(n, k)| {
                let parent_id;
                if n == num_of_last {
                    parent_id = None;
                } else {
                    parent_id = Some(SpeciesLeftIdentifier::new(&*format!("__{}", n + 1)));
                }
                match k {
                    CountingElement::_SpecificNameFromLeft(_) => unreachable!(),
                    CountingElement::GeneralDescription(a, b) => SpeciesLeft {
                        species_name: a.clone(),
                        id: SpeciesLeftIdentifier::new(&*format!("__{}", n)),
                        parent_number: parent_id,
                        attribute_constraint: b.clone(),
                    },
                }
            })
            .collect();
        def.reverse();

        match find_matches(&def, possible) {
            None => {
                println!("foun none");
                vec![]
            }
            Some(x) => x.into_iter().fold(vec![], |mut acc, x| {
                assert_eq!(x.len(), self.definition.len());
                acc.push(
                    x.get(&SpeciesLeftIdentifier::new(&*format!("__{}", 0)))
                        .unwrap()
                        .clone(),
                );
                println!("found {:?}", acc);
                acc
            }),
        }*/
    }
}
impl RateExpr {
    pub fn replace_global_counts_in_rate_expr(
        self,
        all_species: &HashMap<SpeciesName, Vec<EnumeratedSpecies>>,
        all_species_by_idx: &Vec<EnumeratedSpecies>,
        left_side: Option<&BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>>,
        global_lookup: &HashMap<CompartmentID, usize>,
    ) -> RateExpr {
        match self {
            RateExpr::Single(e) => RateExpr::Single(replace_global_counts_in_rate_term(
                all_species,
                all_species_by_idx,
                e,
                left_side,
                global_lookup,
            )),
            RateExpr::Double(l, op, r) => RateExpr::Double(
                replace_global_counts_in_rate_term(
                    all_species,
                    all_species_by_idx,
                    l,
                    left_side,
                    global_lookup,
                ),
                op,
                replace_global_counts_in_rate_term(
                    all_species,
                    all_species_by_idx,
                    r,
                    left_side,
                    global_lookup,
                ),
            ),
        }
    }
}
