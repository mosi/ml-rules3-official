use crate::input::structures::*;
use itertools::Itertools;
use log::*;
use more_asserts::assert_gt;
//use rayon::prelude::*;
use std::collections::HashSet;
use std::collections::{BTreeMap, HashMap};
use std::fmt::Formatter;
use std::mem::swap;
use std::{cmp, fmt, iter};

use serde::{Deserialize, Serialize};

//use rayon::prelude::*;
use crate::expansion::static_matching::MatchingIterator;
use RateExprTerm::StarAttRef;

#[derive(PartialEq, Eq, Debug, Hash, Clone, Serialize, Deserialize, PartialOrd, Ord)]
pub struct CompartmentID {
    pub(crate) number: isize,
    pub(crate) species: SpeciesName,
    // TODO remove this next parent
    //pub next_parent: Option<Box<CompartmentID>>,
}

impl CompartmentID {
    pub fn to_string(&self) -> String {
        format!("{:?}_{}", self.species, self.number)
    }
}
/*impl fmt::Display for CompartmentID {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.number {
            0 => write!(f, "{}", self.species)?,
            n => write!(f, "{}_{}", self.species, n)?,
        }
        match &self.next_parent {
            Some(v) => write!(f, "_in{}", *v),
            None => write!(f, ""),
        }
    }
}*/

// TODO clarify hash -> problem with expanded attributes?
#[derive(PartialEq, Eq, Debug, Hash, Clone, Serialize, Deserialize, PartialOrd, Ord)]
pub struct EnumeratedSpecies {
    pub species_name: SpeciesName,
    pub expanded_attribute_values: Vec<(OptimizedString, PrimitiveTypes)>,
    pub parent_id: Option<CompartmentID>,
    pub my_id: Option<CompartmentID>,
    //pub(crate) my_flattend_idx: Option<usize>,
}
impl EnumeratedSpecies {
    pub fn sort(mut self) -> EnumeratedSpecies {
        self.expanded_attribute_values
            .sort_by(|(a, _), (b, _)| a.cmp(b));
        EnumeratedSpecies {
            species_name: self.species_name,
            expanded_attribute_values: self.expanded_attribute_values,
            parent_id: self.parent_id,
            my_id: self.my_id,
            //my_flattend_idx: None,
        }
    }
    pub fn get_attribute(&self, key: &OptimizedString) -> PrimitiveTypes {
        assert_eq!(
            self.expanded_attribute_values
                .iter()
                .filter(|(k, _v)| *k == *key)
                .count(),
            1
        );
        self.expanded_attribute_values
            .iter()
            .filter(|(k, _v)| *k == *key)
            .next()
            .unwrap()
            .1
            .clone()
    }
}

/*impl RateExprTerm {
    pub fn _to_string_for_name(&self) -> String {
        match self {
            RateExprTerm::ExpandedAttRef(_) => unreachable!(),
            RateExprTerm::Expr(_) => unreachable!("Expression not collapsed"),
            RateExprTerm::SpecCount(_) => unreachable!(),

            RateExprTerm::Primitive(p) => match p {
                PrimitiveTypes::String(s) => s.clone(),
                PrimitiveTypes::Boolean(b) => b.to_string(),
                PrimitiveTypes::Float(f) => f.to_string().replace(".", "d"),
                PrimitiveTypes::Integer(i) => i.to_string(),
            },
            StarAttRef(_) => unimplemented!(),
            RateExprTerm::GlobalTime() => unreachable!("Time can not be in name!"),
            RateExprTerm::Parameter(x) => x.clone(),
            RateExprTerm::GlobalCount(_) => {
                unimplemented!()
            }
            RateExprTerm::Condition(_c) => {
                unimplemented!()
            }
        }
    }
}*/

#[derive(Debug)]
pub enum PartialEvaluationExpr {
    Primitive(PrimitiveTypes),
    Incomplete(RateExpr),
}

impl PartialEvaluationExpr {
    fn to_term(self) -> PartialEvaluationExprTerm {
        match self {
            PartialEvaluationExpr::Primitive(x) => PartialEvaluationExprTerm::Primitive(x),
            PartialEvaluationExpr::Incomplete(incp) => match incp {
                RateExpr::Single(y) => PartialEvaluationExprTerm::Incomplete(y),
                RateExpr::Double(_, _, _) => {
                    PartialEvaluationExprTerm::Incomplete(RateExprTerm::Expr(Box::new(incp)))
                }
            },
        }
    }
    fn to_fin_expr(self) -> RateExpr {
        match self {
            PartialEvaluationExpr::Primitive(x) => RateExpr::Single(RateExprTerm::Primitive(x)),
            PartialEvaluationExpr::Incomplete(incp) => incp,
        }
    }
}

pub enum PartialEvaluationExprTerm {
    Primitive(PrimitiveTypes),
    Incomplete(RateExprTerm),
}
impl PartialEvaluationExpr {
    pub fn to_string(&self, string_conversion: &HashMap<String, OptimizedString>) -> String {
        match self {
            PartialEvaluationExpr::Primitive(p) => p.to_string(string_conversion),
            PartialEvaluationExpr::Incomplete(k) => {
                format!("{:?}", k)
            }
        }
    }
}
/*impl fmt::Display for PartialEvaluationExpr {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            PartialEvaluationExpr::Primitive(x) => write!(f, "{}", x.to_string()),
            PartialEvaluationExpr::Incomplete(x) => write!(f, "{:?}", x), //TODO real output format
        }
    }
}*/

impl RateExpr {
    #[allow(dead_code)]
    fn get_all_involved(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    ) -> HashSet<EnumeratedSpecies> {
        match self {
            RateExpr::Single(x) => x.get_all_involved(left_match),
            RateExpr::Double(l, _, r) => l
                .get_all_involved(left_match)
                .into_iter()
                .chain(r.get_all_involved(left_match))
                .collect(),
        }
    }
    pub fn evaluate_partial_for_left(
        &self,
        left_match: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        //global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
        time: Option<f64>,
        eval_random: bool,
    ) -> PartialEvaluationExpr {
        match self {
            RateExpr::Single(x) => {
                let tmp = x.evaluate_partial(
                    left_match,
                    all_spec_counts,
                    /*global_vals,*/ time,
                    eval_random,
                );
                match tmp {
                    PartialEvaluationExprTerm::Primitive(x) => PartialEvaluationExpr::Primitive(x),
                    PartialEvaluationExprTerm::Incomplete(RateExprTerm::Expr(expr)) => expr
                        .evaluate_partial_for_left(left_match, all_spec_counts, time, eval_random),
                    PartialEvaluationExprTerm::Incomplete(other) => {
                        PartialEvaluationExpr::Incomplete(RateExpr::Single(other))
                    }
                }
            }
            RateExpr::Double(left, op, term) => {
                let r = term.evaluate_partial(
                    left_match,
                    all_spec_counts,
                    /*global_vals,*/ time,
                    eval_random,
                );
                match (
                    left.evaluate_partial(
                        left_match,
                        all_spec_counts,
                        /*global_vals,*/ time,
                        eval_random,
                    ),
                    r,
                ) {
                    (
                        PartialEvaluationExprTerm::Primitive(l),
                        PartialEvaluationExprTerm::Primitive(r),
                    ) => PartialEvaluationExpr::Primitive(op.apply(&l, &r).unwrap()),
                    (
                        PartialEvaluationExprTerm::Primitive(l),
                        PartialEvaluationExprTerm::Incomplete(r),
                    ) => PartialEvaluationExpr::Incomplete(RateExpr::Double(
                        RateExprTerm::Primitive(l),
                        *op,
                        r,
                    )),
                    (
                        PartialEvaluationExprTerm::Incomplete(l),
                        PartialEvaluationExprTerm::Primitive(r),
                    ) => PartialEvaluationExpr::Incomplete(RateExpr::Double(
                        l,
                        *op,
                        RateExprTerm::Primitive(r),
                    )),
                    (
                        PartialEvaluationExprTerm::Incomplete(l),
                        PartialEvaluationExprTerm::Incomplete(r),
                    ) => PartialEvaluationExpr::Incomplete(RateExpr::Double(l, *op, r)),
                }
            }
        }
    }
    pub fn eval_to_primitive(
        &self,
        left_match: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        time: Option<f64>,
        allow_random: bool,
    ) -> Result<PrimitiveTypes, String> {
        match self.evaluate_partial_for_left(left_match, all_spec_counts, time, allow_random) {
            PartialEvaluationExpr::Primitive(p) => Ok(p),
            PartialEvaluationExpr::Incomplete(s) => Err(format!("==> {:?}", s)),
        }
    }

    fn eval_partial(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
        eval_random: bool,
    ) -> PrimitiveOrExpr {
        match self {
            RateExpr::Single(x) => match x.eval_partial(left_match, eval_random) {
                PrimitiveOrExprTerm::Primitive(x) => PrimitiveOrExpr::Primitive(x),
                PrimitiveOrExprTerm::ExprTerm(x) => PrimitiveOrExpr::Expr(RateExpr::Single(x)),
            },
            RateExpr::Double(l, op, term) => {
                match (
                    l.eval_partial(left_match, eval_random),
                    term.eval_partial(left_match, eval_random),
                ) {
                    (PrimitiveOrExprTerm::Primitive(l), PrimitiveOrExprTerm::Primitive(r)) => {
                        PrimitiveOrExpr::Primitive(op.apply(&l, &r).unwrap())
                    }
                    (l, r) => PrimitiveOrExpr::Expr(RateExpr::Double(
                        match l {
                            PrimitiveOrExprTerm::Primitive(x) => RateExprTerm::Primitive(x),
                            PrimitiveOrExprTerm::ExprTerm(x) => x,
                        },
                        op.clone(),
                        match r {
                            PrimitiveOrExprTerm::Primitive(x) => RateExprTerm::Primitive(x),
                            PrimitiveOrExprTerm::ExprTerm(x) => x,
                        },
                    )),
                }
            }
        }
    }
}

impl RateExprTerm {
    #[allow(dead_code)]
    fn get_all_involved(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    ) -> HashSet<EnumeratedSpecies> {
        let mut r = HashSet::new();
        match self {
            RateExprTerm::Primitive(_) => r,
            RateExprTerm::ExpandedAttRef(_) => unreachable!(),
            RateExprTerm::Expr(y) => y.get_all_involved(left_match),
            RateExprTerm::SpecCount(x) => {
                r.insert(left_match.get(&x.idx_on_left).unwrap().clone());
                r
            }
            RateExprTerm::GlobalCount(gc) => match gc {
                GlobalCountEnum::GenericUnspecified(_) => {
                    unreachable!()
                }
                GlobalCountEnum::SpecificEnumerated(x) => x.iter().cloned().collect(),
                GlobalCountEnum::SpecificEnumeratedGuard(x) => {
                    x.iter().fold(r, |mut set, (s, guard)| {
                        set.insert(s.clone());
                        match guard {
                            None => {}
                            Some(x) => set.extend(x.get_all_involved(left_match)),
                        }
                        set
                    })
                }
            },
            StarAttRef(x) => {
                r.insert(left_match.get(&x.species_id.clone()).unwrap().clone());
                r
            }
            RateExprTerm::Condition(condition) => {
                let mut y = condition.guard.get_all_involved(left_match);

                y.extend(condition.result_true.get_all_involved(left_match));
                match &condition.result_false {
                    None => {}
                    Some(p) => y.extend(p.get_all_involved(left_match)),
                }
                y
            }
            RateExprTerm::BuildinFunction(bif) => {
                let tmps: Vec<_> = bif
                    .params
                    .iter()
                    .map(|k| k.get_all_involved(left_match))
                    .collect();
                tmps.iter()
                    .map(|k| k.into_iter().cloned())
                    .flatten()
                    .collect()
            }
        }
    }

    fn eval_partial(
        &self,
        left_match: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
        eval_random: bool, //global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
    ) -> PrimitiveOrExprTerm {
        match self {
            RateExprTerm::Expr(x) => match x.eval_partial(left_match, eval_random) {
                PrimitiveOrExpr::Primitive(x) => PrimitiveOrExprTerm::Primitive(x),
                PrimitiveOrExpr::Expr(x) => {
                    PrimitiveOrExprTerm::ExprTerm(RateExprTerm::Expr(Box::new(x)))
                }
            },
            RateExprTerm::GlobalCount(_x) => PrimitiveOrExprTerm::ExprTerm(self.clone()),

            RateExprTerm::Primitive(x) => PrimitiveOrExprTerm::Primitive(x.clone()),

            _ => match self.evaluate_partial(
                &AllOrSingleLeft::All(left_match),
                None,
                None,
                eval_random,
            ) {
                PartialEvaluationExprTerm::Primitive(x) => PrimitiveOrExprTerm::Primitive(x),
                PartialEvaluationExprTerm::Incomplete(k) => PrimitiveOrExprTerm::ExprTerm(k),
            }, /*RateExprTerm::AttRef(_) => {},
               RateExprTerm::SpecCount(_) => {},
               RateExprTerm::SpecificCount(_) => {},*/
        }
    }
    fn evaluate_partial(
        &self,
        left_match: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        //global_vals: Option<&HashMap<EnumeratedGlobalValue, PrimitiveTypes>>,
        time: Option<f64>,
        eval_random: bool,
    ) -> PartialEvaluationExprTerm {
        match self {
            RateExprTerm::ExpandedAttRef(attref) => match (&attref.species_id, left_match) {
                (_, AllOrSingleLeft::SingleLeft(sl)) => {
                    PartialEvaluationExprTerm::Primitive(sl.get_attribute(&attref.att_name))
                }
                (v, AllOrSingleLeft::All(hm)) => PartialEvaluationExprTerm::Primitive(
                    hm.get(v).unwrap().get_attribute(&attref.att_name),
                ),
                (_v, AllOrSingleLeft::None) => PartialEvaluationExprTerm::Incomplete(self.clone()),
            },
            StarAttRef(attref) => PartialEvaluationExprTerm::Incomplete(StarAttRef(attref.clone())),
            /*match global_vals {
                None => StrOrPrimitive::Str(match (&attref.species_id, left_match) {
                    (None, AllOrSingleLeft::SingleLeft(sl)) => EnumeratedGlobalValue {
                        spec: sl.my_id.as_ref().unwrap().clone(),
                        name: attref.att_name.clone(),
                    }
                    .string_name(),
                    (Some(v), AllOrSingleLeft::All(hm)) => {
                        //println!("\n{:?}\n", hm);
                        EnumeratedGlobalValue {
                            spec: hm.get(v).unwrap().my_id.as_ref().unwrap().clone(),
                            name: attref.att_name.clone(),
                        }
                    }
                    .string_name(),
                    _ => unreachable!(),
                }),
                Some(vals) => StrOrPrimitive::Primitive(
                    vals.get(&match (&attref.species_id, left_match) {
                        (None, AllOrSingleLeft::SingleLeft(sl)) => EnumeratedGlobalValue {
                            spec: (*sl).my_id.as_ref().unwrap().clone(),
                            name: attref.att_name.clone(),
                        },

                        (Some(v), AllOrSingleLeft::All(hm)) => EnumeratedGlobalValue {
                            spec: hm.get(v).unwrap().my_id.as_ref().unwrap().clone(),
                            name: attref.att_name.clone(),
                        },
                        _ => unreachable!(),
                    })
                    .unwrap()
                    .clone(),
                ),
            }*/
            RateExprTerm::Expr(x) => {
                match x.evaluate_partial_for_left(left_match, all_spec_counts, time, eval_random) {
                    PartialEvaluationExpr::Primitive(k) => PartialEvaluationExprTerm::Primitive(k),
                    PartialEvaluationExpr::Incomplete(x) => match x {
                        RateExpr::Single(k) => PartialEvaluationExprTerm::Incomplete(k),
                        RateExpr::Double(_, _, _) => {
                            PartialEvaluationExprTerm::Incomplete(RateExprTerm::Expr(Box::new(x)))
                        }
                    },
                }
            }
            RateExprTerm::SpecCount(sc) => match all_spec_counts {
                Some(all_counts) => PartialEvaluationExprTerm::Primitive(PrimitiveTypes::Integer(
                    match left_match {
                        AllOrSingleLeft::All(x) => {
                            *all_counts.get(&x.get(&sc.idx_on_left).unwrap()).unwrap() as i64
                        }

                        AllOrSingleLeft::SingleLeft(_) => unreachable!(),
                        AllOrSingleLeft::None => unreachable!(),
                    },
                )),
                None => match left_match {
                    AllOrSingleLeft::All(_x) => {
                        PartialEvaluationExprTerm::Incomplete(RateExprTerm::SpecCount(sc.clone()))
                    }
                    /*PartialEvaluationExpr::Str(
                        "#".to_string() + &x.get(&sc.idx_on_left).unwrap().short_string_name(),
                    ),*/
                    AllOrSingleLeft::SingleLeft(_) => unreachable!(),
                    AllOrSingleLeft::None => {
                        PartialEvaluationExprTerm::Incomplete(RateExprTerm::SpecCount(sc.clone()))
                    }
                },
            },
            RateExprTerm::Primitive(p) => PartialEvaluationExprTerm::Primitive(p.clone()),
            RateExprTerm::GlobalCount(gc) => match gc {
                GlobalCountEnum::GenericUnspecified(x) => {
                    PartialEvaluationExprTerm::Incomplete(RateExprTerm::GlobalCount(
                        GlobalCountEnum::GenericUnspecified(x.clone()),
                    ))
                    // unimplemented!("Generic global counts are not yet implemented completly");
                    //PartialEvaluationExpr::Incomplete(RateExpr{ left: RateExprTerm::GlobalCount(), op: None })
                    //PartialEvaluationExpr::Str(format!("Global_Count_of_{}", x.name_in_model_code))
                }
                GlobalCountEnum::SpecificEnumerated(y) => match all_spec_counts {
                    None => {
                        if y.is_empty() {
                            PartialEvaluationExprTerm::Primitive(PrimitiveTypes::Integer(0))
                        } else {
                            PartialEvaluationExprTerm::Incomplete(RateExprTerm::GlobalCount(
                                gc.clone(),
                            ))
                        }
                    }

                    Some(vect) => PartialEvaluationExprTerm::Primitive(PrimitiveTypes::Integer(
                        y.iter().map(|v| vect.get(v).unwrap()).sum::<usize>() as i64,
                    )),
                },
                GlobalCountEnum::SpecificEnumeratedGuard(y) => match all_spec_counts {
                    None => {
                        if y.is_empty() {
                            PartialEvaluationExprTerm::Primitive(PrimitiveTypes::Integer(0))
                        } else {
                            PartialEvaluationExprTerm::Incomplete(RateExprTerm::GlobalCount(
                                gc.clone(),
                            ))
                        }
                    }

                    Some(_vect) => {
                        let evaled_guards = y
                            .iter()
                            .filter_map(|(k, guard)| match guard {
                                None => Some((k.clone(), None)),
                                Some(x) => {
                                    match x.evaluate_partial_for_left(
                                        left_match,
                                        all_spec_counts,
                                        time,
                                        eval_random,
                                    ) {
                                        PartialEvaluationExpr::Primitive(x) => {
                                            if x.as_bool() {
                                                Some((k.clone(), None))
                                            } else {
                                                None
                                            }
                                        }
                                        PartialEvaluationExpr::Incomplete(x) => {
                                            Some((k.clone(), Some(x)))
                                        }
                                    }
                                }
                            })
                            .collect::<Vec<_>>();
                        if evaled_guards.iter().any(|(_, guard)| guard.is_none()) {
                            RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumerated(
                                evaled_guards.into_iter().map(|(k, _)| k).collect(),
                            ))
                            .evaluate_partial(
                                left_match,
                                all_spec_counts,
                                time,
                                eval_random,
                            )
                        } else {
                            PartialEvaluationExprTerm::Incomplete(RateExprTerm::GlobalCount(
                                GlobalCountEnum::SpecificEnumeratedGuard(evaled_guards),
                            ))
                        }
                    }
                },
            },

            RateExprTerm::Condition(cond) => match (
                cond.guard.evaluate_partial_for_left(
                    left_match,
                    all_spec_counts,
                    time,
                    eval_random,
                ),
                cond.result_true.evaluate_partial_for_left(
                    left_match,
                    all_spec_counts,
                    time,
                    eval_random,
                ),
            ) {
                (PartialEvaluationExpr::Primitive(p), inner_true) => match p.as_bool() {
                    true => inner_true.to_term(),
                    false => match &cond.result_false {
                        None => PartialEvaluationExprTerm::Primitive(PrimitiveTypes::Integer(0)),
                        Some(cond2) => cond2
                            .evaluate_partial_for_left(
                                left_match,
                                all_spec_counts,
                                time,
                                eval_random,
                            )
                            .to_term(),
                    },
                },
                (
                    PartialEvaluationExpr::Incomplete(k),
                    PartialEvaluationExpr::Primitive(PrimitiveTypes::Float(f)),
                ) => match f {
                    zero_float if zero_float == 0.0 => {
                        PartialEvaluationExprTerm::Primitive(0.0.into())
                    }
                    inner_prim => {
                        assert_gt!(inner_prim, 0.0);
                        PartialEvaluationExprTerm::Incomplete(RateExprTerm::Condition(Box::new(
                            Condition {
                                guard: k,
                                result_true: RateExpr::Single(RateExprTerm::Primitive(
                                    inner_prim.into(),
                                )),
                                result_false: match &cond.result_false {
                                    None => None,
                                    Some(s) => Some(
                                        s.evaluate_partial_for_left(
                                            left_match,
                                            all_spec_counts,
                                            time,
                                            eval_random,
                                        )
                                        .to_fin_expr(),
                                    ),
                                },
                            },
                        )))
                    }
                },
                (PartialEvaluationExpr::Incomplete(k), PartialEvaluationExpr::Primitive(p)) => {
                    PartialEvaluationExprTerm::Incomplete(RateExprTerm::Condition(Box::new(
                        Condition {
                            guard: k,
                            result_true: RateExpr::Single(RateExprTerm::Primitive(p)),
                            result_false: match &cond.result_false {
                                None => None,
                                Some(s) => Some(
                                    s.evaluate_partial_for_left(
                                        left_match,
                                        all_spec_counts,
                                        time,
                                        eval_random,
                                    )
                                    .to_fin_expr(),
                                ),
                            },
                        },
                    )))
                }
                (
                    PartialEvaluationExpr::Incomplete(guard),
                    PartialEvaluationExpr::Incomplete(inner_true),
                ) => PartialEvaluationExprTerm::Incomplete(RateExprTerm::Condition(Box::new(
                    Condition {
                        guard: guard,
                        result_true: inner_true,
                        result_false: match &cond.result_false {
                            None => None,
                            Some(s) => Some(
                                s.evaluate_partial_for_left(
                                    left_match,
                                    all_spec_counts,
                                    time,
                                    eval_random,
                                )
                                .to_fin_expr(),
                            ),
                        },
                    },
                ))),
            },

            RateExprTerm::BuildinFunction(bf) => {
                if !bf.function.is_constant() && !eval_random {
                    return PartialEvaluationExprTerm::Incomplete(self.clone());
                }
                let evaluated_parameters: Vec<_> = bf
                    .params
                    .iter()
                    .map(|k| {
                        k.evaluate_partial_for_left(left_match, all_spec_counts, time, eval_random)
                    })
                    .collect();
                if evaluated_parameters.iter().all(|k| match k {
                    PartialEvaluationExpr::Primitive(_) => true,
                    PartialEvaluationExpr::Incomplete(_) => false,
                }) && (!bf.function.is_random() || eval_random)
                {
                    PartialEvaluationExprTerm::Primitive(
                        bf.function.eval(
                            &evaluated_parameters
                                .into_iter()
                                .map(|k| match k {
                                    PartialEvaluationExpr::Primitive(p) => p,
                                    PartialEvaluationExpr::Incomplete(_) => {
                                        unreachable!()
                                    }
                                })
                                .collect(),
                            eval_random,
                            time,
                        ),
                    )
                } else {
                    let params = evaluated_parameters
                        .into_iter()
                        .map(|k| match k {
                            PartialEvaluationExpr::Primitive(k) => {
                                RateExpr::Single(RateExprTerm::Primitive(k))
                            }
                            PartialEvaluationExpr::Incomplete(k) => k,
                        })
                        .collect();
                    PartialEvaluationExprTerm::Incomplete(RateExprTerm::BuildinFunction(
                        BuildinFunctionCall {
                            function: bf.function.clone(),
                            params,
                        },
                    ))
                }
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum EnumeratedRate {
    MassActionConstant(f64),
    MassActionExpression(RateExpr),
    ComplexExpression(RateExpr),
}

impl EnumeratedRate {
    fn simplify(&mut self, left: &AllOrSingleLeft) {
        *self = match self {
            EnumeratedRate::MassActionConstant(k) => EnumeratedRate::MassActionConstant(*k),
            EnumeratedRate::MassActionExpression(k) => EnumeratedRate::MassActionExpression(
                match k.evaluate_partial_for_left(left, None, None, false) {
                    PartialEvaluationExpr::Incomplete(k) => k,
                    PartialEvaluationExpr::Primitive(_) => {
                        unreachable!("Schould have been simplified earlier")
                    }
                },
            ),
            EnumeratedRate::ComplexExpression(k) => EnumeratedRate::ComplexExpression(
                match k.evaluate_partial_for_left(left, None, None, false) {
                    PartialEvaluationExpr::Incomplete(k) => k,
                    PartialEvaluationExpr::Primitive(prim) => {
                        RateExpr::Single(RateExprTerm::Primitive(prim))
                    }
                },
            ),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct EnumeratedReaction {
    pub reaction_number_of_prototype: usize,
    pub reaction_parent: Option<CompartmentID>,
    pub rate: EnumeratedRate,
    pub guards_from_left_side: Vec<RateExpr>,
    //pub guard: Vec<RateExpr>,
    pub species_in: BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    pub species_out: Vec<(Option<RateExpr>, EnumeratedSpecies)>,
    pub star_attribute_expressions: Vec<Vec<(OptimizedString, PrimitiveOrExpr)>>,
    //pub global_val_changes: HashMap<EnumeratedGlobalValue, PrimitiveOrExpr>,
    pub invokes_structural_changes: bool,
    pub max_count: Option<usize>,
    //pub moving_rest_solutions: Vec<(CompartmentID, Option<CompartmentID>)>,
    is_not_mass_action: bool,
}

impl EnumeratedReaction {
    fn finalize_output(
        mut self,
        prototype: &Transition,
        reaction_parent: Option<CompartmentID>,
        all_species: &HashMap<SpeciesName, Species>,
    ) -> EnumeratedReaction {
        let mut starr_assignments: Vec<Vec<(OptimizedString, PrimitiveOrExpr)>> = vec![];
        self.species_out = {
            if prototype.has_structural_changes {
                vec![]
            } else {
                prototype
                    .right
                    .iter()
                    .map(|v| {
                        let attrs = new_right_attributes(v, &self.species_in, all_species);
                        let enum_spec = EnumeratedSpecies {
                            species_name: v.species_name.clone(),
                            expanded_attribute_values: attrs.expanded,
                            parent_id: match v.compartment_constraint {
                                None => reaction_parent.clone(),
                                Some(x) => Some(
                                    self.species_in
                                        .get(
                                            &prototype.right[x]
                                                .corresponding_on_left
                                                .as_ref()
                                                .expect("Corresponding left not found!"),
                                        )
                                        .as_ref()
                                        .unwrap()
                                        .my_id
                                        .as_ref()
                                        .expect("Corresponding had no ID")
                                        .clone(),
                                ),
                            },

                            my_id: match &v.corresponding_on_left {
                                None => None,
                                Some(left_idx) => self
                                    .species_in
                                    .get(left_idx)
                                    .as_ref()
                                    .expect("not found on left")
                                    .my_id
                                    .clone(), // may be none, for not compartment
                            },
                            //my_flattend_idx: None,
                        }
                        .sort();
                        assert_eq!(
                            attrs.starr.len(),
                            all_species[&v.species_name].not_expanding_attributes.len()
                        );
                        let my_amt = match &v.amount {
                            CreationAmt::StaticNum(_) => v.amount.clone(),
                            CreationAmt::DynExpr(k) => {
                                match k.eval_partial(&self.species_in, false) {
                                    PrimitiveOrExpr::Primitive(p) => {
                                        CreationAmt::StaticNum(match p {
                                            PrimitiveTypes::Integer(i) => {
                                                assert!(i >= 0);
                                                i as usize
                                            }
                                            _ => unreachable!(),
                                        })
                                    }
                                    PrimitiveOrExpr::Expr(x) => CreationAmt::DynExpr(x),
                                }
                            }
                        };
                        match my_amt {
                            CreationAmt::StaticNum(k) => {
                                for _ in 0..k {
                                    starr_assignments.push(attrs.starr.clone());
                                }
                                iter::repeat((None, enum_spec)).take(k)
                            }
                            CreationAmt::DynExpr(amt_expr) => {
                                starr_assignments.push(attrs.starr);
                                iter::repeat((Some(amt_expr), enum_spec)).take(1)
                            }
                        }
                    })
                    .flatten()
                    .collect()
            }
        };

        self.star_attribute_expressions = {
            if prototype.has_structural_changes {
                vec![]
            } else {
                starr_assignments
            }
        };
        self
    }
    fn partial_guard_eval(mut self) -> Option<EnumeratedReaction> {
        /*if self
            .guard
            .iter()
            .any(|v| match v.eval_partial(&self.species_in, false) {
                PrimitiveOrExpr::Primitive(k) => !k.as_bool(),
                PrimitiveOrExpr::Expr(_x) => false,
            })
        {
            return None;
        }*/
        match &self.rate {
            EnumeratedRate::MassActionConstant(x) => {
                if *x == 0.0 {
                    return None;
                }
            }
            EnumeratedRate::MassActionExpression(x) => {
                match x.evaluate_partial_for_left(
                    &AllOrSingleLeft::All(&self.species_in),
                    None,
                    None,
                    false,
                ) {
                    PartialEvaluationExpr::Primitive(x) => {
                        if x.as_float().unwrap() <= 0.0 {
                            if x.as_float().unwrap() < 0. {
                                panic!("Negative rate!");
                            }
                            return None;
                        }
                    }
                    PartialEvaluationExpr::Incomplete(incomp) => {
                        self.rate = EnumeratedRate::MassActionExpression(incomp);
                    }
                }
            }
            EnumeratedRate::ComplexExpression(x) => {
                match x.evaluate_partial_for_left(
                    &AllOrSingleLeft::All(&self.species_in),
                    None,
                    None,
                    false,
                ) {
                    PartialEvaluationExpr::Primitive(x) => {
                        if x.as_float().unwrap() <= 0.0 {
                            if x.as_float().unwrap() < 0. {
                                panic!("Negative rate!");
                            }
                            return None;
                        }
                    }
                    PartialEvaluationExpr::Incomplete(incomp) => {
                        self.rate = EnumeratedRate::ComplexExpression(incomp);
                    }
                }
            }
        }
        /* self.guard = self
        .guard
        .iter()
        .filter_map(|v| match v.eval_partial(&self.species_in, false) {
            PrimitiveOrExpr::Primitive(k) => {
                assert!(k.as_bool());
                None
            }
            PrimitiveOrExpr::Expr(x) => Some(x),
        })
        .collect();*/
        Some(self)
    }

    fn print_rate(
        &self,
        name_conv: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> String {
        match &self.rate {
            EnumeratedRate::MassActionConstant(k) => format!(
                "{} {}",
                k,
                self.species_in
                    .values()
                    .map(|v| " * #".to_string()
                        + &*v.short_string_name(name_conv, att_name_conversion))
                    .join("")
            )
            .to_string(),
            EnumeratedRate::ComplexExpression(y) => y
                .evaluate_partial_for_left(
                    &AllOrSingleLeft::All(&self.species_in),
                    None,
                    None,
                    false,
                )
                .to_string(att_name_conversion),
            //.as_str(),
            EnumeratedRate::MassActionExpression(ex) => {
                format!(
                    "{}* {}",
                    ex.evaluate_partial_for_left(
                        &AllOrSingleLeft::All(&self.species_in),
                        None,
                        None,
                        false
                    )
                    .to_string(att_name_conversion)
                    .as_str(),
                    self.species_in
                        .values()
                        .map(|v| " * #".to_string()
                            + &*v.short_string_name(name_conv, att_name_conversion))
                        .join("")
                )
                /*format!(
                    "{}{} {}",
                    guard_string,
                    ex.eval_for_left_str(&AllOrSingleLeft::All(&self.species_in), None, None, None)
                        .to_string()
                        .as_str(),
                    self.species_in
                        .values()
                        .map(|v| " * #".to_string() + &*v.string_name())
                        .join("")
                )
                    .to_string()*/
            }
        }
    }
    fn string_name_reac(
        &self,
        all_species: &Vec<EnumeratedSpecies>,
        name_conv: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> String {
        //println!(" REACTION: {:#?}", self);
        let side_effect = "".to_string();
        /* if self.global_val_changes.is_empty() {
            side_effect = "".to_string();
        } else {
            side_effect = self
                .global_val_changes
                .iter()
                .map(|(a, b)| {
                    format!(
                        "| {} = {} ",
                        a.string_name(),
                        match b {
                            PrimitiveOrExpr::Primitive(x) =>
                                StrOrPrimitive::Primitive((*x).clone()),
                            PrimitiveOrExpr::Expr(x) => x.eval_for_left_str(
                                &AllOrSingleLeft::All(&self.species_in),
                                None,
                                None,
                                None
                            ),
                        }
                    )
                })
                .join("");
        }*/
        format!(
            "{} -> {} {}@ {} {};",
            self.species_in
                .values()
                .map(|v| v.string_name(all_species, name_conv, att_name_conversion))
                .join(" + "),
            self.species_out
                .iter()
                .map(|(_, v)| v.string_name(all_species, name_conv, att_name_conversion))
                .join(" + "),
            side_effect,
            self.print_rate(name_conv, att_name_conversion),
            match self.invokes_structural_changes {
                true => ">>INVOKE STRUCTURAL CHANGES<<",
                false => "",
            },
        )
    }
}

fn get_id_in_initial(initial: &Vec<SpeciesRight>, idx: usize) -> CompartmentID {
    let species_looking_for = initial[idx].species_name.clone();
    let count = initial
        .iter()
        .take(idx)
        .filter(|v2| v2.species_name == species_looking_for)
        .count() as isize;
    CompartmentID {
        number: count,
        species: species_looking_for.clone(),
        /*next_parent: match initial[idx].compartment_constraint {
            -1 => None,
            x => Some(Box::new(get_id_in_initial(initial, x as usize))),
        },*/
    }
}

impl EnumeratedSpecies {
    fn first_part_of_string_name(
        &self,
        name_conv: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> String {
        format!(
            "{}{}{}{}{}",
            name_conv.get_rev(&self.species_name),
            //self.species_name.clone(),
            match &self.my_id {
                None => "".to_string(),
                Some(x) => format!(
                    "{}",
                    match x.number {
                        0 => "".to_string(),
                        n => format!("{{{}}}", n).to_string(),
                    }
                ),
            },
            match self.expanded_attribute_values.is_empty() {
                true => format!(""),
                false => format!("("),
            },
            self.expanded_attribute_values
                .iter()
                .map(|(name, val)| format!(
                    "{}={}",
                    name.get_original_string(att_name_conversion),
                    val.to_string(att_name_conversion)
                ))
                .join(";"),
            match self.expanded_attribute_values.is_empty() {
                true => format!(""),
                false => format!(")"),
            }
        )
    }
    pub fn short_string_name(
        &self,
        name_conv: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> String {
        format!(
            "{:?}{}",
            self.first_part_of_string_name(name_conv, att_name_conversion),
            match &self.parent_id {
                None => "".to_string(),
                Some(x) => format!(" in {}{{{}}}", x.species, x.number),
            }
        )
    }
    pub fn string_name(
        &self,
        all_counts: &Vec<EnumeratedSpecies>,
        name_conv: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> String {
        //println!("{:#?}",all_counts);
        //println!("looking for: {:#?}",self);
        format!(
            "{}{}",
            self.first_part_of_string_name(name_conv, att_name_conversion),
            match &self.parent_id {
                None => "".to_string(),
                Some(x) => format!(
                    " in {}",
                    match all_counts
                        .iter()
                        .find(|k| k.my_id.is_some() && k.my_id.as_ref().unwrap() == x)
                    {
                        None => {
                            panic!("Something? \n{:?} \nnot in {:#?}", self, all_counts)
                            //                            "SOMETHING".to_string()
                        }
                        Some(x) => {
                            x.string_name(all_counts, name_conv, att_name_conversion)
                        }
                    }
                ),
            }
        )
    }
    fn single_from_species_right_initial(
        v: &SpeciesRight,
        model: &Model,
        compartment_counter: &mut HashMap<SpeciesName, usize>,
        aggegated_counts: &Vec<usize>,
    ) -> EnumeratedSpecies {
        {
            let parent_id = match v.compartment_constraint {
                None => None,
                Some(x) => {
                    debug_assert!(
                        model
                            .species
                            .get(&model.inital[x].species_name)
                            .unwrap()
                            .is_compartment
                    );
                    debug_assert_eq!(
                        get_id_in_initial(&model.inital, x).number as usize,
                        aggegated_counts[x]
                    );
                    Some(CompartmentID {
                        number: aggegated_counts[x] as isize,
                        species: model.inital[x].species_name.clone(),
                    })
                    //Some(get_id_in_initial(&model.inital, x))
                }
            };

            EnumeratedSpecies {
                //my_flattend_idx: None,
                species_name: v.species_name.clone(),
                expanded_attribute_values: v
                    .expanded_attribute_assignments
                    .iter()
                    .map(|(n, v)| {
                        (
                            n.clone(),
                            v.eval_to_primitive(&AllOrSingleLeft::None, None, None, true)
                                .unwrap(),
                        )
                    })
                    .collect(),
                parent_id: parent_id.clone(),
                my_id: match model.species.get(&v.species_name).unwrap().is_compartment {
                    true => match compartment_counter.get_mut(&v.species_name) {
                        None => {
                            compartment_counter.insert(v.species_name.clone(), 1);
                            Some(CompartmentID {
                                number: 0,
                                species: v.species_name.clone(),
                                //next_parent: boxed_parent,
                            })
                        }
                        Some(k) => {
                            *k += 1;
                            Some(CompartmentID {
                                number: *k as isize - 1,
                                species: v.species_name.clone(),
                                //next_parent: boxed_parent,
                            })
                        }
                    },
                    false => None,
                },
            }
            .sort()
        }
    }
}

#[derive(Debug)]
pub enum AllOrSingleLeft<'a> {
    All(&'a BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>),
    SingleLeft(&'a EnumeratedSpecies),
    None,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum PrimitiveOrExpr {
    Primitive(PrimitiveTypes),
    Expr(RateExpr),
}

enum PrimitiveOrExprTerm {
    Primitive(PrimitiveTypes),
    ExprTerm(RateExprTerm),
}

impl RateExpr {
    pub fn eval_partial_to_bool(
        &self,
        all_or_single_left: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        time: Option<f64>,
        allow_random: bool,
    ) -> Option<bool> {
        match self.eval_to_primitive(all_or_single_left, all_spec_counts, time, allow_random) {
            Ok(k) => match k {
                PrimitiveTypes::Boolean(b) => Some(b),
                _ => {
                    unreachable!()
                }
            },
            Err(_) => None,
        }
    }
}

impl LogicalConstraint {
    pub fn eval_if_available(
        &self,
        all_or_single_left: &AllOrSingleLeft,
        all_spec_counts: Option<&HashMap<EnumeratedSpecies, usize>>,
        time: Option<f64>,
    ) -> Option<bool> {
        let l = self
            .left
            .eval_to_primitive(all_or_single_left, all_spec_counts, time, false);
        let l = match l {
            Ok(k) => k,
            Err(_y) => {
                return None;
            }
        };
        let r = self
            .right
            .eval_to_primitive(all_or_single_left, all_spec_counts, time, false);
        let r = match r {
            Ok(k) => k,
            Err(_y) => {
                return None;
            }
        };

        Some(match (l, r) {
            (PrimitiveTypes::Integer(l), PrimitiveTypes::Integer(r)) => self.op.apply_all(&l, &r),
            (PrimitiveTypes::Float(l), PrimitiveTypes::Float(r)) => self.op.apply_all(&l, &r),
            (PrimitiveTypes::Float(l), PrimitiveTypes::Integer(r)) => {
                self.op.apply_all(&l, &(r as f64))
            }
            (PrimitiveTypes::Integer(l), PrimitiveTypes::Float(r)) => {
                self.op.apply_all(&(l as f64), &r)
            }
            (PrimitiveTypes::Boolean(l), PrimitiveTypes::Boolean(r)) => self.op.apply_eq(&l, &r),
            (PrimitiveTypes::String(l), PrimitiveTypes::String(r)) => self.op.apply_eq(&l, &r),
            _ => unreachable!(),
        })
    }

    /*fn _eval_partial(
        &self,
        matched_left: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    ) -> BoolOrConstraint {
        let l = self.left.eval_partial(matched_left);
        let r = self.right.eval_partial(matched_left);
        match (l, r) {
            (PrimitiveOrExpr::Primitive(l), PrimitiveOrExpr::Primitive(r)) => {
                BoolOrConstraint::Bool(match (l, r) {
                    (PrimitiveTypes::Integer(l), PrimitiveTypes::Integer(r)) => {
                        self.op.apply_all(&l, &r)
                    }
                    (PrimitiveTypes::Float(l), PrimitiveTypes::Float(r)) => {
                        self.op.apply_all(&l, &r)
                    }
                    (PrimitiveTypes::Float(l), PrimitiveTypes::Integer(r)) => {
                        self.op.apply_all(&l, &(r as f64))
                    }
                    (PrimitiveTypes::Integer(l), PrimitiveTypes::Float(r)) => {
                        self.op.apply_all(&(l as f64), &r)
                    }
                    (PrimitiveTypes::Boolean(l), PrimitiveTypes::Boolean(r)) => {
                        self.op.apply_eq(&l, &r)
                    }
                    (PrimitiveTypes::String(l), PrimitiveTypes::String(r)) => {
                        self.op.apply_eq(&l, &r)
                    }
                    _ => unreachable!(),
                })
            }
            (l, r) => BoolOrConstraint::Constraint(LogicalConstraint {
                left: match l {
                    PrimitiveOrExpr::Primitive(x) => RateExpr {
                        left: RateExprTerm::Primitive(x),
                        op: None,
                    },
                    PrimitiveOrExpr::Expr(x) => x,
                },
                op: self.op.clone(),
                right: match r {
                    PrimitiveOrExpr::Primitive(x) => RateExpr {
                        left: RateExprTerm::Primitive(x),
                        op: None,
                    },
                    PrimitiveOrExpr::Expr(x) => x,
                },
            }),
        }
    }*/
}

/*#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct EnumeratedGlobalValue {
    pub spec: CompartmentID,
    pub name: String,
}

impl EnumeratedGlobalValue {
    pub fn string_name(&self) -> String {
        self.spec.to_string() + "_" + &self.name
    }
}*/

#[derive(PartialEq, Ord, Hash, Serialize, Deserialize, PartialOrd, Eq, Clone, Debug)]
pub struct OptimizedString {
    pub name: usize,
    #[cfg(debug_assertions)]
    pub name_orig: String,
}
impl OptimizedString {
    pub fn get_original_string(
        &self,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> String {
        att_name_conversion
            .iter()
            .find(|(_s, name)| *name == self)
            .unwrap()
            .0
            .clone()
    }

    #[cfg(debug_assertions)]
    pub fn new(num: usize, opt_name: &String) -> OptimizedString {
        OptimizedString {
            name: num,
            name_orig: opt_name.clone(),
        }
    }
    #[cfg(not(debug_assertions))]
    pub fn new(num: usize, _opt_name: &String) -> OptimizedString {
        OptimizedString { name: num }
    }

    #[cfg(debug_assertions)]
    pub fn dummy() -> OptimizedString {
        OptimizedString {
            name: usize::MAX,
            name_orig: "__not_found__".to_string(),
        }
    }
    #[cfg(not(debug_assertions))]
    pub fn dummy() -> OptimizedString {
        OptimizedString { name: usize::MAX }
    }

    #[cfg(debug_assertions)]
    pub fn dummy_wl(line: &str) -> OptimizedString {
        OptimizedString {
            name: usize::MAX,
            name_orig: "__not_found__at_".to_string() + line,
        }
    }
    #[cfg(not(debug_assertions))]
    pub fn dummy_wl(_: &str) -> OptimizedString {
        OptimizedString { name: usize::MAX }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum SpeciesAmt {
    NumOnly(usize),
    CompartmentWithStar(bool, StarAttributeValues),
    #[cfg(feature = "hybrid_att")]
    NotExpanded(Vec<(usize, StarAttributeValues)>),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StarAttributeValues {
    pub names: Vec<OptimizedString>, // TODO optimize not saving names every time
    pub values: Vec<PrimitiveTypes>,
}

impl StarAttributeValues {
    pub fn is_sorted(&self) -> bool {
        debug_assert_eq!(self.names.len(), self.values.len());
        self.names.windows(2).all(|w| w[0] <= w[1])
    }
    pub fn get_by_str(&self, name: &OptimizedString) -> Option<&PrimitiveTypes> {
        for (n, v) in self.names.iter().zip(self.values.iter()) {
            if n.name == name.name {
                return Some(v);
            }
        }
        None
    }

    pub fn sort(self) -> StarAttributeValues {
        Self::from(
            self.names
                .into_iter()
                .zip(self.values.into_iter())
                .collect(),
        )
    }
    pub fn from(mut data: Vec<(OptimizedString, PrimitiveTypes)>) -> StarAttributeValues {
        data.sort_by(|(s1, _), (s2, _)| s1.cmp(s2));
        let mut res = StarAttributeValues {
            names: vec![],
            values: vec![],
        };
        for (name, val) in data {
            res.names.push(name);
            res.values.push(val);
        }
        res
    }
}

impl cmp::PartialEq for StarAttributeValues {
    fn eq(&self, other: &Self) -> bool {
        debug_assert!(self.is_sorted());
        debug_assert!(other.is_sorted());
        assert_eq!(self.names.len(), other.names.len());
        assert_eq!(self.names.len(), other.values.len());
        assert_eq!(self.names.len(), self.values.len());
        for (name1, name2, value1, value2) in itertools::izip!(
            self.names.iter(),
            other.names.iter(),
            self.values.iter(),
            other.values.iter()
        ) {
            assert_eq!(name1, name2);
            if value1 != value2 {
                return false;
            }
        }
        return true;
    }
}

impl std::fmt::Display for SpeciesAmt {
    fn fmt(&self, _f: &mut Formatter<'_>) -> fmt::Result {
        todo!()
    }
}

impl SpeciesAmt {
    pub fn to_not_expanded(&self) -> Option<Vec<(usize, StarAttributeValues)>> {
        match &self {
            SpeciesAmt::NumOnly(_) => None,
            #[cfg(feature = "hybrid_att")]
            SpeciesAmt::NotExpanded(x) => Some(x.clone()),
            SpeciesAmt::CompartmentWithStar(_, k) => Some(vec![(1, k.clone())]),
        }
    }
    pub fn total_amt(&self) -> usize {
        match self {
            SpeciesAmt::NumOnly(x) => *x,
            #[cfg(feature = "hybrid_att")]
            SpeciesAmt::NotExpanded(k) => k.iter().map(|(num, _)| num).sum(),
            SpeciesAmt::CompartmentWithStar(is_present, _) => {
                if *is_present {
                    1
                } else {
                    0
                }
            }
        }
    }
    pub fn is_not_zero(&self) -> bool {
        let r = match self {
            SpeciesAmt::NumOnly(x) => *x > 0,
            #[cfg(feature = "hybrid_att")]
            SpeciesAmt::NotExpanded(k) => k.len() > 0 || (!k.is_empty() && k[0].0 > 0),
            SpeciesAmt::CompartmentWithStar(is_present, _) => *is_present,
        };
        if cfg!(debug_assertions) {
            assert_eq!(self.total_amt() > 0, r);
        }
        r
    }
    pub fn to_string(&self) -> String {
        match self {
            SpeciesAmt::NumOnly(x) => format!("{x}"),
            #[cfg(feature = "hybrid_att")]
            SpeciesAmt::NotExpanded(y) => format!("{:?}", y),
            SpeciesAmt::CompartmentWithStar(k, b) => {
                format!("{k}x{:?}", b)
            }
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FlatModel {
    pub model: Model,
    pub species: Vec<EnumeratedSpecies>,
    pub reactions: Vec<EnumeratedReaction>,
    pub initial_species: HashMap<EnumeratedSpecies, SpeciesAmt>,
    pub observations: Vec<(String, RateExpr)>,
}

impl Default for FlatModel {
    fn default() -> Self {
        FlatModel {
            model: Model {
                species: Default::default(),
                transitions: vec![],
                inital: vec![],
                observations: vec![],
                parameters: Default::default(),
                species_name_conversion: SpeciesNameConverter {
                    map: Default::default(),
                },
                string_conversion: Default::default(),
                original_file_name: "".to_string(),
            },
            species: vec![],
            reactions: vec![],
            initial_species: Default::default(),
            observations: vec![],
        }
    }
}

#[derive(Debug)]
pub struct NewRightAttributes {
    pub(crate) expanded: Vec<(OptimizedString, PrimitiveTypes)>,
    pub starr: Vec<(OptimizedString, PrimitiveOrExpr)>,
}

pub fn new_right_attributes(
    s_right: &SpeciesRight,
    left_side: &BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    all_species: &HashMap<SpeciesName, Species>,
) -> NewRightAttributes {
    //println!("\nAttributes for {:?}", s_right);
    let mut ret: Vec<(OptimizedString, PrimitiveTypes)> = s_right
        .expanded_attribute_assignments
        .iter()
        .map(|(n, expr)| {
            (
                n.clone(),
                expr.eval_to_primitive(&AllOrSingleLeft::All(left_side), None, None, true)
                    .unwrap(),
            )
        })
        .collect();
    if ret.len()
        != all_species
            .get(&s_right.species_name)
            .unwrap()
            .expanding_attributes
            .len()
    {
        //unreachable!("New species can not have a corresponding left");
        for att in all_species
            .get(&s_right.species_name)
            .unwrap()
            .expanding_attributes
            .keys()
        {
            if ret
                .iter()
                .map(|(a, _b)| a)
                .filter(|name| *name == att)
                .count()
                == 0
            {
                //print!("\nleft: {:#?}",left_side);
                ret.push((
                    att.clone(),
                    left_side
                        .get(
                            s_right
                                .corresponding_on_left
                                .as_ref()
                                .expect("Attribute of new species not defined?"),
                        )
                        .unwrap()
                        .get_attribute(att),
                ))
            }
        }
        assert_eq!(
            ret.len(),
            all_species
                .get(&s_right.species_name)
                .unwrap()
                .expanding_attributes
                .len()
        );
    }

    let mut global_asign: Vec<_> = s_right
        .star_attribute_assignment
        .iter()
        .map(|(n, expr)| (n.clone(), expr.eval_partial(left_side, false)))
        .collect();

    match &s_right.corresponding_on_left {
        None => {
            if global_asign.len()
                != all_species
                    .get(&s_right.species_name)
                    .unwrap()
                    .not_expanding_attributes
                    .len()
            {
                panic!("It appears not all attributes for '{:?}' have been speciefied in structure changing transition!", s_right.species_name);
            }
            //print!("glob assgn: {:?}", global_asign);
            assert_eq!(
                ret.len(),
                all_species
                    .get(&s_right.species_name)
                    .unwrap()
                    .expanding_attributes
                    .len()
            );
            assert_eq!(
                global_asign.len(),
                all_species
                    .get(&s_right.species_name)
                    .unwrap()
                    .not_expanding_attributes
                    .len()
            );
        }
        Some(left) => {
            for (att, _) in all_species
                .get(&s_right.species_name)
                .unwrap()
                .not_expanding_attributes
                .iter()
            {
                if global_asign.iter().find(|(n, _)| n == att).is_none() {
                    global_asign.push((
                        att.clone(),
                        PrimitiveOrExpr::Expr(RateExpr::Single(StarAttRef(AttributeRef {
                            species_id: left.clone(),
                            att_name: att.clone(),
                        }))),
                    ))
                }
            }
        }
    }

    //println!("Right: {:?}", global_asign);

    NewRightAttributes {
        expanded: ret,
        starr: global_asign,
    }
}

impl FlatModel {
    pub fn from_model(model: Model) -> FlatModel {
        //println!("species: {:#?}", model.inital);
        let mut compartment_counter = HashMap::new();
        let mut compartment_counter_2: HashMap<SpeciesName, usize> = HashMap::new();
        let mut initial_amts: HashMap<EnumeratedSpecies, SpeciesAmt> = HashMap::new();
        let aggegated_counts: Vec<usize> = model
            .inital
            .iter()
            .map(|k| match compartment_counter_2.get_mut(&k.species_name) {
                Some(k) => {
                    *k += 1;
                    *k
                }
                None => {
                    compartment_counter_2.insert(k.species_name.clone(), 0);
                    0
                }
            })
            .collect();
        model
            .inital
            .iter()
            .map(|k: &SpeciesRight| {
                (
                    EnumeratedSpecies::single_from_species_right_initial(
                        &k,
                        &model,
                        &mut compartment_counter,
                        &aggegated_counts,
                    ),
                    match k.star_attribute_assignment.is_empty() {
                        true => SpeciesAmt::NumOnly(
                            k.amount
                                .as_num()
                                .expect("Initial amt has expression? Should not be possible..."),
                        ),
                        false => {
                            let att_vals = StarAttributeValues::from(
                                k.star_attribute_assignment
                                    .iter()
                                    .map(|(name, v)| {
                                        (
                                            name.clone(),
                                            v.eval_to_primitive(
                                                &AllOrSingleLeft::None,
                                                None,
                                                None,
                                                false,
                                            )
                                            .unwrap(),
                                        )
                                    })
                                    .collect(),
                            );
                            if model.species[&k.species_name].is_compartment {
                                assert!(k.amount.is_one());
                                SpeciesAmt::CompartmentWithStar(true, att_vals)
                            } else {
                                #[cfg(feature = "hybrid_att")]
                                {
                                    SpeciesAmt::NotExpanded(vec![(
                                        k.amount
                                            .as_num()
                                            .expect("initial solution amt is expression"),
                                        att_vals,
                                    )])
                                }
                                #[cfg(not(feature = "hybrid_att"))]
                                panic!("You need to activate the gated feature")
                            }
                        }
                    },
                )
            })
            .for_each(|(s, amt): (EnumeratedSpecies, SpeciesAmt)| {
                match initial_amts.insert(s.clone(), amt.clone()) {
                    None => {}
                    Some(previous_amt) => match (previous_amt, amt) {
                        (SpeciesAmt::NumOnly(k1), SpeciesAmt::NumOnly(k2)) => {
                            initial_amts
                                .insert(s, SpeciesAmt::NumOnly(k1 + k2))
                                .unwrap();
                        }
                        #[cfg(feature = "hybrid_att")]
                        (
                            SpeciesAmt::NotExpanded(mut part_previous),
                            SpeciesAmt::NotExpanded(part2),
                        ) => {
                            for (num, val) in part2 {
                                match part_previous.iter_mut().find(|(_n1, v1)| *v1 == val) {
                                    None => {
                                        part_previous.push((num, val));
                                    }
                                    Some((found_num, _)) => {
                                        *found_num += num;
                                    }
                                }
                            }
                            initial_amts.insert(s, SpeciesAmt::NotExpanded(part_previous));
                        }
                        _ => unreachable!(),
                    },
                }
            });
        assert_eq!(
            model
                .inital
                .iter()
                .map(|s| s.amount.as_num().expect("initial amt not be expression"))
                .sum::<usize>(),
            initial_amts
                .iter()
                .map(|(_, k)| k.total_amt())
                .sum::<usize>()
        );
        /*let species: Vec<(usize, EnumeratedSpecies)> = model
        .inital
        .iter()
        .map(|v| {
            (
                v.amount,
                EnumeratedSpecies::single_from_species_right_initial(
                    v,
                    &model,
                    &mut compartment_counter,
                ),
            )
        })
        .collect();

        //let mut global_values = HashMap::new();

        model
            .inital
            .iter()
            .zip(species.iter())
            .for_each(|(m, (_amt, en))| {
                assert_eq!(m.species_name, en.species_name);
                m.not_expanded_attribute_assignment
                    .iter()
                    .for_each(|(n, v)| {
                        assert!(model.species.get(&*m.species_name).unwrap().is_compartment);
                        assert!(global_values
                            .insert(
                                EnumeratedGlobalValue {
                                    spec: en.my_id.as_ref().unwrap().clone(),
                                    name: n.clone(),
                                },
                                v.eval_to_primitive(&AllOrSingleLeft::None, None, None, None)
                                    .unwrap(),
                            )
                            .is_none());
                    });
            });*/
        FlatModel {
            species: initial_amts.iter().map(|(b, _)| b).cloned().collect(),
            initial_species: initial_amts,
            observations: model.observations.clone(),
            /*.iter()
            .map(|(n, k)| {
                (
                    n.clone(),
                    match k.evaluate_partial_for_left(&AllOrSingleLeft::None, None, None, false){
                        PartialEvaluationExpr::Primitive(x) => {RateExpr::Single(RateExprTerm::Primitive(x))}
                        PartialEvaluationExpr::Incomplete(x) => {x}
                    },
                )
            })
            .collect(),*/
            reactions: vec![],
            model: model,
            //global_values: global_values,
        }
    }

    pub fn replace_global_counts(&mut self) {
        let mut enumerated_by_species: HashMap<SpeciesName, Vec<EnumeratedSpecies>> =
            HashMap::new();
        for s in self.species.iter() {
            match enumerated_by_species.get_mut(&s.species_name) {
                None => {
                    enumerated_by_species.insert(s.species_name.clone(), vec![s.clone()]);
                }
                Some(d) => d.push(s.clone()),
            }
        }
        let global_lookup: HashMap<CompartmentID, usize> = self
            .species
            .iter()
            .enumerate()
            .filter_map(|(idx, s)| match &s.my_id {
                None => None,
                Some(x) => Some((x.clone(), idx)),
            })
            .collect();
        let all_spec = self.species.clone();
        for (_, obs) in self.observations.iter_mut() {
            *obs = obs.clone().replace_global_counts_in_rate_expr(
                &enumerated_by_species,
                &all_spec,
                None,
                &global_lookup,
            );
        }
        self.reactions.iter_mut().for_each(|r| {
            for (a, _) in r.species_out.iter_mut() {
                match a {
                    None => {}
                    Some(amt) => {
                        *amt = amt.clone().replace_global_counts_in_rate_expr(
                            &enumerated_by_species,
                            &all_spec,
                            Some(&r.species_in),
                            &global_lookup,
                        );
                    }
                }
            }
            r.rate = match &r.rate {
                EnumeratedRate::MassActionConstant(y) => EnumeratedRate::MassActionConstant(*y),
                EnumeratedRate::MassActionExpression(x) => EnumeratedRate::MassActionExpression(
                    x.clone().replace_global_counts_in_rate_expr(
                        &enumerated_by_species,
                        &all_spec,
                        Some(&r.species_in),
                        &global_lookup,
                    ),
                ),
                EnumeratedRate::ComplexExpression(x) => {
                    EnumeratedRate::ComplexExpression(x.clone().replace_global_counts_in_rate_expr(
                        &enumerated_by_species,
                        &all_spec,
                        Some(&r.species_in),
                        &global_lookup,
                    ))
                }
            }
        });
    }

    pub fn expantion_step(
        &mut self,
        previous_new_species: HashSet<EnumeratedSpecies>,
    ) -> HashSet<EnumeratedSpecies> {
        let reac_prototypes = self.model.transitions.clone();
        if cfg!(debug_assertions) {
            for r in self.reactions.iter() {
                debug_assert!(r
                    .species_in
                    .iter()
                    .all(|(_, k)| !previous_new_species.contains(k)));
            }
        }

        let new_reacs: Vec<_> = self
            .model
            .transitions
            .iter()
            //.par_iter()
            .map(|t| MatchingIterator::new(&self.species, &t.left, &previous_new_species))
            .enumerate()
            //.filter(|(_n, k)| k.is_some())
            // find matches based on species and attributes
            .map(|(n, matches)| {
                matches
                    .filter(|k| {
                        if k.is_empty() {
                            self.reactions.is_empty()
                        } else {
                            true
                        }
                    })
                    .map(move |v| (v, n))
                    .filter_map(|(matched_left, n2)| {
                        let reaction_parent = match matched_left.get(
                            &self.model.transitions[n2]
                                .left
                                .iter()
                                .map(|v| &v.id)
                                .next()
                                .unwrap_or(&SpeciesLeftIdentifier::new_dummy()), //new("__DUMMY !! DUMMY")),
                        ) {
                            None => None,
                            Some(x) => x.parent_id.clone(),
                        };
                        assert_eq!(matched_left.len(), reac_prototypes[n2].left.len());

                        EnumeratedReaction {
                            reaction_number_of_prototype: n2,
                            reaction_parent: reaction_parent.clone(),
                            species_out: vec![],

                            rate: match !reac_prototypes[n2].is_not_mass_action {
                                true => {
                                    match reac_prototypes[n2].rate_expr.eval_to_primitive(
                                        &AllOrSingleLeft::All(&matched_left),
                                        None,
                                        None,
                                        false,
                                    ) {
                                        Ok(r) => {
                                            let r = r.as_float().unwrap();
                                            assert!(r >= 0.);
                                            EnumeratedRate::MassActionConstant(r)
                                        }
                                        Err(_y) => EnumeratedRate::MassActionExpression(
                                            reac_prototypes[n2].rate_expr.clone(),
                                        ),
                                    }
                                }
                                false => EnumeratedRate::ComplexExpression(
                                    match reac_prototypes[n2]
                                        .rate_expr
                                        .clone()
                                        .eval_partial(&matched_left, false)
                                    {
                                        PrimitiveOrExpr::Primitive(k) => {
                                            RateExpr::Single(RateExprTerm::Primitive(k))
                                        }
                                        PrimitiveOrExpr::Expr(x) => x,
                                    },
                                ),
                            },
                            /*guard: reac_prototypes[n2]
                            .guard
                            .iter()
                            .cloned()
                            /*.chain( // TODO check where guards are included
                                reac_prototypes[n2]
                                    .left
                                    .iter()
                                    .map(|p| p.attribute_constraint.clone())
                                    .flatten(),
                            )*/
                            .collect(),*/
                            invokes_structural_changes: reac_prototypes[n2].has_structural_changes,
                            star_attribute_expressions: vec![],
                            species_in: matched_left,
                            is_not_mass_action: reac_prototypes[n2].is_not_mass_action,
                            //global_val_changes: global_assign,
                            max_count: reac_prototypes[n2].max_fire,
                            guards_from_left_side: vec![],
                        }
                        .partial_guard_eval()
                        .map(|k| {
                            k.finalize_output(
                                &reac_prototypes[n2],
                                reaction_parent,
                                &self.model.species,
                            )
                        })
                    })
            })
            //.flatten_iter()
            .flatten()
            .inspect(|r| assert_eq!(r.species_out.len(), r.star_attribute_expressions.len()))
            .collect();
        //self.reactions.extend(new_reacs.into_iter());
        if cfg!(debug_assertions) {
            for new_r in new_reacs.iter() {
                debug_assert!(!self.reactions.contains(new_r));
            }
        }

        // todo fix is slow to copy, use swap
        let mut tmp = vec![];
        swap(&mut tmp, &mut self.species);
        let mut previous_all_spec: HashSet<EnumeratedSpecies> = tmp.into_iter().collect();
        //assert_eq!(previous_all_spec.len(), self.species.len());

        let all_species_in_new_reacs: HashSet<&EnumeratedSpecies> = new_reacs
            .iter()
            .map(|v| {
                v.species_in
                    .values()
                    .chain(v.species_out.iter().map(|(_, s)| s))
            })
            .flatten()
            .collect();
        let all_species_in_new_reacs: HashSet<EnumeratedSpecies> =
            all_species_in_new_reacs.into_iter().cloned().collect();

        self.reactions.extend(new_reacs);

        let new_s: HashSet<EnumeratedSpecies> = all_species_in_new_reacs
            .difference(&previous_all_spec)
            .cloned()
            .collect();
        for a in new_s.iter() {
            debug_assert!(!previous_all_spec.contains(a));
        }
        let _num_in_new = all_species_in_new_reacs.len();
        let num_old = previous_all_spec.len();

        previous_all_spec.extend(all_species_in_new_reacs.into_iter());

        for s in self
            .reactions
            .iter()
            .map(|v| {
                v.species_in
                    .values()
                    .chain(v.species_out.iter().map(|(_, s)| s))
            })
            .flatten()
        {
            debug_assert!(previous_all_spec.contains(s));
        }
        self.species = previous_all_spec.into_iter().collect();
        assert_eq!(num_old + new_s.len(), self.species.len());
        //println!("{} of {} are new", new_s.len(), self.species.len());
        new_s
    }

    pub fn flatten_finalize(&mut self) {
        self.replace_global_counts();

        self.species
            .sort_by(|a, b| a.species_name.partial_cmp(&b.species_name).unwrap());
        let extra_guards: Vec<Vec<_>> = self
            .reactions
            .iter()
            .map(|reac| {
                /*let prototype =*/
                /*let remaining_constraint: Vec<_> = */
                self.model.transitions[reac.reaction_number_of_prototype]
                    .left
                    .iter()
                    .map(|l| (l, &reac.species_in[&l.id]))
                    .map(|(left_spec, left_enum)| {
                        let tmp: Vec<_> = left_spec
                            .attribute_constraint
                            .iter()
                            .filter(|p| {
                                p.eval_partial_to_bool(
                                    &AllOrSingleLeft::SingleLeft(&left_enum.clone()),
                                    None,
                                    None,
                                    false,
                                )
                                .is_none()
                            })
                            .map(|k| {
                                match k.evaluate_partial_for_left(
                                    &AllOrSingleLeft::SingleLeft(&left_enum.clone()),
                                    None,
                                    None,
                                    false,
                                ) {
                                    PartialEvaluationExpr::Primitive(_) => {
                                        unreachable!()
                                    }
                                    PartialEvaluationExpr::Incomplete(k) => k,
                                }
                            })
                            //.cloned()
                            .collect();
                        tmp.into_iter()
                    })
                    .flatten()
                    .collect()
                //reac.guard.append( remaining_constraint);
            })
            .collect();
        for (reac, mut xtra) in self.reactions.iter_mut().zip(extra_guards.into_iter()) {
            reac.guards_from_left_side.append(&mut xtra)
        }
        for r in self.reactions.iter_mut() {
            r.rate.simplify(&AllOrSingleLeft::All(&r.species_in));
        }

        /*self.reactions
            .iter_mut()
            .for_each(|r| r.remove_all_compartments());

        self.reactions.sort_by(|a, b| {
            a.reaction_number_of_prototype
                .partial_cmp(&b.reaction_number_of_prototype)
                .unwrap()
        });*/

        self.reactions.sort_by_cached_key(|a| {
            (
                a.reaction_number_of_prototype,
                a.species_in
                    .iter()
                    .map(|(v1, v2)| (v1.clone(), v2.clone()))
                    .sorted()
                    .collect::<Vec<_>>(),
            )
        });
    }

    pub fn flatten_a(
        &mut self,
        max_steps: usize,
        print_io: bool,
        //previous_reacs: Vec<EnumeratedReaction>,
    ) {
        if print_io {
            info!("-- Expanding the system");
        }
        self.reactions.clear();

        /*println!(
            "--Initial: {:?}",
            self.species
                .iter()
                .map(|v| v.string_name())
                .collect::<Vec<_>>()
        );*/
        /* println!(
            "--Initial: {:#?}",
            self.species
                .iter()
                .map(|v| format!("{:?}",v))
                .collect::<Vec<_>>()
        );*/
        assert!(max_steps >= 2);
        let mut new_species = self.initial_species.keys().cloned().collect();
        for i in 1..=max_steps {
            let _old_length = self.species.len();
            new_species = self.expantion_step(new_species);

            info!(
                "   Iteration {}: {} species, {} reactions",
                i,
                self.species.len(),
                self.reactions.len()
            );

            if new_species.len() == 0 {
                info!("-- Expantion successfull");
                break;
            }
            if i == max_steps {
                error!("WARNING: Model Expantion not complete after {} steps. You can try more steps, but the Simulation will not be correct as is!!",max_steps);
            }
        }
        self.flatten_finalize();
    }

    pub fn print_model(
        &self,
        name_conv: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> String {
        let initial_names: HashSet<_> = self.initial_species.keys().collect();
        let spec1 = self
            .initial_species
            .iter()
            .map(|(v, k)| {
                v.string_name(&self.species, name_conv, att_name_conversion)
                    + "("
                    + &*k.to_string()
                    + ");\n"
            })
            .join("");
        /*println!("{:?}\n{:?}",initial_names,self
        .species
        .iter()
        .filter(|n| !initial_names.contains(n)));*/

        /*let spec = self
        .species
        .iter()
        .filter(|n| !initial_names.contains(n))
        .map(|v| format!("{}();\n", v.string_name()))
        .join("");*/
        let mut spec = "".to_string();
        for (s, _s_def) in self.model.species.iter() {
            spec += &*format!(
                "\n// {:?}\n{}",
                s,
                self.species
                    .iter()
                    .filter(|n| n.species_name == *s)
                    .map(|v| format!(
                        "{}{}();\n",
                        match initial_names.contains(&v) {
                            true => "// (is initial) ",
                            false => "",
                        },
                        v.string_name(&self.species, name_conv, att_name_conversion)
                    ))
                    .join("")
            );
        }

        let mut reactions = "".to_string();
        for i in 0..self.model.transitions.len() {
            reactions += &*format!(
                "\n\n// Reactions based on rule {}\n// {}\n{}",
                i + 1,
                self.model.transitions[i].original_string,
                self.reactions
                    .iter()
                    .filter(|v| v.reaction_number_of_prototype == i)
                    .map(|v| v.string_name_reac(&self.species, name_conv, att_name_conversion))
                    .join("\n")
            );
        }
        /*let obs = match &self.observations {
            None => "".to_string(),
            Some(v) => {
                "\nOBSERVATIONS\n".to_string()
                    + &*v
                        .iter()
                        .map(|(n, list)| {
                            format!(
                                "\"{}\" = {}",
                                n.replace("\"", ""),
                                list.iter()
                                    .map(|s| s.string_name(
                                        &self.species,
                                        &self.model.species_name_conversion,
                                        &self.model.string_conversion
                                    ))
                                    .join(" + ")
                            )
                        })
                        .join("\n")
            }
        };*/
        let obs = "";
        let constant_defs = self
            .model
            .parameters
            .iter()
            .map(|(n, v)| {
                format!(
                    "const {} = {};",
                    n,
                    v.evaluate_partial_for_left(&AllOrSingleLeft::None, None, None, false)
                        .to_string(att_name_conversion)
                )
            })
            .join("\n");
        format!(
            "\nCONSTANTS\n////some constants are already replaced\n{}\nSPECIES\n{}\n\n{}\nRULES{}\n{}\n",
            constant_defs
            ,spec1, spec, reactions, obs
        )
    }
}
