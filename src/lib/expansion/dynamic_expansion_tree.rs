use crate::expansion::static_expansion::{
    new_right_attributes, CompartmentID, EnumeratedReaction, EnumeratedSpecies, FlatModel,
    OptimizedString, PrimitiveOrExpr, SpeciesAmt, StarAttributeValues,
};
use crate::input::structures::{
    CreationAmt, GenericGlobalCountInsideOf, GlobalCountEnum, PrimitiveTypes, RateExpr,
    RateExprTerm, RestSolutionTransition, Species, SpeciesLeftIdentifier, SpeciesName,
    SpeciesRight,
};
use crate::simulator::simulate_fast::*;
use more_asserts::*;
use rand_distr::Distribution;
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::iter;
use std::rc::{Rc, Weak};

#[derive(Debug)]
struct TreeModelNodeInternal {
    species_name: SpeciesName,
    attribute_values: Vec<(OptimizedString, PrimitiveTypes)>,
    children: Vec<TreeModelNode>,
    count: usize,
    parent: Option<Weak<RefCell<TreeModelNodeInternal>>>,
    old_id: Option<CompartmentID>,
}
impl Default for TreeModelNodeInternal {
    fn default() -> Self {
        TreeModelNodeInternal {
            species_name: SpeciesName::dummy(),
            attribute_values: vec![],
            children: vec![],
            count: 0,
            parent: None,
            old_id: None,
        }
    }
}

#[derive(Clone, Debug)]
struct TreeModelNode {
    internal: Rc<RefCell<TreeModelNodeInternal>>,
}
impl PartialEq for TreeModelNode {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.internal, &other.internal)
    }
}
impl Eq for TreeModelNode {}
impl TreeModelNode {
    fn count_rek(&self, spec_name: &SpeciesName, rek: bool) -> usize {
        let init = if self.get_species_name() == *spec_name {
            1
        } else {
            0
        };
        if rek {
            init + self
                .get_children()
                .iter()
                .map(|k| k.count_rek(spec_name, rek))
                .sum::<usize>()
        } else {
            init
        }
    }
    fn to_enumerated(
        dat: &mut Vec<(EnumeratedSpecies, usize)>,
        parent_id: Option<CompartmentID>,
        node: TreeModelNode,
    ) {
        let me: &RefCell<TreeModelNodeInternal> = node.internal.borrow();
        let me = me.take();
        dat.push((
            EnumeratedSpecies {
                species_name: me.species_name,
                expanded_attribute_values: me.attribute_values,
                parent_id: parent_id,
                my_id: me.old_id.clone(),
            },
            me.count,
        ));
        for child in me.children {
            assert!(me.old_id.is_some());
            TreeModelNode::to_enumerated(dat, me.old_id.clone(), child)
        }
    }
    fn split_off(&self, amount: usize) -> TreeModelNode {
        assert_gt!(amount, 0);
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        //println!("Split off :{:?}", me);

        assert_ge!(me.borrow().count, amount);
        if amount < me.borrow().count {
            assert!(
                me.borrow().children.is_empty(),
                "Split {} of {}",
                amount,
                me.borrow().count
            );
        }
        assert!(me.borrow().old_id.is_none());
        me.borrow_mut().count -= amount;
        let res = TreeModelNode {
            internal: Rc::new(RefCell::new(TreeModelNodeInternal {
                species_name: me.borrow().species_name.clone(),
                attribute_values: me.borrow().attribute_values.clone(),
                children: me.borrow().children.clone(),
                count: amount,
                parent: me.borrow().parent.clone(),
                old_id: None,
            })),
        };
        let parent = me.borrow().parent.as_ref().unwrap().upgrade().unwrap();
        let parent: &RefCell<TreeModelNodeInternal> = parent.borrow();
        parent.borrow_mut().children.push(res.clone());
        res
    }
    fn same_species_and_attributes(&self, other: &TreeModelNode) -> bool {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let other: &RefCell<TreeModelNodeInternal> = other.internal.borrow();
        if me.borrow().species_name == other.borrow().species_name {
            assert_eq!(
                me.borrow().attribute_values.len(),
                other.borrow().attribute_values.len()
            );
            for (a, b) in me
                .borrow()
                .attribute_values
                .iter()
                .zip(other.borrow().attribute_values.iter())
            {
                if a != b {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    fn get_attribute_value(&self, att_name: &OptimizedString) -> Option<PrimitiveTypes> {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        match me
            .borrow()
            .attribute_values
            .iter()
            .find(|(n, _)| *n == *att_name)
        {
            None => None,
            Some((_, p)) => Some(p.clone()),
        }
    }
    fn get_amount(&self) -> usize {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        me.borrow().count
    }
    fn get_species_name(&self) -> SpeciesName {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        me.borrow().species_name.clone()
    }
    fn get_num_attributes(&self) -> usize {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        me.borrow().attribute_values.len()
    }
    fn set_amount(&self, new_amount: usize) {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        me.borrow_mut().count = new_amount;
    }
    fn get_parent(&self) -> Option<TreeModelNode> {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        match me.borrow().parent.as_ref() {
            None => None,
            Some(x) => Some(TreeModelNode {
                internal: x.upgrade().unwrap(),
            }),
        }
    }
    // Clone is inefficent and not needed
    fn get_children(&self) -> Vec<TreeModelNode> {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        //println!("Returning {} children", me.borrow().children.len());
        me.borrow().children.clone()
    }
    fn new_root() -> TreeModelNode {
        TreeModelNode {
            internal: Rc::new(RefCell::new(TreeModelNodeInternal {
                species_name: SpeciesName::dummy(),
                attribute_values: vec![],
                children: vec![],
                count: 0,
                parent: None,
                old_id: None,
            })),
        }
    }
    fn new_dummy() -> TreeModelNode {
        TreeModelNode {
            internal: Rc::new(RefCell::new(TreeModelNodeInternal {
                species_name: SpeciesName::dummy(),
                attribute_values: vec![],
                children: vec![],
                count: 0,
                parent: None,
                old_id: None,
            })),
        }
    }
    fn set(
        &self,
        species_name: SpeciesName,
        attribute_values: Vec<(OptimizedString, PrimitiveTypes)>,
        parent: &TreeModelNode,
        count: usize,
        old_id: Option<CompartmentID>,
    ) {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let mut me = me.borrow_mut();
        me.species_name = species_name;
        me.attribute_values = attribute_values;
        me.parent = Some(Rc::downgrade(&parent.internal));
        me.count = count;
        me.old_id = old_id;
    }
    fn set_attributes(&self, attribute_values: Vec<(OptimizedString, PrimitiveTypes)>) {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let mut me = me.borrow_mut();
        assert_eq!(me.attribute_values.len(), attribute_values.len());
        me.attribute_values = attribute_values;
    }
    fn add_child_no_check_only_add(&self, n: &TreeModelNode) {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let mut me = me.borrow_mut();
        assert!(!me.children.contains(n));
        me.children.push(n.clone());
    }
    fn add_child(&self, n: &TreeModelNode) {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let mut me = me.borrow_mut();
        assert!(!me.children.contains(n));
        let n_int: &RefCell<TreeModelNodeInternal> = n.internal.borrow();
        if n_int.borrow().old_id.is_none() {
            for c in me.children.iter() {
                if c.same_species_and_attributes(n) {
                    c.set_amount(c.get_amount() + n.get_amount());
                    return;
                }
            }
        }
        me.children.push(n.clone());
        let n_int: &RefCell<TreeModelNodeInternal> = n.internal.borrow();
        assert!(n_int.borrow().parent.is_none());
        n_int.borrow_mut().parent = Some(Rc::downgrade(&self.internal));
        assert!(me.children.contains(n));
    }

    fn deep_clone(
        &self,
        compartment_lookup: &mut HashMap<CompartmentID, TreeModelNode>,
    ) -> TreeModelNode {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let id = match &me.borrow().old_id {
            None => None,
            Some(k) => {
                let mut id_counter = CompartmentID {
                    number: k.number,
                    species: me.borrow().species_name.clone(),
                };
                while compartment_lookup.get(&id_counter).is_some() {
                    id_counter.number += 1;
                }
                Some(id_counter)
            }
        };

        let node = TreeModelNode {
            internal: Rc::new(RefCell::new(TreeModelNodeInternal {
                species_name: me.borrow().species_name.clone(),
                attribute_values: me.borrow().attribute_values.clone(),
                children: me
                    .borrow()
                    .children
                    .iter()
                    .map(|k| k.deep_clone(compartment_lookup))
                    .collect(),
                count: me.borrow().count,
                parent: None,
                old_id: id.clone(),
            })),
        };
        match id {
            None => {}
            Some(k) => {
                assert!(compartment_lookup.insert(k, node.clone()).is_none())
            }
        }
        node
    }
    fn remove_child(&self, n: &TreeModelNode) {
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let mut me = me.borrow_mut();
        assert!(me.children.contains(n));
        me.children.retain(|k| k != n);

        let n_int: &RefCell<TreeModelNodeInternal> = n.internal.borrow();
        assert!(Rc::ptr_eq(
            &n_int.borrow().parent.as_ref().unwrap().upgrade().unwrap(),
            &self.internal
        ));
        n_int.borrow_mut().parent = None;
    }
    fn _print_test(&self, count: usize) {
        for _ in 0..count {
            print!("\t");
        }
        let me: &RefCell<TreeModelNodeInternal> = self.internal.borrow();
        let me = me.borrow();
        println!("{:?}", me.species_name);
        for x in me.children.iter() {
            x._print_test(count + 1);
        }
    }
}

struct TreeModelRepresentation {
    root: TreeModelNode,
    compartment_lookup: HashMap<CompartmentID, TreeModelNode>,
    species_left_lookup: HashMap<SpeciesLeftIdentifier, TreeModelNode>,
}
impl TreeModelRepresentation {
    fn eval_term(&self, term: &RateExprTerm) -> PrimitiveTypes {
        match term {
            RateExprTerm::Primitive(a) => a.clone(),
            RateExprTerm::ExpandedAttRef(a) => self
                .species_left_lookup
                .get(&a.species_id)
                .unwrap()
                .get_attribute_value(&a.att_name)
                .unwrap(),
            RateExprTerm::StarAttRef(a) => self
                .species_left_lookup
                .get(&a.species_id)
                .unwrap()
                .get_attribute_value(&a.att_name)
                .unwrap(),
            RateExprTerm::Expr(x) => self.eval_expr(x),
            RateExprTerm::SpecCount(k) => PrimitiveTypes::Integer(
                self.species_left_lookup
                    .get(&k.idx_on_left)
                    .unwrap()
                    .get_amount() as i64,
            ),
            RateExprTerm::GlobalCount(GlobalCountEnum::GenericUnspecified(gu)) => {
                assert!(gu.constraints.is_empty());
                let count = match &gu.inside_of {
                    GenericGlobalCountInsideOf::SpecificNone => {
                        self.root.count_rek(&gu.species_name, true)
                    }
                    GenericGlobalCountInsideOf::SpecificOneLevel(l) => self
                        .species_left_lookup
                        .get(&l)
                        .unwrap()
                        .get_children()
                        .iter()
                        .map(|k| k.count_rek(&gu.species_name, false))
                        .sum(),
                    GenericGlobalCountInsideOf::SpecificAnyDepth(l) => self
                        .species_left_lookup
                        .get(&l)
                        .unwrap()
                        .count_rek(&gu.species_name, true),
                    GenericGlobalCountInsideOf::GlobalOneLevel(parent_name, parent_constr) => self
                        .compartment_lookup
                        .iter()
                        .filter(|(_, k)| {
                            (k.get_species_name() == *parent_name)
                                || parent_constr
                                    .iter()
                                    .all(|xpr| self.eval_expr(xpr).as_bool())
                        })
                        .map(|(_, node)| {
                            node.get_children()
                                .iter()
                                .filter(|k| {
                                    (k.get_species_name() == gu.species_name)
                                        || gu
                                            .constraints
                                            .iter()
                                            .all(|xpr| self.eval_expr(xpr).as_bool())
                                })
                                .map(|k| k.get_amount())
                                .sum::<usize>()
                        })
                        .sum::<usize>(),
                    GenericGlobalCountInsideOf::GlobalAnyDepth(_, _) => {
                        unimplemented!()
                    }
                    GenericGlobalCountInsideOf::GlobalNone => {
                        unimplemented!()
                    }
                };
                //println!("evaled to {}", count);
                PrimitiveTypes::Integer(count as i64)
            }
            RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumerated(_y)) => {
                unimplemented!() // not possible?
            }
            RateExprTerm::GlobalCount(GlobalCountEnum::SpecificEnumeratedGuard(_)) => {
                unimplemented!() // not possible?
            }

            /*   match gce {
                    GlobalCountEnum::GenericUnspecified(_) => {
                        unreachable!()
                    }
                    GlobalCountEnum::SpecificEnumerated(y) => {
                        IndexedRateExprTerm::SpecificCountFromGlobal(
                            y.iter()
                                .map(|v| converse_map.get(v).unwrap())
                                .cloned()
                                .collect(),
                        )
                    }
                }
            }*/
            RateExprTerm::Condition(c) => match self.eval_expr(&c.guard) {
                PrimitiveTypes::Boolean(true) => self.eval_expr(&c.result_true),
                PrimitiveTypes::Boolean(false) => self.eval_expr(
                    &c.result_false
                        .as_ref()
                        .expect("False branche needed for assignment"),
                ),
                _ => unreachable!(),
            },
            RateExprTerm::BuildinFunction(bf) => bf.function.eval(
                &bf.params.iter().map(|k| self.eval_expr(k)).collect(),
                true,
                None,
            ),
        }
    }
    fn eval_expr(&self, expr: &RateExpr) -> PrimitiveTypes {
        match expr {
            RateExpr::Single(x) => self.eval_term(x),
            RateExpr::Double(left, op, right) => {
                let right = self.eval_term(&right);
                let left = self.eval_term(&left);
                op.apply(&left, &right).unwrap()
            }
        }
        //println!("\nEval:{:?} ====== {:?}", expr, r);
        //r
    }
    fn from(
        initial_species: Vec<(EnumeratedSpecies, usize)>,
        reac: &EnumeratedReaction,
        left_species_identifiers: &HashMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
    ) -> TreeModelRepresentation {
        assert_eq!(left_species_identifiers.len(), reac.species_in.len());
        debug_assert_eq!(
            left_species_identifiers.len(),
            left_species_identifiers
                .iter()
                .map(|k| k.0.clone())
                .collect::<HashSet<_>>()
                .len()
        );
        //println!("\nREACTION in {:?}", reac.reaction_number_of_prototype);
        //println!("INITIAL {:?}", initial_species);
        let mut species_we_need_for_reaction: HashMap<
            EnumeratedSpecies,
            Vec<SpeciesLeftIdentifier>,
        > = HashMap::new();
        for (a, b) in left_species_identifiers.iter() {
            match species_we_need_for_reaction.insert(b.clone(), vec![a.clone()]) {
                None => {}
                Some(y) => {
                    assert!(b.my_id.is_none());
                    species_we_need_for_reaction.insert(
                        b.clone(),
                        y.iter().cloned().chain(iter::once(a.clone())).collect(),
                    );
                    /*if y != *a {
                        panic!("douplicate entry {:?} and {:?} for {:?}", y, a, b);
                    }*/
                }
            }
        }
        /*     left_species_identifiers
                .iter()
                .map(|(a, b)| (b.clone(), a.clone()))
                .collect();
        assert_eq!(
            species_we_need_for_reaction.len(),
            left_species_identifiers.len(),
            "{:?}",
            left_species_identifiers
        );*/
        //println!("LEFT ids : {:?}", left_species_identifiers);
        //println!("Result {:?}", species_we_need_for_reaction);

        let lookup_compartments: HashMap<_, _> = initial_species
            .iter()
            .map(|(a, _b)| a)
            .enumerate()
            .filter(|(_, s)| s.my_id.is_some())
            .map(|(num, s)| (s.my_id.as_ref().unwrap().clone(), num))
            .collect();
        let mut species_left_lookup = HashMap::new();
        //println!("-- lookup {:?}", lookup_compartments);

        let mut new_nodes: Vec<TreeModelNode> = vec![];
        while new_nodes.len() < initial_species.len() {
            new_nodes.push(TreeModelNode::new_dummy());
        }
        new_nodes.push(TreeModelNode::new_root());

        for ((old_linear_node, amount), new_tree_node) in
            initial_species.into_iter().zip(new_nodes.iter())
        {
            //println!("{index} --> {:?}", old_linear_node);
            match species_we_need_for_reaction.get(&old_linear_node) {
                None => {
                    //println!("No equivalent for {:?}", old_linear_node)
                }
                Some(x) => {
                    assert!(species_left_lookup
                        .insert(x.clone(), new_tree_node.clone())
                        .is_none());
                }
            }

            let index_of_parent = match old_linear_node.parent_id {
                None => match old_linear_node.my_id {
                    None => None,
                    Some(_) => Some(new_nodes.len() - 1),
                },
                Some(idx) => Some(match idx.number {
                    -1 => new_nodes.len() - 1,
                    _x => *lookup_compartments.get(&idx).unwrap(),
                }),
            };
            //println!("{} -> {:?}", index, index_of_parent);
            let parent = match index_of_parent {
                Some(xidx) => {
                    new_nodes[xidx].add_child(new_tree_node);
                    &new_nodes[xidx]
                }
                None => {
                    new_nodes.last().unwrap().add_child(new_tree_node);
                    new_nodes.last().as_ref().unwrap()
                }
            };
            new_tree_node.set(
                old_linear_node.species_name,
                old_linear_node.expanded_attribute_values,
                parent,
                amount,
                old_linear_node.my_id,
            );
        }
        //println!("Final: {:?}", species_left_lookup);
        assert_eq!(
            species_left_lookup.len(),
            species_we_need_for_reaction.len()
        );

        TreeModelRepresentation {
            compartment_lookup: lookup_compartments
                .into_iter()
                .map(|(a, b)| (a, new_nodes[b].clone()))
                .collect(),

            root: new_nodes.last().unwrap().clone(),
            species_left_lookup: species_left_lookup
                .into_iter()
                .map(|(id, node)| {
                    //assert_eq!(id.len(), 1); // todo is das ok?
                    id.into_iter().map(move |k| (k.clone(), node.clone()))
                    //(id[0].clone(), node)
                })
                .flatten()
                .collect(),
        }
    }
}

struct RestsolutionTreeModelNode {
    node: TreeModelNode,
    _total: usize,
    total_unused: usize,
}
impl RestsolutionTreeModelNode {
    fn new(node: TreeModelNode, total: usize) -> RestsolutionTreeModelNode {
        RestsolutionTreeModelNode {
            node,
            _total: total,
            total_unused: total,
        }
    }
}

fn expand_species_amt_to_usize(
    values_in: HashMap<EnumeratedSpecies, SpeciesAmt>,
    species_left_lookup: &mut HashMap<SpeciesLeftIdentifier, (EnumeratedSpecies, usize)>,
    available_species: &HashMap<SpeciesName, Species>,
) -> Vec<(EnumeratedSpecies, usize)> {
    let mut d = vec![];
    let species_in_reaction: HashSet<_> = species_left_lookup
        .values()
        .map(|(a, _)| a.clone())
        .collect();
    for (key, val) in values_in {
        match val {
            SpeciesAmt::NumOnly(amt) => d.push((key, amt)),
            #[cfg(feature = "hybrid_att")]
            SpeciesAmt::NotExpanded(amt_vec) => {
                let corresponding_left: Option<(SpeciesLeftIdentifier, usize)>;
                if species_in_reaction.contains(&key) {
                    let (found_val, (_, variation_idx)) = species_left_lookup
                        .iter()
                        .find(|(_l, (s, _))| *s == key)
                        .unwrap();
                    corresponding_left = Some((found_val.clone(), *variation_idx));
                } else {
                    corresponding_left = None;
                }
                for (variation_idx, (amt, variant)) in amt_vec.into_iter().enumerate() {
                    let mut sub_key = key.clone();

                    sub_key.expanded_attribute_values.extend(
                        variant
                            .names
                            .iter()
                            .cloned()
                            .zip(variant.values.iter().cloned()),
                    );
                    match &corresponding_left {
                        None => {}
                        Some((lid, n)) => {
                            if *n == variation_idx {
                                assert_eq!(
                                    species_left_lookup
                                        .insert(lid.clone(), (sub_key.clone(), 0))
                                        .unwrap(),
                                    (key.clone(), *n)
                                );
                            }
                        }
                    }

                    d.push((sub_key, amt))
                }
            }
            SpeciesAmt::CompartmentWithStar(is_present, spec) => {
                if is_present {
                    let corresponding_left: Option<(SpeciesLeftIdentifier, usize)>;
                    if species_in_reaction.contains(&key) {
                        let (found_val, (_, variation_idx)) = species_left_lookup
                            .iter()
                            .find(|(_l, (s, _))| *s == key)
                            .unwrap();
                        corresponding_left = Some((found_val.clone(), *variation_idx));
                    } else {
                        corresponding_left = None;
                    }
                    //for (variation_idx, (amt, variant)) in amt_vec.into_iter().enumerate() {
                    let mut sub_key = key.clone();

                    sub_key
                        .expanded_attribute_values
                        .extend(spec.names.iter().cloned().zip(spec.values.iter().cloned()));
                    match &corresponding_left {
                        None => {}
                        Some((lid, n)) => {
                            assert_eq!(
                                species_left_lookup
                                    .insert(lid.clone(), (sub_key.clone(), 0))
                                    .unwrap(),
                                (key.clone(), *n)
                            );
                        }
                    }

                    d.push((sub_key, 1))
                    // }
                }
            }
        }
    }
    for (species, _) in d.iter() {
        let species_info = &available_species[&species.species_name];
        assert_eq!(
            species_info.expanding_attributes.len() + species_info.not_expanding_attributes.len(),
            species.expanded_attribute_values.len()
        );
    }
    d
}

fn compact_usize_to_species_amt(
    counts: Vec<(EnumeratedSpecies, usize)>,
    available_species: &HashMap<SpeciesName, Species>,
) -> HashMap<EnumeratedSpecies, SpeciesAmt> {
    let mut res = HashMap::new();
    for (mut species, amt) in counts {
        let species_info = &available_species[&species.species_name];
        assert_eq!(
            species_info.expanding_attributes.len() + species_info.not_expanding_attributes.len(),
            species.expanded_attribute_values.len()
        );
        if species_info.not_expanding_attributes.is_empty() {
            //println!("{:?} into {:?}", species, res);
            assert!(res.insert(species, SpeciesAmt::NumOnly(amt)).is_none(),)
        } else {
            // case of starr attributes
            let extra_attributes = species
                .expanded_attribute_values
                .split_off(species_info.expanding_attributes.len());
            let att_values = StarAttributeValues {
                values: extra_attributes.iter().map(|(_a, b)| b.clone()).collect(),
                names: extra_attributes.into_iter().map(|(a, _b)| a).collect(),
            };
            let new_amt = if species_info.is_compartment {
                assert_eq!(amt, 1);
                SpeciesAmt::CompartmentWithStar(true, att_values)
            } else {
                #[cfg(feature = "hybrid_att")]
                {
                    SpeciesAmt::NotExpanded(vec![(amt, att_values)])
                }
                #[cfg(not(feature = "hybrid_att"))]
                unreachable!()
            };
            match res.insert(species, new_amt.clone()) {
                None => {}
                Some(k) => match (k, new_amt) {
                    (SpeciesAmt::NumOnly(_), SpeciesAmt::NumOnly(_)) => {
                        unreachable!("Num only for Star?")
                    }
                    #[cfg(feature = "hybrid_att")]
                    (SpeciesAmt::NotExpanded(mut a), SpeciesAmt::NotExpanded(b)) => {
                        for (b_num, b_val) in b {
                            match a.iter_mut().find(|k| k.1 == b_val) {
                                None => {
                                    a.push((b_num, b_val));
                                }
                                Some((v_found, _)) => {
                                    *v_found += b_num;
                                }
                            }
                        }
                    }
                    (_, _) => unreachable!("Different amount types for same species?"),
                },
            }
        }
    }
    res
}

fn find_max_index_of_children(start_idx: usize, data: &Vec<SpeciesRight>) -> usize {
    let mut idx = start_idx;
    while idx + 1 < data.len()
        && match data[idx + 1].compartment_constraint {
            None => false,
            Some(k) => k >= start_idx,
        }
    {
        idx += 1;
    }
    idx
}

fn expand_right_side_with_dynamic_amts(
    plain: &Vec<SpeciesRight>,
    tree: &TreeModelRepresentation,
    all_species: &HashMap<SpeciesName, Species>,
) -> Vec<SpeciesRight> {
    if plain.is_empty() {
        return vec![];
    }
    let mut res = vec![];
    expand_right_side_with_dynamic_amts_rek(
        plain,
        tree,
        all_species,
        0,
        plain.len() - 1,
        vec![],
        &mut res,
    );
    res
}

fn expand_right_side_with_dynamic_amts_rek(
    plain: &Vec<SpeciesRight>,
    tree: &TreeModelRepresentation,
    all_species: &HashMap<SpeciesName, Species>,
    first_idx: usize,
    last_idx: usize,
    mut converter_map: Vec<usize>,
    result: &mut Vec<SpeciesRight>,
) /* -> Vec<SpeciesRight>*/
{
    assert_eq!(converter_map.len(), first_idx);
    assert!(last_idx < plain.len());
    //let mut converter_map = vec![];
    //let mut result = vec![];
    //for (idx, s) in plain.iter().enumerate() {
    assert_eq!(converter_map.len(), first_idx);
    let s = &plain[first_idx];
    let amt: usize = match &s.amount {
        CreationAmt::StaticNum(l) => *l,
        CreationAmt::DynExpr(expr) => {
            let new_amt = tree.eval_expr(expr);
            match new_amt {
                PrimitiveTypes::String(_) => {
                    unreachable!()
                }
                PrimitiveTypes::Boolean(_) => {
                    unreachable!()
                }
                PrimitiveTypes::Float(_) => {
                    unreachable!()
                }
                PrimitiveTypes::Integer(k) => {
                    if k < 0 {
                        panic!("Negative dynamic amt! ({} < 0) This is a model error.", k)
                    }
                    k as usize
                }
            }
        }
    };
    if amt == 0 {
        match all_species[&s.species_name].is_compartment {
            true => {
                let max_child = find_max_index_of_children(first_idx, plain);
                for _ in first_idx..(max_child + 1) {
                    converter_map.push(usize::MAX);
                }
                if max_child + 1 < last_idx {
                    expand_right_side_with_dynamic_amts_rek(
                        plain,
                        tree,
                        all_species,
                        max_child + 1,
                        last_idx,
                        converter_map,
                        result,
                    )
                }
            }
            false => {
                converter_map.push(usize::MAX);
                if first_idx < last_idx {
                    expand_right_side_with_dynamic_amts_rek(
                        plain,
                        tree,
                        all_species,
                        first_idx + 1,
                        last_idx,
                        converter_map,
                        result,
                    )
                }
            }
        }
        //println!("Zero amt to add!")
    } else {
        match all_species[&s.species_name].is_compartment {
            true => {
                let max_child = find_max_index_of_children(first_idx, plain);
                let cv_copy = converter_map;
                for _ in 0..(amt - 1) {
                    converter_map = cv_copy.clone();
                    converter_map.push(result.len());
                    let mut new_r = s.clone();
                    new_r.compartment_constraint = match s.compartment_constraint {
                        None => None,
                        Some(k) => Some(converter_map[k]),
                    };
                    new_r.amount = CreationAmt::StaticNum(1);
                    result.push(new_r);
                    if first_idx < max_child {
                        expand_right_side_with_dynamic_amts_rek(
                            plain,
                            tree,
                            all_species,
                            first_idx + 1,
                            max_child,
                            converter_map,
                            result,
                        )
                    }
                }
                converter_map = cv_copy;
                converter_map.push(result.len());
                let mut new_r = s.clone();
                new_r.compartment_constraint = match s.compartment_constraint {
                    None => None,
                    Some(k) => Some(converter_map[k]),
                };
                new_r.amount = CreationAmt::StaticNum(1);
                result.push(new_r);
                if first_idx < last_idx {
                    expand_right_side_with_dynamic_amts_rek(
                        plain,
                        tree,
                        all_species,
                        first_idx + 1,
                        last_idx,
                        converter_map,
                        result,
                    )
                }
            }
            false => {
                converter_map.push(result.len());
                let mut new_r = s.clone();
                new_r.compartment_constraint = match s.compartment_constraint {
                    None => None,
                    Some(k) => Some(converter_map[k]),
                };
                new_r.amount = CreationAmt::StaticNum(amt);
                result.push(new_r);
                if first_idx < last_idx {
                    expand_right_side_with_dynamic_amts_rek(
                        plain,
                        tree,
                        all_species,
                        first_idx + 1,
                        last_idx,
                        converter_map,
                        result,
                    )
                }
            }
        }
    }
}

pub fn change_structure_and_reexpand_v2(
    mut model: FlatModel,
    idx_of_reaction: usize,
    max_steps: usize,
    print_io: bool,
    mut species_left_lookup: HashMap<SpeciesLeftIdentifier, (EnumeratedSpecies, usize)>,
) -> FlatModel {
    model.observations = model.model.observations.clone();
    model.initial_species.retain(|_, k| k.total_amt() > 0);
    let modified_initial = expand_species_amt_to_usize(
        model.initial_species,
        &mut species_left_lookup,
        &model.model.species,
    );
    let species_left_lookup: HashMap<_, _> = species_left_lookup
        .into_iter()
        .map(|(a, (b, c))| {
            assert_eq!(c, 0);
            (a, b)
        })
        .collect();
    let mut tree = TreeModelRepresentation::from(
        modified_initial,
        &model.reactions[idx_of_reaction],
        &species_left_lookup,
    );
    /*{
        println!("Before");
        tree.root.print_test(0);
    }*/

    let reac = &model.reactions[idx_of_reaction];
    let reac_proto = &model.model.transitions[reac.reaction_number_of_prototype];

    let mut new_added_species: HashMap<usize, TreeModelNode> = HashMap::new();

    /* Identify rest solution before changes are made */
    let rest_sol_parents: HashSet<_> = reac_proto
        .rest_solution_transiotions
        .iter()
        .map(|k| k.leftsideparent.clone())
        .inspect(|k| {
            assert!(
                species_left_lookup.contains_key(k),
                "problem is {}.\nLookup: {:?}\nHave: {:?}",
                reac_proto.original_string,
                species_left_lookup,
                reac_proto.rest_solution_transiotions
            )
        })
        .collect();
    let all_potential_restsolution_elems_amounts: Vec<Vec<RestsolutionTreeModelNode>> =
        rest_sol_parents
            .iter()
            .map(|sub_sol_area| {
                let parent_of_restsolution = tree.species_left_lookup.get(&sub_sol_area).unwrap();
                parent_of_restsolution
                    .get_children()
                    .iter()
                    .filter_map(|k| {
                        //println!("Finding rest");
                        let existing_vals = tree
                            .species_left_lookup
                            .values()
                            .filter(|sll| k == *sll)
                            .count();
                        match existing_vals {
                            0 => Some(RestsolutionTreeModelNode::new(k.clone(), k.get_amount())),
                            _x => {
                                assert_ge!(k.get_amount(), existing_vals);
                                if k.get_amount() == existing_vals {
                                    None
                                } else {
                                    //println!("Rest rec as -> {}", k.get_amount());
                                    assert_gt!(k.get_amount(), existing_vals);
                                    Some(RestsolutionTreeModelNode::new(
                                        k.clone(),
                                        k.get_amount() - existing_vals,
                                    ))
                                }
                            }
                        }
                    })
                    .collect()
            })
            .collect();

    //println!("have on right {:?}", reac_proto.right);
    // move all species into the position on the right
    let expanded_right_side =
        expand_right_side_with_dynamic_amts(&reac_proto.right, &tree, &model.model.species);
    for (idx_on_right, r) in expanded_right_side.iter().enumerate() {
        //.right.iter().enumerate() {
        let amount_pro_add = r.amount.as_num().expect("Should have been expanded");
        let new_parent = match r.compartment_constraint {
            None => match &reac.reaction_parent {
                None => tree.root.clone(),
                Some(x) => tree.compartment_lookup.get(x).unwrap().clone(),
            },
            Some(x) => match &expanded_right_side[x as usize].corresponding_on_left {
                None => {
                    // child of new species
                    new_added_species.get(&(x as usize)).unwrap().clone()
                }
                Some(x) => tree.species_left_lookup.get(x).unwrap().clone(),
            },
        };

        //println!("New attrs: {:?}", attrs);

        let attrs = new_right_attributes(r, &reac.species_in, &model.model.species);
        let attrs: Vec<_> = attrs
            .expanded
            .into_iter()
            .chain(attrs.starr.into_iter().map(|(n, k)| match k {
                PrimitiveOrExpr::Primitive(p) => (n, p),
                PrimitiveOrExpr::Expr(e) => {
                    (n, tree.eval_expr(&e)) // todo: check if tree still valid
                }
            }))
            .collect();

        let me: TreeModelNode = match &r.corresponding_on_left {
            None => {
                // Create new species
                let new_s = TreeModelNode::new_dummy();
                let my_id: Option<CompartmentID> = match model
                    .model
                    .species
                    .get(&r.species_name)
                    .unwrap()
                    .is_compartment
                {
                    true => {
                        let mut id_counter = CompartmentID {
                            number: 0,
                            species: r.species_name.clone(),
                        };
                        while tree.compartment_lookup.get(&id_counter).is_some() {
                            id_counter.number += 1;
                        }
                        Some(id_counter)
                    }
                    false => None,
                };
                new_s.set(
                    r.species_name.clone(),
                    attrs.clone(),
                    &tree.root,
                    amount_pro_add,
                    my_id.clone(),
                );
                tree.root.add_child_no_check_only_add(&new_s);
                match &my_id {
                    Some(x) => {
                        assert!(tree
                            .compartment_lookup
                            .insert(x.clone(), new_s.clone())
                            .is_none());
                    }
                    None => {}
                }
                new_added_species.insert(idx_on_right, new_s.clone());
                //println!("Add new {:?}", new_s);
                new_s
            }
            Some(x) => {
                assert!(
                    r.amount.as_num().is_some(),
                    "Left right correspondence is only expected for static amts"
                );
                let me = tree
                    .species_left_lookup
                    .get(x)
                    .expect(format!("could not find {x:?}").as_str())
                    .clone();
                //me.set_attributes(attrs);
                me
            }
        };
        let old_parent = me.get_parent().unwrap();
        assert!(me.get_amount() >= amount_pro_add);
        /*println!(
            "HAve {} and need split of {}, with {} attributes so far, will set {}",
            me.get_amount(),
            r.amount,
            me.get_num_attributes(),
            attrs.len()
        );*/
        if me.get_amount() == amount_pro_add {
            old_parent.remove_child(&me);
            new_parent.add_child(&me);
            me.set_attributes(attrs);
        } else {
            let split = me.split_off(amount_pro_add);
            old_parent.remove_child(&split);
            new_parent.add_child(&split);
            split.set_attributes(attrs);
        }
        let species_info = &model.model.species[&me.get_species_name()];
        assert_eq!(
            species_info.expanding_attributes.len() + species_info.not_expanding_attributes.len(),
            me.get_num_attributes()
        );
        //old_parent.borrow_mut().children.(&me).
    }
    let species_mentioned_on_right: Vec<_> = model.model.transitions
        [reac.reaction_number_of_prototype]
        .right
        .iter()
        .filter_map(|r| r.corresponding_on_left.as_ref())
        //.map(|k| tree.species_left_lookup.get(&k).unwrap())
        .cloned()
        .collect();

    for (lid, _) in reac.species_in.iter() {
        let s = tree.species_left_lookup.get(lid).unwrap().clone();
        debug_assert!(s.get_parent().is_some());
    }

    for (sub_sol_area, mut potential_restsolution_elems_amounts) in rest_sol_parents
        .into_iter()
        .zip(all_potential_restsolution_elems_amounts.into_iter())
    {
        let parent_of_restsolution = tree.species_left_lookup.get(&sub_sol_area).unwrap();
        let rest_sols: Vec<&RestSolutionTransition> = reac_proto
            .rest_solution_transiotions
            .iter()
            .filter(|k| k.leftsideparent == sub_sol_area)
            .collect();

        let rest_sol_quantities: Vec<f64> = rest_sols
            .iter()
            .map(|k| match &k.amount_quanti {
                None => 1.,
                Some(x) => tree.eval_expr(x).as_float().unwrap(), /*match  x.eval_to_primitive(
                                                                      &AllOrSingleLeft::All(
                                                                          &species_left_lookup
                                                                              .iter()
                                                                              .map(|(a, b)| (a.clone(), b.clone()))
                                                                              .collect(),
                                                                      ),
                                                                      None,
                                                                      None,
                                                                      true,
                                                                  ) {
                                                                      Ok(x) => match x {
                                                                          PrimitiveTypes::Float(x) => x,
                                                                          PrimitiveTypes::Integer(y) => y as f64,
                                                                          _ => unreachable!("Non numeric in rest sol amount"),
                                                                      },
                                                                      Err(y) => {
                                                                          panic!("Could not eval rest solution amount.\n{}", y)
                                                                      }
                                                                  },*/
            })
            .collect();

        let rest_sol_quantity_sum: f64 = rest_sol_quantities.iter().sum();
        assert!(rest_sol_quantity_sum > 0.);

        // todo, exact stochastic sampling
        for (number_of_rest_sol, (restsol, quantity_skaler)) in
            rest_sols.iter().zip(rest_sol_quantities.iter()).enumerate()
        {
            let restsolution_elems: Vec<TreeModelNode> = potential_restsolution_elems_amounts
                .iter_mut()
                .filter_map(|rest_node| {
                    /*println!(
                        "Have {} remaning of {}",
                        rest_node.total_unused,
                        rest_node.node.get_amount()
                    );*/
                    let to_take;
                    if number_of_rest_sol + 1 == rest_sols.len() {
                        //println!("B1");
                        to_take = rest_node.total_unused;
                    } else {
                        let bin_distr = rand_distr::Binomial::new(
                            rest_node.total_unused as u64,
                            *quantity_skaler / rest_sol_quantity_sum,
                        );

                        to_take = match bin_distr {
                            Ok(bin_distr) => bin_distr.sample(&mut rand::thread_rng()) as usize,
                            Err(rand_distr::BinomialError::ProbabilityTooSmall) => 0,
                            Err(_) => {
                                unimplemented!()
                            }
                        };
                        //println!("B2 take {to_take}");
                    }
                    if to_take == 0 {
                        return None;
                    }
                    assert!(rest_node.total_unused >= to_take);
                    rest_node.total_unused -= to_take;
                    let ret = if rest_node.node.get_amount() == to_take {
                        rest_node.node.clone()
                    } else {
                        rest_node.node.split_off(to_take)
                    };
                    /*println!(
                        "After taking {to_take} have {} availale of {}",
                        rest_node.total_unused,
                        rest_node.node.get_amount()
                    );*/
                    Some(ret)
                })
                .collect();

            if restsol.idx_right.is_empty() {
                for rest_elem_to_be_removed in restsolution_elems.iter() {
                    parent_of_restsolution.remove_child(rest_elem_to_be_removed);
                }
            } else {
                for (idx_of_rest_on_right, new_parent_idx) in
                    restsol.idx_right.iter().copied().enumerate()
                {
                    let move_to: TreeModelNode = {
                        if new_parent_idx == -1 {
                            match &reac.reaction_parent {
                                None => tree.root.clone(),
                                Some(x) => tree.compartment_lookup.get(x).unwrap().clone(),
                            }
                        } else {
                            assert!(new_parent_idx >= 0);
                            match &reac_proto.right[new_parent_idx as usize].corresponding_on_left {
                                None => {
                                    // child of new species
                                    assert!(new_parent_idx >= 0);
                                    new_added_species
                                        .get(&(new_parent_idx as usize))
                                        .unwrap()
                                        .clone()
                                }
                                Some(x) => tree.species_left_lookup.get(x).unwrap().clone(),
                            }
                        }
                    };
                    if idx_of_rest_on_right + 1 == restsol.idx_right.len() {
                        for rest_elem_to_be_moved in restsolution_elems.iter() {
                            rest_elem_to_be_moved
                                .get_parent()
                                .unwrap()
                                .remove_child(rest_elem_to_be_moved);
                            move_to.add_child(rest_elem_to_be_moved);
                        }
                    } else {
                        for rest_elem_to_be_moved in restsolution_elems.iter() {
                            move_to.add_child(
                                &rest_elem_to_be_moved.deep_clone(&mut tree.compartment_lookup),
                            );
                        }
                    }
                }
            }
        }
    }

    // Remove no longer needed species
    for (lid, _) in reac.species_in.iter() {
        if !species_mentioned_on_right.contains(lid) {
            let s = tree.species_left_lookup.get(lid).unwrap().clone();
            let s_parent = s.get_parent().unwrap();
            if s.get_amount() == 1 {
                s_parent.remove_child(&s);
            } else {
                assert!(s.get_amount() > 1);
                s.set_amount(s.get_amount() - 1);
            }
        }
    }

    /*{
        println!("After");
        tree.root.print_test(0);
    }*/

    /* Generate enumerated model from Tree */
    let mut species_counts = vec![];
    for c in tree.root.get_children() {
        TreeModelNode::to_enumerated(&mut species_counts, None, c);
    }
    model.initial_species = compact_usize_to_species_amt(species_counts, &model.model.species);
    model.species = model.initial_species.keys().cloned().collect();
    /*if !model.global_values.is_empty() {
        let still_existing_compartments: HashSet<_> = model
            .species
            .iter()
            .filter_map(|k| k.my_id.as_ref())
            .collect();
        model
            .global_values
            .retain(|n, _| still_existing_compartments.contains(&n.spec));
    }*/

    model.flatten_a(max_steps, print_io);
    model
}

impl SimulationFast {
    pub fn take_current_state_as_initial(&self, model: &mut FlatModel) {
        model.initial_species = self
            .species_lookup
            .iter()
            //todo remove this -> expensive!
            .inspect(|(en, _idx)| assert!(model.species.contains(en)))
            .map(|(en, idx)| (en.clone(), self.species_counts[*idx].clone()))
            .collect();
    }

    pub fn reset_to_changed_model(
        self,
        model: &mut FlatModel,
        generate_new_templates: bool,
    ) -> Result<SimulationFast, String> {
        let mut new_sim = SimulationFast::new(&model, None, generate_new_templates, Some(0))?;

        new_sim.time = self.time;

        new_sim.observation_values = self.observation_values;

        assert_eq!(new_sim.reaction_names.len(), self.reaction_names.len());
        new_sim.obs_reaction_counts = self.obs_reaction_counts;
        new_sim.special_times = self.special_times;

        new_sim.reaction_counts = self.reaction_counts;
        new_sim.update_all_propensities_max_count()?;
        new_sim.state_jsons = self.state_jsons;

        new_sim.metrics = self.metrics;
        new_sim.rng = self.rng;
        new_sim.last_io = self.last_io;
        assert_le!(self.last_time_dependend_update, self.time);
        new_sim.last_time_dependend_update = self.last_time_dependend_update;
        assert!(new_sim.time >= new_sim.last_time_dependend_update);
        new_sim.obs_times = self.obs_times;
        new_sim.update_all_propensities_time_dep()?;

        assert_eq!(new_sim.obs_times.len(), new_sim.observation_values.len(),);
        Ok(new_sim)
    }
}
