use crate::expansion::static_expansion::{AllOrSingleLeft, EnumeratedSpecies};
use crate::input::structures::{SpeciesLeft, SpeciesLeftIdentifier};
use itertools::Itertools;
use std::collections::{BTreeMap, HashSet};

fn matches_description(
    to_test: &EnumeratedSpecies,
    description: &SpeciesLeft,
    partial_result: &Vec<EnumeratedSpecies>,
    desci: &Vec<SpeciesLeft>,
    lookup_at: &Vec<usize>,
) -> bool {
    if to_test.my_id.is_some() {
        // don't double match compartments:
        if lookup_at.iter().any(|k| partial_result[*k] == *to_test) {
            return false;
        }
        if cfg!(debug_assertions) {
            if partial_result.iter().contains(to_test) {
                panic!("Missing optimization");
            }
        }
    }
    /*|to_test|*/
    match &description.parent_number {
        None => match partial_result.get(
            desci
                .iter()
                .position(|v| v.parent_number.is_none())
                .unwrap(),
        ) {
            Some(x) => x.parent_id == to_test.parent_id,
            None => true, // To_test is the first with parent id
        },
        Some(p) => match to_test.parent_id.as_ref() {
            Some(pid) => {
                *pid == *partial_result
                    .get(desci.iter().position(|k| k.id == *p).unwrap())
                    .expect("not in partial")
                    .my_id
                    .as_ref()
                    .expect("no my id")
            }
            None => false,
        },
    }
}

fn get_iterator_for_all_matching<'a>(
    desci: &'a SpeciesLeft,
    available: &'a Vec<EnumeratedSpecies>,
) -> impl Iterator<Item = &'a EnumeratedSpecies> {
    available.iter().filter(move |k| {
        (k.species_name == desci.species_name)
            && desci.attribute_constraint.iter().all(|v| {
                v.eval_partial_to_bool(&AllOrSingleLeft::SingleLeft(k), None, None, false)
                    .unwrap_or(true)
            })
    })
}

pub struct MatchingIterator<'a> {
    set_of_new_species: &'a HashSet<EnumeratedSpecies>,
    description: &'a Vec<SpeciesLeft>,
    optimized_doublicate_lookup: Vec<Vec<usize>>,
    rough_matches: Vec<Vec<&'a EnumeratedSpecies>>,
    internal_iterators: Vec<usize>,
    partial_result: Vec<EnumeratedSpecies>, //BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>,
}

impl<'a> Iterator for MatchingIterator<'a> {
    type Item = BTreeMap<SpeciesLeftIdentifier, EnumeratedSpecies>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.description.is_empty() {
            if self.internal_iterators[0] == 0 {
                self.internal_iterators[0] = 1;
                return Some(BTreeMap::new());
            } else {
                return None;
            }
        }
        debug_assert_eq!(self.internal_iterators.len(), self.description.len());
        //println!("next");

        loop {
            if self.partial_result.len() == self.description.len() {
                if self
                    .partial_result
                    .iter()
                    .any(|k| self.set_of_new_species.contains(k))
                {
                    let r_clone = self
                        .description
                        .iter()
                        .map(|k| k.id.clone())
                        .zip(self.partial_result.iter().cloned())
                        .collect();
                    self.partial_result.pop();
                    return Some(r_clone);
                } else {
                    self.partial_result.pop();
                }
            }
            //(self.partial_result.len() < self.description.len()) {
            // exausted one list
            if self.internal_iterators[self.partial_result.len()]
                >= self.rough_matches[self.partial_result.len()].len()
            {
                if self.partial_result.len() == 0 {
                    return None;
                }
                for i in self.partial_result.len()..self.description.len() {
                    self.internal_iterators[i] = 0;
                }
                self.partial_result.pop().unwrap();
                continue;
            }
            let to_test = self.rough_matches[self.partial_result.len()]
                [self.internal_iterators[self.partial_result.len()]];
            self.internal_iterators[self.partial_result.len()] += 1;
            if matches_description(
                to_test,
                &self.description[self.partial_result.len()],
                &self.partial_result,
                self.description,
                &self.optimized_doublicate_lookup[self.partial_result.len()],
            ) {
                self.partial_result.push(to_test.clone());
            }
        }
    }
}

impl<'a> MatchingIterator<'a> {
    pub(crate) fn new(
        all_species: &'a Vec<EnumeratedSpecies>,
        description: &'a Vec<SpeciesLeft>,
        set_of_new_species: &'a HashSet<EnumeratedSpecies>,
    ) -> MatchingIterator<'a> {
        let mut rough_matches: Vec<_> = description
            .iter()
            .map(|k| get_iterator_for_all_matching(k, all_species).collect::<Vec<_>>())
            .collect();
        if rough_matches.iter().any(|k| k.is_empty()) {
            return MatchingIterator {
                set_of_new_species,
                description,
                optimized_doublicate_lookup: vec![vec![]; description.len()],
                rough_matches: vec![vec![]; description.len()],
                internal_iterators: vec![0; description.len()],
                partial_result: Default::default(),
            };
        }
        if description.is_empty() {
            rough_matches = vec![vec![]];
        }
        MatchingIterator {
            set_of_new_species,
            internal_iterators: vec![0; rough_matches.len()],
            rough_matches,
            optimized_doublicate_lookup: description
                .iter()
                .enumerate()
                .map(|(idx_1, k)| {
                    description
                        .iter()
                        .enumerate()
                        .filter(|(idx_2, k2)| {
                            (*idx_2 < idx_1) && (k2.species_name == k.species_name)
                        })
                        .map(|(a, _)| a)
                        .collect()
                })
                .collect(),
            description: description,
            partial_result: Default::default(),
        }
    }
}
