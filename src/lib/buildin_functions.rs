use crate::input::structures::{Dimension, PrimitiveTypes};
use rand::distributions::Distribution;
use rand::{thread_rng, Rng};
use rand_distr::num_traits::FloatConst;
use rand_distr::Normal;
use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

#[derive(Debug, Clone, Serialize, Deserialize, EnumIter, Copy, PartialEq)]
pub enum BuildinFunction {
    // Constants
    Pi,
    Euler,

    Sin,
    Cos,
    Exp,
    Abs,
    AbsOrZero,

    // Conversion
    Round,
    Ceil,
    Floor,

    // Random
    RandomGauss,
    RandomUniform,
    RandomPoisson,

    // Special
    Time,
}

impl BuildinFunction {
    pub fn input_output_types(&self) -> (Vec<Dimension>, Dimension) {
        match self {
            BuildinFunction::Round | BuildinFunction::Ceil | BuildinFunction::Floor => (
                vec![Dimension::dimensionless()],
                Dimension::dimonsionless_integer(),
            ),
            BuildinFunction::Sin
            | BuildinFunction::Cos
            | BuildinFunction::Abs
            | BuildinFunction::AbsOrZero
            | BuildinFunction::Exp => {
                (vec![Dimension::dimensionless()], Dimension::dimensionless())
            }
            BuildinFunction::Pi | BuildinFunction::Euler => (vec![], Dimension::dimensionless()),
            //BuildinFunction::Euler => {}
            BuildinFunction::RandomGauss | BuildinFunction::RandomUniform => (
                vec![Dimension::dimensionless(), Dimension::dimensionless()],
                Dimension::dimensionless(),
            ),
            BuildinFunction::RandomPoisson => (
                vec![Dimension::dimensionless()],
                Dimension::dimonsionless_integer(),
            ),
            BuildinFunction::Time => {
                let mut d = Dimension::dimensionless();
                d.time = 1;
                (vec![], d)
            }
        }
    }

    pub fn is_random(&self) -> bool {
        match self {
            BuildinFunction::Pi
            | BuildinFunction::Euler
            | BuildinFunction::Sin
            | BuildinFunction::Exp
            | BuildinFunction::Cos
            | BuildinFunction::Round
            | BuildinFunction::Abs
            | BuildinFunction::AbsOrZero
            | BuildinFunction::Ceil
            | BuildinFunction::Floor => false,
            BuildinFunction::RandomGauss
            | BuildinFunction::RandomPoisson
            | BuildinFunction::RandomUniform => true,
            BuildinFunction::Time => false,
        }
    }

    pub fn is_constant(&self) -> bool {
        match self {
            BuildinFunction::Pi
            | BuildinFunction::Euler
            | BuildinFunction::Sin
            | BuildinFunction::Exp
            | BuildinFunction::Cos
            | BuildinFunction::Abs
            | BuildinFunction::AbsOrZero
            | BuildinFunction::Round
            | BuildinFunction::Ceil
            | BuildinFunction::Floor => true,
            BuildinFunction::RandomGauss
            | BuildinFunction::RandomPoisson
            | BuildinFunction::RandomUniform => false,
            BuildinFunction::Time => false,
        }
    }

    pub fn eval(
        &self,
        data: &Vec<PrimitiveTypes>,
        final_evaluation: bool,
        time: Option<f64>,
    ) -> PrimitiveTypes {
        match self {
            BuildinFunction::Sin => data[0].as_float().unwrap().sin().into(),
            BuildinFunction::Cos => data[0].as_float().unwrap().cos().into(),
            BuildinFunction::Pi => f64::PI().into(),
            BuildinFunction::Abs => data[0].as_float().unwrap().abs().into(),
            BuildinFunction::AbsOrZero => {
                if data[0].as_float().unwrap() > 0. {
                    data[0].as_float().unwrap().into()
                } else {
                    0.0.into()
                }
            }
            BuildinFunction::Euler => f64::E().into(),
            BuildinFunction::RandomUniform => {
                assert!(final_evaluation);
                let mut rng = thread_rng();
                rng.gen_range(data[0].as_float().unwrap()..data[1].as_float().unwrap())
                    .into()
            }
            BuildinFunction::RandomPoisson => {
                assert!(final_evaluation);
                let mut rng = thread_rng();
                let dist = rand_distr::Poisson::new(data[0].as_float().unwrap());
                match dist {
                    Ok(d) => PrimitiveTypes::Integer(d.sample(&mut rng).round() as i64),
                    Err(k) => {
                        panic!("Error sampling from Poisson: {}", k)
                    }
                }
            }
            BuildinFunction::RandomGauss => {
                assert!(final_evaluation);
                let mut rng = thread_rng();
                let dist = Normal::new(data[0].as_float().unwrap(), data[1].as_float().unwrap());
                match dist {
                    Ok(d) => d.sample(&mut rng).into(),
                    Err(k) => {
                        panic!("Error sampling from Gauss: {}", k)
                    }
                }
            }
            BuildinFunction::Round => (data[0].as_float().unwrap().round() as i64).into(),
            BuildinFunction::Ceil => {
                //println!("{:?}", data[0]);
                (data[0].as_float().unwrap().ceil() as i64).into()
            }
            BuildinFunction::Floor => (data[0].as_float().unwrap().floor() as i64).into(),
            BuildinFunction::Time => {
                assert!(final_evaluation);
                time.unwrap().into()
            }
            BuildinFunction::Exp => data[0].as_float().unwrap().exp().into(),
        }
    }
}
pub fn list_of_buildin_functions() -> Vec<(String, BuildinFunction)> {
    BuildinFunction::iter()
        .map(|k| (format!("{:?}", k).to_lowercase(), k))
        .collect()
}
