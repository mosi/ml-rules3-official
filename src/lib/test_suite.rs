use crate::input::semantic_conversion;
use crate::running::*;
use glob::glob;
use more_asserts::*;
use rayon::prelude::*;

#[test]
#[cfg(feature = "hybrid_att")]
fn example_1() {
    let output = test_output_initial_propensities("testing/a2b_rates.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 15.);
    output.assert_propensity(1, 0.);
    output.assert_propensity(2, 10. + 12.);
    output.assert_propensity(3, 10.);
    output.assert_propensity(4, 10.);
    output.assert_propensity(5, 2440.);
    output.assert_propensity(6, 2440.);
}

#[test]
fn example_2() {
    let output = test_output_state("testing/a2b_cpx.mlr", TestUntil::Time(10.));
    println!("Output: {}", output);
    assert_eq!(output.count_species("A"), 0);
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["k"].value_si == 100.into()),
        0
    );
    assert_eq!(output.count_species("B"), 1000);
    assert_eq!(
        output.count_species_constraint("B", |b| b.attributes["v"].value_si == "test".into()),
        1000
    );
}

#[test]
#[cfg(feature = "hybrid_att")]
fn example_3() {
    let output = test_output_state("testing/a2b_cpx_star2.mlr", TestUntil::Time(10.));
    println!("Output: {}", output);
    assert_eq!(output.count_species("A1"), 0);
    assert_eq!(
        output.count_species("Cslow") + output.count_species("B1"),
        1000
    );
    assert_gt!(output.count_species("B1"), 990);
    assert_lt!(output.count_species("Cslow"), 10);
}
#[test]
fn example_4() {
    test_state_eq(
        "testing/error_cases_dez_22/error_1.mlr",
        "System{38 L(ext = 1) + P(inside = 0){L(ext = 0)} + Cell + P(inside = 0){L(ext = 0)}}",
        TestUntil::Step(2),
    );
}

#[test]
fn immideate() {
    //let output = test_output_state("testing/a2b_imm.mlr", TestUntil::Time(10.));
    test_state_eq("testing/a2b_imm.mlr", "10000 C", TestUntil::NoMoreReaction);
}

#[test]
fn test_time_conversion() {
    assert_eq!(semantic_conversion::string_to_seconds("1[s]").unwrap(), 1.0);
    assert_eq!(
        semantic_conversion::string_to_seconds("1[min]").unwrap(),
        60.0
    );
    assert_eq!(
        semantic_conversion::string_to_seconds("30[h]").unwrap(),
        60. * 60. * 30.
    );
    assert_eq!(
        semantic_conversion::string_to_seconds("1.2[day]").unwrap(),
        103680.
    );
}
#[test]
#[should_panic]
fn time_conversion_fail() {
    assert!(semantic_conversion::string_to_seconds("1.4[Sim]").is_none());
}

/*#[test]
#[cfg(feature = "hybrid_att")]
fn test_all_benchmark() {
    let vector: Vec<_> = glob("benchmarkmodels/*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}
*/
 */

#[test]
fn test_d() {
    let vector: Vec<_> = glob("benchmarkmodels/d*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());

    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}

#[test]
fn test_gm() {
    let vector: Vec<_> = glob("benchmarkmodels/GM.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());

    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Time(10000.));
    });
}

#[test]
fn test_lip() {
    let vector: Vec<_> = glob("benchmarkmodels/Lipo*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());

    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Time(10000.));
    });
}

#[test]
fn test_nested() {
    let vector: Vec<_> = glob("benchmarkmodels/nested*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Time(1.));
    });
}

#[test]
#[ignore]
fn test_netw() {
    let vector: Vec<_> = glob("benchmarkmodels/*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        if !path.contains("network_static.mlr") {
            test_output_state(path.as_str(), TestUntil::Step(10));
        }
    });
}

#[test]
#[ignore]
fn test_wnt() {
    let vector: Vec<_> = glob("benchmarkmodels/*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());
    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}

#[test]
#[should_panic]
fn test_netw2() {
    let vector: Vec<_> = glob("benchmarkmodels/network_hybrid.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());
    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}

#[test]
fn test_yea() {
    let vector: Vec<_> = glob("benchmarkmodels/Yea*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());
    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}

#[test]
fn test_stm() {
    let vector: Vec<_> = glob("benchmarkmodels/Stem*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());

    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}
#[test]
#[ignore]
#[cfg(feature = "hybrid_att")]
fn test_p() {
    let vector: Vec<_> = glob("benchmarkmodels/P*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());

    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}
#[test]
fn test_p2() {
    let vector: Vec<_> = glob("benchmarkmodels/p*.mlr")
        .unwrap()
        .map(|k| k.unwrap())
        .collect();
    assert!(!vector.is_empty());

    vector.par_iter().for_each(|m| {
        //.into_par_iter() {
        let path = format!("{}", m.display());
        println!("Testing bench: {}", path);
        test_output_state(path.as_str(), TestUntil::Step(10));
    });
}

#[test]
fn decay_nesting1() {
    test_state_eq(
        "testing/test_suit/decay_nesting1.mlr",
        "A{20 C}",
        TestUntil::Step(1),
    );
}

#[test]
fn shuttle1() {
    test_state_eq(
        "testing/test_suit/shuttle1.mlr",
        "A{5 B(att = 1)} + 5 B(att = 0)",
        TestUntil::Step(5),
    );
}
#[test]
fn fission1() {
    let output = test_output_state("testing/test_suit/fission.mlr", TestUntil::NoMoreReaction);
    println!("Output: {}", output);
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 10.into()),
        0
    );
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 20.into()),
        0
    );
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 1.into()),
        1
    );
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 2.into()),
        1
    );
    println!("{}", output);
    assert_eq!(output.count_species("B"), 10);
}

#[test]
fn fusion() {
    let output = test_output_state("testing/fusion.mlr", TestUntil::Step(1));
    println!("Output: {}", output);
}

#[test]
fn fusion1() {
    let output = test_output_state("testing/test_suit/fusion1.mlr", TestUntil::Step(1));
    println!("Output: {}", output);
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 3.into()),
        1
    );
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 1.into()),
        0
    );
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 2.into()),
        0
    );
    assert_eq!(output.count_species("B"), 15);
}

#[test]
#[cfg(feature = "hybrid_att")]
fn timed_event1() {
    let output = test_output_state("testing/test_suit/timed_event1.mlr", TestUntil::Time(9.9));
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 1.into()),
        10
    );
    assert_eq!(
        output.count_species_constraint("A", |a| a.attributes["att"].value_si == 2.into()),
        10
    );
}
#[test]
#[cfg(feature = "hybrid_att")]
fn timed_event2() {
    let output2 = test_output_state("testing/test_suit/timed_event1.mlr", TestUntil::Time(10.1));
    assert_eq!(
        output2.count_species_constraint("A", |a| a.attributes["att"].value_si == 1.into()),
        0
    );
    assert_eq!(
        output2.count_species_constraint("A", |a| a.attributes["att"].value_si == 2.into()),
        10
    );
}

#[test]
fn bimolecular1() {
    let output = test_output_initial_propensities("testing/test_suit/bimolecular_rates.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 100.);
    output.assert_propensity(1, 100.);
}

#[test]
fn bimolecular2() {
    let output = test_output_initial_propensities("testing/test_suit/bimolecular_rates2.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 380.);
    output.assert_propensity(1, 400.);
}

#[test]
fn si_units() {
    let output = test_output_initial_propensities("testing/test_suit/SI_units.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 60.);
    output.assert_propensity(1, 1.);
    output.assert_propensity(2, 0.01);
    output.assert_propensity(3, 0.001);
    output.assert_propensity(4, 1.0e-6);
}

#[test]
fn observe_basic() {
    test_observe(
        vec!["A", "B", "C", "D"],
        " 10 D {3 B} + 11 B",
        "##B",
        (3 * 10 + 11).into(),
    )
}

#[test]
fn observe_basic2() {
    test_observe(
        vec!["A", "B", "C", "D"],
        " 10 D {3 B} + 11 B",
        "##(B in D)",
        (3 * 10).into(),
    )
}

#[test]
fn observe_basic3() {
    test_observe(
        vec!["A", "B", "C", "D"],
        " 10 D {3 B + C{ 10 B}} + 11 B",
        "##(B anywhere_in D)",
        (10 * (3 + 10)).into(),
    )
}

#[test]
#[should_panic]
fn observe_basic4() {
    test_observe(
        vec!["A", "B", "C", "D"],
        " 10 D {3 B + C{ 10 B}} + 11 B",
        "#(B anywhere_in D)",
        (10 * (3 + 10)).into(),
    )
}

#[test]
fn observe_basic5() {
    test_observe(
        vec!["A(a: int)", "B(b:int)", "C(c:int)", "D(d:int)"],
        " 10 D(d = 10) {3 B(b=1) + C(c=4){ 4B(b=-10) + 6 B(b=10)}} + 11 B(b=100)",
        "##(B(b<10) anywhere_in D)",
        (10 * (3 + 4)).into(),
    )
}

#[test]
fn observe_basic6() {
    test_observe(
        vec!["A(a: int)", "B(b:int)", "C(c:int)", "D(d:int)"],
        " 10 D(d = 10) {3 B(b=1) + C(c=4){ 4B(b=-10) + 6 B(b=10)}} + 11 B(b=100)",
        "##(B(b<10 + time()/1[s]) anywhere_in D)",
        (10 * (3 + 4)).into(),
    )
}

#[test]
#[ignore]
fn observe_basic7() {
    test_observe(
        vec!["A(a: int)", "B(b:int)", "C(c:int)", "D(d:int)"],
        " 10 D(d = 10) {3 B(b=1) + C(c=4){ 4B(b=-10) + 6 B(b=10)}} + 11 B(b=100) + D(d = -100){100B(b = -100)}",
        "##(B(b<10 + __time__/1[s]) anywhere_in D(d > 0))",
        (10 * (3 + 4)).into(),
    )
}
#[test]
fn nested_counts_in_rate() {
    let output = test_output_initial_propensities("testing/nested_counts_in_rate.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 10. * 9.);
    output.assert_propensity(1, 10. * 9.);
    output.assert_propensity(2, 10. * 9. * 10.);
    output.assert_propensity(3, 10. * 10. * (9. + 3. * 8. * 9.));
    output.assert_propensity(4, 10. * (9. + 3. * 8. * 9.));
}

#[test]
fn nested_counts_as_amt() {
    test_state_eq(
        "testing/nested_counts_as_amt.mlr",
        "90 F",
        TestUntil::Step(10),
    );
}

#[test]
#[should_panic]
fn nested_counts_observe() {
    test_output_state("testing/nested_counts_observe.mlr", TestUntil::Step(10));
}
#[test]
#[should_panic]
fn nested_counts_observe_fail() {
    test_output_initial_propensities("testing/nested_counts_observe_fail.mlr");
}

#[test]
#[should_panic]
fn nested_counts_observe_fail_b() {
    test_output_initial_propensities("testing/nested_counts_observe_fail_B.mlr");
}

#[test]
#[cfg(feature = "hybrid_att")]
fn attributes1() {
    test_state_eq(
        "testing/test_suit/attributes1.mlr",
        "A(i = 2 , f1 = 3.0, f2 = 2.0, s = 'neu')",
        TestUntil::Step(1),
    );
}

#[test]
#[cfg(feature = "hybrid_att")]
fn attributes_if() {
    test_state_eq(
        "testing/test_suit/attributes_if.mlr",
        "A(i = 2 , f1 = 3.0, f2 = 2.0, s = 'neu')",
        TestUntil::Step(1),
    );
}

#[test]
#[cfg(feature = "hybrid_att")]
fn attributes2() {
    test_state_eq(
        "testing/test_suit/attributes2.mlr",
        "A(i = 1, s = 'TeSt', f = 5.4 [V])",
        TestUntil::Step(1),
    );
}

#[test]
#[cfg(feature = "hybrid_att")]
fn logic1() {
    let output = test_output_initial_propensities("testing/test_suit/logic1.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 0.);
    output.assert_propensity(1, 0.);
    output.assert_propensity(2, 1.);
    output.assert_propensity(3, 0.);
    output.assert_propensity(4, 1.);
    output.assert_propensity(5, 1.);
    output.assert_propensity(6, 1.);
    output.assert_propensity(7, 1.);
    output.assert_propensity(8, 1.);
    output.assert_propensity(9, 0.);
}
#[test]
#[should_panic]
fn missing_attribute() {
    test_output_state(
        "testing/test_suit/missing_attribute.mlr",
        TestUntil::Step(10),
    );
}

#[test]
fn rate_expr_parsing() {
    let output = test_output_initial_propensities("testing/test_suit/guard_rate_law1.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 50.);
    output.assert_propensity(1, 10.);
}

#[test]
#[should_panic]
fn wrong_units1() {
    test_output_state("testing/test_suit/wrong_units1.mlr", TestUntil::Step(10));
}

#[test]
fn guard_rate_law1() {
    let output = test_output_initial_propensities("testing/test_suit/guard_rate_law1.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 50.);
    output.assert_propensity(1, 10.);
}

#[test]
#[cfg(feature = "hybrid_att")]
fn guard_rate_law2() {
    let output = test_output_initial_propensities("testing/test_suit/guard_rate_law2.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 10.);
    output.assert_propensity(1, 0.);
    output.assert_propensity(2, 30.);
    output.assert_propensity(3, 0.);
}

#[test]
#[cfg(feature = "hybrid_att")]
fn guard_rate_law3() {
    let output = test_output_initial_propensities("testing/test_suit/guard_rate_law3.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 10.);
    output.assert_propensity(1, 0.);
    output.assert_propensity(2, 30.);
    output.assert_propensity(3, 0.);
}

#[test]
fn create() {
    let n = 100;
    let average_a = (0..n)
        .map(|_| {
            test_output_state("testing/test_suit/create.mlr", TestUntil::Time(1000.))
                .count_species("A") as f64
        })
        .sum::<f64>()
        / n as f64;
    test_output_initial_propensities("testing/test_suit/create.mlr").assert_propensity(0, 1.);
    assert_lt!(average_a, 1010. * 1.1);
    assert_gt!(average_a, 990. * 0.9);
}

#[test]
fn decay() {
    let n = 20;
    let average_a = (0..n)
        .map(|_| {
            test_output_state("testing/test_suit/decay.mlr", TestUntil::Time(7.)).count_species("A")
                as f64
        })
        .sum::<f64>()
        / n as f64;
    test_output_initial_propensities("testing/test_suit/decay.mlr").assert_propensity(0, 1000.);
    assert_lt!(average_a, 5214.1);
    assert_gt!(average_a, 4717.6);
}

#[test]
fn michaelis_menten1() {
    let n = 10;
    let average_s = (0..n)
        .map(|_| {
            test_output_state(
                "testing/test_suit/michaelis_menten1.mlr",
                TestUntil::Time(2.),
            )
            .count_species("S") as f64
        })
        .sum::<f64>()
        / n as f64;
    assert_lt!(average_s, 284.0 * 1.2);
    assert_gt!(average_s, 272.9 * 0.8);
}

#[test]
fn michaelis_menten2() {
    let n = 20;
    let average_s = (0..n)
        .map(|_| {
            test_output_state(
                "testing/test_suit/michaelis_menten2.mlr",
                TestUntil::Time(6.),
            )
            .count_species("S") as f64
        })
        .sum::<f64>()
        / n as f64;
    assert_lt!(average_s, 830.8 * 1.2);
    assert_gt!(average_s, 798.3 * 0.8);
}

#[test]
fn functions_on_solutions1() {
    let output = test_output_initial_propensities("testing/test_suit/functions_on_solutions.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 10.);
}

#[test]
fn functions_on_solutions2() {
    let output = test_output_state(
        "testing/test_suit/functions_on_solutions2.mlr",
        TestUntil::Step(1),
    );
    assert_eq!(output.count_species("Cell"), 2);
    assert_eq!(output.count_species("A"), 9);
    assert_eq!(output.count_species("B"), 21);
}
#[test]
fn lipoplex_small() {
    let n = 50;
    let average_a = (0..n)
        .map(|_| {
            test_output_state(
                "testing/test_suit/multi_model_40.mlr",
                TestUntil::Time(4000.),
            )
            .count_species("L") as f64
        })
        .sum::<f64>()
        / n as f64;
    assert_lt!(average_a, 40.);
    assert_gt!(average_a, 0.);
}
#[test]
fn lipoplex() {
    let n = 10;
    let average_a = (0..n)
        .map(|_| {
            test_output_state("testing/test_suit/multi_model.mlr", TestUntil::Time(4000.))
                .count_species("L") as f64
        })
        .sum::<f64>()
        / n as f64;
    assert_lt!(average_a, 400.);
    assert_gt!(average_a, 0.);
}

#[test]
fn variable_mrna() {
    let output = test_output_initial_propensities("testing/test_suit/variable_mRNA.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 0.0001666666666667);
    output.assert_propensity(1, 0.0);
}

#[test]
fn attributes_env_a() {
    test_output_state(
        "testing/test_suit/attributes_env_f_pth_1.mlr",
        TestUntil::Time(7000.),
    );
}

#[test]
fn attributes_env_b() {
    test_output_state(
        "testing/test_suit/attributes_env_f_pth_100.mlr",
        TestUntil::Time(7000.),
    );
}

#[test]
fn species_without_attr() {
    test_output_state(
        "testing/test_suit/species_without_attr.mlr",
        TestUntil::Time(1000.),
    );
}

#[test]
fn clone1() {
    test_state_eq(
        "testing/test_suit/clone.mlr",
        "2 A{10 B}",
        TestUntil::Step(1),
    );
}

#[test]
fn clone2() {
    test_state_eq(
        "testing/test_suit/clone2.mlr",
        "2 A{9 B + 4 C{3D}}",
        TestUntil::Step(1),
    );
}

#[test]
fn attribute_split() {
    test_state_eq(
        "testing/test_suit/attribute_split.mlr",
        "A(vol=5[dm^3]){10 C} + A(vol=5[dm^3]){} + B(vol=5){10 C} + B(vol=5){}",
        TestUntil::Time(300.),
    );
}

#[test]
fn comp_structural_change() {
    test_output_state_no_struc_change(
        "testing/test_suit/comp-structure-change.mlr",
        TestUntil::Time(1000.),
    );
}

#[test]
fn comp_no_structural_change() {
    test_output_state_no_struc_change(
        "testing/test_suit/comp-no-structure-change.mlr",
        TestUntil::Time(1000.),
    );
}

#[test]
#[ignore]
fn assertion_failed() {
    test_output_state(
        "testing/test_suit/assertion_failed.mlr",
        TestUntil::Time(1000000.),
    );
}

#[test]
fn dyn_amount_1() {
    let output = test_output_state("testing/test_suit/dyn_amount_1.mlr", TestUntil::Time(10.));
    println!("Output: {}", output);
    assert_eq!(
        (output.count_species("A") * (output.count_species("A") - 1)) / 2,
        output.count_species("B")
    );
}

#[test]
fn dyn_amount_2() {
    test_state_eq(
        "testing/test_suit/dyn_amount_2.mlr",
        "A(i = 11) + 55 B",
        TestUntil::Step(10),
    );
}

#[test]
fn dyn_amount_3() {
    let output = test_output_state("testing/test_suit/dyn_amount_3.mlr", TestUntil::Step(10));
    println!("Output: {}", output);
    /*assert_eq!(
        (output.count_species("A") * (output.count_species("A") - 1)) / 2,
        output.count_species("B")
    );*/
    assert_eq!(output.count_species("A") - 1, output.count_species("B"));
    assert_eq!(output.count_species("C"), 100);
    assert_eq!(output.count_species("B"), output.count_species("D"));
}

#[test]
fn dyn_amount_4() {
    let output = test_output_state("testing/test_suit/dyn_amount_4.mlr", TestUntil::Step(10));
    println!("Output: {}", output);
    assert_eq!(output.count_species("A"), output.count_species("B") + 2);
    assert_eq!(output.count_species("C"), 200);
}

#[test]
fn dyn_amount_5() {
    test_state_eq(
        "testing/test_suit/dyn_amount_5.mlr",
        "100 B{3 D}",
        TestUntil::Step(10),
    );
}

#[test]
fn dyn_amount_6() {
    test_state_eq(
        "testing/test_suit/dyn_amount_6.mlr",
        "100 B{5 D{ (2) C}}",
        TestUntil::Step(10),
    );
}

#[test]
fn split_error_1() {
    let output = test_output_state("testing/test_suit/split_test_1.mlr", TestUntil::Step(10));
    println!("Output: {}", output);
}

#[test]
fn immediately() {
    let _output = test_output_state("testing/test_suit/immediately.mlr", TestUntil::Step(10));
}

#[test]
fn count_in_condition() {
    let _output = test_output_state(
        "testing/test_suit/count_in_condition.mlr",
        TestUntil::Step(10),
    );
}

#[test]
fn count_in_if() {
    let _output = test_output_state("testing/test_suit/count.mlr", TestUntil::Step(10));
}

#[test]
fn dynamic_amount_zero() {
    let _output = test_output_state(
        "testing/test_suit/dynamic_amount_zero.mlr",
        TestUntil::Step(10),
    );
}

#[test]
fn eval_to_zero() {
    let _output = test_output_state("testing/test_suit/eval_to_zero.mlr", TestUntil::Step(86400));
}

#[test]
#[ignore]
fn split_random() {
    let output = test_output_state("testing/test_suit/split_random.mlr", TestUntil::Step(3));
    println!("Output: {}", output);
    assert_eq!(output.count_species("Test1"), 1);
    assert_eq!(output.count_species("Test2"), 1);
}

#[test]
fn empty_sol_init() {
    let output2 = test_output_initial_propensities("testing/test_suit/empty_sol.mlr");
    output2.assert_propensity(0, 1.0);
}
#[test]
fn empty_sol() {
    let output = test_output_state("testing/test_suit/empty_sol.mlr", TestUntil::Step(0));
    println!("Output: {}", output);
    assert_eq!(output.count_species("X"), 0);
    assert_eq!(output.count_species("Y"), 0);
}
#[test]
fn empty_sol2() {
    let output2 = test_output_state("testing/test_suit/empty_sol.mlr", TestUntil::Step(5));
    println!("Output: {}", output2);
    assert_eq!(output2.count_species("Y"), 5);
}

#[test]
#[should_panic]
fn gauss_test() {
    test_output_state("testing/test_suit/gauss_dist.mlr", TestUntil::Step(10));
}

#[test]
fn var_create_1() {
    let output = test_output_state(
        "testing/test_suit/var_create_1.mlr",
        TestUntil::NoMoreReaction,
    );
    println!("Output: {}", output);
    assert_eq!(output.count_species("A"), 5);
    assert_eq!(output.count_species("B"), 5);
    assert_eq!(output.count_species("C"), 110);
}

#[test]
fn var_create_2() {
    let output = test_output_state(
        "testing/test_suit/var_create_2.mlr",
        TestUntil::NoMoreReaction,
    );
    println!("Output: {}", output);
    assert_eq!(output.count_species("A"), 10);
    assert_eq!(output.count_species("B"), 10);
    assert_eq!(output.count_species("C"), 130);
}

#[test]
fn var_create_3() {
    let output = test_output_state(
        "testing/test_suit/var_create_3.mlr",
        TestUntil::NoMoreReaction,
    );
    println!("Output: {}", output);
    assert_eq!(output.count_species("A"), 5);
    assert_eq!(output.count_species("B"), 5);
    assert_eq!(output.count_species("C"), 100);
}

#[test]
#[cfg(feature = "hybrid_att")]
fn feature_test1() {
    assert!(use_hybrid_att())
}

#[test]
#[cfg(not(feature = "hybrid_att"))]
fn feature_test2() {
    assert!(!use_hybrid_att())
}
#[test]
#[cfg(feature = "hybrid_att")]
#[should_panic]
fn var_create_4() {
    let output = test_output_state(
        "testing/test_suit/var_create_4.mlr",
        TestUntil::NoMoreReaction,
    );
    println!("Output: {}", output);
    assert_eq!(output.count_species("A"), 5);
    assert_eq!(output.count_species("B"), 5);
    assert_eq!(output.count_species("C"), 100);
}

#[test]
fn var_create_5() {
    let output = test_output_state(
        "testing/test_suit/var_create_5.mlr",
        TestUntil::NoMoreReaction,
    );
    println!("Output: {}", output);
    assert_eq!(output.count_species("A"), 5);
    assert_eq!(output.count_species("B"), 5);
    assert_lt!(output.count_species("C"), 19);
    assert_gt!(output.count_species("C"), 11);
}

#[test]
fn struc_test() {
    test_state_eq(
        "testing/test_suit/struc_test.mlr",
        "Cell{LR{2Protein} + 98 Protein}",
        TestUntil::Step(1),
    );
    test_state_eq(
        "testing/test_suit/struc_test.mlr",
        "Cell{LR{2Protein} + LR{2Protein} + 96 Protein}",
        TestUntil::Step(2),
    );
}

#[test]
#[cfg(feature = "hybrid_att")]
#[should_panic]
fn three_att_reaction1() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_reaction1.mlr");
    println!("Output: {}", output);
    /*output.assert_propensity(0, 9900.0);
    output.assert_propensity(1, 5000.0);*/
}

#[test]
fn three_att_reaction2() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_reaction2.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 9900.0);
    output.assert_propensity(1, 5000.0);
}

#[test]
#[cfg(feature = "hybrid_att")]
#[should_panic]
fn three_att_reaction3() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_reaction3.mlr");
    println!("Output: {}", output);
    /*output.assert_propensity(0, 9900.0);
    output.assert_propensity(1, 5000.0);*/
}

#[test]
#[cfg(feature = "hybrid_att")]
fn three_att_reaction4() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_reaction4.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 9900.0);
    output.assert_propensity(1, 5000.0);
}

#[test]
#[cfg(feature = "hybrid_att")]
#[should_panic]
fn three_att_reaction5() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_reaction5.mlr");
    println!("Output: {}", output);
    /*output.assert_propensity(0, 9900.0);
    output.assert_propensity(1, 5000.0);*/
}

#[test]
fn three_att_reaction6() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_reaction6.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 9900.0);
    output.assert_propensity(1, 5000.0);
}

#[test]
#[cfg(feature = "hybrid_att")]
#[should_panic]
fn three_att_reaction7() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_reaction7.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 9900.0);
    output.assert_propensity(1, 5000.0);
}

#[test]
#[cfg(feature = "hybrid_att")]
#[should_panic]
fn three_att_create1() {
    let output = test_output_initial_propensities("testing/test_suit/three_att_create1.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 9900.0);
}

#[test]
fn large_rule() {
    let output = test_output_initial_propensities("testing/test_suit/large_rule.mlr");
    println!("Output: {}", output);
    output.assert_propensity(0, 2991600.0 * 0.);
    output.assert_propensity(1, 855.0);
}

#[test]
fn large_rule2() {
    let output2 = test_output_state("testing/test_suit/large_rule.mlr", TestUntil::Step(100));
    println!("Output: {}", output2);
    assert_eq!(
        output2.count_species("A") + output2.count_species("B") + 2 * output2.count_species("C"),
        167928
    );
    assert_eq!(
        output2.count_species("A") + output2.count_species("C"),
        167055
    );
    assert_eq!(output2.count_species("B") + output2.count_species("C"), 873);
}

#[test]
fn max_count_1() {
    let output = test_output_state("testing/test_suit/max_count_1.mlr", TestUntil::Time(10.));
    println!("Output: {}", output);
    assert_eq!(output.count_species("B"), 20);
}
