#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct MLRgrammar;

use crate::input::structures::{
    AttributeDimensionality, Dimension, FloatOp, LanguageError, LogicalOp, NonNumericalDimension,
};

use askama::Template;
use log::*;
use pest::error::{Error, ErrorVariant, LineColLocation};
use pest::iterators::{Pair, Pairs};
use pest::pratt_parser::*;
use pest::{Parser, Span};
use std::collections::{HashMap, HashSet};
use std::fmt::Formatter;
use std::ops;

use crate::input::units::match_string_to_unit;
use itertools::Itertools;
use lazy_static::*;

impl LanguageError {
    pub fn pest(x: pest::error::Error<Rule>, line: u32, file: &str) -> LanguageError {
        let (l1, l2, c1, c2) = match x.line_col {
            LineColLocation::Pos((l, c)) => (l, l, c, c + 2),
            LineColLocation::Span((l1, c1), (l2, c2)) => (l1, l2, c1, c2),
        };
        assert!(l2 >= l1);
        assert!(c2 >= c1);
        LanguageError {
            lstart: l1,
            lstop: l2,
            cstart: c1,
            cstop: c2,
            what: "Syntax: ".to_string() + &x.to_string(),
            line,
            file: file.to_string(),
        }
    }
    pub fn from_span(span: &pest::Span, message: &str, line: u32, file: &str) -> LanguageError {
        let r = LanguageError::pest(
            Error::<Rule>::new_from_span(
                ErrorVariant::CustomError {
                    message: message.to_string(),
                },
                span.clone(),
            ),
            line,
            file,
        );
        if cfg!(debug_assertions) && cfg!(not(target_arch = "wasm32")) {
            panic!("Syntax error:{}\n{}", message, r.what)
        }
        r
    }
}

lazy_static! {
    static ref PRATT_PARSER: PrattParser<Rule> = {
        use pest::pratt_parser::{Assoc::*, Op};
        use Rule::*;

        // Precedence is defined lowest to highest
        PrattParser::new().
        op(Op::infix(op_boolean,Left)).op(Op::infix(op_comparision,Left))
            // Addition and subtract have equal precedence
            .op(Op::infix(add, Left) | Op::infix(subtract, Left))
            .op(Op::infix(multiply, Left) | Op::infix(divide, Left)).op(Op::infix(power,Right))

    };
}

#[derive(Debug, Clone)]
pub struct AttributeConstraints<'a> {
    pub name: String,
    pub constraint: String,
    pub span: Span<'a>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ValueWithUnit {
    pub unit: Dimension,
    pub value: f64,
}
impl ops::Mul<ValueWithUnit> for ValueWithUnit {
    type Output = ValueWithUnit;
    fn mul(self, rhs: ValueWithUnit) -> Self::Output {
        ValueWithUnit {
            unit: Dimension {
                length: self.unit.length + rhs.unit.length,
                time: self.unit.time + rhs.unit.time,
                weight: self.unit.weight + rhs.unit.weight,
                current: self.unit.current + rhs.unit.current,
                temperature: self.unit.temperature + rhs.unit.temperature,
                amount: self.unit.amount + rhs.unit.amount,
                non_numerical: NonNumericalDimension::Nothing,
            },
            value: self.value * rhs.value,
        }
    }
}

impl ops::Mul<f64> for ValueWithUnit {
    type Output = ValueWithUnit;
    fn mul(mut self, rhs: f64) -> Self::Output {
        self.value *= rhs;
        self
    }
}

impl ValueWithUnit {
    pub fn dimensionless(v: f64) -> ValueWithUnit {
        ValueWithUnit {
            unit: Dimension::dimensionless(),
            value: v,
        }
    }
    /*fn try_add(&self, rhs: &ValueWithUnit) -> Option<ValueWithUnit> {
        match self.unit == rhs.unit {
            false => None,
            true => Some(ValueWithUnit {
                unit: self.unit.clone(),
                value: self.value + rhs.value,
            }),
        }
    }

    fn try_div(self, rhs: ValueWithUnit) -> Option<ValueWithUnit> {
        if rhs.value == 0.0 {
            return None;
        }
        Some(ValueWithUnit {
            unit: Dimension {
                length: self.unit.length - rhs.unit.length,
                time: self.unit.time - rhs.unit.time,
                weight: self.unit.weight - rhs.unit.weight,
                current: self.unit.current - rhs.unit.current,
                temperature: self.unit.temperature - rhs.unit.temperature,
                amount: self.unit.amount - rhs.unit.amount,

                non_numerical: NonNumericalDimension::Nothing,
            },
            value: self.value / rhs.value,
        })
    }*/

    pub fn my_power(self, p: i32) -> ValueWithUnit {
        //assert_eq!(self.value,1.0);
        let my_p = p as i64;
        ValueWithUnit {
            unit: Dimension {
                length: self.unit.length * my_p,
                time: self.unit.time * my_p,
                weight: self.unit.weight * my_p,
                current: self.unit.current * my_p,
                temperature: self.unit.temperature * my_p,
                amount: self.unit.amount * my_p,
                non_numerical: NonNumericalDimension::Nothing,
            },
            value: (self.value).powi(p),
        }
    }
    fn from_pair_list(pair: pest::iterators::Pair<Rule>) -> Result<ValueWithUnit, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::unit_list);
        pair.into_inner()
            .map(|u| {
                assert_eq!(u.as_rule(), Rule::unit_single);
                let inner = u.clone().into_inner().next().unwrap();
                let (unit_str, pow) = match inner.as_rule() {
                    Rule::unit_single_reg => (u.as_str(), 1),
                    Rule::unit_single_exp => {
                        let mut k = inner.into_inner();
                        let unit_name = k.next().unwrap().as_str();
                        let exp: i32 = k.next().unwrap().as_str().parse().unwrap();
                        (unit_name, exp)
                    }
                    _ => panic!("Invalid type"),
                };
                let unit = match_string_to_unit(unit_str);
                if unit.is_none() {
                    return Err(LanguageError::from_span(
                        &u.as_span(),
                        format!("The name '{}' is not a known unit", unit_str).as_str(),
                        line!(),
                        file!(),
                    ));
                }
                Ok(unit.unwrap().my_power(pow))
            })
            .fold(Ok(match_string_to_unit("1").unwrap()), |a, b| {
                match (a, b) {
                    (Ok(a), Ok(b)) => Ok(a * b),
                    (Err(x), _) => Err(x),
                    (_, Err(x)) => Err(x),
                }
            })
    }
    pub fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        with_prefix: bool, //constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<(ValueWithUnit, String), LanguageError> {
        let mut pair2;
        let prefix_value;
        if with_prefix {
            assert_eq!(pair.as_rule(), Rule::float_w_unit);
            pair2 = pair.into_inner();

            let prefix_pair = pair2.next().unwrap();
            prefix_value = prefix_pair.as_str().parse().unwrap();
        } else {
            assert!(pair.as_rule() == Rule::atn_float_unit);
            pair2 = pair.into_inner();
            prefix_value = 1.0;
        }

        let unit_expr = pair2.next();

        if unit_expr.is_none() {
            return Ok((ValueWithUnit::dimensionless(prefix_value), "".to_string()));
        }

        let mut unit_expr = unit_expr.unwrap().into_inner();

        let top_list = unit_expr.next().unwrap();
        let top_unit = ValueWithUnit::from_pair_list(top_list.clone())? * prefix_value;
        let buttom_list = unit_expr.next();
        match buttom_list {
            Some(v) => Ok((
                top_unit * (ValueWithUnit::from_pair_list(v.clone())?.my_power(-1)),
                format!(
                    "{}/{}",
                    top_list.as_str().to_string(),
                    v.as_str().to_string()
                ),
            )),
            None => Ok((top_unit, top_list.as_str().to_string())),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct AttributeRefStx {
    pub spec_name: String,
    pub att_name: String,
}

impl AttributeRefStx {
    pub fn from_pair(pair: pest::iterators::Pair<Rule>) -> AttributeRefStx {
        assert_eq!(pair.as_rule(), Rule::attribute_reference);

        let mut pair = pair.into_inner();
        AttributeRefStx {
            spec_name: pair.next().unwrap().as_str().to_string(),
            att_name: pair.next().unwrap().as_str().to_string(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct ConditionStx<'a> {
    pub span: Span<'a>,
    pub condition: Box<ExprStx<'a>>,
    pub true_branch: Box<ExprStx<'a>>,
    pub false_branch: Option<Box<ExprStx<'a>>>,
}

impl ConditionStx<'_> {
    pub fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        allow_counts: bool,
        allow_attributes: bool,
        allow_global_counts: bool,
        //constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<ConditionStx, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::expr_if);
        let mut pairs = pair.clone().into_inner();
        Ok(ConditionStx {
            span: pair.clone().as_span(),
            condition: Box::new(ExprStx::from_pair(
                pairs.next().unwrap(),
                allow_counts,
                allow_attributes,
                allow_global_counts,
            )?),
            true_branch: Box::new(ExprStx::from_pair(
                pairs.next().unwrap(),
                allow_counts,
                allow_attributes,
                allow_global_counts,
            )?),
            false_branch: match pairs.next() {
                None => None,
                Some(false_branch) => Some(Box::new(ExprStx::from_pair(
                    false_branch,
                    allow_counts,
                    allow_attributes,
                    allow_global_counts,
                )?)),
            },
        })
        /*assert_eq!(pair.as_rule(), Rule::attribute_reference);

        let mut pair = pair.into_inner();
        AttributeRefStx {
            spec_name: pair.next().unwrap().as_str().to_string(),
            att_name: pair.next().unwrap().as_str().to_string(),
        }*/
    }
}

#[derive(Debug, Clone)]
pub enum ExprTermStx<'a> {
    NumUnit(ValueWithUnit, String),
    NamedVariable(String),
    DottedAttributeRef(AttributeRefStx),
    Condition(ConditionStx<'a>),
    Expr(Box<ExprStx<'a>>),
    LeftSpeciesCount(String),
    ComplexSpeciesCount(ComplexCountStx<'a>),
    ActualString(String),
    ActualInt(isize),
    FunctionCall(FunctionCallStx<'a>),
    ActualBool(bool),
    Immidiate,
}
use float_pretty_print::PrettyPrintFloat;

fn pretty_print_latex(x: f64) -> String {
    let k = format!("{}", PrettyPrintFloat(x));
    if k.contains("e") {
        return k.replace("e", "\\cdot 10^{") + "}";
    } else {
        return k;
    }
}
impl<'a> ExprTermStx<'a> {
    fn get_all_strings(&self) -> HashSet<String> {
        match self {
            ExprTermStx::NumUnit(_a, _b) => HashSet::new(),
            ExprTermStx::NamedVariable(k) => HashSet::from([k.clone()]),
            ExprTermStx::DottedAttributeRef(k) => {
                HashSet::from([k.att_name.clone(), k.spec_name.clone()])
            }
            ExprTermStx::Condition(k) => {
                let mut r = k.condition.get_all_strings();
                r.extend(k.true_branch.get_all_strings());
                match &k.false_branch {
                    None => {}
                    Some(fb) => r.extend(fb.get_all_strings()),
                }
                r
            }
            ExprTermStx::Expr(x) => x.get_all_strings(),
            ExprTermStx::LeftSpeciesCount(lsc) => HashSet::from([lsc.clone()]),
            ExprTermStx::ComplexSpeciesCount(gsc) => {
                let mut r = gsc.what.get_all_strings();
                r.insert(gsc.string_name.clone());
                match &gsc.inside_of {
                    InsideOfCountStx::FromLeft(x) => {
                        r.insert(x.clone());
                    }
                    InsideOfCountStx::Global(l) => {
                        r.extend(l.get_all_strings());
                    }

                    InsideOfCountStx::NoneLocal => {}
                    InsideOfCountStx::NoneGlobal => {}
                }
                r
            }
            ExprTermStx::ActualString(s) => HashSet::from([s.clone()]),
            ExprTermStx::ActualInt(_) => HashSet::new(),
            ExprTermStx::FunctionCall(f) => f
                .params
                .iter()
                .map(|k| k.get_all_strings().into_iter())
                .flatten()
                .collect(),
            ExprTermStx::ActualBool(_) => HashSet::new(),
            ExprTermStx::Immidiate => HashSet::new(),
        }
    }
    fn render_latex(&self, may_need_braces: bool) -> String {
        match self {
            ExprTermStx::NumUnit(x, str) => format!(
                "{}  {}",
                pretty_print_latex(x.value),
                if str.is_empty() {
                    "".to_owned()
                } else {
                    format!("\\, \\text{{{}}}", str.replace("1", ""))
                }
            ),
            ExprTermStx::NamedVariable(y) => format!("\\text{{{}}}", y.clone()),
            ExprTermStx::DottedAttributeRef(x) => {
                format!("\\text{{{}}}.\\text{{{}}}", x.spec_name, x.att_name)
            }
            ExprTermStx::Expr(y) => format!("{}", y.render_latex(may_need_braces)),
            ExprTermStx::LeftSpeciesCount(y) => format!("\\text{{\\#{} }}", y),
            ExprTermStx::ComplexSpeciesCount(y) => format!("\\text{{\\#\\#{}}}", y.string_name),
            ExprTermStx::ActualString(y) => format!("\\text{{\"\\emph{{{}}}\"}}", y),
            ExprTermStx::ActualInt(y) => format!("{}", y),
            ExprTermStx::ActualBool(y) => format!("\\text{{ {} }}", y),
            ExprTermStx::FunctionCall(fc) => format!(
                "{}({})",
                fc.name,
                fc.params.iter().map(|y| y.render_latex(false)).join(", ")
            ),
            ExprTermStx::Condition(_) => {
                format!("")
            }
            ExprTermStx::Immidiate => {
                format!("Immediately")
            }
        }
    }
}

fn get_expr_tree(
    pair_in: Pair<Rule>,
    allow_counts: bool,
    allow_attributes: bool,
    allow_global_counts: bool,
) -> Result<ExprStx, LanguageError> {
    //println!("Testing: {:?}", pair_in);
    //TODO check why this is needed
    /*if pair_in.clone().into_iter().next().is_none() {
        return Err(vec![]);
    }*/
    //assert_eq!(pair_in.as_rule(), Rule::expr_float_w_unit);
    let span = pair_in.as_span();
    let mut res =
        PRATT_PARSER
            .map_primary(|pair: Pair<Rule>| match pair.as_rule() {
                Rule::integer => Ok(ExprStx {
                    left: ExprTermStx::ActualInt(pair.as_str().parse().unwrap()),
                    op: None,
                    span: pair.as_span(),
                }),
                Rule::string_value => Ok(ExprStx {
                    left: ExprTermStx::ActualString(
                        pair.clone()
                            .into_inner()
                            .next()
                            .unwrap()
                            .as_str()
                            .to_string(),
                    ),
                    op: None,
                    span: pair.as_span(),
                }),
                Rule::boolean_value => Ok(ExprStx {
                    left: ExprTermStx::ActualBool(pair.as_str().parse().unwrap()),
                    op: None,
                    span: pair.as_span(),
                }),
                Rule::float => Ok(ExprStx::from_val_w_unit(
                    ValueWithUnit::dimensionless(pair.as_str().parse().unwrap()),
                    pair.as_span(),
                    "".to_string(),
                )),
                Rule::float_w_unit => {
                    let (v, unit_str) = ValueWithUnit::from_pair(pair.clone(), true)?;
                    Ok(ExprStx::from_val_w_unit(v, pair.as_span(), unit_str))
                }
                Rule::expr_float_w_unit => {
                    get_expr_tree(pair, allow_counts, allow_attributes, allow_global_counts)
                }
                Rule::name => Ok(ExprStx {
                    left: ExprTermStx::NamedVariable(pair.as_str().to_string()),
                    op: None,
                    span: pair.as_span(),
                }),
                Rule::attribute_reference => Ok(match allow_attributes {
                    true => ExprStx {
                        left: ExprTermStx::DottedAttributeRef(AttributeRefStx::from_pair(
                            pair.clone(),
                        )),
                        op: None,
                        span: pair.as_span(),
                    },
                    false => {
                        return Err(LanguageError::from_span(
                            &pair.as_span(),
                            "Attributes not possible here",
                            line!(),
                            file!(),
                        ));
                    }
                }),
                Rule::species_count => Ok(match allow_counts {
                    true => ExprStx {
                        left: ExprTermStx::LeftSpeciesCount(
                            pair.clone()
                                .into_inner()
                                .next()
                                .unwrap()
                                .as_str()
                                .to_string(),
                        ),
                        op: None,
                        span: pair.as_span(),
                    },
                    false => {
                        return Err(LanguageError::from_span(
                            &pair.as_span(),
                            "species Counts (#) are not allowed here",
                            line!(),
                            file!(),
                        ));
                    }
                }),
                Rule::local_nested_count => Ok(match allow_counts {
                    true => ExprStx {
                        span: pair.as_span(),
                        left: ExprTermStx::ComplexSpeciesCount(ComplexCountStx::from_pair(pair)?),
                        op: None,
                    },
                    false => {
                        return Err(LanguageError::from_span(
                            &pair.as_span(),
                            "Global species Counts (##) are not allowed here. For example it is not allowed for mass action.",
                            line!(),
                            file!(),
                        ));
                    }
                }),
                Rule::global_species_count => Ok(match allow_global_counts {
                    true => ExprStx {
                        span: pair.as_span(),
                        left: ExprTermStx::ComplexSpeciesCount(ComplexCountStx::from_pair(pair)?),
                        op: None,
                    },
                    false => {
                        //panic!();
                        return Err(LanguageError::from_span(
                            &pair.as_span(),
                            "Global species Counts (##) are not allowed here",
                            line!(),
                            file!(),
                        ));
                    }
                }),
                Rule::function_call => Ok(ExprStx {
                    span: pair.as_span().clone(),
                    left: ExprTermStx::FunctionCall(FunctionCallStx::from_pair(pair)?),
                    op: None,
                }),
                Rule::expr_if => Ok(ExprStx {
                    left: ExprTermStx::Condition(ConditionStx::from_pair(
                        pair.clone(),
                        allow_counts,
                        allow_attributes,
                        allow_global_counts,
                    )?),
                    op: None,
                    span: pair.as_span().clone(),
                }),
                Rule::transition_rate_immidiatly => Ok(ExprStx {
                    left: ExprTermStx::Immidiate,
                    op: None,
                    span: pair.as_span().clone(),
                }),

                x => unreachable!("is {:?}", x),
            })
            .map_infix(
                |lhs: Result<ExprStx, _>, op: Pair<Rule>, rhs: Result<ExprStx, _>| {
                    let lhs = lhs?;
                    let rhs = rhs?;
                    match op.as_rule() {
                        Rule::add => Ok(ExprStx {
                            left: ExprTermStx::Expr(Box::new(lhs)),
                            op: Some((FloatOp::Add, ExprTermStx::Expr(Box::new(rhs)))),
                            span: op.as_span(),
                        }),
                        Rule::subtract => Ok(ExprStx {
                            left: ExprTermStx::Expr(Box::new(lhs)),
                            op: Some((FloatOp::Sub, ExprTermStx::Expr(Box::new(rhs)))),
                            span: op.as_span(),
                        }),
                        Rule::multiply => Ok(ExprStx {
                            left: ExprTermStx::Expr(Box::new(lhs)),
                            op: Some((FloatOp::Mul, ExprTermStx::Expr(Box::new(rhs)))),
                            span: op.as_span(),
                        }),
                        Rule::divide => Ok(ExprStx {
                            left: ExprTermStx::Expr(Box::new(lhs)),
                            op: Some((FloatOp::Div, ExprTermStx::Expr(Box::new(rhs)))),
                            span: op.as_span(),
                        }),
                        Rule::power => Ok(ExprStx {
                            left: ExprTermStx::Expr(Box::new(lhs)),
                            op: Some((FloatOp::Pow, ExprTermStx::Expr(Box::new(rhs)))),
                            span: op.as_span(),
                        }),
                        Rule::op_comparision => Ok(ExprStx {
                            left: ExprTermStx::Expr(Box::new(lhs)),
                            op: Some((
                                FloatOp::Comparison(LogicalOp::from_str(&op.as_str()).expect(
                                    &*format!("Could nout generate op from {}", op.as_str()),
                                )),
                                ExprTermStx::Expr(Box::new(rhs)),
                            )),
                            span: op.as_span(),
                        }),
                        Rule::op_boolean => Ok(ExprStx {
                            left: ExprTermStx::Expr(Box::new(lhs)),
                            op: Some((
                                FloatOp::Comparison(LogicalOp::from_str(&op.as_str()).expect(
                                    &*format!("Could nout generate op from {}", op.as_str()),
                                )),
                                ExprTermStx::Expr(Box::new(rhs)),
                            )),
                            span: op.as_span(),
                        }),
                        x => unreachable!("{:?}", x),
                    }
                },
            )
            .parse(pair_in.into_inner())?;
    res.span = span;
    return Ok(res);
}

#[derive(Debug, Clone)]
pub struct ExprStx<'a> {
    pub left: ExprTermStx<'a>,
    pub op: Option<(FloatOp, ExprTermStx<'a>)>,
    pub span: Span<'a>,
}

impl ExprStx<'_> {
    fn get_all_strings(&self) -> HashSet<String> {
        let mut r = self.left.get_all_strings();
        match &self.op {
            None => {}
            Some((_, rig)) => r.extend(rig.get_all_strings()),
        }
        r
    }
    fn from_val_w_unit(v: ValueWithUnit, s: Span, unit_str: String) -> ExprStx {
        ExprStx {
            left: ExprTermStx::NumUnit(v, unit_str),
            op: None,
            span: s,
        }
    }
    pub fn from_string(str: &str) -> Option<ExprStx<'_>> {
        let res: Result<Pairs<_>, pest::error::Error<_>> =
            MLRgrammar::parse(Rule::runtime_expr, str);
        //let res = res.unwrap();
        //panic!("have {str} -> ~{}~ ({:?})", res.as_str(), res)
        match res {
            Ok(mut k) => Some(
                match ExprStx::from_pair(k.next().unwrap(), false, false, false) {
                    Ok(x) => x,
                    Err(_) => {
                        return None;
                    }
                },
            ),
            Err(_x) => {
                return None;
            }
        }
    }
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        allow_counts: bool,
        allow_attributes: bool,
        allow_global_counts: bool,
    ) -> Result<ExprStx, LanguageError> {
        get_expr_tree(pair, allow_counts, allow_attributes, allow_global_counts)
    }
    pub fn render_latex_rate(&self) -> String {
        self.render_latex(false)
    }
    pub fn render_latex(&self, may_need_braces: bool) -> String {
        let (brace_left, brace_right) = match may_need_braces {
            true => ("\\left(", "\\right)"),
            false => ("", ""),
        };
        match &self.op {
            None => self.left.render_latex(may_need_braces),
            Some((op, right)) => {
                /*match right{
                    ExprTermStx::NumUnit(a, b) => {
                        if *op == FloatOp::Mul && *a == ValueWithUnit::dimensionless(1.){
                            return self.left.render_latex(may_need_braces);
                        }
                    }
                    _ => {}
                }*/

                match op {
                    FloatOp::Add => format!(
                        "{}{} + {}{}",
                        brace_left,
                        self.left.render_latex(false),
                        right.render_latex(false),
                        brace_right
                    ),
                    FloatOp::Sub => format!(
                        "{}{} - {}{}",
                        brace_left,
                        self.left.render_latex(false),
                        right.render_latex(false),
                        brace_right
                    ),
                    FloatOp::Div => format!(
                        "\\dfrac{{\\scriptsize {} }}{{\\scriptsize {} }}",
                        self.left.render_latex(false),
                        right.render_latex(false)
                    ),
                    FloatOp::Mul => format!(
                        "{} \\cdot {}",
                        self.left.render_latex(true),
                        right.render_latex(true)
                    ),
                    FloatOp::Pow => format!(
                        "\\left({}\\right) ^{{ {} }}",
                        self.left.render_latex(false),
                        right.render_latex(false)
                    ),
                    FloatOp::Comparison(p) => format!(
                        "{}{} {} {}{}",
                        brace_left,
                        self.left.render_latex(false),
                        p,
                        right.render_latex(false),
                        brace_right
                    ),
                }
            }
        }
    }
}
impl<'a> std::fmt::Display for ExprStx<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self.op {
            Some((op, right)) => write!(f, "({:?}) {} ({:?})", self.left, op, right),
            None => write!(f, "{:?}", self.left),
        }
    }
}

#[derive(Debug, Clone)]
pub struct TransitionStx<'a> {
    pub left: SpeciesLeftSolution<'a>,
    pub right: SpeciesRightSolution<'a>,
    pub constraints: ReactionConstraintsStx<'a>,
    pub rate: ExprStx<'a>,
    pub span: Span<'a>,
    pub is_not_mass_action: bool,
    pub original_string: String,
}

impl<'a> TransitionStx<'a> {
    fn get_all_strings(&self) -> HashSet<String> {
        let mut r = self.left.get_all_strings();
        r.extend(self.right.get_all_strings());
        /*r.extend(
            self.constraints
                .logical_constraints
                .iter()
                .map(|k| k.get_all_strings().into_iter())
                .flatten(),
        );*/
        r.extend(self.rate.get_all_strings());
        r
    }
    fn vec_from_pair(
        pair: Vec<pest::iterators::Pair<'a, Rule>>,
        constants: HashMap<String, ConstantDefinition<'a>>,
    ) -> Vec<Result<TransitionStx<'a>, LanguageError>> {
        //assert_eq!(pair.as_rule(), Rule::transitions);
        pair.into_iter()
            .map(|p| TransitionStx::from_pair(p, constants.clone()))
            .collect()
    }

    fn from_pair(
        pair: pest::iterators::Pair<'a, Rule>,
        _constants: HashMap<String, ConstantDefinition>,
    ) -> Result<TransitionStx<'a>, LanguageError> {
        assert!(pair.as_rule() == Rule::transition || pair.as_rule() == Rule::transition_extra);
        let mut original_string = pair
            .as_str()
            .to_string()
            .replace("\n", " ")
            .replace("\r", "");
        while original_string.contains("  ") {
            original_string = original_string.replace("  ", " ");
        }
        let span = pair.as_span().clone();
        let mut pair_inner = pair.into_inner();

        let left = SpeciesLeftSolution::from_pair(pair_inner.next().unwrap());
        let tmp = pair_inner.next().unwrap();
        let (right, extra_constrains) = match tmp.as_rule() {
            Rule::species_right_solution => (
                SpeciesRightSolution::from_pair(tmp),
                ReactionConstraintsStx {
                    firing_constraints: vec![],
                    //logical_constraints: vec![],
                },
            ),
            Rule::transition_properties => (
                SpeciesRightSolution::from_pair(pair_inner.next().unwrap()),
                ReactionConstraintsStx::from_pair(tmp)?,
            ),
            x => unreachable!("{:?}", x),
        };

        //let transition_guard = pair_inner.next().unwrap();
        //let transition_guard_inner = transition_guard.into_inner().next();

        let rate_pair_outer = pair_inner.next().unwrap();

        let allow_counts = match rate_pair_outer.as_rule() {
            Rule::transition_rate_FULL => true,
            Rule::transition_rate_MA => false,
            Rule::transition_rate_immidiatly => true,
            x => {
                unreachable!("{:?}", x);
            }
        };

        let rate = match rate_pair_outer.as_rule() {
            _ => ExprStx::from_pair(
                rate_pair_outer.clone().into_inner().next().unwrap().clone(),
                allow_counts,
                true,
                allow_counts,
            )?,
        };

        /*let guard = match transition_guard_inner {
            None => vec![],
            Some(x) => x
                .into_inner()
                .map(|v| LogicalConstraintStx::from_pair(v, true))
                .collect(),
        };*/
        //let mut errs = vec![];
        /*let guard = guard
            .into_iter()
            .filter_map(|v| match v {
                Ok(x) => Some(x),
                Err(x) => {
                    errs.extend(x.into_iter());
                    None
                }
            })
            .collect();
        if !errs.is_empty() {
            return Err(errs);
        }*/

        let left = left?;
        let right = right?;
        Ok(TransitionStx {
            left,
            right,
            constraints: extra_constrains,
            rate,
            span,
            is_not_mass_action: allow_counts,
            original_string,
        })
    }
}

#[derive(Debug, Clone)]
pub struct StxSpeciesAttribute<'a> {
    pub name: String,
    pub dim: AttributeDimensionality,
    pub can_be_expanded: bool,
    pub span: Span<'a>,
}

#[derive(Debug, Clone)]
pub struct FunctionDefinitionStx<'a> {
    pub name: String,
    pub params: Vec<String>,
    pub expr: ExprStx<'a>,
    pub span: Span<'a>,
}

impl FunctionDefinitionStx<'_> {
    fn get_all_strings(&self) -> HashSet<String> {
        [self.name.clone()]
            .iter()
            .cloned()
            .chain(self.expr.get_all_strings().into_iter())
            .chain(self.params.iter().cloned())
            .collect()
    }
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
    ) -> Result<FunctionDefinitionStx, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::function_definition);
        let span = pair.as_span();
        let mut pairs = pair.into_inner();
        let name = pairs.next().unwrap();
        let mut params = vec![];
        while pairs.peek().is_some() && pairs.peek().unwrap().as_rule() == Rule::name {
            params.push(pairs.next().unwrap().as_str().to_string())
        }

        Ok(FunctionDefinitionStx {
            name: name.as_str().to_string(),
            params,
            expr: ExprStx::from_pair(pairs.next().unwrap(), false, false, true)?,
            span,
        })
    }
}

#[derive(Debug, Clone)]
pub struct FunctionCallStx<'a> {
    pub name: String,
    pub params: Vec<ExprStx<'a>>,
    pub span: Span<'a>,
}

impl FunctionCallStx<'_> {
    fn from_pair(pair: pest::iterators::Pair<Rule>) -> Result<FunctionCallStx, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::function_call);
        let span = pair.as_span();
        let mut pairs = pair.into_inner();
        let name = pairs.next().unwrap();

        Ok(FunctionCallStx {
            name: name.as_str().to_string().to_lowercase(),
            params: pairs
                .map(|k| Ok(ExprStx::from_pair(k, true, true, true)?))
                .collect::<Result<Vec<_>, LanguageError>>()?,
            span,
        })
    }
}

#[derive(Debug)]
pub struct SpeciesDefinitionStx<'a> {
    pub name: String,
    pub attributes: Vec<StxSpeciesAttribute<'a>>,
    pub span: Span<'a>,
}
impl SpeciesDefinitionStx<'_> {
    fn get_all_strings(&self) -> HashSet<String> {
        let mut res = HashSet::from([self.name.clone()]);
        for a in self.attributes.iter() {
            match &a.dim.dim.non_numerical {
                NonNumericalDimension::Enumeration(x) => res.extend(x.iter().cloned()),
                _ => {}
            }
        }
        res.extend(self.attributes.iter().map(|k| k.name.clone()));
        res
    }
}

#[derive(Debug, Clone)]
pub enum FiringLimitationsStx<'a> {
    MaxCount { num: usize, span: Span<'a> },
    DUMMY,
}

#[derive(Debug, Clone)]
pub struct ReactionConstraintsStx<'a> {
    pub firing_constraints: Vec<FiringLimitationsStx<'a>>,
}

impl ReactionConstraintsStx<'_> {
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
    ) -> Result<ReactionConstraintsStx, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::transition_properties);
        let mut r = ReactionConstraintsStx {
            firing_constraints: vec![],
            //logical_constraints: vec![],
        };
        for x in pair.into_inner().map(|k| k.into_inner().next().unwrap()) {
            match x.as_rule() {
                Rule::tp_times => {
                    let y = x.clone().into_inner().next().unwrap();
                    match y.as_rule() {
                        Rule::pos_integer => {
                            r.firing_constraints.push(FiringLimitationsStx::MaxCount {
                                num: y.as_str().parse().unwrap(),
                                span: x.as_span(),
                            })
                        }
                        y => unreachable!("{:?}", y),
                    }
                }
                o => unreachable!("{:?}", o),
            }
        }
        Ok(r)
    }
}

/*#[derive(Debug, Clone)]
pub struct LogicalConstraintStx<'a> {
    pub left: ExprStx<'a>,
    pub op: LogicalOp,
    pub right: ExprStx<'a>,
    pub span: Span<'a>,
}

impl LogicalConstraintStx<'_> {
    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        allow_counts: bool,
    ) -> Result<LogicalConstraintStx, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::logical_constraint_outer);
        let span = pair.as_span();
        let mut att_assign = pair.into_inner();
        Ok(LogicalConstraintStx {
            left: ExprStx::from_pair(att_assign.next().unwrap(), allow_counts, true, allow_counts)?,
            op: LogicalOp::from_str(att_assign.next().unwrap().as_str()).unwrap(),
            right: ExprStx::from_pair(
                att_assign.next().unwrap(),
                allow_counts,
                true,
                allow_counts,
            )?,
            span,
        })
    }
}*/

impl Dimension {
    pub fn from_pair_definiton(pair: Pair<Rule>) -> Result<AttributeDimensionality, LanguageError> {
        let mut str_prefak = None;
        let dim_only = match pair.as_rule() {
            Rule::atn_int => Dimension::non_numeric(NonNumericalDimension::Integer),
            Rule::atn_str => Dimension::non_numeric(NonNumericalDimension::String),
            Rule::atn_bool => Dimension::non_numeric(NonNumericalDimension::Bool),
            Rule::atn_float => Dimension::dimensionless(),
            Rule::atn_float_unit => {
                let (val, stringo) = ValueWithUnit::from_pair(pair, false)?;
                str_prefak = Some((stringo, val.value));
                val.unit
            }
            Rule::atn_enum => Dimension::non_numeric(NonNumericalDimension::Enumeration(
                pair.into_inner()
                    .into_iter()
                    .map(|k| k.as_str().to_string())
                    .collect(),
            )),
            _ => unreachable!(),
        };
        Ok(AttributeDimensionality {
            dim: dim_only,
            dim_str_prefak: str_prefak,
        })
    }
}

impl SpeciesDefinitionStx<'_> {
    fn from_pair(
        pair: Vec<pest::iterators::Pair<Rule>>,
    ) -> Result<Vec<SpeciesDefinitionStx>, LanguageError> {
        //assert_eq!(pair.as_rule(), Rule::species_definitions);
        //let mut err = vec![];
        pair.into_iter()
            .map(|att_asign| {
                assert_eq!(att_asign.as_rule(), Rule::species_definition);
                att_asign
            })
            .map(|p| p.into_inner())
            .map(|mut p| {
                let name_pair = p.next().unwrap();
                Ok(SpeciesDefinitionStx {
                    name: name_pair.as_str().to_string(),
                    span: name_pair.as_span(),
                    attributes: p
                        .map(|att_def| {
                            let mut att_def_inner = att_def.clone().into_inner();
                            let name = att_def_inner.next().unwrap().as_str().to_string();
                            let dim_pair = att_def_inner.next().unwrap();
                            let dim = Dimension::from_pair_definiton(dim_pair)?;
                            let starred = att_def_inner.next().unwrap();
                            assert_eq!(starred.as_rule(), Rule::attribute_star);

                            Ok(StxSpeciesAttribute {
                                name,
                                dim: dim,
                                can_be_expanded: !starred.as_str().contains("*"),
                                span: att_def.as_span(),
                            })
                        })
                        .collect::<Result<Vec<_>, LanguageError>>()?,
                })
            })
            .collect()
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesLeftStx<'a> {
    pub species_name: String,
    pub is_rest_solution: bool,
    pub rest_solution_amount: Option<Box<ExprStx<'a>>>,
    pub extra_name: Option<String>,
    pub span: Span<'a>,
    pub attribute_constraints: Vec<ExprStx<'a>>,
    pub child_solution: Option<SpeciesLeftSolution<'a>>,
}

impl<'a> SpeciesLeftStx<'a> {
    pub fn is_definition_only(self) -> Result<SpeciesLeftStx<'a>, LanguageError> {
        if self.is_rest_solution
            || self.rest_solution_amount.is_some()
            || self.extra_name.is_some()
            || self.child_solution.is_some()
        {
            Err(LanguageError::from_span(
                &self.span,
                "Rest solution/nesting or name not allowed here",
                line!(),
                file!(),
            ))
        } else {
            Ok(self)
        }
    }
}
impl SpeciesLeftStx<'_> {
    pub fn get_all_strings(&self) -> HashSet<String> {
        let mut res = HashSet::from([self.species_name.clone()]);
        match &self.rest_solution_amount {
            None => {}
            Some(k) => res.extend(k.get_all_strings()),
        };
        match &self.extra_name {
            None => {}
            Some(k) => {
                res.insert(k.clone());
            }
        }
        for ac in self.attribute_constraints.iter() {
            res.extend(ac.get_all_strings());
        }
        match &self.child_solution {
            None => {}
            Some(s) => {
                res.extend(s.get_all_strings());
            }
        }
        res
    }
    pub fn render_latex(&self) -> String {
        let sub = match &self.child_solution {
            None => "".to_string(),
            Some(y) => format!("\\left[{}\\right]", y.render_latex()),
        };
        let mut att_const = self
            .attribute_constraints
            .iter()
            .map(|ac| format!(" {}", ac.render_latex_rate(),))
            .join(" \\&\\& ");
        if att_const != "" {
            att_const = format!("\\left({}\\right)", att_const)
        }
        let my_text = format!("\\text{{{}}}{}", self.species_name, att_const);
        if sub != "".to_string() {
            format!("\\underbrace{{ {} }}_{}", sub, my_text)
        } else {
            my_text
        }
    }
    fn from_pair(pair: pest::iterators::Pair<Rule>) -> Result<SpeciesLeftStx, LanguageError> {
        if pair.as_rule() == Rule::rest_solution {
            let span = pair.as_span();
            //println!("{:?}", pair);
            let mut pairs = pair.into_inner();
            let species_name = pairs.next().unwrap().as_str().to_string();
            assert!(species_name.chars().last().unwrap() != ' ');
            return Ok(SpeciesLeftStx {
                species_name,
                is_rest_solution: true,
                rest_solution_amount: match pairs.next() {
                    Some(expr) => Some(Box::new(ExprStx::from_pair(expr, false, true, true)?)),
                    None => None,
                },
                extra_name: None,
                span: span,
                attribute_constraints: vec![],
                child_solution: None,
            });
        }
        assert_eq!(pair.as_rule(), Rule::species_left);
        let mut pair = pair.into_inner();
        let mut species_def = pair.next().unwrap().into_inner();
        let extra_name = pair.next().unwrap();
        let subsol = pair.next();
        let spec_name = species_def.next().unwrap();
        let species_name = spec_name.as_str().to_string();
        assert!(species_name.chars().last().unwrap() != ' ');
        Ok(SpeciesLeftStx {
            species_name,
            is_rest_solution: false,
            rest_solution_amount: None,
            extra_name: match extra_name.into_inner().next() {
                Some(x) => Some(x.as_str().to_string()),
                None => None,
            },
            span: spec_name.as_span(),
            attribute_constraints: match species_def.next() {
                Some(x) => x
                    .into_inner()
                    .map(|att_assign| Ok(ExprStx::from_pair(att_assign, false, true, false)?))
                    .collect::<Result<_, _>>()?,
                None => vec![],
            },
            child_solution: match subsol {
                Some(x) => Some(SpeciesLeftSolution::from_pair(x)?),
                None => None,
            },
        })
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesRightStx<'a> {
    pub species_name: String,
    pub amt: ExprStx<'a>,
    pub is_rest_solution: bool,
    pub child_solution: Option<SpeciesRightSolution<'a>>,
    pub attribute_assignments: Vec<(String, ExprStx<'a>)>,
    pub extra_name: Option<String>,
    pub span: Span<'a>,
}
impl SpeciesRightStx<'_> {
    pub fn get_all_strings(&self) -> HashSet<String> {
        let mut res = HashSet::from([self.species_name.clone()]);
        res.extend(self.amt.get_all_strings());
        match &self.extra_name {
            None => {}
            Some(k) => {
                res.insert(k.clone());
            }
        }
        for (ac, val) in self.attribute_assignments.iter() {
            //println!("{}", ac);
            res.insert(ac.clone());
            res.extend(val.get_all_strings());
        }
        match &self.child_solution {
            None => {}
            Some(s) => {
                res.extend(s.get_all_strings());
            }
        }
        res
    }
    pub fn render_latex(&self) -> String {
        let sub = match &self.child_solution {
            None => "".to_string(),
            Some(y) => format!("\\left[{}\\right]", y.render_latex()),
        };
        let mut att_const = self
            .attribute_assignments
            .iter()
            .map(|(n, val)| format!(" \\text{{ {}}}= {}", n, val.render_latex_rate()))
            .join(" ,\\; ");
        if att_const != "" {
            att_const = format!("\\left({}\\right)", att_const)
        }
        let mut my_amt = self.amt.render_latex(true);
        if my_amt == "1" {
            my_amt = "".to_owned();
        } else {
            my_amt = my_amt + "\\,";
        }
        let my_text = format!("{}\\text{{{}}}{}", my_amt, self.species_name, att_const);
        if sub != "".to_string() {
            format!("\\underbrace{{ {} }}_{}", sub, my_text)
        } else {
            my_text
        }
    }
    fn from_pair(pair_in: pest::iterators::Pair<Rule>) -> Result<SpeciesRightStx, LanguageError> {
        if pair_in.as_rule() == Rule::rest_solution {
            let span = pair_in.as_span();
            let mut pairs = pair_in.into_inner();
            let species_name = pairs.next().unwrap().as_str().to_string();
            assert!(species_name.chars().last().unwrap() != ' ');
            match pairs.next() {
                None => {}
                Some(k) => {
                    return Err(LanguageError::from_span(
                        &k.as_span(),
                        "Unexpected",
                        line!(),
                        file!(),
                    ));
                }
            }
            assert!(pairs.next().is_none());
            return Ok(SpeciesRightStx {
                species_name,
                amt: ExprStx {
                    left: ExprTermStx::ActualInt(1),
                    op: None,
                    span,
                },
                is_rest_solution: true,
                child_solution: None,
                attribute_assignments: vec![],
                extra_name: None,
                span: span,
            });
        }
        assert_eq!(pair_in.as_rule(), Rule::species_right);
        let mut pair = pair_in.clone().into_inner();
        let mut species_def = pair.next().unwrap().into_inner();
        let extra_name = pair.next().unwrap();
        let subsol = pair.next();
        let amt_pair = species_def.next().unwrap();
        let amt;
        let name_pair = match amt_pair.as_rule() {
            Rule::name => {
                amt = ExprStx {
                    left: ExprTermStx::ActualInt(1),
                    op: None,
                    span: pair_in.as_span(),
                };
                amt_pair
            }
            Rule::integer => {
                let amt_int = match amt_pair.as_str().parse() {
                    Ok(k) => k,
                    Err(_e) => {
                        return Err(LanguageError::from_span(
                            &pair_in.as_span(),
                            "Value to large",
                            line!(),
                            file!(),
                        ))
                    }
                };
                amt = ExprStx {
                    left: ExprTermStx::ActualInt(amt_int),
                    op: None,
                    span: pair_in.as_span(),
                };
                species_def.next().unwrap()
            }
            Rule::expr_float_w_unit => {
                //println!("AMT: {:?}", amt_pair.as_str());
                amt = ExprStx::from_pair(amt_pair, true, true, true)?;
                species_def.next().unwrap()
            }
            _ => unreachable!(),
        };

        Ok(SpeciesRightStx {
            amt: amt,
            extra_name: match extra_name.into_inner().next() {
                Some(x) => Some(x.as_str().to_string()),
                None => None,
            },
            species_name: name_pair.as_str().to_string(),
            attribute_assignments: match species_def.next() {
                Some(x) => x
                    .into_inner()
                    .map(|att_assign| att_assign.into_inner())
                    .map(|mut att_assign| {
                        let r: Result<(String, ExprStx), LanguageError> = Ok((
                            att_assign.next().unwrap().as_str().to_string(),
                            ExprStx::from_pair(att_assign.next().unwrap(), false, true, false)?,
                        ));
                        return r;
                    })
                    .collect::<Result<_, _>>()?,
                None => vec![],
            },

            child_solution: match subsol {
                Some(x) => Some(SpeciesRightSolution::from_pair(x)?),
                None => None,
            },
            span: name_pair.as_span(),
            is_rest_solution: false,
        })
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesLeftSolution<'a> {
    pub species_in_solution: Vec<SpeciesLeftStx<'a>>,
}
impl SpeciesLeftSolution<'_> {
    fn get_all_strings(&self) -> HashSet<String> {
        self.species_in_solution
            .iter()
            .map(|k| k.get_all_strings().into_iter())
            .flatten()
            .collect()
    }
    fn from_pair(pair: pest::iterators::Pair<Rule>) -> Result<SpeciesLeftSolution, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::species_left_solution);
        Ok(SpeciesLeftSolution {
            species_in_solution: pair
                .into_inner()
                .map(|v| SpeciesLeftStx::from_pair(v))
                .collect::<Result<_, _>>()?,
        })
    }
    pub fn render_latex(&self) -> String {
        format!(
            "{}",
            self.species_in_solution
                .iter()
                .map(|v| v.render_latex())
                .join(" + ")
        )
    }
}

#[derive(Debug, Clone)]
pub struct SpeciesRightSolution<'a> {
    pub species_in_solution: Vec<SpeciesRightStx<'a>>,
}
impl SpeciesRightSolution<'_> {
    fn get_all_strings(&self) -> HashSet<String> {
        self.species_in_solution
            .iter()
            .map(|k| k.get_all_strings().into_iter())
            .flatten()
            .collect()
    }
    fn from_pair(pair: pest::iterators::Pair<Rule>) -> Result<SpeciesRightSolution, LanguageError> {
        assert_eq!(pair.as_rule(), Rule::species_right_solution);
        Ok(SpeciesRightSolution {
            species_in_solution: pair
                .into_inner()
                .filter(|v| v.as_rule() != Rule::species_right_zero)
                .map(|v| SpeciesRightStx::from_pair(v))
                .collect::<Result<_, _>>()?,
        })
    }
    pub fn render_latex(&self) -> String {
        format!(
            "{}",
            self.species_in_solution
                .iter()
                .map(|v| v.render_latex())
                .join(" + ")
        )
    }
}

#[derive(Debug, Clone)]
pub struct ConstantDefinition<'a> {
    pub value: ExprStx<'a>,
    pub comment: String,
}

impl ConstantDefinition<'_> {
    fn vec_from_pair(
        pair: Vec<(pest::iterators::Pair<Rule>, String)>,
    ) -> Result<HashMap<String, ConstantDefinition>, LanguageError> {
        //let mut errs = vec![];
        //assert_eq!(pair.as_rule(), Rule::constant_definitions);
        let itm: Vec<_> = pair
            .into_iter()
            .map(
                |(v, comment)| match ConstantDefinition::from_pair(v.clone(), comment.clone()) {
                    Ok(def) => Ok((def, v)),
                    Err(x) => Err(x),
                },
            )
            .collect::<Result<_, _>>()?;
        itm.into_iter()
            .fold(Ok(HashMap::new()), |map, ((name, def), pair)| match map {
                Ok(mut map) => {
                    if map.contains_key(&*name) {
                        return Err(LanguageError::from_span(
                            &pair.into_inner().next().unwrap().as_span(),
                            "Redundant constant name",
                            line!(),
                            file!(),
                        ));
                    } else {
                        map.insert(name, def);
                        return Ok(map);
                    };
                }

                Err(x) => Err(x),
            })
    }

    fn from_pair(
        pair: pest::iterators::Pair<Rule>,
        comment: String,
    ) -> Result<(String, ConstantDefinition), LanguageError> {
        assert!(pair.as_rule() == Rule::constant_definition);
        let mut pair2 = pair.clone().into_inner();
        let name = pair2.next().unwrap().as_str().to_string();
        let expr = pair2.next().unwrap();

        let val = ExprStx::from_pair(expr, false, false, false);

        Ok((
            name,
            ConstantDefinition {
                value: val?,
                comment: texify_comment(comment),
            },
        ))
    }
}

fn texify_comment(s: String) -> String {
    s.replace("//", "")
        .replace("/*", "")
        .replace("*/", "")
        .replace(";", "\n")
        .replace("\n", "\n\n")
}

#[derive(Debug, Clone)]
pub enum InsideOfCountStx<'a> {
    FromLeft(String),
    Global(SpeciesLeftStx<'a>),
    NoneLocal,
    NoneGlobal,
}

#[derive(Debug, Clone)]
pub struct ComplexCountStx<'a> {
    pub what: SpeciesLeftStx<'a>,
    pub inside_of: InsideOfCountStx<'a>,
    pub inside_of_arbitrary_deep: bool,
    //pub global: bool,
    pub string_name: String,
    pub span: Span<'a>,
}

impl ComplexCountStx<'_> {
    fn from_pair(pair: pest::iterators::Pair<Rule>) -> Result<ComplexCountStx, LanguageError> {
        let global; //two ##
        let mut pairs = match pair.as_rule() {
            Rule::global_species_count => {
                global = true;
                Ok(pair.clone().into_inner().next().unwrap().into_inner())
            }
            Rule::global_count_nested => {
                global = true;
                Ok(pair.clone().into_inner().next().unwrap().into_inner())
            }
            Rule::local_nested_count => {
                global = false;
                Ok(pair.clone().into_inner().next().unwrap().into_inner())
            }
            k => unreachable!("{:?}", k),
        }?;
        let first = pairs.next().unwrap();
        //let first_span = first.as_span();
        assert_eq!(first.as_rule(), Rule::species_left);
        //let mut first = first.into_inner();

        let nesting_keyword = pairs.next();

        //let species_name = first.as_str().to_string();
        //assert!(species_name.chars().last().unwrap() != ' ');
        let what = SpeciesLeftStx::from_pair(first)?.is_definition_only()?;

        let inside_of = pairs.next();
        let inside_of = match inside_of {
            None => {
                if global {
                    InsideOfCountStx::NoneGlobal
                } else {
                    InsideOfCountStx::NoneLocal
                }
            }
            Some(inside_of) => match inside_of.as_rule() {
                Rule::name => InsideOfCountStx::FromLeft(inside_of.as_str().to_string()),
                Rule::species_left => InsideOfCountStx::Global(
                    SpeciesLeftStx::from_pair(inside_of)?.is_definition_only()?,
                ),
                k => {
                    unreachable!("{:?}", k)
                }
            },
        };

        Ok(ComplexCountStx {
            what: what,
            inside_of,
            inside_of_arbitrary_deep: match nesting_keyword {
                None => false,
                Some(k) => {
                    assert_eq!(k.as_rule(), Rule::global_count_nested_keyword);
                    match k.as_str() {
                        "in" => false,
                        "anywhere_in" => true,
                        _ => unreachable!("{:?}", k),
                    }
                }
            },
            string_name: pair.as_str().to_string(),
            span: pair.as_span(),
        })
    }
}

#[derive(Debug, Template)]
#[template(path = "latex.txt")]
pub struct ModelParsed<'a> {
    pub model_span: Span<'a>,
    pub species_definitions: Vec<SpeciesDefinitionStx<'a>>,
    pub initializations: SpeciesRightSolution<'a>,
    pub transitions: Vec<TransitionStx<'a>>,
    pub constants: HashMap<String, ConstantDefinition<'a>>,

    pub observations: Vec<ExprStx<'a>>,
    pub function_definitions: Vec<FunctionDefinitionStx<'a>>,

    pub species_names: HashSet<String>,
}

impl<'a> ModelParsed<'a> {
    pub fn get_all_strings(&self) -> HashSet<String> {
        let mut res: HashSet<String> = self.species_names.iter().cloned().collect();
        for i in self.species_definitions.iter() {
            res.extend(i.get_all_strings());
        }
        res.extend(self.initializations.get_all_strings());
        for t in self.transitions.iter() {
            res.extend(t.get_all_strings());
        }
        for (n, cd) in self.constants.iter() {
            res.insert(n.clone());
            res.extend(cd.value.get_all_strings());
        }
        for n in self.observations.iter() {
            res.extend(n.get_all_strings());
        }
        for func in self.function_definitions.iter() {
            res.extend(func.get_all_strings());
        }
        res
    }
    pub fn make_latex(&self) -> String {
        self.render().unwrap()
    }
    pub fn from_string(
        model_string: &'a String,
        _allow_dynamic_structures: bool,
    ) -> Result<ModelParsed<'a>, LanguageError> {
        let res: Result<Pairs<_>, pest::error::Error<_>> =
            MLRgrammar::parse(Rule::model, &**model_string);

        let mut model: Pairs<Rule> = match res {
            Ok(k) => k,
            Err(x) => {
                return Err(LanguageError::pest(x, line!(), file!()));
            }
        };
        info!("-- Model syntactically correct");

        let model2 = model.next().unwrap();
        assert_eq!(model2.as_rule(), Rule::model);
        let mut constant_defs = vec![];

        let mut initializations = vec![];
        let mut transitions = vec![];
        let mut species_defs = vec![];
        let mut observations = vec![];
        let mut function_definitions = vec![];

        let mut last_comment_line = 0;

        model2
            .clone()
            .into_inner()
            .filter(|v| v.as_rule() != Rule::EOI)
            .map(|state| {
                assert_eq!(state.as_rule(), Rule::statement);
                state.into_inner().next().unwrap()
            })
            .map(|v| {
                let old_last = last_comment_line;
                last_comment_line = v.as_span().end();
                let start = v.as_span().start();
                (v, (old_last, start))
            })
            .for_each(|(pair, (start, stop))| match pair.as_rule() {
                Rule::constant_definition => {
                    constant_defs.push((pair, model_string[start..stop].to_string()))
                }
                Rule::initalization => initializations.push(pair),
                Rule::transition_extra => transitions.push(pair),
                Rule::transition => transitions.push(pair),
                Rule::species_definition => species_defs.push(pair),
                Rule::observation => observations.push(pair),
                Rule::function_definition => function_definitions.push(pair),
                _ => unreachable!("found: {:?}", pair.as_rule()),
            });

        info!(
            "-- {} Rules, {} species and {} constants",
            transitions.len(),
            species_defs.len(),
            constant_defs.len(),
        );

        match initializations.len() {
            0 => {
                return Err(LanguageError {
                    lstart: 0,
                    lstop: 10,
                    cstart: 0,
                    cstop: 05,
                    what: "Missing initalization (starting with Initial: ".to_string(),
                    line: line!(),
                    file: file!().to_string(),
                })
            }
            1 => (),
            _x => {
                return Err(LanguageError::from_span(
                    &transitions.last().unwrap().as_span(),
                    "Multiple Initial sections",
                    line!(),
                    file!(),
                ))
            }
        }

        let species_definitions = SpeciesDefinitionStx::from_pair(species_defs)?;
        let initializations = SpeciesRightSolution::from_pair(
            initializations[0].clone().into_inner().next().unwrap(),
        )?;
        let constants = ConstantDefinition::vec_from_pair(constant_defs)?;

        let observations: Vec<_> = observations
            .into_iter()
            .map(|v| ExprStx::from_pair(v.into_inner().next().unwrap(), true, false, true))
            .collect::<Result<_, _>>()?;

        let transitions = TransitionStx::vec_from_pair(transitions, constants.clone())
            .into_iter()
            .collect::<Result<_, _>>()?;
        let function_definitions = function_definitions
            .iter()
            .map(|p| FunctionDefinitionStx::from_pair(p.clone()))
            .collect::<Result<_, _>>()?;
        let res = ModelParsed {
            model_span: model2.as_span(),
            species_names: species_definitions.iter().map(|v| v.name.clone()).collect(),
            species_definitions,
            initializations,
            transitions,
            constants,
            observations,
            function_definitions: function_definitions,
        };
        Ok(res)
    }
}
