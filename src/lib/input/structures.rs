use itertools::Itertools;
use num_rational::Rational64;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::collections::{BTreeSet, HashMap, HashSet};
use std::convert::TryFrom;
use std::fmt;
use std::fmt::{Display, Formatter};

use crate::expansion::static_expansion::{
    AllOrSingleLeft, EnumeratedSpecies, OptimizedString, PartialEvaluationExpr,
};
//use crate::input::structures::RateExprTerm::Primitive;
use crate::buildin_functions::BuildinFunction;
use byteorder::ByteOrder;
use pest::Span;
use sha2::{Digest, Sha256};
use std::hash::{Hash, Hasher};

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub struct LanguageError {
    pub lstart: usize,
    pub lstop: usize,
    pub cstart: usize,
    pub cstop: usize,
    pub what: String,
    pub line: u32,
    pub file: String,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum LogicalOp {
    Equal,
    Less,
    Greater,
    LessOrEqual,
    GreaterOrEqual,
    Unequal,
    And,
    Or,
}

impl fmt::Display for LogicalOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            LogicalOp::Equal => write!(f, "=="),
            LogicalOp::Less => write!(f, "<"),
            LogicalOp::Greater => write!(f, ">"),
            LogicalOp::LessOrEqual => write!(f, "<="),
            LogicalOp::GreaterOrEqual => write!(f, ">="),
            LogicalOp::Unequal => write!(f, "!="),
            LogicalOp::And => write!(f, "&&"),
            LogicalOp::Or => write!(f, "||"),
        }
    }
}

impl LogicalOp {
    pub fn dim_ok(&self, a: &Dimension, b: &Dimension) -> bool {
        if !Dimension::numericals_match(b, b) {
            return false;
        }
        match (&a.non_numerical, &b.non_numerical) {
            (NonNumericalDimension::Integer, NonNumericalDimension::Integer) => true,
            (NonNumericalDimension::Nothing, NonNumericalDimension::Integer) => true,
            (NonNumericalDimension::Integer, NonNumericalDimension::Nothing) => true,
            (NonNumericalDimension::Nothing, NonNumericalDimension::Nothing) => true,
            (NonNumericalDimension::Bool, NonNumericalDimension::Bool) => match self {
                LogicalOp::Equal => false,
                LogicalOp::Less => false,
                LogicalOp::Greater => false,
                LogicalOp::LessOrEqual => false,
                LogicalOp::GreaterOrEqual => false,
                LogicalOp::Unequal => true,
                LogicalOp::And => true,
                LogicalOp::Or => true,
            },
            (_, _) => match self {
                LogicalOp::Equal => true,
                LogicalOp::Less => false,
                LogicalOp::Greater => false,
                LogicalOp::LessOrEqual => false,
                LogicalOp::GreaterOrEqual => false,
                LogicalOp::Unequal => true,
                LogicalOp::And => false,
                LogicalOp::Or => false,
            },
        }
    }
    pub fn apply_eq<T: Eq>(&self, a: &T, b: &T) -> bool {
        match self {
            LogicalOp::Equal => a == b,
            LogicalOp::Unequal => a != b,
            _ => unreachable!(),
        }
    }
    pub fn apply_all<T: PartialEq + PartialOrd>(&self, a: &T, b: &T) -> bool {
        match self {
            LogicalOp::Equal => a == b,
            LogicalOp::Unequal => a != b,
            LogicalOp::Less => a < b,
            LogicalOp::Greater => a > b,
            LogicalOp::LessOrEqual => a <= b,
            LogicalOp::GreaterOrEqual => a >= b,
            LogicalOp::And => unreachable!(),
            LogicalOp::Or => unreachable!(),
        }
    }
}
impl LogicalOp {
    pub fn from_str(s: &str) -> Option<LogicalOp> {
        match s {
            "==" => Some(LogicalOp::Equal),
            "<" => Some(LogicalOp::Less),
            ">" => Some(LogicalOp::Greater),
            "<=" => Some(LogicalOp::LessOrEqual),
            ">=" => Some(LogicalOp::GreaterOrEqual),
            "!=" => Some(LogicalOp::Unequal),
            "&&" => Some(LogicalOp::And),
            "||" => Some(LogicalOp::Or),
            _ => None,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum FloatOp {
    Add,
    Sub,
    Div,
    Mul,
    Pow,
    Comparison(LogicalOp),
}

impl FloatOp {
    pub fn combine_dimension(
        &self,
        a: &Dimension,
        b: &Dimension,
        value_of_a: &RateExprTerm,
        value_of_b: &RateExprTerm,
        string_conv: &HashMap<String, OptimizedString>,
    ) -> Option<Dimension> {
        match self {
            FloatOp::Comparison(comp) => match comp {
                LogicalOp::Equal | LogicalOp::Unequal => {
                    match (&a.non_numerical, &b.non_numerical) {
                        (NonNumericalDimension::String, NonNumericalDimension::String)
                        | (NonNumericalDimension::Bool, NonNumericalDimension::Bool) => {
                            return Some(Dimension::non_numeric(NonNumericalDimension::Bool));
                        }
                        (NonNumericalDimension::Enumeration(_x), NonNumericalDimension::String) => {
                            if Dimension::assignable_enum(
                                a,
                                b,
                                &RateExpr::Single(value_of_b.clone()),
                                string_conv,
                            ) {
                                return Some(Dimension::non_numeric(NonNumericalDimension::Bool));
                            }
                        }
                        (NonNumericalDimension::String, NonNumericalDimension::Enumeration(_x)) => {
                            if Dimension::assignable_enum(
                                b,
                                a,
                                &RateExpr::Single(value_of_a.clone()),
                                string_conv,
                            ) {
                                return Some(Dimension::non_numeric(NonNumericalDimension::Bool));
                            }
                        }
                        _ => {}
                    }
                }
                LogicalOp::And | LogicalOp::Or => {
                    if a.non_numerical == NonNumericalDimension::Bool
                        && b.non_numerical == NonNumericalDimension::Bool
                    {
                        assert_eq!(a, b);
                        return Some(a.clone());
                    } else {
                        return None;
                    }
                }
                _ => {}
            },
            _ => {}
        }
        match &a.non_numerical {
            NonNumericalDimension::Nothing => (),
            NonNumericalDimension::String => return None,
            NonNumericalDimension::Bool => return None,
            NonNumericalDimension::Integer => (),
            NonNumericalDimension::Enumeration(_) => return None,
        }
        match &b.non_numerical {
            NonNumericalDimension::Nothing => {}
            NonNumericalDimension::String => return None,
            NonNumericalDimension::Bool => return None,
            NonNumericalDimension::Integer => {}
            NonNumericalDimension::Enumeration(_) => return None,
        }

        let new_non_num_dim = match (&a.non_numerical, &b.non_numerical) {
            (NonNumericalDimension::Integer, NonNumericalDimension::Integer) => {
                NonNumericalDimension::Integer
            }
            _ => NonNumericalDimension::Nothing,
        };

        match self {
            FloatOp::Add | FloatOp::Sub => match Dimension::numericals_match(a, b) {
                true => {
                    let mut tmp = a.clone();
                    tmp.non_numerical = new_non_num_dim;
                    Some(tmp)
                }
                false => None,
            },
            FloatOp::Comparison(_k) => match Dimension::numericals_match(a, b) {
                true => {
                    /*let mut tmp = a.clone();
                    tmp.non_numerical = new_non_num_dim;*/
                    Some(Dimension::non_numeric(NonNumericalDimension::Bool))
                }
                false => None,
            },
            FloatOp::Div => Some(Dimension {
                length: a.length - b.length,
                time: a.time - b.time,
                weight: a.weight - b.weight,
                current: a.current - b.current,
                temperature: a.temperature - b.temperature,
                amount: a.amount - b.amount,

                non_numerical: NonNumericalDimension::Nothing,
            }),
            FloatOp::Mul => Some(Dimension {
                length: a.length + b.length,
                time: a.time + b.time,
                weight: a.weight + b.weight,
                current: a.current + b.current,
                temperature: a.temperature + b.temperature,
                amount: a.amount + b.amount,

                non_numerical: new_non_num_dim,
            }),
            FloatOp::Pow => match b.non_numerical {
                NonNumericalDimension::Nothing => {
                    if *a == Dimension::dimensionless() && *b == Dimension::dimensionless() {
                        Some(Dimension::dimensionless())
                    } else {
                        None
                    }
                }
                NonNumericalDimension::String => None,
                NonNumericalDimension::Bool => None,
                NonNumericalDimension::Integer => {
                    let fak: i64 = match value_of_b {
                        RateExprTerm::Primitive(k) => match k {
                            PrimitiveTypes::Integer(val) => *val,
                            _ => panic!("non integer power B"),
                        },
                        RateExprTerm::Expr(ex) => match ex.as_ref() {
                            RateExpr::Single(x) => match x {
                                RateExprTerm::Primitive(k) => match k {
                                    PrimitiveTypes::Integer(val) => *val,
                                    _ => panic!("non integer power C"),
                                },
                                _ => panic!("non integer power D"),
                            },
                            RateExpr::Double(_, _, _) => {
                                panic!("double")
                            }
                        },
                        _ => panic!("non integer power A is {:#?}", value_of_b),
                    };
                    Some(Dimension {
                        length: a.length * fak,
                        time: a.time * fak,
                        weight: a.weight * fak,
                        current: a.current * fak,
                        temperature: a.temperature * fak,
                        amount: a.amount * fak,
                        non_numerical: NonNumericalDimension::Nothing,
                    })
                }
                NonNumericalDimension::Enumeration(_) => None,
            },
        }
    }
    fn is_comparison(&self) -> bool {
        match self {
            FloatOp::Comparison(_) => true,
            FloatOp::Mul | FloatOp::Sub | FloatOp::Div | FloatOp::Add => false,
            FloatOp::Pow => false,
        }
    }
    pub fn apply(&self, a: &PrimitiveTypes, b: &PrimitiveTypes) -> Option<PrimitiveTypes> {
        if self.is_comparison() {
            match self.apply_comparison(a, b) {
                None => {
                    return None;
                }
                Some(k) => {
                    return Some(PrimitiveTypes::Boolean(k));
                }
            };
        }
        assert!(!self.is_comparison());
        match a {
            PrimitiveTypes::Float(a) => match b {
                PrimitiveTypes::Float(b) => Some(PrimitiveTypes::Float(self.apply_float(*a, *b))),
                PrimitiveTypes::Integer(b) => match self {
                    FloatOp::Pow => {
                        let r = a.powi(i32::try_from(*b).expect("Exponent is to large"));
                        if r < 0. {
                            //    panic!("Negative power? {}^{} = {}", a, b, r);
                        }
                        Some(PrimitiveTypes::Float(r))
                    }
                    _ => Some(PrimitiveTypes::Float(self.apply_float(*a, *b as f64))),
                },

                PrimitiveTypes::String(_) => None,
                PrimitiveTypes::Boolean(_) => None,
            },
            PrimitiveTypes::Integer(a) => match b {
                PrimitiveTypes::Float(b) => {
                    Some(PrimitiveTypes::Float(self.apply_float(*a as f64, *b)))
                }
                PrimitiveTypes::Integer(b) => match self {
                    // Promote int to float for division
                    FloatOp::Div => Some(PrimitiveTypes::Float(
                        self.apply_float(*a as f64, *b as f64),
                    )),
                    _ => Some(PrimitiveTypes::Integer(self.apply_int(*a, *b))),
                },

                PrimitiveTypes::String(_) => None,
                PrimitiveTypes::Boolean(_) => None,
            },

            PrimitiveTypes::String(_) => None,
            PrimitiveTypes::Boolean(_) => None,
        }
    }
    pub fn apply_int(&self, a: i64, b: i64) -> i64 {
        match self {
            FloatOp::Add => a + b,
            FloatOp::Sub => a - b,
            FloatOp::Div => panic!(
                "Divison of integers {}/{}! Those should have been promoted to float.",
                a, b
            ), //a / b,
            FloatOp::Mul => a * b,

            FloatOp::Pow => {
                if b >= 0 {
                    let r = a.pow(b as u32);
                    if r < 0 {
                        panic!("Negative power -> {}^{} = {}", a, b, r);
                    }
                    r
                } else {
                    panic!("Negative integer power not implemented!")
                }
            }
            FloatOp::Comparison(_) => unreachable!(),
        }
    }
    pub fn apply_float(&self, a: f64, b: f64) -> f64 {
        match self {
            FloatOp::Add => a + b,
            FloatOp::Sub => a - b,
            FloatOp::Div => a / b,
            FloatOp::Mul => a * b,
            FloatOp::Pow => a.powf(b),
            FloatOp::Comparison(_) => unreachable!(),
        }
    }
    pub fn apply_bool_to_f64(&self, a: &f64, b: &f64) -> bool {
        match self {
            FloatOp::Add => unreachable!(),
            FloatOp::Sub => unreachable!(),
            FloatOp::Div => unreachable!(),
            FloatOp::Mul => unreachable!(),
            FloatOp::Pow => unreachable!(),
            FloatOp::Comparison(k) => match k {
                LogicalOp::Equal => a == b,
                LogicalOp::Less => a < b,
                LogicalOp::Greater => a > b,
                LogicalOp::LessOrEqual => a <= b,
                LogicalOp::GreaterOrEqual => a >= b,
                LogicalOp::Unequal => a != b,
                LogicalOp::And => unreachable!(),
                LogicalOp::Or => unreachable!(),
            },
        }
    }
    pub fn apply_bool_to_int(&self, a: &i64, b: &i64) -> bool {
        match self {
            FloatOp::Add => unreachable!(),
            FloatOp::Sub => unreachable!(),
            FloatOp::Div => unreachable!(),
            FloatOp::Mul => unreachable!(),
            FloatOp::Pow => unreachable!(),
            FloatOp::Comparison(k) => match k {
                LogicalOp::Equal => a == b,
                LogicalOp::Less => a < b,
                LogicalOp::Greater => a > b,
                LogicalOp::LessOrEqual => a <= b,
                LogicalOp::GreaterOrEqual => a >= b,
                LogicalOp::Unequal => a != b,
                LogicalOp::And => {
                    unreachable!()
                }
                LogicalOp::Or => {
                    unreachable!()
                }
            },
        }
    }
    pub fn apply_comparison(&self, a: &PrimitiveTypes, b: &PrimitiveTypes) -> Option<bool> {
        let ret = match (a, b) {
            (PrimitiveTypes::Boolean(a), PrimitiveTypes::Boolean(b)) => match self {
                FloatOp::Comparison(k) => match k {
                    LogicalOp::Equal => Some(*a == *b),
                    LogicalOp::Less => None,
                    LogicalOp::Greater => None,
                    LogicalOp::LessOrEqual => None,
                    LogicalOp::GreaterOrEqual => None,
                    LogicalOp::Unequal => Some(*a != *b),
                    LogicalOp::And => Some(*a && *b),
                    LogicalOp::Or => Some(*a || *b),
                },
                _ => None,
            },
            (PrimitiveTypes::Float(a), PrimitiveTypes::Float(b)) => {
                Some(self.apply_bool_to_f64(a, b))
            }
            (PrimitiveTypes::Float(a), PrimitiveTypes::Integer(b)) => {
                Some(self.apply_bool_to_f64(a, &(*b as f64)))
            }
            (PrimitiveTypes::Integer(a), PrimitiveTypes::Float(b)) => {
                Some(self.apply_bool_to_f64(&(*a as f64), b))
            }
            (PrimitiveTypes::Integer(a), PrimitiveTypes::Integer(b)) => {
                Some(self.apply_bool_to_int(a, b))
            }
            (PrimitiveTypes::String(a), PrimitiveTypes::String(b)) => match self {
                FloatOp::Comparison(p) => match p {
                    LogicalOp::Equal => Some(a == b),
                    LogicalOp::Unequal => Some(a != b),
                    LogicalOp::Less
                    | LogicalOp::Greater
                    | LogicalOp::LessOrEqual
                    | LogicalOp::GreaterOrEqual
                    | LogicalOp::And
                    | LogicalOp::Or => None,
                },
                _ => unreachable!(),
            },
            (_, _) => None,
        };
        //println!("{:?} {:?} {:?} --- {:?}", a, b, self, ret);
        ret
    }
}

impl std::fmt::Display for FloatOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            FloatOp::Add => write!(f, "+"),
            FloatOp::Sub => write!(f, "-"),
            FloatOp::Div => write!(f, "/"),
            FloatOp::Mul => write!(f, "*"),
            FloatOp::Pow => write!(f, "^"),
            FloatOp::Comparison(p) => write!(f, "{p}"),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct AttributeRef {
    pub species_id: SpeciesLeftIdentifier,
    pub att_name: OptimizedString,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct SpeciesCount {
    pub idx_on_left: SpeciesLeftIdentifier,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum PrimitiveTypes {
    String(OptimizedString),
    Boolean(bool),
    Float(f64),
    Integer(i64),
}

impl From<f64> for PrimitiveTypes {
    fn from(value: f64) -> Self {
        PrimitiveTypes::Float(value)
    }
}

impl PrimitiveTypes {
    pub fn as_int(&self) -> Option<i64> {
        match self {
            PrimitiveTypes::Integer(i) => Some(*i),
            _ => None,
        }
    }
    pub fn to_string(&self, string_conversion: &HashMap<String, OptimizedString>) -> String {
        match self {
            PrimitiveTypes::String(k) => k.get_original_string(string_conversion),
            PrimitiveTypes::Boolean(k) => format!("{k}"),
            PrimitiveTypes::Float(k) => format!("{k}"),
            PrimitiveTypes::Integer(k) => format!("{k}"),
        }
    }
}

impl Ord for PrimitiveTypes {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (PrimitiveTypes::Integer(a), PrimitiveTypes::Integer(b)) => a.cmp(b),
            (PrimitiveTypes::String(a), PrimitiveTypes::String(b)) => a.cmp(b),
            (PrimitiveTypes::Boolean(a), PrimitiveTypes::Boolean(b)) => a.cmp(b),
            (PrimitiveTypes::Float(a), PrimitiveTypes::Float(b)) => {
                float_ord::FloatOrd { 0: *a }.cmp(&float_ord::FloatOrd { 0: *b })
            }
            (ta, tb) => {
                let dsc_a = ta.to_discr_int();
                let dsc_b = tb.to_discr_int();
                assert_ne!(dsc_a, dsc_b);
                dsc_a.cmp(&dsc_b)
            }
        }
    }
}

impl PartialOrd for PrimitiveTypes {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (PrimitiveTypes::Integer(a), PrimitiveTypes::Integer(b)) => a.partial_cmp(b),
            (PrimitiveTypes::String(a), PrimitiveTypes::String(b)) => a.partial_cmp(b),
            (PrimitiveTypes::Boolean(a), PrimitiveTypes::Boolean(b)) => a.partial_cmp(b),
            (PrimitiveTypes::Float(a), PrimitiveTypes::Float(b)) => a.partial_cmp(b),
            (ta, tb) => {
                let dsc_a = ta.to_discr_int();
                let dsc_b = tb.to_discr_int();
                assert_ne!(dsc_a, dsc_b);
                dsc_a.partial_cmp(&dsc_b)

                //dsc_a.partial_cmp(dsc_b)
            }
        }
    }
}

impl PrimitiveTypes {
    fn to_discr_int(&self) -> usize {
        match self {
            PrimitiveTypes::String(_) => 1,
            PrimitiveTypes::Boolean(_) => 2,
            PrimitiveTypes::Float(_) => 3,
            PrimitiveTypes::Integer(_) => 4,
        }
    }
    pub fn as_bool(&self) -> bool {
        match self {
            PrimitiveTypes::Boolean(k) => *k,
            _ => unreachable!("not bool"),
        }
    }
}

/*impl std::convert::From<&str> for PrimitiveTypes {
    fn from(value: &str) -> Self {
        PrimitiveTypes::String(value.to_string())
    }
}*/

impl std::convert::From<i64> for PrimitiveTypes {
    fn from(value: i64) -> Self {
        PrimitiveTypes::Integer(value)
    }
}

impl Hash for PrimitiveTypes {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            PrimitiveTypes::String(v) => v.hash(state),
            PrimitiveTypes::Boolean(v) => v.hash(state),
            PrimitiveTypes::Integer(v) => v.hash(state),
            PrimitiveTypes::Float(v) => Rational64::approximate_float(*v).hash(state),
        }
    }
}

impl PrimitiveTypes {
    pub fn as_float_no_int(&self) -> Option<f64> {
        match self {
            PrimitiveTypes::Float(f) => Some(*f),
            _ => None,
        }
    }
    pub fn as_float(&self) -> Option<f64> {
        match self {
            PrimitiveTypes::Float(f) => Some(*f),
            PrimitiveTypes::Integer(i) => Some(*i as f64),
            _ => None,
        }
    }
}

/*impl Display for PrimitiveTypes {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            PrimitiveTypes::String(s) => write!(f, "{}", s),
            PrimitiveTypes::Boolean(s) => write!(f, "{}", s),
            PrimitiveTypes::Float(s) => write!(f, "{}", s),
            PrimitiveTypes::Integer(s) => write!(f, "{}", s),
        }
    }
}*/

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Condition {
    pub guard: RateExpr,
    pub(crate) result_true: RateExpr,
    pub result_false: Option<RateExpr>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum GlobalCountEnum {
    GenericUnspecified(GenericGlobalCount),
    SpecificEnumerated(Vec<EnumeratedSpecies>),
    SpecificEnumeratedGuard(Vec<(EnumeratedSpecies, Option<RateExpr>)>),
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct BuildinFunctionCall {
    pub function: BuildinFunction,
    pub params: Vec<RateExpr>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum RateExprTerm {
    Primitive(PrimitiveTypes),
    ExpandedAttRef(AttributeRef),
    StarAttRef(AttributeRef),
    Expr(Box<RateExpr>),
    SpecCount(SpeciesCount),
    GlobalCount(GlobalCountEnum),
    Condition(Box<Condition>),
    BuildinFunction(BuildinFunctionCall),
}

/*impl PartialEq for PrimitiveTypes {
    fn eq(&self, other: &Self) -> bool {
        unimplemented!()
        /*match (self,other){
            (RateExprTerm::Num(a),RateExprTerm::Num(b)) => {
                approx_eq!(f64,a,b,ulps=10)
            }
            (RateExprTerm::Integer(a),RateExprTerm::Num(b)) => {
                approx_eq!(f64,a as f64,b,ulps=10)
            }
            (RateExprTerm::Num(a),RateExprTerm::Integer(b)) => {
                approx_eq!(f64,a,b as f64,ulps=10)
            }


        }*/
    }
}*/
impl Eq for PrimitiveTypes {}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum RateExpr {
    Single(RateExprTerm),
    Double(RateExprTerm, FloatOp, RateExprTerm),
}

impl RateExprTerm {
    pub fn can_be_evaluated_during_expansion(&self) -> bool {
        match &self {
            RateExprTerm::Primitive(_) => true,
            RateExprTerm::ExpandedAttRef(_) => true,
            RateExprTerm::StarAttRef(_) => false,
            RateExprTerm::Expr(k) => k.can_be_evaluated_during_expansion(),
            RateExprTerm::SpecCount(_) => false,
            RateExprTerm::GlobalCount(_) => false,
            RateExprTerm::Condition(k) => {
                k.guard.can_be_evaluated_during_expansion()
                    && k.result_true.can_be_evaluated_during_expansion()
                    && match &k.result_false {
                        None => true,
                        Some(o) => o.can_be_evaluated_during_expansion(),
                    }
            }
            RateExprTerm::BuildinFunction(bf) => {
                bf.params
                    .iter()
                    .all(|k| k.can_be_evaluated_during_expansion())
                    && !bf.function.is_random()
            }
        }
    }
}

impl RateExpr {
    pub fn can_be_evaluated_during_expansion(&self) -> bool {
        match self {
            RateExpr::Single(x) => x.can_be_evaluated_during_expansion(),
            RateExpr::Double(l, _, r) => {
                l.can_be_evaluated_during_expansion() && r.can_be_evaluated_during_expansion()
            }
        }
    }
}

#[derive(Eq, PartialEq, Debug, Clone, Hash, Serialize, Deserialize, PartialOrd, Ord)]
pub struct SpeciesName {
    //name: String,
    id: usize,
}
impl SpeciesName {
    pub fn dummy() -> SpeciesName {
        SpeciesName {
            //name: "dummy".to_string(),
            id: usize::MAX,
        }
    }
}

impl Display for SpeciesName {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SpeciesNameConverter {
    pub(crate) map: HashMap<String, usize>,
}
impl SpeciesNameConverter {
    pub(crate) fn new(names: &Vec<String>) -> SpeciesNameConverter {
        SpeciesNameConverter {
            map: names
                .iter()
                .cloned()
                .enumerate()
                .map(|(a, b)| (b, a))
                .collect(),
        }
    }
    pub fn try_get(&self, name: &String) -> Option<SpeciesName> {
        match self.map.get(name) {
            None => None,
            Some(k) => Some(SpeciesName {
                // name: name.clone(),
                id: *k,
            }),
        }
        /*assert!(self.map.contains_key(name));
        SpeciesName { name: name.clone(), }*/
    }
    pub fn get(&self, name: &String) -> SpeciesName {
        match self.try_get(name) {
            None => {
                panic!("Name '{name}' not found. Have {:?}", self.map)
            }
            Some(x) => x,
        }
    }
    pub fn get_rev(&self, name: &SpeciesName) -> String {
        self.map
            .iter()
            .filter(|(_a, b)| **b == name.id)
            .next()
            .unwrap()
            .0
            .clone()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AttributeDimensionality {
    pub dim: Dimension,
    pub dim_str_prefak: Option<(String, f64)>,
}
impl std::fmt::Display for AttributeDimensionality {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self.dim_str_prefak {
            None => {
                write!(f, "{}", self.dim)
            }
            Some((s, _)) => {
                write!(f, "'{}' i.e. {}", s, self.dim)
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Species {
    pub is_compartment: bool,
    pub name: SpeciesName,
    pub expanding_attributes: HashMap<OptimizedString, AttributeDimensionality>,
    pub not_expanding_attributes: HashMap<OptimizedString, AttributeDimensionality>,
}
impl Species {
    pub fn dim_from_attribute_name(
        &self,
        name: &OptimizedString,
    ) -> Option<&AttributeDimensionality> {
        match self
            .expanding_attributes
            .iter()
            .chain(self.not_expanding_attributes.iter())
            .find(|(k, _)| *k == name)
        {
            None => None,
            Some((_, a)) => Some(a),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize, Ord, PartialOrd)]
pub struct SpeciesLeftIdentifier {
    //name: String,
    //name_hash: u64,
    name_hash: usize,
}
use std::convert::TryInto;

impl SpeciesLeftIdentifier {
    pub fn new_from_int(name: usize) -> SpeciesLeftIdentifier {
        SpeciesLeftIdentifier { name_hash: name }
    }
    pub fn new_from_optim(name: &OptimizedString) -> SpeciesLeftIdentifier {
        SpeciesLeftIdentifier {
            name_hash: name.name,
        }
    }
    /*pub(crate) fn new(name: &str) -> SpeciesLeftIdentifier {
        unimplemented!();
        /*let mut hasher = Sha256::new(); //DefaultHasher::new();
                                        //name.hash(&mut hasher);
        Digest::update(&mut hasher, name);
        let hash = hasher.finalize();

        let hash_num = byteorder::LittleEndian::read_u128(hash.as_slice());
        //let (head, _): (GenericArray<_, U16>, _) = Split::split(hash);
        //let hash128 = u128::from_le_bytes(*head.as_ref());
        /*let hash_val: GenericArray<u8, <Sha256 as Digest>::OutputSize> = hasher.finalize();

        let hash_num = enumerate(hash_val.into_iter().take(16))
            .map(|(n, v)| (v as u128) << (n * 8))
            .sum();*/

        /*if !name.contains("DUMMY") {
            println!(
                "HASH: {}\t{}\t {}",
                name,
                hash_num,
                hash_num as f64 / u128::MAX as f64
            );
        }*/

        SpeciesLeftIdentifier {
            name_hash: hash_num, //hash_val.into(),
                                 //name: name.to_string(),
        }*/
    }*/

    pub fn new_dummy() -> SpeciesLeftIdentifier {
        SpeciesLeftIdentifier { name_hash: 0 }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SpeciesLeft {
    pub species_name: SpeciesName,
    pub id: SpeciesLeftIdentifier,
    //pub extra_name: Option<String>,

    //pub number_in_match: isize,
    pub parent_number: Option<SpeciesLeftIdentifier>, // number of the species on the left side that is my parent (-1 means not assigned)
    pub attribute_constraint: Vec<RateExpr>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum CreationAmt {
    StaticNum(usize),
    DynExpr(RateExpr),
}
impl CreationAmt {
    pub fn is_one(&self) -> bool {
        match self {
            CreationAmt::StaticNum(k) => *k == 1,
            CreationAmt::DynExpr(_) => false,
        }
    }
    pub fn as_num(&self) -> Option<usize> {
        match self {
            CreationAmt::StaticNum(k) => Some(*k as usize),
            CreationAmt::DynExpr(_) => None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SpeciesRight {
    pub species_name: SpeciesName,
    pub extra_name: Option<String>,
    pub compartment_constraint: Option<usize>, // number of the species on the right side that is my parent (-1 means not assigned)
    pub corresponding_on_left: Option<SpeciesLeftIdentifier>,
    pub expanded_attribute_assignments: Vec<(OptimizedString, RateExpr)>,
    pub star_attribute_assignment: Vec<(OptimizedString, RateExpr)>,
    pub amount: CreationAmt,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LogicalConstraint {
    pub left: RateExpr,
    pub op: LogicalOp,
    pub right: RateExpr,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum RestSolutionRightTarget {
    ReactionLevel,
    OtherCompartment(SpeciesRight),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum StructuralChange {
    RemoveCompartment(SpeciesLeftIdentifier),
    RemoveRestSolution(RestSolutionLeft),
    MoveRestSolutionTo {
        old: RestSolutionLeft,
        new: RestSolutionRightTarget,
    },
    OtherUnimplemented,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RestSolutionTransition {
    pub(crate) leftsideparent: SpeciesLeftIdentifier,
    pub amount_quanti: Option<RateExpr>,
    pub(crate) idx_right: Vec<isize>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Transition {
    pub left: Vec<SpeciesLeft>,
    pub right: Vec<SpeciesRight>,
    pub rate_expr: RateExpr,
    pub max_fire: Option<usize>,
    pub is_not_mass_action: bool,
    pub original_string: String,
    pub has_structural_changes: bool,
    pub rest_solution_transiotions: Vec<RestSolutionTransition>,
}

impl Transition {
    #[cfg(feature = "hybrid_att")]
    pub fn validate_not_multiple_hybrid_of_same_name_on_left_for_mass_action(
        &self,
        all_species: &HashMap<SpeciesName, Species>,
    ) -> Result<(), String> {
        if self.is_not_mass_action && !self.has_structural_changes {
            return Ok(());
        }
        let left_species_names: Vec<_> = self
            .left
            .iter()
            .filter(|k| {
                !all_species
                    .get(&k.species_name)
                    .unwrap()
                    .not_expanding_attributes
                    .is_empty()
                    && !all_species.get(&k.species_name).unwrap().is_compartment
            })
            .map(|k| k.species_name.clone())
            .collect();
        if left_species_names.len()
            != left_species_names
                .iter()
                .cloned()
                .collect::<HashSet<_>>()
                .len()
        {
            if !self.is_not_mass_action {
                Err("When using automatic Mass action (i.e. '@' not '@@') you can not currently use the same species with network free (*) attributes on the left side multiple times".to_string())
            } else if self.has_structural_changes {
                Err("Structural changes with multiple network free species of the same type are not yet implemented.".to_string())
            } else {
                unreachable!()
            }
        } else {
            Ok(())
        }
    }
    #[cfg(not(feature = "hybrid_att"))]
    pub fn validate_not_multiple_hybrid_of_same_name_on_left_for_mass_action(
        &self,
        _all_species: &HashMap<SpeciesName, Species>,
    ) -> Result<(), String> {
        Ok(())
    }
    pub fn get_left_by_id(&self, id: &SpeciesLeftIdentifier) -> Option<&SpeciesLeft> {
        self.left.iter().find(|v| v.id == *id)
    }
    pub fn use_names_for_left_right_correspondence(
        &mut self,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> Result<(), String> {
        let named_right: Vec<_> = self
            .right
            .iter()
            .filter(|v| v.extra_name.is_some())
            .map(|v| v.extra_name.as_ref().clone().unwrap())
            .collect();
        let named_right_set: HashSet<_> = named_right.iter().cloned().collect();
        if named_right.len() != named_right_set.len() {
            return Err(format!("Double use of :name on right site"));
        }

        let count_left = self.left.clone();

        self.right
            .iter_mut()
            .filter(|v| v.extra_name.is_some())
            .map(|v| {
                let n = v.extra_name.as_ref().clone().unwrap();
                match count_left
                    .iter()
                    .filter(|l| l.id == SpeciesLeftIdentifier::new_from_optim(&att_name_conversion[n]))
                    .count()
                {
                    0 => { Err(format!("Name {} not used on left side", n)) },
                    1 => {
                        if v.amount.is_one() {
                            v.corresponding_on_left = Some(
                                count_left
                                    .iter()
                                    .find(|l| l.id == SpeciesLeftIdentifier::new_from_optim(&att_name_conversion[n]))
                                    .unwrap()
                                    .id
                                    .clone(),
                            )
                        }
                        Ok(())
                    }
                    x => { Err(format!("Name {} used multiple times ({}x) on left side (or unclear correspondence due to multiple created)", x, n))},
                }
            }).collect()
    }

    pub fn check_assignments(
        &self,
        all_species: &HashMap<SpeciesName, Species>,
        span: &pest::Span,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> Result<(), LanguageError> {
        for r in self
            .right
            .iter()
            .filter(|k| k.corresponding_on_left.is_none())
        {
            let starr_names: BTreeSet<_> = r
                .star_attribute_assignment
                .iter()
                .map(|(a, _)| a.clone())
                .collect();
            /*println!(
                "{} ---> {:?}/{:?} in {:?}",
                r.species_name,
                r.not_expanded_attribute_assignment,
                starr_names,
                all_species[&r.species_name].not_expanding_attributes
            );*/
            for (known_name, _) in all_species[&r.species_name].not_expanding_attributes.iter() {
                if !starr_names.contains(known_name) {
                    return Err(LanguageError::from_span(
                        &span,
                        format!(
                            "Missing assignment for '{}' in '{}' on right side of reaction",
                            known_name.get_original_string(att_name_conversion),
                            r.species_name
                        )
                        .as_str(),
                        line!(),
                        file!(),
                    ));
                }
            }
        }
        Ok(())
    }
    pub fn find_left_right_correspondence(&mut self) {
        let count_right = self.right.clone();
        let count_left = self.left.clone();
        self.right
            .iter_mut()
            .filter(|r| r.corresponding_on_left.is_none())
            .filter(|s| {
                count_right
                    .iter()
                    .map(|v| &v.species_name)
                    .filter(|name| **name == s.species_name)
                    .count()
                    == 1
            })
            .filter(|s_right| {
                count_left
                    .iter()
                    .filter(|sl| sl.species_name == s_right.species_name)
                    .count()
                    == 1
            })
            .for_each(|s_right| {
                if s_right.amount.is_one() {
                    s_right.corresponding_on_left = Some(
                        count_left
                            .iter()
                            .find(|l| l.species_name == s_right.species_name)
                            .unwrap()
                            .id
                            .clone(),
                    )
                }
            });
    }
}

/*#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum CountingElement {
    _SpecificNameFromLeft(usize),
    GeneralDescription(String, Vec<RateExpr>),
    Single(String),
    Nested(String, String, Vec<RateExpr>),
}*/
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum GenericGlobalCountInsideOf {
    SpecificNone,
    GlobalNone,
    SpecificOneLevel(SpeciesLeftIdentifier),
    SpecificAnyDepth(SpeciesLeftIdentifier),
    GlobalOneLevel(SpeciesName, Vec<RateExpr>),
    GlobalAnyDepth(SpeciesName, Vec<RateExpr>),
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct GenericGlobalCount {
    pub(crate) species_name: SpeciesName,
    pub constraints: Vec<RateExpr>,
    pub(crate) inside_of: GenericGlobalCountInsideOf,
    pub(crate) name_in_model_code: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Model {
    pub species: HashMap<SpeciesName, Species>,
    pub transitions: Vec<Transition>,
    pub inital: Vec<SpeciesRight>,
    pub observations: Vec<(String, RateExpr)>,
    pub parameters: HashMap<String, RateExpr>,
    pub species_name_conversion: SpeciesNameConverter,
    pub string_conversion: HashMap<String, OptimizedString>,
    pub original_file_name: String,
}

impl Model {
    /* species that occur exactly once on both sides, can automatically transfer attributes. For this we need to find the corresponding species as done in this function*/

    pub fn _check_left_right_correspondence_for_compartments(&self) {
        let compartments: Vec<_> = self
            .species
            .values()
            .filter(|s| s.is_compartment)
            .cloned()
            .collect();
        compartments.into_iter().for_each(|c| {
            self.transitions.iter().for_each(| t| {
                let count_left = t.left.iter().filter(|v| v.species_name == c.name).count();
                let count_right = t.right.iter().filter(|v| v.species_name == c.name).count();
                if count_left > 1 || count_right > 1 || count_left != count_right {
                    panic!("Compartmeant {} occurs {} on left and {} on right, but it has to be exatly once on both sides",c.name,count_left,count_right);
                }
                if count_right == 1 {
                    let _pos_left = t.left.iter().find_position(|v|v.species_name==c.name).unwrap();
                    assert!(t.right.iter().find(|v|v.species_name==c.name).unwrap().corresponding_on_left.is_some())
                    //t.right.iter_mut().find(|v|v.species_name==c.name).unwrap().corresponding_on_left = Some(pos_left.0);
                }
            })
        });
    }

    pub fn verify_spicies<'a>(
        &'a self,
        transition_spans: Vec<Span<'a>>,
        model_span: Span<'a>,
    ) -> Result<(), (String, Option<Span>)> {
        for (name, spec) in self.species.iter() {
            if *name != spec.name {
                panic!("Illformed species! {} != {}", name, spec.name);
            }
        }
        for s in self.inital.iter() {
            self.species.get(&s.species_name).expect(stringify!(
                "species Name (in initializiation) {} does not exist",
                s.species_name
            ));
            let testset: HashSet<OptimizedString> = s
                .expanded_attribute_assignments
                .iter()
                .map(|(n, _v)| n)
                .cloned()
                .collect();
            if !(s.corresponding_on_left.is_none()) {
                return Err((
                    "There is a strange left correspondence here!".to_string(),
                    Some(model_span),
                ));
            }

            if testset
                != self
                    .species
                    .get(&s.species_name)
                    .unwrap()
                    .expanding_attributes
                    .keys()
                    .cloned()
                    .collect()
            {
                return Err((
                    format!(
                        "Not all attributes are defined for initialization, species: {:?}",
                        self.species
                            .get(&s.species_name)
                            .unwrap()
                            .expanding_attributes
                    )
                    .to_string(),
                    Some(model_span.clone()),
                ));
            }
            let test_set2: HashSet<OptimizedString> = s
                .star_attribute_assignment
                .iter()
                .map(|(v, _)| v)
                .cloned()
                .collect();
            if test_set2.len() != s.star_attribute_assignment.len() {
                return Err((
                    "Duplicate star attribute assignment".to_string(),
                    Some(model_span.clone()),
                ));
            }
            if test_set2
                != self
                    .species
                    .get(&s.species_name)
                    .unwrap()
                    .not_expanding_attributes
                    .keys()
                    .cloned()
                    .collect()
            {
                return Err((format!(
                    "Not all attributes are defined in initialization for '{}'. We have only: '{:?}'",
                    s.species_name,
                    self.species
                        .get(&s.species_name)
                        .unwrap()
                        .expanding_attributes
                ),Some(model_span.clone())));
            }
        }
        for (t, span) in self.transitions.iter().zip(transition_spans.into_iter()) {
            for s in t.left.iter() {
                self.species
                    .get(&s.species_name)
                    .expect(stringify!("species Name {} does not exist", s.species_name));
            }
            for s in t.right.iter() {
                self.species
                    .get(&s.species_name)
                    .expect(stringify!("species Name {} does not exist", s.species_name));
                let defined: HashSet<OptimizedString> = s
                    .expanded_attribute_assignments
                    .iter()
                    .map(|(n, _v)| n)
                    .cloned()
                    .collect();
                if s.corresponding_on_left.is_none() {
                    if defined
                        != self
                            .species
                            .get(&s.species_name)
                            .unwrap()
                            .expanding_attributes
                            .keys()
                            .cloned()
                            .collect()
                    {
                        let needed: HashSet<_> = self
                            .species
                            .get(&s.species_name)
                            .unwrap()
                            .expanding_attributes
                            .keys()
                            .cloned()
                            .collect();
                        return Err((format!("There is an error in the model!\nNot all needed attributes are defined\n\nMissing: {:?}",needed.difference(&defined)),Some(span.clone())));
                    }
                }
            }
        }
        return Ok(());
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RestSolutionLeft {
    pub name: String,
    pub parent_id: SpeciesLeftIdentifier,
    pub amount_expr: Option<RateExpr>,
}

pub struct RestSolutionRight {
    pub name: String,
    pub compartment_constraint: isize, // number of the species on the right side that is my parent (-1 means not assigned)
}

#[derive(Debug, Eq, PartialEq, Clone, Hash, Serialize, Deserialize)]
pub enum NonNumericalDimension {
    Nothing,
    String,
    Bool,
    Integer,
    Enumeration(Vec<String>),
}

#[derive(Eq, PartialEq, Debug, Clone, Hash, Serialize, Deserialize)]
pub struct Dimension {
    pub length: i64,
    pub time: i64,
    pub weight: i64,
    pub current: i64,
    pub temperature: i64,
    pub amount: i64,

    pub non_numerical: NonNumericalDimension,
}

impl Dimension {
    /*pub fn _power(&self, p: isize) -> Option<Dimension> {
        match self.non_numerical {
            NonNumericalDimension::Nothing | NonNumericalDimension::Integer => Some(Dimension {
                length: self.length * p,
                time: self.time * p,
                weight: self.weight * p,
                current: self.current * p,
                temperature: self.temperature * p,
                amount: self.amount * p,
                non_numerical: self.non_numerical.clone(),
            }),
            NonNumericalDimension::String | NonNumericalDimension::Bool => None,
        }
    }*/
    fn all_potential_str_term(x: RateExprTerm) -> Vec<OptimizedString> {
        match x {
            RateExprTerm::Primitive(PrimitiveTypes::String(s)) => vec![s],
            RateExprTerm::Primitive(_x) => {
                vec![]
            }
            RateExprTerm::ExpandedAttRef(l) => {
                vec![l.att_name]
            }
            RateExprTerm::StarAttRef(l) => {
                vec![l.att_name]
            }
            RateExprTerm::Expr(x) => Dimension::all_potential_str(*x),
            RateExprTerm::SpecCount(_) => {
                vec![]
            }
            RateExprTerm::GlobalCount(_) => {
                vec![]
            }
            RateExprTerm::Condition(c) => match c.result_false {
                None => Dimension::all_potential_str(c.result_true),
                Some(l) => Dimension::all_potential_str(l)
                    .into_iter()
                    .chain(Dimension::all_potential_str(c.result_true))
                    .collect(),
            },
            RateExprTerm::BuildinFunction(_) => {
                vec![]
            }
        }
    }
    fn all_potential_str(x: RateExpr) -> Vec<OptimizedString> {
        match x {
            RateExpr::Single(x) => Dimension::all_potential_str_term(x),
            RateExpr::Double(a, _, b) => Dimension::all_potential_str_term(a)
                .into_iter()
                .chain(Dimension::all_potential_str_term(b).into_iter())
                .collect(),
        }
    }
    pub fn assignable_enum(
        to: &Dimension,
        from: &Dimension,
        from_val: &RateExpr,
        string_conv: &HashMap<String, OptimizedString>,
    ) -> bool {
        match (&to.non_numerical, &from.non_numerical) {
            (NonNumericalDimension::Enumeration(vals), NonNumericalDimension::String) => {
                match from_val.evaluate_partial_for_left(&AllOrSingleLeft::None, None, None, false)
                {
                    PartialEvaluationExpr::Primitive(PrimitiveTypes::String(s)) => {
                        vals.iter()
                            .map(|k| string_conv.get(k).unwrap())
                            .find(|p| **p == s)
                            .is_some()
                        /*{
                            None => {
                                panic!(
                                    "{:?} -> {:?}",
                                    vals.iter()
                                        .map(|k| string_conv[k].clone())
                                        .collect::<Vec<_>>(),
                                    s
                                );
                            }
                            Some(_) => true,
                        }*/
                    }
                    PartialEvaluationExpr::Primitive(_p) => unreachable!("is prim"),
                    PartialEvaluationExpr::Incomplete(x) => {
                        let r = Dimension::all_potential_str(x);
                        assert!(!r.is_empty());
                        r.into_iter().all(|s| {
                            vals.iter()
                                .map(|k| string_conv.get(k).unwrap())
                                .find(|p| **p == s)
                                .is_some()
                        })
                    } //_ => false,
                      //x => unreachable!("{:?}", x), //RateExpr::Double(_, _, _) => { unreachable!()}
                }
            }
            _ => false,
        }
    }
    pub fn assignable(to: &Dimension, from: &Dimension) -> bool {
        if !Dimension::numericals_match(to, from) {
            return false;
        }
        if to.non_numerical == from.non_numerical {
            return true;
        }
        match (&to.non_numerical, &from.non_numerical) {
            (NonNumericalDimension::Nothing, NonNumericalDimension::Integer) => true,
            (NonNumericalDimension::Enumeration(_vals), NonNumericalDimension::String) => {
                false // Could be true but need assignable_enum to check
            }
            _ => false,
        }
    }
    fn numericals_match(a: &Dimension, b: &Dimension) -> bool {
        a.length == b.length
            && a.time == b.time
            && a.weight == b.weight
            && a.current == b.current
            && a.temperature == b.temperature
            && a.amount == b.amount
    }
    pub const fn dimonsionless_integer() -> Dimension {
        Dimension {
            length: 0,
            time: 0,
            weight: 0,
            current: 0,
            temperature: 0,
            amount: 0,
            non_numerical: NonNumericalDimension::Integer,
        }
    }
    pub const fn dimensionless() -> Dimension {
        Dimension {
            length: 0,
            time: 0,
            weight: 0,
            current: 0,
            temperature: 0,
            amount: 0,
            non_numerical: NonNumericalDimension::Nothing,
        }
    }
    pub fn non_numeric(nn: NonNumericalDimension) -> Dimension {
        let mut k = Dimension::dimensionless();
        k.non_numerical = nn;
        k
    }

    fn exp_if_not_1(v: i64) -> String {
        match v {
            0 => "".to_string(),
            x => format!("^{}", x).to_string(),
        }
    }
}
impl fmt::Display for Dimension {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self.non_numerical {
            NonNumericalDimension::Nothing => {}
            NonNumericalDimension::String => write!(f, "<STRING>")?,
            NonNumericalDimension::Bool => write!(f, "<BOOL>")?,
            NonNumericalDimension::Integer => write!(f, "<INTEGER>")?,
            NonNumericalDimension::Enumeration(vals) => write!(f, "<{{{:?}}}>", vals)?,
        }
        if self.length == 0 && self.weight == 0 && self.time == 0 {
            write!(f, "{{}}")
        } else {
            write!(f, "{{")?;
            if self.length != 0 {
                write!(f, "length{} ", Dimension::exp_if_not_1(self.length))?;
            }
            if self.weight != 0 {
                write!(f, "weight{} ", Dimension::exp_if_not_1(self.weight))?;
            }
            if self.time != 0 {
                write!(f, "time{} ", Dimension::exp_if_not_1(self.time))?;
            }
            if self.amount != 0 {
                write!(
                    f,
                    "molecular_amount{} ",
                    Dimension::exp_if_not_1(self.amount)
                )?;
            }
            if self.current != 0 {
                write!(
                    f,
                    "electric_current{} ",
                    Dimension::exp_if_not_1(self.current)
                )?;
            }
            if self.temperature != 0 {
                write!(
                    f,
                    "temperature{} ",
                    Dimension::exp_if_not_1(self.temperature)
                )?;
            }
            write!(f, "}}")
        }
    }
}
