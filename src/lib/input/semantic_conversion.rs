use crate::input::structures;
use crate::input::structures::*;
use std::cmp::min;

use crate::input::syntax::FiringLimitationsStx;
use crate::input::syntax::*;
use crate::input::units::UNIT_SECOND;
//use crate::flatten::*;
//use itertools::Itertools;

//use nanoid::nanoid;
use std::collections::{BTreeSet, HashMap, HashSet};
use std::convert::{TryFrom, TryInto};
use std::fs;
//use std::any::Any;
use crate::buildin_functions::list_of_buildin_functions;
use crate::expansion::static_expansion::{AllOrSingleLeft, OptimizedString, PartialEvaluationExpr};
use itertools::Itertools;
use pest::Span;
/*
#[allow(unreachable_code)]
fn rek_global_count(
    elem: &Option<String>,
    all_species: &HashMap<String, Species>,
    all_constants: &HashMap<String, ConstantDefinition>,
) -> Result<Vec<CountingElement>, LanguageError> {
    match elem {
        None => Ok(vec![]),
        Some(x) => {
            let prelim = rek_global_count(&x.inside_of, all_species, all_constants)?;
            if !all_species.contains_key(&x.what.species_name) {
                return Err(LanguageError::from_span(
                    &x.span,
                    format!(
                        "The name '{}' is not a declared name for a species",
                        x.what.species_name
                    )
                    .as_str(),
                    line!(),
                    file!(),
                ));
            }
            let my_species = all_species.get(&x.what.species_name).unwrap();
            let my_elem = CountingElement::GeneralDescription(
                x.what.species_name.clone(),
                x.what
                    .attribute_constraints
                    .iter()
                    .map(|v| {
                        v.check_and_convert_on_left_side(
                            my_species,
                            unimplemented!(),
                            all_constants,
                        )
                    })
                    .collect::<Result<_, _>>()?,
            );
            let res = vec![my_elem];
            Ok(res.into_iter().chain(prelim.into_iter()).collect())
        }
    }
}*/

impl GenericGlobalCount {
    fn from_stx(
        d: &ComplexCountStx,
        mut context: SemanticContext,
    ) -> Result<GenericGlobalCount, LanguageError> {
        if !context
            .species_name_conversion
            .try_get(&d.what.species_name)
            .is_some()
        {
            return Err(LanguageError::from_span(
                &d.what.span,
                format!(
                    "The name '{}' is not a declared name for a species",
                    d.what.species_name
                )
                .as_str(),
                line!(),
                file!(),
            ));
        }
        if d.what.child_solution.is_some()
            || d.what.extra_name.is_some()
            || d.what.rest_solution_amount.is_some()
            || d.what.is_rest_solution
        {
            return Err(LanguageError::from_span(
                &d.what.span,
                format!("Invalid count").as_str(),
                line!(),
                file!(),
            ));
        }
        let species_name = context.species_name_conversion.get(&d.what.species_name);
        context.single_reference_species = Some((
            context.all_species.get(&species_name).unwrap(),
            SpeciesLeftIdentifier::new_from_optim(
                &context.att_name_conversion[&d.what.species_name],
            ),
        ));

        Ok(GenericGlobalCount {
            species_name: species_name,
            constraints: d
                .what
                .attribute_constraints
                .iter()
                .map(|constr| constr.check_and_convert_in_guard(&context))
                .collect::<Result<_, _>>()?,

            inside_of: match (&d.inside_of, d.inside_of_arbitrary_deep) {
                /*(&d.inside_of, d.inside_of_arbitrary_deep, d.global) {
                (None, _, true) => GenericGlobalCountInsideOf::None,
                (Some(n), false, false) => GenericGlobalCountInsideOf::SpecificOneLevel(
                    context.find_left_id_from_name(&n, &d.span)?,
                ),
                (Some(n), false, true) => GenericGlobalCountInsideOf::GlobalOneLevel(),
                (Some(n), true, false) => GenericGlobalCountInsideOf::SpecificAnyDepth(
                    context.find_left_id_from_name(&n, &d.span)?,
                ),*/
                (InsideOfCountStx::FromLeft(l), false) => {
                    GenericGlobalCountInsideOf::SpecificOneLevel(
                        context.find_left_id_from_name(&l, &d.span)?,
                    )
                }
                (InsideOfCountStx::FromLeft(l), true) => {
                    GenericGlobalCountInsideOf::SpecificAnyDepth(
                        context.find_left_id_from_name(&l, &d.span)?,
                    )
                }
                (InsideOfCountStx::Global(g), arb_deep) => {
                    let name = match context.species_name_conversion.try_get(&g.species_name) {
                        None => {
                            return Err(LanguageError::from_span(
                                &g.span,
                                "name not found",
                                line!(),
                                file!(),
                            ));
                        }
                        Some(x) => x,
                    };
                    let mut my_context = context.clone();
                    my_context.single_reference_species = Some((
                        context.all_species.get(&name).unwrap(),
                        SpeciesLeftIdentifier::new_dummy(),
                    ));
                    let constr = g
                        .attribute_constraints
                        .iter()
                        .map(|k| {
                            let (expr, dim) = RateExpr::from_tree(k.clone(), &my_context)?;
                            if dim.non_numerical != NonNumericalDimension::Bool {
                                Err(LanguageError::from_span(
                                    &g.span,
                                    "Not a boolean guard",
                                    line!(),
                                    file!(),
                                ))
                            } else {
                                Ok(expr)
                            }
                        })
                        .collect::<Result<Vec<_>, _>>()?;
                    match arb_deep {
                        true => GenericGlobalCountInsideOf::GlobalAnyDepth(name, constr),
                        false => GenericGlobalCountInsideOf::GlobalOneLevel(name, constr),
                    }
                }

                (InsideOfCountStx::NoneLocal, _) => GenericGlobalCountInsideOf::SpecificNone,
                (InsideOfCountStx::NoneGlobal, _) => GenericGlobalCountInsideOf::GlobalNone,
            },

            name_in_model_code: d.string_name.clone(),
        })

        /* let definition =
            rek_global_count(&Some(Box::new((*d).clone())), all_species, all_constants)?;
        Ok(GenericGlobalCount {
            definition,
            name_in_model_code: d.string_name.clone(),
        })*/
    }
}

impl Model {
    pub fn from_string(
        model_string: &String,
        allow_dynamic_structures: bool,
        constant_overwrites: &Vec<String>,
        filename: String,
        errors_only: bool,
    ) -> Result<(Model, String), Vec<LanguageError>> {
        Model::from_string_multiple(
            model_string,
            allow_dynamic_structures,
            constant_overwrites,
            filename,
            errors_only,
        )
        /* match std::panic::catch_unwind(||Model::from_string_multiple(model_string)){
            Ok(x) => x,
            Err(_y) => Err(vec![LanguageError{
                lstart: 0,
                lstop: 30,
                cstart: 0,
                cstop: 0,
                what: "INTERNAL CODE PANIC".to_string(),
                line: 0,
                file: "".to_string()
            }])
        }*/
    }

    fn from_string_internal(
        model_string: &String,
        allow_dynamic_structures: bool,
        constant_overwrites: &Vec<String>,
        file_name: String,
        errors_only: bool,
    ) -> Result<(Model, String), LanguageError> {
        let mut m = ModelParsed::from_string(model_string, allow_dynamic_structures)?;

        for c in constant_overwrites {
            let parts: Vec<_> = c.split("=").collect();
            if parts.len() != 2 {
                panic!("Constant overwrite '{}' failed. Wrong number of =", c)
            }
            match m.constants.get_mut(&parts[0].to_string()) {
                None => {
                    panic!("No constant '{}' to overwrite", parts[0])
                }
                Some(cd) => {
                    println!(
                        "Overwrite '{}' from '{}' -> '{}'",
                        parts[0],
                        cd.value.span.as_str(),
                        parts[1]
                    );
                    cd.value = ExprStx::from_string(parts[1])
                        .expect(&*format!("Could not parse {}", parts[1]));
                }
            }
        }
        let m = m;

        let species_name_conversion =
            SpeciesNameConverter::new(&m.species_names.iter().cloned().collect());

        let mut availabel_functions = HashMap::new();
        for def in m.function_definitions.iter() {
            if availabel_functions
                .insert(
                    def.name.clone().to_lowercase(),
                    FunctionDefinition::NewFunction(def.clone()),
                )
                .is_some()
            {
                return Err(LanguageError::from_span(
                    &def.span,
                    "Douplicate Function name!",
                    line!(),
                    file!(),
                ));
            }
        }
        availabel_functions.extend(
            list_of_buildin_functions()
                .into_iter()
                .map(|(s, k)| (s, FunctionDefinition::BuildIn(k))),
        );
        let availabel_functions = availabel_functions;

        let all_att_names: HashSet<String> = m.get_all_strings();
        let att_name_conversion: HashMap<String, OptimizedString> = all_att_names
            .into_iter()
            .enumerate()
            .map(|(num, k)| (k.clone(), OptimizedString::new(num, &k)))
            .collect();

        /* check all constants */
        for (_, c) in m.constants.iter() {
            match RateExpr::from_tree(
                c.value.clone(),
                &SemanticContext {
                    left_match: &vec![],
                    all_species: &Default::default(),
                    all_constants: &m.constants,
                    single_reference_species: None,
                    available_functions: &availabel_functions,
                    current_function_context: HashMap::new(),
                    allow_starr_attributes: false,
                    allow_time: false,
                    allow_random: false,
                    species_name_conversion: &species_name_conversion,
                    att_name_conversion: &att_name_conversion,
                },
            ) {
                Ok(_x) => {}
                Err(y) => return Err(y),
            }
        }

        let parameters: Vec<std::result::Result<(std::string::String, RateExpr), LanguageError>> =
            m.constants
                .iter()
                .map(|(n, v)| {
                    Ok((
                        n.clone(),
                        RateExpr::from_tree(
                            v.value.clone(),
                            &SemanticContext {
                                left_match: &vec![],
                                all_species: &Default::default(),
                                all_constants: &m.constants,
                                single_reference_species: None,
                                available_functions: &availabel_functions,
                                current_function_context: HashMap::new(),
                                allow_starr_attributes: false,
                                allow_time: false,
                                allow_random: false,
                                species_name_conversion: &species_name_conversion,
                                att_name_conversion: &att_name_conversion,
                            },
                        )?
                        .0,
                    ))
                })
                .collect();
        let parameters: HashMap<_, _> = match parameters.into_iter().collect::<Result<_, _>>() {
            Ok(x) => x,
            Err(y) => return Err(y),
        };

        let latex = m.make_latex();

        let all_species =
            match m.validate_and_get_species(&species_name_conversion, &att_name_conversion) {
                Ok(x) => x,
                Err(y) => return Err(y),
            };
        let all_species: HashMap<SpeciesName, Species> = all_species
            .into_iter()
            .map(|(a, b)| (species_name_conversion.get(&a), b))
            .collect();

        let observations: Vec<_> = m
            .observations
            .iter()
            .map(|v| {
                let (re, d) = RateExpr::from_tree(
                    v.clone(),
                    &SemanticContext {
                        left_match: &vec![],
                        all_species: &all_species,
                        all_constants: &m.constants,
                        single_reference_species: None,
                        available_functions: &availabel_functions,
                        current_function_context: HashMap::new(),
                        allow_starr_attributes: true,
                        allow_time: true,
                        allow_random: false,
                        species_name_conversion: &species_name_conversion,
                        att_name_conversion: &att_name_conversion,
                    },
                )?;
                if !Dimension::assignable(&Dimension::dimensionless(), &d) {
                    return Err(LanguageError::from_span(
                        &v.span,
                        "Observation as to be dimensionless",
                        line!(),
                        file!(),
                    ));
                }
                Ok((v.span.as_str().to_string(), re))
            }) //&all_species, &m.constants))
            .collect::<Result<_, _>>()?;

        let mut transitions: Vec<Transition> = vec![];
        for t in m.transitions.clone().into_iter().map(|t| {
            ModelParsed::check_and_get_transition(
                t,
                &all_species,
                &m.constants,
                &availabel_functions,
                allow_dynamic_structures,
                &species_name_conversion,
                &att_name_conversion,
                errors_only,
            )
        }) {
            match t {
                Ok(y) => transitions.push(y),
                Err(e) => return Err(e),
            }
        }

        /*let transition_errs: Vec<_> = transitions
            .iter()
            .filter_map(|v| match v {
                Ok(_x) => None,
                Err(x) => Some(x),
            })
            .cloned()
            .collect();
        if !transition_errs.is_empty() {
            return Err(transition_errs);
        }*/

        let initial_set = match rekursive_right_side2(
            -1,
            Some(m.initializations.clone()),
            &all_species,
            &vec![],
            &m.constants,
            false,
            availabel_functions.clone(),
            &species_name_conversion,
            &att_name_conversion,
            false,
            errors_only,
        ) {
            Ok(x) => x.0,
            Err(y) => return Err(y),
        };

        let new_m = Model {
            inital: initial_set,
            observations,

            species: all_species,
            transitions,
            parameters: parameters,
            species_name_conversion,
            string_conversion: att_name_conversion,
            original_file_name: file_name,
        };

        match new_m.verify_spicies(
            m.transitions.iter().map(|k| k.span.clone()).collect(),
            m.model_span.clone(), //m.initializations.species_in_solution[0].span.clone(),
        ) {
            Ok(_) => {}
            Err((msg, spn)) => match spn {
                None => {
                    return Err(LanguageError {
                        lstart: 0,
                        lstop: 0,
                        cstart: 100,
                        cstop: 100,
                        what: msg,
                        line: line!(),
                        file: file!().parse().unwrap(),
                    })
                }
                Some(spn) => {
                    return Err(LanguageError::from_span(&spn, &*msg, line!(), file!()));
                }
            },
        };
        #[cfg(feature = "hybrid_att")]
        {
            let num_hybrid_spec = new_m
                .species
                .iter()
                .filter(|(_, k)| (!k.not_expanding_attributes.is_empty()) && (!k.is_compartment))
                .count();
            if num_hybrid_spec == 0 {
                if cfg!(debug_assertions) {
                    println!("\n ------ There is no need to use the hybrid_att feature! ----- \n");
                } else {
                    return Err(LanguageError::from_span(
                        &m.model_span,
                        "There is no need to use the hybrid_att feature!",
                        line!(),
                        file!(),
                    ));
                }
            }
        }

        Ok((new_m, latex))
    }

    fn from_string_multiple(
        model_string: &String,
        allow_dynamic_structures: bool,
        constant_overwrites: &Vec<String>,
        filename: String,
        errors_only: bool,
    ) -> Result<(Model, String), Vec<LanguageError>> {
        let mut errors = HashSet::new();
        let mut modif_string = model_string.clone() + "\n";

        let mut max_error = 2;

        loop {
            max_error -= 1;
            if max_error == 0 {
                println!("Reached a maximum of {} errors!", max_error - 1);
                return Err(errors.into_iter().collect());
            }
            let res = Model::from_string_internal(
                &modif_string,
                allow_dynamic_structures,
                constant_overwrites,
                filename.clone(),
                errors_only,
            );
            match res {
                Ok(x) => {
                    if errors.is_empty() {
                        return Ok(x);
                    } else {
                        return Err(errors.into_iter().collect());
                    }
                }
                Err(e) => {
                    let old_size = errors.len();
                    errors.insert(e.clone());
                    //println!("Found new error: {}",errors.last().unwrap().what);
                    if 1 + old_size != errors.len() {
                        println!("could not recoverer errors");
                        return Err(errors.into_iter().collect());
                    }

                    let first_error_line = match e.lstart {
                        0 => 1,
                        x => x,
                    };
                    let first_error_column = match e.cstart {
                        0 => 1,
                        x => x,
                    };

                    assert!(first_error_line > 0);
                    //println!("the line({}): {}",first_error_line,modif_string.split("\n").skip(first_error_line-1).next().unwrap());
                    let number_of_statements_before = modif_string
                        .split("\n")
                        .take(first_error_line - 1)
                        .join(" ")
                        .matches(";")
                        .count()
                        + match modif_string.split("\n").skip(first_error_line - 1).next() {
                            None => 0,
                            Some(x) => {
                                if x.len() < first_error_column {
                                    0
                                } else {
                                    x[..(first_error_column - 1)].matches(";").count()
                                }
                            }
                        };

                    /*println!(
                        "Crit Statement: {}\nCritend",
                        modif_string
                            .split(";")
                            .skip(number_of_statements_before)
                            .next()
                            .unwrap()
                    );
                    println!(
                        "All Statement: {}\nAllend",
                        modif_string.split(";").clone().join(";---")
                    );
                    println!("B4 Critical is : {}\n", number_of_statements_before);*/
                    modif_string = modif_string
                        .split(";")
                        .take(number_of_statements_before)
                        .join(";")
                        + ";"
                        + &*"\n".repeat(
                            modif_string
                                .split(";")
                                .skip(number_of_statements_before)
                                .next()
                                .unwrap()
                                .matches("\n")
                                .count(),
                        )
                        + &*modif_string
                            .split(";")
                            .skip(number_of_statements_before + 1)
                            .join(";");

                    //println!("{}", modif_string.replace("\n", "\n##"));
                }
            }
        }
    }

    pub fn from_file(
        filename: &str,
        allow_dynamic_structures: bool,
        constant_overwrites: &Vec<String>,
    ) -> Result<(Model, String), Vec<LanguageError>> {
        let model_string = fs::read_to_string(&filename).expect(&*format!(
            "Something went wrong reading the file '{}'. Does it exist?",
            filename
        ));

        Model::from_string(
            &model_string,
            allow_dynamic_structures,
            constant_overwrites,
            filename.to_string(),
            false,
        )
    }
}

struct AttributeAssignments {
    expanded: Vec<(OptimizedString, RateExpr)>,
    not_expanded: Vec<(OptimizedString, RateExpr)>,
}

fn get_attribute_assignments(
    species: &SpeciesRightStx,
    all_species: &HashMap<SpeciesName, Species>,
    left_match: &Vec<SpeciesLeft>,
    all_constants: &HashMap<String, ConstantDefinition>,
    available_functions: &HashMap<String, FunctionDefinition>,
    species_name_conversion: &SpeciesNameConverter,
    att_name_conversion: &HashMap<String, OptimizedString>,
) -> Result<AttributeAssignments, LanguageError> {
    let species_name = match species_name_conversion.try_get(&species.species_name) {
        None => {
            return Err(LanguageError::from_span(
                &species.span,
                "species name not found",
                line!(),
                file!(),
            ))
        }
        Some(k) => k,
    };
    let names: Vec<_> = species
        .attribute_assignments
        .iter()
        .map(|(n, _a)| n)
        .collect();
    let names_set: HashSet<_> = names.iter().collect();
    if names_set.len() != names.len() {
        return Err(LanguageError::from_span(
            &species.span,
            "An attribute is defined multiple times here",
            line!(),
            file!(),
        ));
    }

    let checked_assignments: Vec<_> = species
        .attribute_assignments
        .iter()
        .map(|val| {
            let (name_orig, assign) = val;
            let name = match att_name_conversion.get(name_orig) {
                None => {
                    return Err(LanguageError::from_span(
                        &species.span,
                        &*format!(
                            "The name '{}' is not an attribute for species '{}'",
                            name_orig, species.species_name
                        ),
                        line!(),
                        file!(),
                    ));
                }
                Some(k) => k,
            };
            Ok((name, assign))
        })
        .collect::<Result<_, _>>()?;
    Ok(AttributeAssignments {
        expanded: checked_assignments
            .iter()
            .filter(|(name, _)| {
                all_species
                    .get(&species_name)
                    .unwrap()
                    .expanding_attributes
                    .contains_key(*name)
            })
            .map(|(name, assign)| {
                let (val, dim) = RateExpr::from_tree(
                    (*assign).clone(),
                    &SemanticContext {
                        left_match,
                        all_species,
                        all_constants,
                        single_reference_species: None,
                        available_functions: available_functions,
                        current_function_context: HashMap::new(),
                        allow_starr_attributes: false, // important here
                        allow_time: false,
                        allow_random: true,
                        species_name_conversion:&species_name_conversion,
                        att_name_conversion,
                    },
                )?;
                let to_dim = &all_species
                    .get(&species_name)
                    .unwrap()
                    .expanding_attributes
                    .get(*name)
                    .unwrap().dim;
                if !Dimension::assignable_enum(to_dim,&dim,&val,&att_name_conversion) && !Dimension::assignable(to_dim
                    ,
                    &dim,
                ) {

                    return Err(LanguageError::from_span(
                        &assign.span,
                        &*format!(
                            //Todo standarize unit error
                            "Unit or type missmatch ((is) {} != (should){})",
                            dim,
                            all_species
                                .get(&species_name)
                                .unwrap()
                                .expanding_attributes
                                .get(*name)
                                .unwrap().dim
                        ),
                        line!(),
                        file!(),
                    ));
                }

                if dim.non_numerical == NonNumericalDimension::Nothing {
                    match val.evaluate_partial_for_left(&AllOrSingleLeft::None, None, None,false){
                        PartialEvaluationExpr::Primitive(_) => {}
                        PartialEvaluationExpr::Incomplete(s) => {
                            if !s.can_be_evaluated_during_expansion() {
                                return Err(LanguageError::from_span(
                                    &assign.span,
                                    &*format!(
                                        "Assigning to a float is only possible for 'immidiate' values. That means you can not have a mathematical expression ('{:?}'). You have to use * attributes instead.", s,
                                    ),
                                    line!(),
                                    file!(),
                                ));
                            }
                        }
                    };

                }
                Ok(((*name).clone(), val))
            })
            .collect::<Result<_, _>>()?,
        not_expanded: checked_assignments
            .iter()
            .filter(|(name, _)| {
                all_species
                    .get(&species_name)
                    .unwrap()
                    .not_expanding_attributes
                    .contains_key(*name)
            })
            .map(|(name, assign)| {
                let (val, dim) = RateExpr::from_tree(
                    (*assign).clone(),
                    &SemanticContext {
                        left_match,
                        all_species,
                        all_constants,
                        single_reference_species: None,
                        available_functions,
                        current_function_context: HashMap::new(),
                        allow_starr_attributes: true,
                        allow_time: false,
                        allow_random: true,
                        species_name_conversion:&species_name_conversion,

                        att_name_conversion,
                    },
                )?;
                if !Dimension::assignable(
                    &all_species
                        .get(&species_name)
                        .unwrap()
                        .not_expanding_attributes
                        .get(*name)
                        .unwrap().dim,
                    &dim,
                ) {
                    return Err(LanguageError::from_span(
                        &assign.span,
                        &*format!(
                            //Todo standarize unit error
                            "Unit or type missmatch ((is) {} != (should){})",
                            dim,
                            *all_species
                                .get(&species_name)
                                .unwrap()
                                .not_expanding_attributes
                                .get(*name)
                                .unwrap()
                        ),
                        line!(),
                        file!(),
                    ));
                }
                Ok(((*name).clone(), val))
            })
            .collect::<Result<_, _>>()?,
    })
}

fn rekursive_right_side2(
    parent_number: isize,
    solution: Option<SpeciesRightSolution>,
    all_species: &HashMap<SpeciesName, Species>,
    left_match: &Vec<SpeciesLeft>,
    all_constants: &HashMap<String, ConstantDefinition>,
    allow_rest_solution: bool,
    available_functions: HashMap<String, FunctionDefinition<'_>>,
    species_name_conversion: &SpeciesNameConverter,
    att_name_conversion: &HashMap<String, OptimizedString>,
    allow_dynamic: bool,
    errors_only: bool,
) -> Result<(Vec<SpeciesRight>, Vec<RestSolutionRight>), LanguageError> {
    let mut res: Vec<SpeciesRight> = vec![];
    let mut res_rest: Vec<RestSolutionRight> = vec![];
    match solution {
        None => {}
        Some(solution) => {
            for species in solution.species_in_solution {
                if species.is_rest_solution {
                    if !allow_rest_solution {
                        return Err(LanguageError::from_span(
                            &species.span,
                            "Rest solutions are not allowed here",
                            line!(),
                            file!(),
                        ));
                    }
                    res_rest.push(RestSolutionRight {
                        name: species.species_name,
                        compartment_constraint: parent_number,
                    });
                } else {
                    if species_name_conversion
                        .try_get(&species.species_name)
                        .is_none()
                        || !all_species
                            .contains_key(&species_name_conversion.get(&species.species_name))
                    {
                        return Err(LanguageError::from_span(
                            &species.span,
                            &*format!(
                                "The name '{}' is not a declared name for a species",
                                species.species_name
                            ),
                            line!(),
                            file!(),
                        ));
                    }

                    let (amt_expr, dim) = RateExpr::from_tree(
                        species.amt.clone(),
                        &SemanticContext {
                            left_match: &left_match,
                            all_species: all_species,
                            all_constants,
                            single_reference_species: None,
                            available_functions: &available_functions,
                            current_function_context: HashMap::new(),
                            allow_starr_attributes: true,
                            allow_time: false,
                            allow_random: true,
                            species_name_conversion,
                            att_name_conversion,
                        },
                    )?;
                    // println!("--> {:?}, {:?}", amt_expr, dim);

                    if dim != Dimension::dimonsionless_integer() {
                        return Err(LanguageError::from_span(
                            &species.span,
                            &*format!("Expected an integer/dimensionless number as amount!\n",),
                            line!(),
                            file!(),
                        ));
                    }
                    let mut unfolded_adds = 1;
                    let amount: CreationAmt = match amt_expr.evaluate_partial_for_left(
                        &AllOrSingleLeft::None,
                        None,
                        None,
                        false,
                    ) {
                        PartialEvaluationExpr::Primitive(k) => {
                            let num: i64 = match k {
                                PrimitiveTypes::String(_) => unreachable!("Is string"),
                                PrimitiveTypes::Boolean(_) => unreachable!("is bool"),
                                PrimitiveTypes::Float(_f) => {
                                    return Err(LanguageError::from_span(
                                        &species.span,
                                        "Non integer amt",
                                        line!(),
                                        file!(),
                                    ));
                                }
                                PrimitiveTypes::Integer(y) => y,
                            };
                            if num < 0 {
                                return Err(LanguageError::from_span(
                                    &species.span,
                                    "A negative or zero amt is not allowed!",
                                    line!(),
                                    file!(),
                                ));
                            }
                            if all_species
                                .get(&species_name_conversion.get(&species.species_name))
                                .unwrap()
                                .is_compartment
                            {
                                unfolded_adds = num;
                                CreationAmt::StaticNum(1)
                            } else {
                                unfolded_adds = 1;
                                CreationAmt::StaticNum(match usize::try_from(num) {
                                    Ok(i) => i,
                                    Err(_f) => {
                                        return Err(LanguageError::from_span(
                                            &species.span,
                                            "Amt to large",
                                            line!(),
                                            file!(),
                                        ));
                                    }
                                })
                            }
                        }
                        PartialEvaluationExpr::Incomplete(r) => {
                            /*if !allow_dynamic {
                                return Err(LanguageError::from_span(&species.span,"The amount could not be evaluated statically. Amounts can not be assigned dynamically here. This includes using attributes or counts",line!(),file!()));
                            }*/
                            CreationAmt::DynExpr(r)
                        }
                    };
                    let assignmentns = get_attribute_assignments(
                        &species,
                        all_species,
                        left_match,
                        all_constants,
                        &available_functions,
                        &species_name_conversion,
                        att_name_conversion,
                    )?;
                    let starr_names: BTreeSet<_> = assignmentns
                        .not_expanded
                        .iter()
                        .map(|(a, _)| a.clone())
                        .collect();
                    if starr_names.len() < assignmentns.not_expanded.len() {
                        return Err(LanguageError::from_span(
                            &species.span,
                            "Duplicate attribute assignment",
                            line!(),
                            file!(),
                        ));
                    }
                    let new_spec = SpeciesRight {
                        species_name: species_name_conversion
                            .get(&species.species_name.to_string()),
                        extra_name: species.extra_name.clone(),
                        compartment_constraint: match parent_number {
                            -1 => None,
                            x => {
                                assert!(x >= 0);
                                Some(x as usize)
                            }
                        },
                        corresponding_on_left: None,
                        amount: amount.clone(),
                        expanded_attribute_assignments: assignmentns.expanded,
                        star_attribute_assignment: assignmentns.not_expanded,
                    };
                    if errors_only {
                        unfolded_adds = min(unfolded_adds, 2);
                    }
                    if unfolded_adds as i128 > isize::MAX as i128 {
                        return Err(LanguageError::from_span(
                            &species.span,
                            "Amount to large for type",
                            line!(),
                            file!(),
                        ));
                    }
                    for _ in 0..unfolded_adds {
                        let my_number = parent_number + res.len() as isize + 1;
                        res.push(new_spec.clone());
                        let sub_res = rekursive_right_side2(
                            my_number,
                            species.child_solution.clone(),
                            all_species,
                            left_match,
                            all_constants,
                            allow_rest_solution,
                            available_functions.clone(),
                            species_name_conversion,
                            att_name_conversion,
                            allow_dynamic,
                            errors_only,
                        )?;
                        res.extend(sub_res.0.into_iter());
                        res_rest.extend(sub_res.1.into_iter());
                    }
                }
            }
        }
    }
    Ok((res, res_rest))
}

impl ExprStx<'_> {
    fn check_and_convert_on_left_side(
        &self,
        spec: &Species,
        spec_left_id: SpeciesLeftIdentifier,
        all_constants: &HashMap<String, ConstantDefinition>,
        available_functions: &HashMap<String, FunctionDefinition<'_>>,
        species_name_conversion: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> Result<RateExpr, LanguageError> {
        let (l, ldim) = RateExpr::from_tree(
            self.clone(),
            &SemanticContext {
                left_match: &vec![],
                all_species: &Default::default(),
                all_constants,
                single_reference_species: Some((spec, spec_left_id.clone())),
                available_functions: available_functions,
                current_function_context: HashMap::new(),
                allow_starr_attributes: true,
                allow_time: false,
                allow_random: false,
                species_name_conversion,
                att_name_conversion: att_name_conversion,
            },
        )?;
        if !(ldim == Dimension::non_numeric(NonNumericalDimension::Bool)) {
            return Err(LanguageError::from_span(
                &self.span,
                &*format!(
                    "Not a boolean expression. Is '{:?}'\nInner:{:?}",
                    ldim, self
                ),
                line!(),
                file!(),
            ));
        }
        Ok(l)
    }
    fn check_and_convert_in_guard(
        &self,
        context: &SemanticContext, /*spec_left: &Vec<SpeciesLeft>,
                                   all_species: &HashMap<String, Species>,
                                   all_constants: &HashMap<String, ConstantDefinition>,*/
    ) -> Result<RateExpr, LanguageError> {
        let (l, ldim) = RateExpr::from_tree(self.clone(), context)?;
        if !(ldim == Dimension::non_numeric(NonNumericalDimension::Bool)) {
            return Err(LanguageError::from_span(
                &self.span,
                &*format!("Not a boolean expression. Is '{:?}'", ldim),
                line!(),
                file!(),
            ));
        }
        Ok(l)
    }
}

/*impl LogicalConstraintStx<'_> {
    fn check_and_convert_on_left_side(
        &self,
        spec: &Species,
        spec_left_id: SpeciesLeftIdentifier,
        all_constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<LogicalConstraint, LanguageError> {
        let (l, ldim) = RateExpr::from_tree(
            self.left.clone(),
            &SemanticContext {
                left_match: &vec![],
                all_species: &Default::default(),
                all_constants,
                single_reference_species: Some((spec, spec_left_id.clone())),
                available_functions: &Default::default(),
                current_function_context: HashMap::new(),
                allow_starr_attributes: true,
                allow_time: false,
            },
        )?;
        let (r, rdim) = RateExpr::from_tree(
            self.right.clone(),
            &SemanticContext {
                left_match: &vec![],
                all_species: &Default::default(),
                all_constants,
                single_reference_species: Some((spec, spec_left_id)),
                available_functions: &Default::default(),
                current_function_context: HashMap::new(),
                allow_starr_attributes: false,
                allow_time: false,
            },
        )?;
        if !self.op.dim_ok(&ldim, &rdim) {
            return Err(LanguageError::from_span(
                &self.span,
                &*format!(
                    "Dimensions don't match for logical comparison {}!={}",
                    ldim, rdim
                ),
                line!(),
                file!(),
            ));
        }
        Ok(LogicalConstraint {
            left: l,
            op: self.op.clone(),
            right: r,
        })
    }
    fn check_and_convert_in_guard(
        &self,
        spec_left: &Vec<SpeciesLeft>,
        all_species: &HashMap<String, Species>,
        all_constants: &HashMap<String, ConstantDefinition>,
    ) -> Result<LogicalConstraint, LanguageError> {
        let (l, ldim) = RateExpr::from_tree(
            self.left.clone(),
            &SemanticContext {
                left_match: spec_left,
                all_species,
                all_constants,
                single_reference_species: None,
                available_functions: &Default::default(),
                current_function_context: HashMap::new(),
                allow_starr_attributes: true,
                allow_time: true,
            },
        )?;
        let (r, rdim) = RateExpr::from_tree(
            self.right.clone(),
            &SemanticContext {
                left_match: spec_left,
                all_species,
                all_constants,
                single_reference_species: None,
                available_functions: &Default::default(),
                current_function_context: HashMap::new(),
                allow_starr_attributes: true,
                allow_time: true,
            },
        )?;
        if !self.op.dim_ok(&ldim, &rdim) {
            return Err(LanguageError::from_span(
                &self.span,
                &*format!(
                    "Dimensions don't match for logical comparison {}!={}",
                    ldim, rdim
                ),
                line!(),
                file!(),
            ));
        }
        Ok(LogicalConstraint {
            left: l,
            op: self.op.clone(),
            right: r,
        })
    }
}*/

fn rekursive_left_side2(
    parent_id: Option<SpeciesLeftIdentifier>,
    solution: Option<SpeciesLeftSolution>,
    all_species: &HashMap<SpeciesName, Species>,
    all_constants: &HashMap<String, ConstantDefinition>,
    available_functions: &HashMap<String, FunctionDefinition<'_>>,
    species_name_conversion: &SpeciesNameConverter,
    att_name_conversion: &HashMap<String, OptimizedString>,
    species_left_seen_so_far: &Vec<SpeciesLeft>,
    left_name_contuer: &mut usize,
) -> Result<(Vec<SpeciesLeft>, Vec<RestSolutionLeft>), LanguageError> {
    let mut res: Vec<SpeciesLeft> = vec![];
    let mut res_rest: Vec<RestSolutionLeft> = vec![];
    match solution {
        None => {}
        Some(solution) => {
            for species_stx in solution.species_in_solution {
                if species_stx.is_rest_solution {
                    if parent_id.is_none() {
                        return Err(LanguageError::from_span(
                            &species_stx.span,
                            "Rest solutions are not allowed here (top hirachy level on left side)",
                            line!(),
                            file!(),
                        ));
                    }
                    res_rest.push(RestSolutionLeft {
                        name: species_stx.species_name,
                        parent_id: parent_id.clone().unwrap(),
                        amount_expr: match species_stx.rest_solution_amount {
                            None => None,
                            Some(x) => {
                                let (val, dim) = RateExpr::from_tree(
                                    *x,
                                    &SemanticContext {
                                        left_match: &species_left_seen_so_far
                                            .iter()
                                            .chain(res.iter())
                                            .cloned()
                                            .collect(),
                                        all_species,
                                        all_constants,
                                        single_reference_species: None,
                                        available_functions: &available_functions,
                                        current_function_context: Default::default(),
                                        allow_starr_attributes: true,
                                        allow_time: false,
                                        allow_random: true,
                                        species_name_conversion,
                                        att_name_conversion: att_name_conversion,
                                    },
                                )?;
                                let mut dim2 = Dimension::dimensionless();
                                dim2.non_numerical = NonNumericalDimension::Integer;
                                if dim != Dimension::dimensionless() && dim != dim2 {
                                    return Err(LanguageError::from_span(
                                        &species_stx.span,
                                        &format!(
                                            "Expected dimensionless number in {{ }}!\nHave{} ",
                                            dim
                                        ),
                                        line!(),
                                        file!(),
                                    ));
                                }
                                Some(val)
                            }
                        },
                    });
                } else {
                    //let my_number = parent_number + res.len() as isize + 1;
                    if species_name_conversion
                        .try_get(&species_stx.species_name)
                        .is_none()
                        || !all_species
                            .contains_key(&species_name_conversion.get(&species_stx.species_name))
                    {
                        return Err(LanguageError::from_span(
                            &species_stx.span,
                            &*format!(
                                "The name '{}' is not a declared name for a species",
                                species_stx.species_name
                            ),
                            line!(),
                            file!(),
                        ));
                    }
                    let my_id = match &species_stx.extra_name {
                        None => {
                            *left_name_contuer += 1;
                            SpeciesLeftIdentifier::new_from_int(usize::MAX - *left_name_contuer)
                        }
                        Some(n) => SpeciesLeftIdentifier::new_from_optim(&att_name_conversion[n]),
                    };

                    let species_name =
                        species_name_conversion.get(&species_stx.species_name.to_string());
                    res.push(SpeciesLeft {
                        //extra_name: species_stx.extra_name.clone(),
                        //number_in_match: 0, // TODO
                        id: my_id.clone(),
                        parent_number: parent_id.clone(),
                        attribute_constraint: species_stx
                            .attribute_constraints
                            .iter()
                            .map(|v| {
                                v.check_and_convert_on_left_side(
                                    all_species.get(&species_name).unwrap(),
                                    my_id.clone(),
                                    all_constants,
                                    available_functions,
                                    &species_name_conversion,
                                    &att_name_conversion,
                                )
                            })
                            .collect::<Result<_, _>>()?,
                        species_name,
                    });
                    let r = rekursive_left_side2(
                        Some(my_id.clone()),
                        species_stx.child_solution,
                        all_species,
                        all_constants,
                        available_functions,
                        &species_name_conversion,
                        att_name_conversion,
                        &res,
                        left_name_contuer,
                    )?;
                    res.extend(r.0.into_iter());
                    res_rest.extend(r.1.into_iter());
                }
            }
        }
    };
    Ok((res, res_rest))
}

#[derive(Clone)]
pub enum FunctionDefinition<'a> {
    NewFunction(FunctionDefinitionStx<'a>),
    BuildIn(crate::buildin_functions::BuildinFunction),
}

impl FunctionDefinition<'_> {
    pub fn num_params(&self) -> usize {
        match &self {
            FunctionDefinition::NewFunction(x) => x.params.len(),
            FunctionDefinition::BuildIn(x) => x.input_output_types().0.len(),
        }
    }
}

#[derive(Clone)]
struct SemanticContext<'a> {
    left_match: &'a Vec<SpeciesLeft>,
    all_species: &'a HashMap<SpeciesName, Species>,
    all_constants: &'a HashMap<String, ConstantDefinition<'a>>,
    single_reference_species: Option<(&'a Species, SpeciesLeftIdentifier)>,
    available_functions: &'a HashMap<String, FunctionDefinition<'a>>,
    current_function_context: HashMap<String, ExprStx<'a>>,
    allow_starr_attributes: bool,
    allow_time: bool,
    allow_random: bool,
    species_name_conversion: &'a SpeciesNameConverter,
    att_name_conversion: &'a HashMap<String, OptimizedString>,
}

impl<'a> SemanticContext<'a> {
    fn find_left_id_from_name(
        &self,
        looking_for: &String,
        span: &Span,
    ) -> Result<SpeciesLeftIdentifier, LanguageError> {
        let looking_for_conv = self.species_name_conversion.try_get(looking_for);
        match self.left_match.iter().find(|v| {
            (looking_for_conv.is_some()
                && (v.species_name == *looking_for_conv.as_ref().unwrap())
                && self
                    .left_match
                    .iter()
                    .filter(|v| v.species_name == *looking_for_conv.as_ref().unwrap())
                    .count()
                    == 1)
                || (v.id
                    == SpeciesLeftIdentifier::new_from_optim(
                        &self.att_name_conversion[looking_for],
                    ))
        }) {
            Some(n) => Ok(n.id.clone()),
            None => Err(LanguageError::from_span(
                &span,
                &*format!(
                    "species name '{}' not found uniquely on left side ('{:?}')",
                    looking_for, self.left_match
                ),
                line!(),
                file!(),
            )),
        }
    }
}

pub fn string_to_seconds(str: &str) -> Option<f64> {
    let term = ExprStx::from_string(str)?; //.expect("could not parse {str} (syntax)");
    let (expr, dim) = match RateExpr::from_tree(
        term,
        &SemanticContext {
            left_match: &vec![],
            all_species: &Default::default(),
            all_constants: &Default::default(),
            single_reference_species: None,
            available_functions: &Default::default(),
            current_function_context: Default::default(),
            allow_starr_attributes: false,
            allow_time: false,
            allow_random: false,
            species_name_conversion: &SpeciesNameConverter::new(&vec![]),
            att_name_conversion: &Default::default(),
        },
    ) {
        Ok(x) => x,
        Err(_) => return None,
    };
    //.expect("could not parse {str} (semantic)");
    let mut dim_t = Dimension::dimensionless();
    dim_t.time = 1;
    //Dimension::assignable
    if !Dimension::assignable(&dim_t, &dim) {
        //println!("{:?} vs {:?}", dim, dim_t);
        //panic!("You have not provided a time as endtime! Add a unit like 's' or 'h'")
        return None;
    }
    //panic!("{:?}", expr);
    match expr {
        RateExpr::Single(k) => match k {
            RateExprTerm::Primitive(k) => Some(k.as_float().unwrap()),
            _ => unreachable!("wrong type 1"),
        },
        RateExpr::Double(_, _, _) => {
            panic!("double")
        }
    }
}

impl RateExprTerm {
    fn from_tree(
        tree: ExprTermStx,
        context: &SemanticContext<'_>,
        span: &pest::Span,
    ) -> Result<(RateExprTerm, Dimension), LanguageError> {
        return match tree {
            ExprTermStx::NumUnit(x, _) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::Float(x.value)),
                x.unit,
            )),
            ExprTermStx::NamedVariable(x) => {
                let att_name = context
                    .att_name_conversion
                    .get(&x)
                    .unwrap_or(&OptimizedString::dummy())
                    .clone();
                if context.single_reference_species.is_some() {
                    match context
                        .single_reference_species
                        .as_ref()
                        .unwrap()
                        .0
                        .expanding_attributes
                        .get(&att_name)
                    {
                        Some(at) => {
                            return Ok((
                                RateExprTerm::ExpandedAttRef(AttributeRef {
                                    species_id: match &context.single_reference_species {
                                        None => {
                                            unreachable!("None 1")
                                        }
                                        Some((_, left_id)) => left_id.clone(),
                                    },
                                    /*assuming only one on left, when performing calculation*/
                                    att_name: att_name.clone().clone(),
                                }),
                                at.clone().dim,
                            ));
                        }

                        None => (),
                    }
                    match context
                        .single_reference_species
                        .as_ref()
                        .unwrap()
                        .0
                        .not_expanding_attributes
                        .get(&att_name)
                    {
                        Some(at) => {
                            if context.allow_starr_attributes {
                                return Ok((
                                    RateExprTerm::StarAttRef(AttributeRef {
                                        species_id: match &context.single_reference_species {
                                            None => {
                                                unreachable!("None 2")
                                            }
                                            Some((_, left_id)) => left_id.clone(),
                                        },
                                        /*assuming only one on left, when performing calculation*/
                                        att_name: att_name.clone().clone(),
                                    }),
                                    at.clone().dim,
                                ));
                            } else {
                                panic!("else");
                                //return Err(LanguageError::from_span(span,format!("The star attribute '{}' can not be used here. A constant value is needed!",x).as_str(),line!(),file!()));
                            }
                        }

                        None => (),
                    }
                }

                match context.all_constants.get(&x) {
                    Some(cd) => {
                        let mut ctx2 = context.clone();
                        ctx2.allow_time = false;
                        let (expr, dim) = RateExpr::from_tree(cd.clone().value, &ctx2)?;
                        return Ok((RateExprTerm::Expr(Box::new(expr)), dim));
                    }
                    None => {}
                }
                match context.current_function_context.get(&x) {
                    None => {}
                    Some(param_expr) => {
                        let (expr, dim) = RateExpr::from_tree(param_expr.clone(), context)?;
                        return Ok((RateExprTerm::Expr(Box::new(expr)), dim));
                    }
                }
                Err(LanguageError::from_span(
                    &span,
                    &*format!("Constant/attribute/Value '{}' not defined.\nIf this has occured in an attribute assignment, maybe try giving the species name\n  (-> use <name>.{} instead of {}). Known attributes are {:?}", x,x,x,context.att_name_conversion.keys().collect::<Vec<_>>()),
                    line!(),
                    file!(),
                ))
            }

            ExprTermStx::DottedAttributeRef(att_ref) => {
                let att_name = context
                    .att_name_conversion
                    .get(&*att_ref.att_name)
                    .unwrap_or(&OptimizedString::dummy())
                    .clone();
                if context.single_reference_species.is_some() {
                    return Err(LanguageError::from_span(
                        &span,"Global attribute references are not allowed for left side Attribute constraints",line!(),file!()));
                }
                let idx_on_left = context.find_left_id_from_name(&att_ref.spec_name, span)?;
                let looking_for_spec_name = &context
                    .left_match
                    .iter()
                    .find(|v| v.id == idx_on_left)
                    .unwrap()
                    .species_name;
                match context
                    .all_species
                    .get(looking_for_spec_name)
                    .expect(&*format!(
                        "Can not find '{:?}' in '{:?}'",
                        looking_for_spec_name, context.all_species
                    ))
                    .expanding_attributes
                    .get(&att_name)
                {
                    Some(x) => Ok((
                        RateExprTerm::ExpandedAttRef(AttributeRef {
                            species_id: idx_on_left.clone(),
                            att_name: att_name.clone(),
                        }),
                        (*x).clone().dim,
                    )),
                    None => match context
                        .all_species
                        .get(
                            &context
                                .left_match
                                .iter()
                                .find(|v| v.id == idx_on_left)
                                .unwrap()
                                .species_name,
                        )
                        .unwrap()
                        .not_expanding_attributes
                        .get(&att_name)
                    {
                        Some(x) => {
                            if context.allow_starr_attributes {
                                Ok((
                                    RateExprTerm::StarAttRef(AttributeRef {
                                        species_id: idx_on_left.clone(),
                                        att_name: att_name.clone(),
                                    }),
                                    (*x).clone().dim,
                                ))
                            } else {
                                return Err(LanguageError::from_span(span,format!("The star attribute '{}' in '{}' can not be used here. A constant (or no star) value is needed!",att_ref.att_name, att_ref.spec_name).as_str(),line!(),file!()));
                            }
                        }
                        None => Err(LanguageError::from_span(
                            &span,
                            &*format!(
                                "The name '{}' is not an attribute for species '{}'",
                                att_ref.att_name, att_ref.spec_name
                            ),
                            line!(),
                            file!(),
                        )),
                    },
                }
            }
            ExprTermStx::Expr(exr) => {
                let (bx, dim) = RateExpr::from_tree(*exr, context)?;
                Ok((RateExprTerm::Expr(Box::new(bx)), dim))
            }
            ExprTermStx::LeftSpeciesCount(count_name) => Ok((
                RateExprTerm::SpecCount(SpeciesCount {
                    idx_on_left: context.find_left_id_from_name(&count_name, span)?,
                }),
                Dimension::dimonsionless_integer(),
            )),
            ExprTermStx::ActualString(x) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::String(
                    context.att_name_conversion[&x].clone(),
                )),
                Dimension::non_numeric(NonNumericalDimension::String),
            )),
            ExprTermStx::ActualBool(x) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::Boolean(x)),
                Dimension::non_numeric(NonNumericalDimension::Bool),
            )),
            ExprTermStx::ActualInt(x) => Ok((
                RateExprTerm::Primitive(PrimitiveTypes::Integer(x.try_into().unwrap())),
                Dimension::non_numeric(NonNumericalDimension::Integer),
            )),
            ExprTermStx::ComplexSpeciesCount(x) => Ok((
                RateExprTerm::GlobalCount(GlobalCountEnum::GenericUnspecified({
                    GenericGlobalCount::from_stx(&x, context.clone())?
                })),
                Dimension::non_numeric(NonNumericalDimension::Integer),
            )),
            ExprTermStx::FunctionCall(func) => {
                let f = match context.available_functions.get(&func.name.to_lowercase()) {
                    None => Err(LanguageError::from_span(
                        &func.span,
                        &format!(
                            "Unknown function name. Known are {:?})",
                            context.available_functions.keys()
                        ),
                        line!(),
                        file!(),
                    )),
                    Some(x) => Ok(x),
                }?;
                if f.num_params() != func.params.len() {
                    return Err(LanguageError::from_span(
                        &func.span,
                        "Wrong number of arguments!",
                        line!(),
                        file!(),
                    ));
                }
                match f {
                    FunctionDefinition::NewFunction(f) => {
                        let mut ctx2 = context.clone();
                        ctx2.current_function_context = f
                            .params
                            .iter()
                            .zip(func.params.iter())
                            .map(|(a, b)| (a.clone(), b.clone()))
                            .collect();
                        let (bx, dim) = RateExpr::from_tree(f.expr.clone(), &ctx2)?;
                        Ok((RateExprTerm::Expr(Box::new(bx)), dim))
                    }
                    FunctionDefinition::BuildIn(bf) => {
                        if bf.is_random() && !context.allow_random {
                            return Err(LanguageError::from_span(
                                &func.span,
                                "Random not possible here",
                                line!(),
                                file!(),
                            ));
                        }
                        let params: Result<_, _> = func
                            .params
                            .iter()
                            .zip(bf.input_output_types().0.iter())
                            .map(|(is, should_dim)| {
                                let (is_expr, is_dim) = RateExpr::from_tree(is.clone(), context)?;
                                if !Dimension::assignable(&should_dim, &is_dim) {
                                    return Err(LanguageError::from_span(
                                        &func.span,
                                        "Wrong parameter type!",
                                        line!(),
                                        file!(),
                                    ));
                                }
                                Ok(is_expr)
                            })
                            .collect();
                        Ok((
                            RateExprTerm::BuildinFunction(BuildinFunctionCall {
                                function: bf.clone(),
                                params: params?,
                            }),
                            bf.input_output_types().1,
                        ))
                    }
                }
            }
            ExprTermStx::Condition(k) => {
                let (guard, guard_dim) = RateExpr::from_tree(*k.condition, context)?;
                if guard_dim != Dimension::non_numeric(NonNumericalDimension::Bool) {
                    return Err(LanguageError::from_span(
                        &k.span,
                        "Not a boolean expression!",
                        line!(),
                        file!(),
                    ));
                }
                let (result_true, rt_dim) = RateExpr::from_tree(*k.true_branch, context)?;
                let result_false = match k.false_branch {
                    None => None,
                    Some(rf) => {
                        let (result_false, rf_dim) = RateExpr::from_tree(*rf, context)?;
                        if rf_dim != rt_dim {
                            return Err(LanguageError::from_span(
                                &k.span,
                                "Dimension of true and false branch don't match",
                                line!(),
                                file!(),
                            ));
                        }
                        Some(result_false)
                    }
                };
                Ok((
                    RateExprTerm::Condition(Box::new(Condition {
                        guard,
                        result_true,
                        result_false,
                    })),
                    rt_dim,
                ))
            }
            ExprTermStx::Immidiate => {
                let mut d = Dimension::dimensionless();
                d.time = -1;
                Ok((RateExprTerm::Primitive(PrimitiveTypes::Float(f64::MAX)), d))
            }
        };
    }
}

impl RateExpr {
    fn from_tree(
        tree: ExprStx,
        context: &SemanticContext<'_>,
    ) -> Result<(RateExpr, Dimension), LanguageError> {
        let (l, ldim) = RateExprTerm::from_tree(tree.left, &context, &tree.span)?;

        match tree.op {
            None => Ok((RateExpr::Single(l), ldim)),
            Some((op, term)) => {
                let (r, rdim) = RateExprTerm::from_tree(term, &context, &tree.span)?;

                let new_dim =
                    match op.combine_dimension(&ldim, &rdim, &l, &r, context.att_name_conversion) {
                        None => Err(LanguageError::from_span(
                            &tree.span,
                            &*format!("Unit Missmatch {} != {}", ldim, rdim),
                            line!(),
                            file!(),
                        )),
                        Some(x) => Ok(x),
                    }?;

                Ok((RateExpr::Double(l, op, r), new_dim))
            }
        }
    }
}

impl structures::Transition {
    fn has_structural_changes(
        &self,
        r_on_left: &Vec<RestSolutionLeft>,
        r_on_right: &Vec<RestSolutionRight>,
        all_species: &HashMap<SpeciesName, Species>,
        allow_dynamic_structures: bool,
        species_name_conversion: &SpeciesNameConverter,
    ) -> Result<bool, String> {
        let mut found_any_structural_changes = self.validate_rest_solution(
            r_on_left,
            r_on_right,
            all_species,
            allow_dynamic_structures,
            species_name_conversion,
        )?;
        for r in self.right.iter() {
            if r.amount.as_num().is_none()
                && all_species.get(&r.species_name).unwrap().is_compartment
            {
                if !allow_dynamic_structures {
                    return Err(format!(
                        "Dynamic amount on right side. This expression can not be evaluated ahead of time and needs to be executed in dynamic mode"
                    ));
                }
                found_any_structural_changes = true;
            }
            for (s, att) in r.expanded_attribute_assignments.iter() {
                if !att.can_be_evaluated_during_expansion() {
                    found_any_structural_changes = true;
                    if !allow_dynamic_structures {
                        return Err(format!("'{:?}' is assigned dynamically on right side", s));
                    }
                }
            }
        }
        for r in self
            .right
            .iter()
            .filter(|k| all_species[&k.species_name].is_compartment)
        {
            match &r.corresponding_on_left {
                None => {
                    if !allow_dynamic_structures {
                        return Err(format!(
                            "'{}' is created on right side",
                            species_name_conversion.get_rev(&r.species_name)
                        ));
                    }
                    found_any_structural_changes = true;
                }
                Some(col) => {
                    // has corresponding on left -> check if same parent
                    let l = self.left.iter().find(|k| k.id == *col).unwrap();
                    match &l.parent_number {
                        None => {
                            if r.compartment_constraint.is_some() {
                                found_any_structural_changes = true;
                                if !allow_dynamic_structures {
                                    return Err(format!(
                                        "'{}' is moved (from root)",
                                        species_name_conversion.get_rev(&r.species_name)
                                    ));
                                }
                            }
                        }
                        Some(lp) => match r.compartment_constraint {
                            None => {
                                found_any_structural_changes = true;
                                if !allow_dynamic_structures {
                                    return Err(format!(
                                        "'{}' is moved (to root)",
                                        species_name_conversion.get_rev(&r.species_name)
                                    ));
                                }
                            }
                            Some(r_parent) => {
                                let right_parent_corresp =
                                    &self.right[r_parent].corresponding_on_left;
                                match right_parent_corresp {
                                    None => {
                                        found_any_structural_changes = true;
                                        if !allow_dynamic_structures {
                                            return Err(format!(
                                                "'{}' is moved (to newly created {})",
                                                species_name_conversion.get_rev(&r.species_name),
                                                species_name_conversion
                                                    .get_rev(&self.right[r_parent].species_name)
                                            ));
                                        }
                                    }
                                    Some(rp) => {
                                        if *rp != *lp {
                                            found_any_structural_changes = true;
                                            if !allow_dynamic_structures {
                                                return Err(format!(
                                                    " '{}' is moved (to {})",
                                                    species_name_conversion
                                                        .get_rev(&r.species_name),
                                                    species_name_conversion.get_rev(
                                                        &self.right[r_parent].species_name
                                                    )
                                                ));
                                            }
                                        }
                                    }
                                }
                            }
                        },
                    }
                }
            }
        }
        for (spec, rest) in self
            .left
            .iter()
            .filter(|k| all_species[&k.species_name].is_compartment)
            .map(|v| (v, r_on_left.iter().find(|rest| rest.parent_id == v.id)))
        {
            if self
                .right
                .iter()
                .find(|r| r.corresponding_on_left == Some(spec.id.clone()))
                .is_none()
            {
                if rest.is_none() {
                    return Err(format!("When you are using dynamic nesting you must specifiy a rest soulution on the left side for each deleted compartment. '{:?}' has no such rest solution set",species_name_conversion.get_rev(&spec.species_name)).to_string());
                }
                if !allow_dynamic_structures {
                    return Err(format!(
                        "Removing '{:?}'",
                        species_name_conversion.get_rev(&spec.species_name)
                    )
                    .to_string());
                } else {
                    found_any_structural_changes = true;
                }
            }
        }
        Ok(found_any_structural_changes)
    }
    fn validate_rest_solution(
        &self,
        r_on_left: &Vec<RestSolutionLeft>,
        r_on_right: &Vec<RestSolutionRight>,
        _all_species: &HashMap<SpeciesName, Species>,
        allow_dynamic_structures: bool,
        _species_name_conversion: &SpeciesNameConverter,
    ) -> Result<bool, String> {
        let mut found_dynamic_changes = false;
        if r_on_left.is_empty() && r_on_right.is_empty() {
            return Ok(false);
        }
        let left_set: HashSet<_> = r_on_left.iter().map(|v| v.name.clone()).collect();

        /*if r_on_right.len() != r_on_right.iter().map(|r| &r.name).unique().count() {
            return Err("A name is used twice for a restsolution on the right side. Duplication of rest solutions is not implemented".to_string());
        }*/

        if left_set.len() != r_on_left.len() {
            return Err("A name is used twice for a restsolution".to_string());
        }

        for r in r_on_right.iter() {
            if !left_set.contains(&*r.name) {
                return Err(format!(
                    "Rest solution \"{}\" does not exist on the left side. We only have {:?}",
                    r.name, left_set
                )
                .to_string());
            } else {
                // is contained on left
                let lefty = r_on_left
                    .iter()
                    .find(|v| v.name == r.name)
                    .expect("should be on left");
                if r.compartment_constraint == -1 {
                    if !allow_dynamic_structures {
                        return Err(format!(
                                "Rest solution \"{}\" is moved to higher level. This is not yet supported.",
                                r.name
                            )
                                .to_string());
                    } else {
                        found_dynamic_changes = true;
                    }
                } else {
                    let idx_from_consstraint: usize = r.compartment_constraint as usize;
                    match &self.right[idx_from_consstraint].corresponding_on_left {
                        None => {
                            if !allow_dynamic_structures {
                                return Err(format!(
                                    "Rest solution \"{}\" is moved to new compartment",
                                    r.name
                                ));
                            } else {
                                found_dynamic_changes = true;
                            }
                        }
                        Some(left_id) => {
                            if *left_id != lefty.parent_id {
                                {
                                    if !allow_dynamic_structures {
                                        return Err(format!(
                                                "Rest solution \"{}\" is moved to different (existing) compartment",
                                                r.name
                                            ));
                                    } else {
                                        found_dynamic_changes = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //println!("Rest solution {} has no affect", r.name);
        }

        for l in r_on_left {
            match r_on_right.iter().find(|v| v.name == l.name) {
                None => {
                    if allow_dynamic_structures {
                        found_dynamic_changes = true;
                        //found_changes.push(StructuralChange::RemoveRestSolution(l.clone()));
                    } else {
                        return Err(format!(
                            "Rest solution \"{}\" is removed. This is not yet supported.",
                            l.name
                        ));
                    }
                }
                Some(_) => {}
            }
        }
        //found_changes.append(&mut compartment_movement);
        Ok(found_dynamic_changes)
    }
}

impl ModelParsed<'_> {
    pub fn check_and_get_transition(
        t: TransitionStx,
        all_species: &HashMap<SpeciesName, Species>,
        all_constants: &HashMap<String, ConstantDefinition>,
        available_functions: &HashMap<String, FunctionDefinition>,
        allow_dynamic_structures: bool,
        species_name_conversion: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
        errors_only: bool,
    ) -> Result<structures::Transition, LanguageError> {
        let left = rekursive_left_side2(
            None,
            Some(t.left.clone()),
            all_species,
            all_constants,
            available_functions,
            species_name_conversion,
            att_name_conversion,
            &vec![],
            &mut 0,
        )?;
        let (rate_expr, rate_expr_dim) = RateExprTerm::from_tree(
            ExprTermStx::Expr(Box::new(t.rate.clone())),
            &SemanticContext {
                left_match: &left.0,
                all_species,
                all_constants,
                single_reference_species: None,
                available_functions: available_functions,
                current_function_context: HashMap::new(),
                allow_starr_attributes: true,
                allow_time: true,
                allow_random: false,
                species_name_conversion,
                att_name_conversion,
            },
            &t.rate.span,
        )?;
        if rate_expr_dim != UNIT_SECOND.my_power(-1).unit {
            return Err(LanguageError::from_span(
                &t.rate.span,
                &*format!("unit is not 1/time\n unit:{}", rate_expr_dim.to_string()),
                line!(),
                file!(),
            ));
        }
        let sol = rekursive_right_side2(
            -1,
            Some(t.right.clone()),
            all_species,
            &left.0,
            all_constants,
            true,
            available_functions.clone(),
            &species_name_conversion,
            &att_name_conversion,
            allow_dynamic_structures,
            errors_only,
        )?;
        let max_fire: Vec<_> = t
            .constraints
            .firing_constraints
            .iter()
            .filter_map(|k| match k {
                FiringLimitationsStx::MaxCount { num, span } => Some((num, span)),
                _ => unimplemented!(),
            })
            .collect();
        if max_fire.len() > 1 {
            return Err(LanguageError::from_span(
                max_fire[1].1,
                "You can not define multiple maximum counts",
                line!(),
                file!(),
            ));
        }
        let mut new_t = structures::Transition {
            original_string: t.original_string,
            rate_expr: RateExpr::Single(rate_expr),
            /*guard: t
            .constraints
            .logical_constraints
            .iter()
            .map(|v| {
                v.check_and_convert_in_guard(&SemanticContext {
                    left_match: &left.0,
                    all_species,
                    all_constants,
                    single_reference_species: None,
                    available_functions: available_functions,
                    current_function_context: HashMap::new(),
                    allow_starr_attributes: true,
                    allow_time: true,
                    allow_random: false,
                    species_name_conversion,
                    att_name_conversion,
                })
            }) //&left.0, all_species, all_constants))
            .collect::<Result<_, _>>()?,*/
            right: sol.0.clone(),
            left: left.0,
            is_not_mass_action: t.is_not_mass_action,
            has_structural_changes: false,
            rest_solution_transiotions: vec![],
            max_fire: match max_fire.first() {
                None => None,
                Some((a, _)) => Some(**a),
            },
        };

        match new_t.use_names_for_left_right_correspondence(att_name_conversion) {
            Ok(_) => {}
            Err(e) => {
                return Err(LanguageError::from_span(
                    &t.span,
                    &format!("Left right Correspondance: {}", e).to_string(),
                    line!(),
                    file!(),
                ))
            }
        }
        new_t.find_left_right_correspondence();
        new_t.check_assignments(all_species, &t.span, att_name_conversion)?;
        new_t.rest_solution_transiotions = left
            .1
            .iter()
            .map(|left| RestSolutionTransition {
                leftsideparent: left.parent_id.clone(),
                amount_quanti: left.amount_expr.clone(),
                idx_right: sol
                    .1
                    .iter()
                    .filter(|r| r.name == left.name)
                    .map(|r| r.compartment_constraint)
                    .collect(),
            })
            .collect();
        new_t.has_structural_changes = match new_t.has_structural_changes(
            &left.1,
            &sol.1,
            all_species,
            allow_dynamic_structures,
            &species_name_conversion,
        ) {
            Ok(y) => y,
            Err(e) => match allow_dynamic_structures {
                true => return Err(LanguageError::from_span(&t.span, &e, line!(), file!())),
                false => {
                    return Err(LanguageError::from_span(
                        &t.span,
                        &format!("Dynamic changes disabled, but: {}", e).to_string(),
                        line!(),
                        file!(),
                    ))
                }
            },
        };
        match new_t.validate_not_multiple_hybrid_of_same_name_on_left_for_mass_action(all_species) {
            Ok(_) => {}
            Err(s) => {
                return Err(LanguageError::from_span(
                    &t.span,
                    s.as_str(),
                    line!(),
                    file!(),
                ))
            }
        }
        Ok(new_t)
    }

    fn get_all_nested_rek_right(sol: &SpeciesRightSolution) -> HashSet<String> {
        sol.species_in_solution
            .iter()
            .filter(|v| v.child_solution.is_some())
            .map(|v| {
                let mut r =
                    ModelParsed::get_all_nested_rek_right(v.child_solution.as_ref().unwrap());
                r.insert(v.species_name.clone());
                r.into_iter()
            })
            .flatten()
            .collect()
    }

    fn get_all_nested_rek_left(sol: &SpeciesLeftSolution) -> HashSet<String> {
        sol.species_in_solution
            .iter()
            .filter(|v| v.child_solution.is_some())
            .map(|v| {
                let mut r =
                    ModelParsed::get_all_nested_rek_left(v.child_solution.as_ref().unwrap());
                r.insert(v.species_name.clone());
                r.into_iter()
            })
            .flatten()
            .collect()
    }

    fn get_all_nested_species(&self) -> HashSet<String> {
        let mut res: HashSet<String> = ModelParsed::get_all_nested_rek_right(&self.initializations)
            .iter()
            .cloned()
            .collect();

        for t in self.transitions.iter() {
            res.extend(
                ModelParsed::get_all_nested_rek_right(&t.right)
                    .iter()
                    .cloned(),
            );
            res.extend(
                ModelParsed::get_all_nested_rek_left(&t.left)
                    .iter()
                    .cloned(),
            );
        }
        res
    }

    fn validate_and_get_species(
        &self,
        species_name_conversion: &SpeciesNameConverter,
        att_name_conversion: &HashMap<String, OptimizedString>,
    ) -> Result<HashMap<String, Species>, LanguageError> {
        let nested_compartments = self.get_all_nested_species();
        //println!("-- compartments: {:?}", nested_compartments);
        let mut used_names = HashMap::new();

        for i in self.species_definitions.iter() {
            #[cfg(not(feature = "hybrid_att"))]
            if !nested_compartments.contains(&*i.name.to_string()) {
                match i.attributes.iter().filter(|k| !k.can_be_expanded).next() {
                    None => {}
                    Some(a) => {
                        return Err(LanguageError::from_span(&a.span, "To use star attributes for non-compartments you have to use the hybrid-expansion feature.", line!(), file!()));
                    }
                }
            }

            //Todo check attributes unique
            match used_names.insert(
                i.name.to_string(),
                Species {
                    is_compartment: nested_compartments.contains(&*i.name.to_string()),
                    name: species_name_conversion.get(&i.name.to_string()),
                    expanding_attributes: i
                        .attributes
                        .iter()
                        .cloned()
                        .filter(|v| v.can_be_expanded)
                        .map(|v| {
                            (
                                match att_name_conversion.get(&v.name) {
                                    None => {
                                        panic!(
                                            "did not find '{}' in {:?}",
                                            v.name,
                                            att_name_conversion.keys()
                                        )
                                    }
                                    Some(k) => k.clone(),
                                },
                                v.dim,
                            )
                        })
                        .collect(),
                    not_expanding_attributes: i
                        .attributes
                        .iter()
                        .cloned()
                        .filter(|v| !v.can_be_expanded)
                        .map(|v| {
                            (
                                match att_name_conversion.get(&v.name) {
                                    None => {
                                        panic!("did not find '{}' in", v.name)
                                    }
                                    Some(k) => k.clone(),
                                },
                                v.dim,
                            )
                        })
                        .collect(),
                },
            ) {
                Some(_) => {
                    return Err(LanguageError::from_span(
                        &i.span,
                        &*format!("The name '{}' is not unique for species", i.name),
                        line!(),
                        file!(),
                    ))
                }
                None => (),
            };
            if (used_names.get(&*i.name).unwrap().expanding_attributes.len()
                + used_names
                    .get(&*i.name)
                    .unwrap()
                    .not_expanding_attributes
                    .len()
                != i.attributes.len())
                || (used_names
                    .get(&*i.name)
                    .unwrap()
                    .expanding_attributes
                    .keys()
                    .any(|v| {
                        used_names
                            .get(&*i.name)
                            .unwrap()
                            .not_expanding_attributes
                            .keys()
                            .filter(|k| *k == v)
                            .count()
                            != 0
                    }))
            {
                return Err(LanguageError::from_span(
                    &i.span,
                    &*format!(
                        "species '{}' has duplicate attribute names\n{:?}",
                        i.name, used_names[&i.name]
                    ),
                    line!(),
                    file!(),
                ));
            }
        }
        Ok(used_names)
    }
}
