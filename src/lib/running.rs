use crate::expansion::dynamic_expansion_tree::change_structure_and_reexpand_v2;
use crate::expansion::static_expansion::*;
use crate::input::structures::{Model, PrimitiveTypes};
use crate::output::{flatten_states_to_csv_file, SimOutput};
use crate::simulator::simulate_fast::*;
use float_pretty_print::PrettyPrintFloat;
use indicatif::{ProgressBar, ProgressStyle};
use itertools::Itertools;
use log::*;
use plotly::layout::Legend;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha512};
use std::collections::{BTreeSet, HashMap};
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::time::{Duration, Instant};

const GENERATE_NEW_TEMPLATE_FOR_TESTS: bool = true;

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct DataForPlot {
    pub observation_values: Vec<HashMap<String, PrimitiveTypes>>,
    pub obs_times: Vec<f64>,
    pub xmax: f64,
    pub xunit: Option<String>,
}

pub fn use_hybrid_att() -> bool {
    if cfg!(feature = "hybrid_att") {
        true
    } else {
        false
    }
}

pub fn generate_plot(dat: &DataForPlot, title: &str) -> plotly::Plot {
    use plotly::{common::Mode, common::Title, layout::Axis, Layout, Plot, Scatter};

    let mut plot = Plot::new();
    let obs_names: BTreeSet<_> = dat
        .observation_values
        .iter()
        .map(|k| k.keys())
        .flatten()
        .collect();
    for o in obs_names {
        let x = dat.obs_times.clone();
        let y: Vec<f64> = dat
            .observation_values
            .iter()
            .map(|k| k[o].as_float().unwrap())
            .collect();
        let trace = Scatter::new(x, y).mode(Mode::Lines).name(o);
        plot.add_trace(trace);
    }
    let layout = Layout::new()
        .x_axis(
            Axis::new()
                .title(Title::from(
                    format!("time {}", dat.xunit.as_ref().unwrap_or(&format!("[s]"))).as_str(),
                ))
                .range(vec![0.0, dat.xmax]),
        )
        .y_axis(Axis::new().title(Title::from("Observation")))
        .title(Title::from(title))
        .legend(Legend::new());
    plot.set_layout(layout);
    plot
}

pub fn read_model_from_file_and_expand(
    filename: &str,
    dynamic: bool,
    use_cache_file: bool,
    num_expansion_steps: usize,
    output_options: &OutputOptions,
    constant_overwrites: &Vec<String>,
) -> Option<FlatModel> {
    let model_file_hash;
    {
        let mut file = fs::read(filename).expect("File not found!");
        let mut hasher = Sha512::new();
        Digest::update(&mut hasher, &mut file);
        let model_file_hash_dat = hasher.finalize();
        model_file_hash = model_file_hash_dat.iter().join("")
    }

    let mut m2: FlatModel;
    let hash_file_name = format!(
        "cache_{}_{}.rule_cache",
        num_expansion_steps, model_file_hash
    );
    let hash_file_name_parent = format!(
        "../cache_{}_{}.rule_cache",
        num_expansion_steps, model_file_hash
    );
    if Path::exists(Path::new(&hash_file_name)) && use_cache_file && constant_overwrites.is_empty()
    {
        warn!(
            "-- this model has been read before, reusing cache file '{}'",
            hash_file_name
        );
        let file = File::open(Path::new(&hash_file_name)).unwrap();
        let reader = BufReader::new(file);
        m2 = bincode::deserialize_from(reader).expect("Error deserializeing");
        warn!(
            "-- reading successfull ({} reactions, {} species)",
            m2.reactions.len(),
            m2.species.len()
        );
    } else if Path::exists(Path::new(&hash_file_name_parent))
        && use_cache_file
        && constant_overwrites.is_empty()
    {
        warn!(
            "-- this model has been read before, reusing cache (parent level) file '{}'",
            hash_file_name_parent
        );
        let file = File::open(Path::new(&hash_file_name_parent)).unwrap();
        let reader = BufReader::new(file);
        m2 = bincode::deserialize_from(reader).expect("Error deserializeing");
        warn!(
            "-- reading successfull ({} reactions, {} species)",
            m2.reactions.len(),
            m2.species.len()
        );
    } else {
        let (m, latex) = match Model::from_file(filename, dynamic, constant_overwrites) {
            Ok(x) => x,
            Err(y) => {
                error!(
                    "There are Errors: \n{}",
                    y.into_iter()
                        .enumerate()
                        .map(|(n, v)| format!(
                            "ERROR {} ({}:{})\n{}",
                            n + 1,
                            v.file,
                            v.line,
                            v.what
                        ))
                        .join("\n")
                );
                return None;
            }
        };

        info!("-- Model specification OK");
        if output_options.debug_outputs {
            let mut file = File::create(filename.to_string() + ".parsed").unwrap();
            write!(file, "{:#?}", m).unwrap();
        }

        if output_options.latex {
            info!("-- Writing Latex");
            {
                let mut file = File::create(filename.to_string() + ".tex").unwrap();
                write!(file, "{}", latex).unwrap();
            }
        }

        //println!("{:?}", m.species);

        //println!("{:?}", m.transitions);
        info!("-- Start expanding model");

        let start_expand = Instant::now();
        m2 = FlatModel::from_model(m);
        m2.flatten_a(num_expansion_steps, true);
        info!(
            "   Expansion took {}s",
            start_expand.elapsed().as_secs_f64()
        );

        if output_options.debug_outputs {
            //println!("{}", m2.print_model());
            let mut file = File::create(filename.to_string() + ".reaction_model")
                .expect("could not open output file");
            write!(
                file,
                "{}",
                m2.print_model(
                    &m2.model.species_name_conversion,
                    &m2.model.string_conversion
                )
            )
            .unwrap();
            info!("-- Reaction network written");
        }

        if m2.reactions.len() + m2.species.len() > 1000 && use_cache_file {
            // no need to store small models
            info!("-- Start writing Cache file");
            let buffer = File::create(Path::new(&hash_file_name)).expect("could not create file");
            bincode::serialize_into(buffer, &m2).expect("error writing");
            info!("-- Done writing cache file '{}'", hash_file_name)
        }
    }
    Some(m2)
}

pub enum TestUntil {
    Step(usize),
    Time(f64),
    Initial,
    NoMoreReaction,
}
pub fn test_state_eq(file_path: &str, target_state: &str, until: TestUntil) {
    let out_a = test_full_output(file_path, until, false, true);
    let out_b = test_different_initial(file_path, target_state);
    error!(
        "Out is:     {}\nOut should: {}",
        out_a.to_string(),
        out_b.to_string()
    );
    assert!(out_a.species_eq(&out_b));
}

pub fn test_different_initial(file_path: &str, initial_state: &str) -> SimOutput {
    let model_string = fs::read_to_string(&file_path).expect(&*format!(
        "Something went wrong reading the file '{}'. Does it exist?",
        file_path
    ));
    let (m1, m2) = model_string.split_once("Initial:").unwrap();
    let (_old_init, m3) = m2.split_once(";").unwrap();
    let model_string = format!("{}\nInitial: {};{}", m1, initial_state, m3);
    {
        let mut file = File::create(file_path.to_string() + ".alternative_init").unwrap();
        write!(file, "{}", model_string).unwrap();
    }
    let (model, _) = Model::from_string(&model_string, true, &vec![], file_path.to_string(), false)
        .expect("reading file failed");

    let mut model_flat = FlatModel::from_model(model);
    model_flat.flatten_a(100, true);
    /*{
        let mut file = File::create(file_path.to_string() + ".flat").unwrap();
        write!(file, "{:#?}", model_flat).unwrap();
    }*/
    let sim =
        SimulationFast::new(&model_flat, None, GENERATE_NEW_TEMPLATE_FOR_TESTS, None).unwrap();
    sim.generate_output(false)
}

pub fn test_observe(
    species_names: Vec<&str>,
    model_state: &str,
    observe: &str,
    val_in: PrimitiveTypes,
) {
    assert!(!model_state.contains("output"));
    let model = format!(
        "{}\ninitial: {};\noutput {};",
        species_names
            .iter()
            .map(|k| {
                if k.contains("(") {
                    k.to_string() + ";"
                } else {
                    k.to_string() + "();"
                }
            })
            .join("\n"),
        model_state,
        observe
    );
    println!("{}", model);
    let (model, _) = match Model::from_string(&model, true, &vec![], format!("no file"), false) {
        Ok(a) => a,
        Err(k) => {
            println!(
                "There are Errors: \n{}",
                k.into_iter()
                    .enumerate()
                    .map(|(n, v)| format!("ERROR {} ({}:{})\n{}", n + 1, v.file, v.line, v.what))
                    .join("\n")
            );
            panic!("Model syntax errors!")
        }
    };
    let mut model_flat = FlatModel::from_model(model);
    model_flat.flatten_a(100, true);
    let mut sim =
        SimulationFast::new(&model_flat, None, GENERATE_NEW_TEMPLATE_FOR_TESTS, None).unwrap();
    sim.make_observations(&OutputOptions {
        json: true,
        compressed_json: false,
        flat_table: false,
        rule_counts: false,
        plot: false,
        regular_csv: false,
        latex: false,
        debug_outputs: false,
    });
    assert_eq!(sim.observation_values.len(), 1);
    let (name, vals) = sim.observation_values[0].iter().next().unwrap();
    println!("{} is {:?} and should be {:?}", name, vals, val_in);
    assert_eq!(val_in, *vals);
}

pub fn test_output_initial_propensities(model_name: &str) -> SimOutput {
    let (model, _) = match Model::from_file(model_name, true, &vec![]) {
        Ok(a) => a,
        Err(k) => {
            println!(
                "There are Errors: \n{}",
                k.into_iter()
                    .enumerate()
                    .map(|(n, v)| format!("ERROR {} ({}:{})\n{}", n + 1, v.file, v.line, v.what))
                    .join("\n")
            );
            panic!("Model syntax errors!")
        }
    };
    /*{
        let mut file = File::create(model_name.to_string() + ".parsed").unwrap();
        write!(file, "{:#?}", model).unwrap();
    }*/
    let mut model_flat = FlatModel::from_model(model);
    model_flat.flatten_a(100, true);
    /*{
        let mut file = File::create(model_name.to_string() + ".flat").unwrap();
        write!(file, "{:#?}", model_flat).unwrap();
    }*/
    let sim =
        SimulationFast::new(&model_flat, None, GENERATE_NEW_TEMPLATE_FOR_TESTS, None).unwrap();
    sim.generate_output(true)
}

pub fn test_output_state(file_path: &str, until: TestUntil) -> SimOutput {
    test_full_output(file_path, until, false, true)
}

pub fn test_output_state_no_struc_change(file_path: &str, until: TestUntil) -> SimOutput {
    test_full_output(file_path, until, false, false)
}

pub fn read_in_for_full_output(
    file_path: &str,
    until: &TestUntil,
    allow_dynamic_structure_changes: bool,
    silent: bool,
) -> (FlatModel, SimulationFast) {
    let (model, _) = match Model::from_file(file_path, allow_dynamic_structure_changes, &vec![]) {
        Ok(a) => a,
        Err(k) => {
            println!(
                "There are Errors: \n{}",
                k.into_iter()
                    .enumerate()
                    .map(|(n, v)| format!("ERROR {} ({}:{})\n{}", n + 1, v.file, v.line, v.what))
                    .join("\n")
            );
            panic!("Model syntax errors!")
        }
    };
    let mut model_flat = FlatModel::from_model(model);
    model_flat.flatten_a(100, !silent);
    let sim = SimulationFast::new(
        &model_flat,
        match until {
            TestUntil::Time(t) => Some((*t, 1)),
            _ => None,
        },
        GENERATE_NEW_TEMPLATE_FOR_TESTS,
        None,
    )
    .unwrap();
    (model_flat, sim)
}

pub fn test_full_output_step(
    mut model_flat: FlatModel,
    mut sim: SimulationFast,
    until: TestUntil,
    allow_dynamic_structure_changes: bool,
) -> SimulationFast {
    let mut stepcount = 0;
    match until {
        TestUntil::Step(_) | TestUntil::Time(_) | TestUntil::NoMoreReaction => loop {
            match until {
                TestUntil::Step(k) => {
                    if stepcount == k {
                        break sim; //.generate_output(include_propensities);
                    }
                }
                _ => {}
            }
            match sim.step(false).unwrap() {
                StepResult::AllGood => {
                    stepcount += 1;
                    if cfg!(debug_assert) {
                        assert_eq!(sim.reactions.len(), model_flat.reactions.len());
                        for (a, b) in model_flat.reactions.iter().zip(sim.reactions.iter()) {
                            assert_eq!(a.species_in.len(), b.species_in_idx.len());
                        }
                    }
                }
                StepResult::NoReactionPossible => {
                    break sim; //.generate_output(include_propensities);
                }
                StepResult::StructuralChange {
                    idx_of_reaction,
                    variation,
                } => {
                    if !allow_dynamic_structure_changes {
                        panic!("Have structural change!")
                    }
                    stepcount += 1;
                    //println!("Check B");
                    if cfg!(debug_assert) {
                        assert_eq!(sim.reactions.len(), model_flat.reactions.len());
                        for (a, b) in model_flat.reactions.iter().zip(sim.reactions.iter()) {
                            assert_eq!(a.species_in.len(), b.species_in_idx.len());
                        }
                    }
                    let lookup =
                        sim.variation_to_lookup(variation, &model_flat.reactions, idx_of_reaction);
                    sim.take_current_state_as_initial(&mut model_flat);
                    model_flat = change_structure_and_reexpand_v2(
                        model_flat,
                        idx_of_reaction,
                        20,
                        false,
                        lookup,
                    );
                    sim = sim
                        .reset_to_changed_model(&mut model_flat, GENERATE_NEW_TEMPLATE_FOR_TESTS)
                        .unwrap();
                    if cfg!(debug_assert) {
                        assert_eq!(sim.reactions.len(), model_flat.reactions.len());
                        for (a, b) in model_flat.reactions.iter().zip(sim.reactions.iter()) {
                            assert_eq!(a.species_in.len(), b.species_in_idx.len());
                        }
                    }
                }
                StepResult::OnlyObservation => {
                    // No observation needed
                    //sim.make_observations(OutputOptions::none())
                }
                StepResult::OnlyTimeDependendUpdate => {}
                StepResult::Completed => {
                    break sim; //.generate_output(include_propensities);
                }
            }
        },
        TestUntil::Initial => sim, //.generate_output(include_propensities),
    }
}

pub struct Benchmark {}

pub fn test_full_output(
    file_path: &str,
    until: TestUntil,
    include_propensities: bool,
    allow_dynamic_structure_changes: bool,
) -> SimOutput {
    let (model_flat, sim) =
        read_in_for_full_output(file_path, &until, allow_dynamic_structure_changes, false);

    assert_eq!(sim.reactions.len(), model_flat.reactions.len());
    for (a, b) in model_flat.reactions.iter().zip(sim.reactions.iter()) {
        assert_eq!(a.species_in.len(), b.species_in_idx.len());
    }

    test_full_output_step(model_flat, sim, until, allow_dynamic_structure_changes)
        .generate_output(include_propensities)
}

pub fn run_in_term(
    mut m: FlatModel,
    target_time: f64,
    tau_step: Option<f64>,
    output_num: usize,
    approximate: Option<f64>,
    print_dynamic_io: bool,
    print_big_debug: bool,
    file_name_prefix: Option<String>,
    silent_for_parallel: bool,
    output_options: &OutputOptions,
    generate_new_templates: bool,
    random_seed: Option<u64>,
) {
    if output_options.compressed_json {
        panic!("compressed json not yet implemented")
    }
    if !silent_for_parallel {
        info!("Start Terminal Execution!");
    }

    let total_time = Instant::now();

    let mut time_extra_expansion = Duration::new(0, 0);
    let mut expansion_counter = 0 as usize;

    //let now = Instant::now();

    let mut fast_sim;
    match approximate {
        Some(_x) => {
            panic!("Approximate no longer implemented")
        } // }01);
        None => {
            fast_sim = SimulationFast::new(
                &m,
                Some((target_time, output_num)),
                generate_new_templates,
                random_seed,
            )
            .unwrap();
        }
    }
    let setup_duration = total_time.elapsed().as_secs_f64();
    let exec_time = Instant::now();
    info!("Successfully generated fast simulator");
    if print_big_debug {
        fast_sim.print_tree_flat();
    }

    //self.write_header();
    //self.make_observations();

    let pb = ProgressBar::new(1000);
    pb.set_style(
        ProgressStyle::default_bar()
            .template(
                "{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {percent}% ({eta})",
            )
            .progress_chars("#>-"),
    );
    //let mut stepcount: usize = 0;
    let mut pb_time = -1.;
    pb.set_position(0);
    while fast_sim.time < target_time {
        if !silent_for_parallel
            && (fast_sim.metrics.step_function_calls % 100 == 0
                || fast_sim.metrics.step_function_calls < 200)
        {
            if pb_time + target_time / 1000. < fast_sim.time {
                pb_time = fast_sim.time;
                pb.set_position((1000. * fast_sim.time / (1000. * target_time) * 1000.) as u64);
            }
        }
        match if tau_step.is_some() {
            let r = fast_sim.step_tau(tau_step.unwrap(), -1., print_big_debug);
            if print_big_debug {
                fast_sim.print_tree_flat();
            }
            r
        } else {
            let r = fast_sim.step(print_big_debug).unwrap();
            r
        } {
            StepResult::AllGood => {
                //println!("Good Step")
            }
            StepResult::NoReactionPossible => {
                warn!("Early termination, because no more reactions possible!")
            }
            StepResult::StructuralChange {
                idx_of_reaction: idx,
                variation,
            } => {
                if print_dynamic_io {
                    debug!("-- Start structural change");
                }
                if pb_time < fast_sim.time + target_time / 1000. {
                    pb_time = fast_sim.time;
                    pb.set_position((1000. * fast_sim.time / (1000. * target_time) * 1000.) as u64);
                }
                let start_expand = Instant::now();
                //println!("{:?}",fast_sim.species_names);
                //println!("{:?}",fast_sim.species_counts);
                expansion_counter += 1;
                fast_sim.take_current_state_as_initial(&mut m);
                let lookup = fast_sim.variation_to_lookup(variation, &m.reactions, idx);
                m = change_structure_and_reexpand_v2(m, idx, 20, print_dynamic_io, lookup);
                fast_sim = fast_sim
                    .reset_to_changed_model(&mut m, generate_new_templates)
                    .unwrap();
                time_extra_expansion += start_expand.elapsed();
            }
            StepResult::OnlyObservation => {
                fast_sim.make_observations(output_options);
                //println!("Obs only")
            }
            StepResult::OnlyTimeDependendUpdate => {}
            StepResult::Completed => {
                fast_sim.make_observations(output_options);
                break;
            }
        }
        if print_big_debug {
            fast_sim.print_tree_flat();
        }
    }
    pb.finish_with_message("Simulation complete");
    let t = exec_time.elapsed().as_secs_f64();

    //fast_sim.debug_print_counts();
    let file_name = match file_name_prefix {
        None => "output".to_string(),
        Some(x) => x,
    };
    if output_options.regular_csv {
        let mut file = File::create(file_name.clone() + ".csv").unwrap();
        let all_obs_names: BTreeSet<_> = fast_sim
            .observation_values
            .iter()
            .map(|k| k.keys())
            .flatten()
            .cloned()
            .collect();
        let all_obs_names: Vec<_> = all_obs_names.into_iter().collect();
        writeln!(file, "time[s],{}", all_obs_names.iter().join(",")).expect("file write error");
        assert_eq!(fast_sim.obs_times.len(), fast_sim.observation_values.len());
        fast_sim
            .observation_values
            .iter()
            .zip(fast_sim.obs_times.iter())
            .for_each(|(v, time)| {
                writeln!(
                    file,
                    "{},{}",
                    time,
                    all_obs_names
                        .iter()
                        .map(|x| match v.get(x) {
                            None => {
                                format!("")
                            }
                            Some(value) => {
                                match value {
                                    PrimitiveTypes::String(y) => {
                                        y.get_original_string(&m.model.string_conversion)
                                    }
                                    PrimitiveTypes::Boolean(y) => format!("{}", y),
                                    PrimitiveTypes::Float(y) => format!("{}", y),
                                    PrimitiveTypes::Integer(y) => format!("{}", y),
                                }
                            }
                        })
                        .join(",")
                )
                .expect("file write error")
            });
    }

    if output_options.plot {
        let dat = DataForPlot {
            observation_values: fast_sim.observation_values.clone(),
            obs_times: fast_sim.obs_times.clone(),
            xmax: target_time,
            xunit: None,
        };
        let plot = generate_plot(&dat, m.model.original_file_name.clone().as_str());
        plot.write_html(file_name.clone() + "_plot.html");
    }

    if output_options.flat_table {
        info!("Writing flat output");
        let file = File::create(file_name.clone() + "_flat.csv").unwrap();
        flatten_states_to_csv_file(file, &fast_sim.state_jsons, &m.model.string_conversion);
        debug!("Writing flat output");
    }
    if output_options.json {
        let mut file = File::create(file_name.clone() + "_structure.json").unwrap();
        write!(file, "[").unwrap();
        for (n, state) in fast_sim.state_jsons.iter().enumerate() {
            write!(
                file,
                "{}",
                serde_json::to_string(&serde_json::to_value(&state).unwrap())
                    .expect("error creating json?")
            )
            .unwrap();
            if n + 1 != fast_sim.state_jsons.len() {
                write!(file, ",").unwrap();
            }
        }
        writeln!(file, "]").unwrap();
        fast_sim.state_jsons = vec![];
    }

    //todo make niceer iteration to save space
    if output_options.rule_counts {
        let obs_reaction_counts_transposed = transpose(fast_sim.obs_reaction_counts.clone());
        let mut file = File::create(file_name.clone() + "_rule_counts.csv").unwrap();
        writeln!(
            file,
            "time[s],{}",
            fast_sim
                .reaction_names
                .iter()
                .map(|k| k.replace(",", " "))
                .join(",")
        )
        .expect("file write error");
        obs_reaction_counts_transposed
            .iter()
            .zip(fast_sim.obs_times.iter())
            .for_each(|(v, time)| {
                writeln!(
                    file,
                    "{},{}",
                    time,
                    v.iter().map(|x| format!("{}", x)).join(",")
                )
                .expect("file write error")
            });
    }

    /*{
        let mut file = File::create(file_name.clone() + "_compartment_attributes.csv").unwrap();
        let all_obs_names: BTreeSet<_> = fast_sim
            .observation_compartment_attributes
            .iter()
            .map(|k| k.keys())
            .flatten()
            .cloned()
            .collect();
        let all_obs_names: Vec<_> = all_obs_names.into_iter().collect();
        writeln!(file, "time[s],{}", all_obs_names.iter().join(",")).expect("file write error");
        assert_eq!(fast_sim.obs_times.len(), fast_sim.observation_values.len());
        fast_sim
            .observation_compartment_attributes
            .iter()
            .zip(fast_sim.obs_times.iter())
            .for_each(|(v, time)| {
                writeln!(
                    file,
                    "{},{}",
                    time,
                    all_obs_names
                        .iter()
                        .map(|x| match v.get(x) {
                            None => {
                                format!("")
                            }
                            Some(value) => {
                                match value {
                                    PrimitiveTypes::String(y) => y.clone(),
                                    PrimitiveTypes::Boolean(y) => format!("{}", y),
                                    PrimitiveTypes::Float(y) => format!("{}", y),
                                    PrimitiveTypes::Integer(y) => format!("{}", y),
                                }
                            }
                        })
                        .join(",")
                )
                .expect("file write error")
            });
    }*/

    //println!("Output written to '{}'", self.output_file_name);
    info!(
        "Setup time: {:.7} seconds",
        PrettyPrintFloat(setup_duration)
    );
    fast_sim.metrics.output(exec_time.elapsed().as_secs_f64());

    if fast_sim.metrics.virtual_steps == 0 && fast_sim.metrics.regular_steps != 0 {
        info!(
            "Average Reaction Idx was {} of {} (distance {}%)",
            fast_sim.reaction_idx_sum / fast_sim.metrics.regular_steps,
            fast_sim.get_number_of_possible_reactions(),
            (fast_sim.reaction_idx_sum / fast_sim.metrics.regular_steps) as f64
                / fast_sim.get_number_of_possible_reactions() as f64
                * 100.
        );
    }
    info!(
        "Dynamic rate updates: {} per step",
        fast_sim.metrics.num_rate_updates / fast_sim.metrics.regular_steps
    );
    if expansion_counter > 0 {
        println!(
            "  -- {} structure changing steps in {}s -> throughput {}/s",
            expansion_counter,
            time_extra_expansion.as_secs_f64(),
            expansion_counter as f64 / time_extra_expansion.as_secs_f64()
        );
        println!(
            "  -- {} regular steps in {}s -> throughput {}/s",
            fast_sim.metrics.regular_steps,
            //fast_sim.metrics.step_function_calls - expansion_counter,
            t - time_extra_expansion.as_secs_f64(),
            (fast_sim.metrics.step_function_calls - expansion_counter) as f64
                / (t - time_extra_expansion.as_secs_f64())
        );
        println!(
            "Ratio spend on structure changes: {}%",
            100. * time_extra_expansion.as_secs_f64() / (t)
        )
    } else {
        info!("No dynamic reactions")
    }
}

/*pub fn step_for_benchmark(
    mut sim: Box<SimulationFast>,
    mut model_flat: Box<FlatModel>,
) -> (Box<SimulationFast>, Box<FlatModel>) {
    unimplemented!()
    /*match sim.step(false) {
        StepResult::AllGood => {}
        StepResult::NoReactionPossible => {
            unreachable!()
        }
        StepResult::StructuralChange {
            idx_of_reaction,
            variation,
        } => {
            let lookup = sim.variation_to_lookup(variation, &model_flat.reactions, idx_of_reaction);
            sim.take_current_state_as_initial(model_flat);
            /* *model_flat = change_structure_and_reexpand_v2(
                *model_flat,
                idx_of_reaction,
                20,
                false,
                lookup,
            ); */
            *sim = Box::new(sim.reset_to_changed_model(model_flat));
            //sim = sim2;
        }
        StepResult::OnlyObservation => {}
        StepResult::OnlyTimeDependendUpdate => {}
        StepResult::Completed => {
            unreachable!()
        }
    }*/
}*/

fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>>
where
    T: Clone,
{
    assert!(!v.is_empty());
    (0..v[0].len())
        .map(|i| v.iter().map(|inner| inner[i].clone()).collect::<Vec<T>>())
        .collect()
}
