use criterion::{criterion_group, criterion_main, BatchSize, Criterion, Throughput};

use ml_rules_3_core::expansion::static_expansion::FlatModel;
use ml_rules_3_core::input::structures::Model;

use ml_rules_3_core::running::{read_in_for_full_output, test_full_output_step, TestUntil};
use ml_rules_3_core::simulator::simulate_fast::SimulationFast;

fn circular_model(c: &mut Criterion) {
    let m = match Model::from_file("benchmarkmodels/degree=4.0.mlr", false, &vec![]) {
        Ok((i, _)) => i,
        Err(_) => {
            unreachable!()
        }
    };
    let mut m = FlatModel::from_model(m);
    m.flatten_a(100, false);

    let mut sim = SimulationFast::new(&m, None, true, None);
    c.bench_function("step", |b| b.iter(|| sim.step(false)));
    let mut cou = 0;
    c.bench_function("single_rate", |b| {
        b.iter(|| {
            cou = (cou + 1) % sim.reactions.len();
            sim.get_propensity_of(cou, false)
        })
    });

    let m = match Model::from_file("benchmarkmodels/degree=4.0_rate2.mlr", false, &vec![]) {
        Ok((i, _)) => i,
        Err(_) => {
            unreachable!()
        }
    };
    let mut m = FlatModel::from_model(m);
    m.flatten_a(100, false);
    let mut sim = SimulationFast::new(&m, None, true, None);
    c.bench_function("cpx step", |b| b.iter(|| sim.step(false)));
    let mut cou = 0;
    c.bench_function("cpx single_rate", |b| {
        b.iter(|| {
            cou = (cou + 1) % sim.reactions.len();
            sim.get_propensity_of(cou, false)
        })
    });
}

/*fn dynamic(c: &mut Criterion) {
    c.bench_function("expand dynamic", |b| {
        b.iter(|| {
            read_in_for_full_output(
                "benchmarkmodels/dynamic.mlrj",
                &TestUntil::Initial,
                true,
                true,
            );
        });
    });
    let num_steps = 10000;
    c.bench_function("run dynamic", |b| {
        b.iter_batched(
            || {
                s.reseed();
                (m.clone(), s.clone())
            },
            |(model, sim)| test_full_output_step(model, sim, TestUntil::Step(num_steps), true),
            BatchSize::LargeInput,
        );
    });
}*/

const NUM_STEPS_NETWORK: usize = 100;
fn network_dyn(c: &mut Criterion) {
    let mut group = c.benchmark_group("Network Dynamic");
    group.bench_function("network dyn inital", |b| {
        b.iter(|| {
            read_in_for_full_output(
                "benchmarkmodels/network_dyn.mlr",
                &TestUntil::Initial,
                true,
                true,
            );
        });
    });
    let (m, mut s) = read_in_for_full_output(
        "benchmarkmodels/network_dyn.mlr",
        &TestUntil::Step(NUM_STEPS_NETWORK),
        true,
        true,
    );

    group.sample_size(10);
    group.throughput(Throughput::Elements(NUM_STEPS_NETWORK as u64));
    group.bench_function("network dyn run", |b| {
        b.iter_batched(
            || {
                s.reseed();
                (m.clone(), s.clone())
            },
            |(model, sim)| {
                test_full_output_step(model, sim, TestUntil::Step(NUM_STEPS_NETWORK), true)
            },
            BatchSize::LargeInput,
        );
    });
}

fn network_att(c: &mut Criterion) {
    let mut group = c.benchmark_group("Network att");
    group.bench_function("network att initial", |b| {
        b.iter(|| {
            read_in_for_full_output(
                "benchmarkmodels/network_static.mlr",
                &TestUntil::Initial,
                true,
                true,
            );
        });
    });
    let (m, mut s) = read_in_for_full_output(
        "benchmarkmodels/network_static.mlr",
        &TestUntil::Step(NUM_STEPS_NETWORK),
        true,
        true,
    );
    group.throughput(Throughput::Elements(NUM_STEPS_NETWORK as u64));
    group.sample_size(10);
    group.bench_function("network att run", |b| {
        b.iter_batched(
            || {
                s.reseed();
                (m.clone(), s.clone())
            },
            |(model, sim)| {
                test_full_output_step(model, sim, TestUntil::Step(NUM_STEPS_NETWORK), true)
            },
            BatchSize::LargeInput,
        );
    });
}
fn network_hybrid(c: &mut Criterion) {
    let mut group = c.benchmark_group("network hybrid");
    group.bench_function("network hybrid initial", |b| {
        b.iter(|| {
            read_in_for_full_output(
                "benchmarkmodels/network_hybrid.mlr",
                &TestUntil::Initial,
                true,
                true,
            );
        });
    });
    let (m, mut s) = read_in_for_full_output(
        "benchmarkmodels/network_hybrid.mlr",
        &TestUntil::Step(NUM_STEPS_NETWORK),
        true,
        true,
    );
    group
        .sample_size(10)
        .throughput(Throughput::Elements(NUM_STEPS_NETWORK as u64));
    group.bench_function("network hybrid run", |b| {
        b.iter_batched(
            || {
                s.reseed();
                (m.clone(), s.clone())
            },
            |(model, sim)| {
                test_full_output_step(model, sim, TestUntil::Step(NUM_STEPS_NETWORK), true)
            },
            BatchSize::LargeInput,
        );
    });
}

/*fn expanding(c: &mut Criterion) {
    let mut m =
        read_model_from_file_and_expand("benchmarkmodels/dynamic.mlrj", true, false, false, 1000)
            .unwrap();
    let mut sim = SimulationFast::new(&m, None);
    let mut m = Box::new(m);
    let mut sim = Box::new(sim);
    c.bench_function("stepcost", |b| {
        b.iter(|| (sim, m) = step_for_benchmark(sim, m))
    });
}*/
criterion_group!(network_benchs, network_dyn, network_hybrid, network_att);
criterion_group!(benches, circular_model); //, */ expanding);
criterion_main!(network_benchs);
//criterion_main!(benches);
