use core::default::Default;
use log::trace;
use ml_rules_3_core;
use ml_rules_3_core::input::structures::LanguageError;
use ml_rules_3_core::running_web::MessageOrPlot;
use ml_rules_3_core::running_web::{GeneratedSim, WebExecutor};
use serde::{Deserialize, Serialize};
use yew::{html, Html};
use yew_agent::{Callback, PublicWorker};
use yew_agent::{HandlerId, Private, Public, WorkerLink};
use yew_plotly::Plotly;

pub fn get_errors(input: &str) -> Vec<LanguageError> {
    //vec![]
    ml_rules_3_core::running_web::execute_mlrules_error_list(input)
}

/*pub fn execute_mlrules(input: &str, endtime: &str) -> MessageOrPlot {
    //ErrorOrPlot::Error(input.to_string())
    ml_rules_3_core::running_web::execute_mlrules_to_string(input, endtime)
}*/

#[derive(Serialize, Deserialize, Debug)]
pub enum SimMessage {
    input(SimulationInput, usize),
    cancel(usize),
    next_step(usize),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SimulationInput {
    pub(crate) input: String,
    pub(crate) endtime: String,
}

pub struct Worker {
    link: WorkerLink<Self>,
    sim: WebExecutor,
    sim_idx: usize,
}
impl yew_agent::Worker for Worker {
    type Input = SimMessage;
    type Message = HandlerId;
    type Output = (MessageOrPlot, usize);
    type Reach = Public<Self>;
    fn create(link: WorkerLink<Self>) -> Self {
        trace!("Web worker init");
        Self {
            link,
            sim: WebExecutor::default(),

            sim_idx: 0,
        }
    }

    fn update(&mut self, msg: Self::Message) {
        //panic!("sim worker update");
        /*match msg {
            SimMessage::next_step(id) => self.handle_input(msg, id),
            _ => unreachable!("{:?}", msg),
        }*/
    }

    fn name_of_resource() -> &'static str {
        "worker.js"
    }

    fn handle_input(&mut self, msg: Self::Input, id: HandlerId) {
        //let id_c = id.clone();
        /*let callback_to_next: Callback<_> = self.link.
        (move |_| id_c);*/
        trace!("Called sim worker");
        match msg {
            SimMessage::cancel(n) => {
                self.sim_idx = 0;
                trace!("sim cancled");
                self.link.respond(
                    id,
                    (
                        MessageOrPlot::FinalMessage(format!("Simulation Cancled")),
                        self.sim_idx,
                    ),
                );
            }
            SimMessage::next_step(n) => {
                if n < self.sim_idx {
                    self.link.respond(
                        id,
                        (
                            MessageOrPlot::FinalMessage(format!(
                                "Simulation Cancled (during step)"
                            )),
                            self.sim_idx,
                        ),
                    );
                }
                let output = self.sim.execute_mlrules_to_string();
                let is_final = output.is_final();
                self.link.respond(id, (output, self.sim_idx));
                if is_final {
                    return;
                }
                //self.link.send_input(SimMessage::next_step);
            }
            SimMessage::input(msg, n) => {
                self.sim_idx = n;
                trace!("sim input");
                /*self.link.respond(
                    id,
                    (
                        MessageOrPlot::IntermediateMessage(format!("Simulation has started")),
                        self.sim_idx,
                    ),
                );*/

                self.sim = match ml_rules_3_core::running_web::WebExecutor::build_mlrules_to_string(
                    &msg.input,
                    &msg.endtime,
                ) {
                    Err(s) => {
                        trace!("report init failed");
                        self.link
                            .respond(id, (MessageOrPlot::FinalMessage(s), self.sim_idx));
                        return;
                    }
                    Ok(a) => {
                        trace!("Init complete");
                        self.link.respond(
                            id,
                            (MessageOrPlot::IntermediateMessage(format!(
                                "Simulation was successfully initialized!\n{} Species & {} Reactions",
                                a.fast_sim.species_counts.len()  ,
                                a.fast_sim.reactions.len()

                            )),self.sim_idx),
                        );
                        a
                    }
                };
                trace!("Callback after init");
                //self.link.send_input(SimMessage::next_step);
                trace!("Callback complete");
                //callback_to_next(());
            }
        };
    }
}
