use crate::mlr_editor::ID;
use itertools::Itertools;
use js_sys::{Array, Object};
use log::*;
use std::fmt::{Display, Formatter};

use gloo_net::*;
use ml_rules_3_core::running::{generate_plot, Benchmark};
use ml_rules_3_core::running_web::{execute_mlrules_error_list, gen_benchmark, MessageOrPlot};
use monaco::sys::editor::IStandaloneCodeEditor;
use monaco::yew::CodeEditorLink;
use monaco::{
    api::{CodeEditorOptions, TextModel},
    sys::{
        editor::{BuiltinTheme, IMarkerData, IModelDecorationOptions, IModelDeltaDecoration},
        IMarkdownString, MarkerSeverity, Range,
    },
    yew::CodeEditor,
};
use std::rc::Rc;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{window, HtmlDivElement, HtmlInputElement, HtmlSelectElement, HtmlTextAreaElement};
use yew::platform::spawn_local;
use yew::prelude::*;
use yew::{html, Callback, Component, Context, Html};
use yew_agent::PublicWorker;
use yew_agent::{Bridge, Bridged};
use yew_plotly::plotly::{Plot, Scatter};
use yew_plotly::Plotly;

pub mod execute;
use execute::*;
pub mod mlr_editor; // Importing the new module
use mlr_editor::*; // Importing the new MLREditor component

use crate::mlr_editor::_MLREditorProps::text_model;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/benchmark/:id/:num_reps/:extra")]
    BenchmarkX {
        id: String,
        num_reps: usize,
        extra: usize,
    },
    #[at("/benchmark/:id/:num_reps")]
    Benchmark { id: String, num_reps: usize },
    #[at("/")]
    Home,
    #[at("/:id")]
    NumberedModel { id: String },
    #[at("/:id/:time/:unit")]
    NumberedModelEndtime {
        id: String,
        time: usize,
        unit: String,
    },
}

//const CONTENT: &str = include_str!("../../testing/a2b_cpx_star_2out.mlr");
const LOCAL_STORAGE_ID: &str = "ml_rules_model";

#[derive(PartialEq)]
struct Flags {
    print_code: bool,
    trace_execution: bool,
    std_mode: bool,
    stress_gc: bool,
    log_gc: bool,
}

impl Flags {
    #[must_use]
    fn new() -> Self {
        Self {
            print_code: false,      //config::PRINT_CODE.load(),
            trace_execution: false, //config::TRACE_EXECUTION.load(),
            std_mode: false,        //config::STD_MODE.load(),
            stress_gc: false,       //config::STRESS_GC.load(),
            log_gc: false,          //config::LOG_GC.load(),
        }
    }
}

#[derive(PartialEq, Properties)]
pub struct ExamplesProps {
    onchange: Callback<&'static str>,
}

/*#[derive(PartialEq, Properties)]
pub struct CheckboxProps {
    label: AttrValue,
    onchange: Callback<bool>,
}

#[function_component]
pub fn Checkbox(props: &CheckboxProps) -> Html {
    let CheckboxProps { label, onchange } = props;

    let onchange = onchange.clone();
    let html_change_handler = use_callback(
        move |e: Event, _| {
            let input = e.target_dyn_into::<HtmlInputElement>();
            if let Some(input) = input {
                onchange.emit(input.checked());
            };
        },
        (),
    );
    html! {
        <label>
            <input type="checkbox" onchange={html_change_handler} />
            {label}
        </label>
    }
}*/

#[function_component]
pub fn Examples(props: &ExamplesProps) -> Html {
    let ExamplesProps { onchange } = props;

    let onchange = onchange.clone();
    let html_on_change =
        use_callback(
            |e: Event, onchange| {
                let select = e.target_dyn_into::<HtmlSelectElement>();
                if let Some(select) = select {
                    match select.value().as_str() {
                        "pe1" => onchange
                            .emit(include_str!("../../testing/error_cases_dez_22/error_1.mlr")),
                        "pe2" => onchange
                            .emit(include_str!("../../testing/error_cases_dez_22/error_2.mlr")),
                        "pe3" => onchange
                            .emit(include_str!("../../testing/error_cases_dez_22/error_3.mlr")),
                        "pe4" => onchange
                            .emit(include_str!("../../testing/error_cases_dez_22/error_4.mlr")),
                        _ => unimplemented!(),
                    }
                    select.set_value("");
                }
            },
            onchange,
        );

    html! {
        <select class="examples" onchange={html_on_change}>
            <option value="" selected={true}>{ "-- Load an Example --" }</option>
            <option value="pe1">{"pe1"}</option>
            <option value="pe2">{"pe2"}</option>
            <option value="pe3">{"pe3"}</option>
            <option value="pe4">{"pe4"}</option>
        </select>
    }
}

#[derive(PartialEq, Properties)]
pub struct OutputProps {
    output_text: MessageOrPlot,
}

#[function_component]
pub fn Output(props: &OutputProps) -> Html {
    let OutputProps { output_text } = props;
    match &output_text {
        MessageOrPlot::FinalMessage(e) | MessageOrPlot::IntermediateMessage(e) => {
            let e: String = e
                .split("\n")
                .map(
                    |k| {
                        if k.len() < 80 {
                            k.chars()
                        } else {
                            k[..80].chars()
                        }
                    }, //k.chars().chunks(80).into_iter().collect::<String>()//.map(|inner|inner.collect::<String>())
                )
                .intersperse("\n".chars())
                .flatten() /*.intersperse("\n")*/
                .collect();
            html! {
            < pre class = "output" >
            { e }
            < / pre >
            }
        }
        MessageOrPlot::FinalPlot(html) | MessageOrPlot::IntermediatePlot(html) =>
        /*html! {
            <pre class="output">
                { "testing" }
            </pre>
        }, // */
        {
            let plot = generate_plot(html, "Plot");
            html!(<Plotly plot={plot.clone()} />)
        }
    }
}

#[derive(PartialEq, Properties, Debug)]
pub struct ModelSelectionProps {
    model: Option<String>,
    endtime: Option<usize>,
    endtimeunit: Option<String>,
}

#[derive(PartialEq, Properties, Debug)]
pub struct benchmarkProps {
    model: String,
    num_reps: usize,
    extra_setup: Option<usize>,
}

#[function_component]
fn BenchmarkWeb(p: &benchmarkProps) -> Html {
    let text = gen_benchmark(&p.model, p.num_reps, p.extra_setup);
    //let test = "test \n test 2";
    info!("called benchmark {:?}", p);
    html! {
    < pre class = "output" >
    { text }
    < / pre >
    }
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::BenchmarkX {
            id,
            num_reps,
            extra,
        } => {
            html! { <> <h1> {"Benchmark Results"} </h1> <BenchmarkWeb model={id} num_reps={num_reps} extra_setup={extra}/>  </>}
        }
        Route::Benchmark { id, num_reps } => {
            html! { <> <h1> {"Benchmark Results"} </h1> <BenchmarkWeb model={id} num_reps={num_reps} />  </>}
        }
        //_ => html!{ <InnerApp />}
        Route::Home => html! {<InnerApp />},
        Route::NumberedModel { id } => html! {<InnerApp model={id}/>},
        Route::NumberedModelEndtime { id, time, unit } => {
            html!(<InnerApp model={id} endtime={time} endtimeunit={unit} />)
        }
    }
}

#[function_component]
pub fn app() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Route> render={switch} /> // <- must be child of <BrowserRouter>
        </BrowserRouter>
    }
}

fn extract_and_decode_url(input: &str) -> Option<String> {
    // Find the position of "url=" in the input string
    if let Some(start) = input.find("url=%22") {
        // Extract the part of the string after "url="
        let url_part = &input[start + 7..];

        // Remove the trailing "%22" if present
        let url_part = if url_part.ends_with("%22") {
            &url_part[..url_part.len() - 3]
        } else {
            url_part
        };

        // Replace '%' with the actual '%' character
        let decoded_url = url_part.replace("%2F", "/").replace("%22", "\"");

        // Check if the decoded URL starts with "http://" or "https://"
        if decoded_url.starts_with("https://") {
            Some(decoded_url.to_string())
        } else {
            // If it doesn't start with a protocol, assume "http://" as default
            Some(format!("https://{}", decoded_url))
        }
    } else {
        None // "url=" not found in the input string
    }
}

async fn fetch_data_from_api(url: &str) -> Result<String, String> {
    trace!("Fetching {url}");
    let request_init = gloo_net::http::Request::get(url).send().await; /*RequestInit::new()
                                                                       .method("GET")
                                                                       .mode(RequestMode::Cors); // Adjust request settings as needed*/

    // l//et request = Request::new_with_str_and_init(&url, &request_init);

    match request_init {
        Ok(response) => {
            if response.status() == 200 {
                let text = response.text().await;
                Ok(text.unwrap_or("Strange error".to_string()))
            } else {
                Err("File not found or other error".to_string())
            }
        }
        Err(i) => Err(format!("Error fetchiing {:?}", i)), //Err("Failed to fetch data".to_string()),
    }
}

fn find_model(name: &String, ctx: &Context<InnerApp>) -> &'static str {
    if name.contains("url=") {
        trace!("url detected: {name}");
        let url = extract_and_decode_url(name);
        if url.is_none() {
            return "illformed url '{name}'";
        }
        let url = url.unwrap();
        let link = ctx.link().clone();
        let url = url.clone();
        let future = async move {
            // Call your async function
            let result = fetch_data_from_api(&url).await;
            //panic!("Fetcuh success!");
            match result {
                Ok(s) => link.send_message(Message::NewModelFetch(s)),
                Err(s) => link.send_message(Message::NewModelFetch(s)),
            }
        };

        // Spawn the future and handle its result
        wasm_bindgen_futures::spawn_local(future);
        return "Waiting for Model";
    }

    match name.to_lowercase().as_str() {
        "gm" => include_str!("../../benchmarkmodels/GM.mlr"),
        "yeast" => include_str!("../../benchmarkmodels/Yeast_Model_MLR3.mlr"),
        "pp" => include_str!("../../benchmarkmodels/pp.mlr"),
        "lipoplex_orig" => include_str!("../../benchmarkmodels/Lipoplex_Model_original.mlr"),
        "lipoplex_ext" | "lipoplex" => {
            include_str!("../../benchmarkmodels/Lipoplex_Model_extended.mlr")
        }
        _ => " // Unknown model identifier",
    }
}

#[derive(PartialEq)]
enum SimStatus {
    Ready,
    Running,
    WaitForCancel,
}

enum ModelStatus {
    Initial,
    Errors,
    Ready,
}

struct InnerApp {
    endtime_string: String,
    sim_status: SimStatus,
    sim_call_num: usize,
    model_status: ModelStatus,
    right_panel: MessageOrPlot,
    text_model: TextModel,
    sim_worker: Box<dyn Bridge<Worker>>,
    _listener:
        monaco::api::DisposableClosure<dyn FnMut(monaco::sys::editor::IModelContentChangedEvent)>,
}
#[derive(Debug)]
enum Message {
    SetEndtime(String),
    TextUpdate,
    NewModelFetch(String),
    WorkerUpdate((MessageOrPlot, usize)),
    DoButton,
}

impl InnerApp {
    fn mark_errors(&mut self) -> bool {
        let errors = execute_mlrules_error_list(self.text_model.get_value().as_str());
        let marker_jsarray = js_sys::Array::new();
        let no_errors_found = errors.is_empty();
        let marker_jsarray: js_sys::Array = errors
            .into_iter()
            .map(|e| {
                let error_marker: IMarkerData = new_object().into();
                error_marker.set_message(e.what.as_str());
                error_marker.set_severity(MarkerSeverity::Error);
                error_marker.set_start_line_number(e.lstart as f64);
                error_marker.set_end_line_number((e.lstop) as f64);
                error_marker.set_start_column(e.cstart as f64);
                error_marker.set_end_column((e.cstop) as f64);
                error_marker
            })
            .collect();
        //let output = output.clone();
        //let text_model = text_model.clone();
        monaco::sys::editor::set_model_markers(self.text_model.as_ref(), "owner?", &marker_jsarray);
        if no_errors_found {
            self.model_status = ModelStatus::Ready;
        } else {
            self.model_status = ModelStatus::Errors;
        }
        return no_errors_found;
    }
    fn start_execution(&mut self) {
        self.mark_errors();
        self.sim_call_num += 1;
        self.right_panel = MessageOrPlot::FinalMessage(format!("Simulation start requested."));
        self.sim_worker.send(
            (SimMessage::input(
                SimulationInput {
                    input: self.text_model.get_value(),
                    endtime: self.endtime_string.clone(),
                },
                self.sim_call_num,
            )),
        );
        trace!("send message to worker");
        /*let result = execute_mlrules(
            self.text_model.get_value().as_str(),
            self.endtime_string.as_str(),
        );*/
        //self.right_panel = result;
        //code.set(text_model.get_value());
        /*spawn_local(async move {
            //let text_model = text_model.clone();

            .await;
            self.right_panel = result
        });*/
    }
    fn set_model_to_local_storage(val: &str) -> Option<()> {
        let w = web_sys::window()?;
        let s = w.local_storage().ok()??;
        s.set_item(LOCAL_STORAGE_ID, val);
        Some(())
    }

    fn get_model_from_local_storage() -> Option<String> {
        let w = web_sys::window()?;
        let s = w.local_storage().ok()??;
        Some(s.get_item(LOCAL_STORAGE_ID).ok()??)
    }
}

impl Component for InnerApp {
    type Message = Message;
    type Properties = ModelSelectionProps;
    fn create(ctx: &Context<Self>) -> Self {
        let props = ctx.props();
        //trace!("Called with {:?}", props);
        let default_code = match &props.model {
            None => {
                Self::get_model_from_local_storage().unwrap_or(format!(
                    "{}",
                    include_str!("../../benchmarkmodels/Yeast_Model_MLR3.mlr")
                ))
                /* match web_sys::window(){
                    None => ,
                    Some(w) => w
                }
                match web_sys::Storage::get("mlrules_model", "") {}*/
            }
            Some(x) => find_model(x, ctx).to_string(),
        };
        let execution_number = use_state_eq(|| 0 as usize);

        let endtime_str = use_state(|| match (&props.endtime, &props.endtimeunit) {
            (Some(t), Some(u)) => format!("{t}[{u}]"),
            (_, _) => format!("200[min]"),
        });
        let mut tm = TextModel::create(default_code.as_str(), Some(ID), None).unwrap();
        let callback_for_change = ctx.link().callback(|_| Message::TextUpdate);
        let listner = tm.on_did_change_content(move |ev| callback_for_change.emit(ev));

        let cb = {
            let link = ctx.link().clone();
            move |e| link.send_message(Self::Message::WorkerUpdate(e))
        };
        trace!("init webworker");
        let worker = Worker::bridge(Rc::new(cb));
        trace!("webworker done");
        InnerApp {
            endtime_string: match (&ctx.props().endtime, &ctx.props().endtimeunit) {
                (Some(t), Some(u)) => format!("{t}[{u}]"),
                (_, _) => format!("300[min]"),
            },
            sim_status: SimStatus::Ready,
            sim_call_num: 0,
            model_status: ModelStatus::Initial,
            right_panel: MessageOrPlot::FinalMessage(format!("Hit Run to start the simulaton!")),
            text_model: tm,
            sim_worker: worker,
            _listener: listner,
        }
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        let endtimeref = NodeRef::default();

        // todo move to message
        let on_endtime_set =
            // let endtime_str = endtime_str.clone();
            {
                let endtimeref2 = endtimeref.clone();
                ctx.link().callback(move|_| Message::SetEndtime(endtimeref2.cast::<HtmlTextAreaElement>().unwrap().value()))
            };
        //Message::None
        //Message::SetEndtime()
        // })
        // })
        //endtime_str.set(endtimeref.cast::<HtmlTextAreaElement>().unwrap().value());
        //trace!("changed endtime to {:?}", endtime_str)
        // }) //endtime_str = k)

        // Communicate with the editor
        //let text_model = use_state_eq(|| TextModel::create(default_code, Some(ID), None).unwrap());

        //let output =
        //     use_state_eq(|| ErrorOrPlot::Error(format!("Hit Run to start the simulaton!")));

        // text_model -> code on button click
        let on_run_clicked = {
            ctx.link().callback(|_| Message::DoButton)
            //let text_model = text_model.clone();
            //let code = code.clone();
            //let endtimeref = endtimeref.clone();
            //let output = output.clone();
            //move |_| {}
        };

        //use_effect_with

        // text_model -> code on hotkey
        let on_editor_created = {
            //let text_model = text_model.clone();
            Callback::from(move |editor_link: CodeEditorLink| {
                log::trace!("render {editor_link:?}");
                editor_link.with_editor(|editor| {
                    // Register Ctrl/Cmd + Enter hotkey
                    /*let keycode = monaco::sys::KeyCode::Enter.to_value()
                    | (monaco::sys::KeyMod::ctrl_cmd() as u32);*/
                    let raw_editor: &IStandaloneCodeEditor = editor.as_ref();
                    /*raw_editor.add_command(
                        keycode.into(),
                        js_closure.as_ref().unchecked_ref(),
                        None,
                    );*/

                    // While we have the raw editor, also set the indentation level
                    let opts: monaco::sys::editor::ITextModelUpdateOptions =
                        Object::new().unchecked_into();
                    opts.set_tab_size(Some(2.0));
                    raw_editor.get_model().unwrap().update_options(&opts);
                });
            })
        };

        // Load examples when requested
        /*let on_example_selected = {
            //let text_model = text_model.clone();
            //let code = code.clone();
            use_callback(
                move |new_code: usize, text_model| {
                    unimplemented!()
                    //self.text_model = new_code;
                    //code.set(String::from(new_code));
                },
                //self.text_model,
            )
        };*/

        trace!("endtime set to '{}'", (self.endtime_string));
        let button_text = match self.sim_status {
            SimStatus::Running => "cancel",
            SimStatus::Ready => "run",
            SimStatus::WaitForCancel => "",
        };
        html! {
            <div class="main-container">
                <div class="controls">
                    <button onclick={on_run_clicked}>{ button_text }</button>
                    <input type="text" ref={endtimeref} oninput={on_endtime_set} value={(self.endtime_string.clone())} />
                    //<Examples onchange={on_example_selected} />
                    //<button class="help" onclick={open_help}>{ "What am I looking at?" }</button>

                    /*<Checkbox label="Show Bytecode" onchange={on_show_bytecode_clicked} />
                    <Checkbox label="Trace Execution" onchange={on_trace_clicked} />
                    <Checkbox label="STD Mode" onchange={on_std_clicked} />
                    <Checkbox label="Stress GC (slow)" onchange={on_stress_gc_clicked} />
                    <Checkbox label="Log GC (spammy)" onchange={on_log_gc_clicked} />*/
                </div>

                <div class="code-container">
                    <MLREditor {on_editor_created} text_model={(self.text_model).clone()} />
                    <Output output_text={(self.right_panel).clone()} />
                </div>

                //<Help onclose={close_help} />
            </div>
        }
    }
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        //trace!("Handle message{:?}", msg);
        match msg {
            Message::NewModelFetch(s) => {
                trace!("new model text");
                self.text_model.set_value(&s);
                true
            }
            Message::TextUpdate => {
                trace!("ev: Text update");
                self.model_status = ModelStatus::Initial;
                self.mark_errors();
                Self::set_model_to_local_storage(&self.text_model.get_value());
                false
            }
            Message::DoButton => {
                trace!("ev: run");
                match self.sim_status {
                    SimStatus::Running => {
                        self.sim_worker.send(SimMessage::cancel(self.sim_call_num));
                        self.sim_call_num += 1;
                        self.sim_status = SimStatus::WaitForCancel;
                        self.right_panel =
                            MessageOrPlot::IntermediateMessage(format!("Cancelation Requested\nif this takes long, you might have to reload the page\nIf the model is large, individual steps take long, and cancelation only happends in between steps."));
                        true
                    }
                    SimStatus::Ready => {
                        self.sim_status = SimStatus::Running;
                        self.start_execution();
                        true
                    }
                    SimStatus::WaitForCancel => false,
                }
            }
            Message::SetEndtime(endtime_str) => {
                trace!("ev: set endtime");
                self.endtime_string = endtime_str;
                false
            }
            Message::WorkerUpdate((ErrorOrPlot, sim_run)) => {
                if sim_run != self.sim_call_num {
                    self.sim_status = SimStatus::Ready;
                    return match &ErrorOrPlot {
                        MessageOrPlot::FinalMessage(_) => {
                            self.right_panel = ErrorOrPlot;
                            true
                        }
                        _ => false,
                    };
                }
                trace!("ev: update from worker");
                if ErrorOrPlot.is_final() {
                    self.sim_status = SimStatus::Ready;
                } else {
                    self.sim_worker
                        .send(SimMessage::next_step(self.sim_call_num));
                }
                self.right_panel = ErrorOrPlot;
                true
            }
        }
    }
}

fn new_object() -> JsValue {
    js_sys::Object::new().into()
}
