//use crate::ml_rules_web::mlr_editor::register_mlr;
fn main() {
    //Worker::register();
    console_log::init_with_level(log::Level::Trace).unwrap();
    ml_rules_web::mlr_editor::register_mlr();
    yew::Renderer::<ml_rules_web::app>::new().render();
}
