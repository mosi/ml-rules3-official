//use web_sys::Worker;
//use yew_agent::Worker;

use ml_rules_web::execute::*;
use yew_agent::PublicWorker;

fn main() {
    console_log::init_with_level(log::Level::Trace).unwrap();
    ml_rules_web::execute::Worker::register();
}
