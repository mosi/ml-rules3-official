/*
use monaco::yew::CodeEditorLink;
use monaco::*;
use monaco::yew::*;
use yew::prelude::*;
use yew::Properties;
use yew::*;


use monaco::{
    api::{CodeEditorOptions, TextModel},
    sys::{
        editor::{BuiltinTheme, IMarkerData, IModelDecorationOptions, IModelDeltaDecoration},
        IMarkdownString, MarkerSeverity, Range,
    },
    yew::CodeEditor,
};
use std::rc::Rc;
use wasm_bindgen::{JsCast, JsValue};
use yew::prelude::*;*/
use js_sys::{Array, Object};
use monaco::sys::editor::IEditorOptionsWordWrap;
use monaco::sys::languages::LanguageConfiguration;
use monaco::{
    api::{CodeEditorOptions, TextModel},
    sys::editor::{BuiltinTheme, IStandaloneCodeEditor},
    yew::{CodeEditor, CodeEditorLink},
};
use wasm_bindgen::closure::Closure;
use wasm_bindgen::prelude::*;
use yew::prelude::*;

use wasm_bindgen::JsCast;

pub const ID: &str = "mlr";
//const CONTENT: &str = include_str!("../../testing/a2b_cpx_star_2out.mlr");

#[derive(PartialEq, Properties)]
pub struct MLREditorProps {
    pub on_editor_created: Callback<CodeEditorLink>,
    pub text_model: monaco::api::TextModel,
}

#[function_component]
pub fn MLREditor(props: &MLREditorProps) -> Html {
    let MLREditorProps {
        on_editor_created,
        text_model,
    } = props;
    //let text_model = TextModel::create(CONTENT,Some(ID),None).unwrap();
    /*let set_markers_callback = {
        let text_model = text_model.clone();
        Callback::from(move |_|{
            let error_marker : IMarkerData = new_object().into();
            error_marker.set_message("test message");
            error_marker.set_severity(MarkerSeverity::Error);
            error_marker.set_start_line_number(2.0);
            error_marker.set_start_column(2.0);
            error_marker.set_end_line_number(2.0);
            error_marker.set_end_column(10.0);
            let marker_jsarray = js_sys::Array::new();
            marker_jsarray.push(&error_marker);
            monaco::sys::editor::set_model_markers(text_model.as_ref(),"owner?",&marker_jsarray);
        })

    };*/

    let mut options = CodeEditorOptions::default()
        .with_language(ID.to_string())
        .with_builtin_theme(BuiltinTheme::Vs)
        .with_automatic_layout(true)
        .to_sys_options(); //
    options.set_word_wrap(Some(IEditorOptionsWordWrap::On));

    html! {
        <CodeEditor classes={"code"} options={ options } model={text_model.clone()} />
    }
}

pub fn register_mlr() {
    monaco::sys::languages::register(&language());
    monaco::sys::languages::set_monarch_tokens_provider(ID, &make_tokens_provider().into());
    monaco::sys::languages::set_language_configuration(ID, &language_configuration());
}

fn language() -> monaco::sys::languages::ILanguageExtensionPoint {
    let lang: monaco::sys::languages::ILanguageExtensionPoint = Object::new().unchecked_into();
    lang.set_id(ID);
    lang
}

#[wasm_bindgen(module = "/src/MlrulesTokensProvider.js")]
extern "C" {
    #[wasm_bindgen(js_name = "makeTokensProvider")]
    fn make_tokens_provider() -> Object;
}

fn language_configuration() -> LanguageConfiguration {
    // I'm sure there's a neater way of doing this but failed to figure it out in like 2 minutes so /shrug
    let brackets = Array::new_with_length(2);
    {
        let pair = Array::new_with_length(2);
        pair.set(0, JsValue::from_str("("));
        pair.set(1, JsValue::from_str(")"));
        brackets.set(0, pair.into());
    }
    {
        let pair = Array::new_with_length(2);
        pair.set(0, JsValue::from_str("{"));
        pair.set(1, JsValue::from_str("}"));
        brackets.set(1, pair.into());
    }

    let cfg: LanguageConfiguration = Object::new().unchecked_into();
    cfg.set_brackets(Some(&brackets));
    cfg
}

// Creates a new `JsValue`. Done for convenience and readability.
fn new_object() -> JsValue {
    js_sys::Object::new().into()
}
