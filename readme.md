# ML-Rules 3

This is a command line tool to read and execute ML-Rules like models.

## Getting started

You can use a web-based version of ML-Ruels 3, found under the link https://mlrules.pages.dev/, or run the simulator via a command line.

To run ML-Rules 3 from a command line tool, you first have to clone this repo (`git clone git@git.informatik.uni-rostock.de:mosi/ml-rules3-official.git` or `git clone https://git.informatik.uni-rostock.de/mosi/ml-rules3-official.git`)

To compile the software, you need the rust compiler.
It's best installed via the [rustup installer](https://rustup.rs/).

To compile the software, go into the directory of the cloned repo and execute the `cargo build --release` command in a command window.
This should build the software.

To run it, you can use the `cargo run --release` command.
If you want to use any flags (see [Usage](#Usage)), they must be separated by a double dash `--`.

## Build a Model
Here, we will show how to build a simple model in the domain-specific language (DSL) ML-Rules by building a predator-prey model. More advanced features of ML-Rules 3, which are needed to build complexer models, are described afterward.

### Constants
At the beginning of the model, we define the constants used. Constants are marked by the keyword `const`.
```
/* constants */
const k_mate = 1 [1/day];
const k_hunt = 0.0001[1/day];
const k_die = 1 [1/day];
const max_pop = 4e5 + 6e5;
```
In the definition of constants, mathematical operators (e.g., +, -, +, /, ...) can be used. Also, constants  can have a unit or be unitless.

### Units
To indicate a unit of a value, use square braces.
Possible unit expressions look like: `[Hz]`,`[1/h]`, `[h^-1 m^2 / m^3 s]` etc.

Currently, the following units are known to the tool:

* *1, centi, milli, micro, µ, m, cm, mm, nm, s, Hz, min, day, h, kg, g, A, V, C, K, mol*

But more can be easily added if you run ML-Rules 3 from a command line tool (see [ml-rules-3\src\lib\input\units.rs](https://git.informatik.uni-rostock.de/mosi/ml-rules3-official/-/blob/main/src/lib/input/units.rs) ).

### Species
Next, we need to list the species present in the model. Here, we only have two species.
```
/* species definitions*/
Wolf()
Sheep()
```

### Initial State
After we have declared the species present in the model, we need to specify the initial state of the solution. This is done by the keyword `Initial:`
```
/* Initial state */
Initial:  1000 Wolf() + 1000 Sheep();
```

### Rules
Rules are defined as:
```
/* Rules */
Wolf + Sheep -> 2 Wolf @ k_hunt;
Sheep -> 2 Sheep @ k_mate;
Wolf ->  @ 1[1/day];
```
They follow the pattern: `substate -> product @ rate`.

### Rates
Rates with one `@` are implicitly mass-action kinetics and there is no need (and it is not possible), to explicitly count a species via `#`.
If you want to count species yourself e.g., if you want to use Michaelis–Menten kinetics or a Hill equation, use two `@` symbols like:
```
A -> B @@ #A*#A * 3[Hz];
``` 
Using explicit names is also possible here:
```
A:a -> B @@ #a*#a * 3[Hz];
```
One should be carefull using this type of rate.
It's propensity will be multiplied by the number of compartment level (i.e. excluding amounts) matches of the left side in the system (see section [Compartments](#Compartments)).

### Output
Lastly, we need to add which species should be observed. Here, the keyword is `output`.
```
output ##Wolf;
output ##Sheep;
```

### Run simulation
The complete predator-prey model can be found at https://mlrules.pages.dev/pp/10/day.
In the web-based simulator, you simply have to add the simulation time (10[day]) at the top left corner and press run.
Note the model does not have to be specified in this order to be executed. All parts can be rearranged in any order.

If the simulation is executed with a command line tool, save the model as `predator_prey.mlr` and execute it with
```
cargo run --release -- predator_prey.mlr --simulate-until 10[day] --output-csv --num-obs 10
```
An explanation of the -- flags and additional ones can be found in the [Usage](#Usage) section.

## Advaced Features
### Compartments
A particle may be nested within another particle, a so-called compartment. The use of curly braces notes this nesting. E.g.,
```
Inital: Compartmetn{10 A} + 5 A;
/* Rules */
A + Compartment -> Compartment{A} @ 1 [Hz];
```

### Dynamic structure
It is possible for Compartments to be created or removed from the system or for the contents of the compartments (referenced by the so-called *rest solution*) to be (re-)moved.
Below are some examples of such reactions.
These reactions incur a relatively high runtime cost every time they fire.
```
Compartment{ A + ?} -> Compartment{A} + ? @@ 0.04[Hz]; // everything but A leaves the compartment
Compartment{ ? } -> @ 0.1 [Hz]; // the compartment and its content is removed
 -> Compartment{3 A} @ 1[Hz]; // a compartment with 3 A is created
Compartment{ ?one } + Compartment{ ?two } ->  ?one + ?two  @ 1[Hz]; // two compartments are removed, but their content remains
Compartment{ ?one } + Compartment{ ?two } -> Compartment { ?one + ?two } @ 1[Hz]; // two compartments  fuse, and their content is combined in the fused one
Compartment{ ? }  -> Compartment {  }  + ? @ 1[Hz]; // the content of a compartment is removed
Compartment{ ?one + ?two} -> Compartment{ ?one } + Compartment{ ?two } @ 1[Hz]; // split the compartment into two and distribute its content evenly between the now compartments
Compartment{ ?one[70] + ?two[30]} -> Compartment{ ?one } + Compartment{ ?two } @ 1[Hz]; // split the compartment into two with 70% of its content in the first new one and 30% of the content in the second one
```
To enable dynamic reactions, pass the `--dynamic` flag to the simulator.

Rest solutions need to be specified for every compartment in such reactions.
Rest solution names start with *?*, and *?* is already a valid name, however, these names need to be unique.

If you run ML-Rules3 from a command line tool, you will get some output to the command line every time such a dynamic reaction happens.
If that is not helpful, use `--dynamic-silent` instead.

### Attributes
When declaring a species, you can also give that species attributes with a name and a specific type.
```
F( k : int, jabel : string, mee : float[m^2]);
```
Here, F has an attribute k of type int and an attribute jabel of type string.
mee is a float that is measuerd in square meters.

On the left side of rules, attributes can be restricted to match only specific entities of a species, e.g.
```
F( k + 4 >= 23) -> F(mee = 23[m^2] - 1 [cm^2]) @ ...
```
On the right side of a rule, they can be assigned.
When assigning attributes they can also be taken from named particle on the left side.
```
Cell:c1 + Cell:c2 -> Cell(volume = c1.volume + c2.volume) @ 1[Hz];
```
Attributes that should not be expanded into a network (hybrid execution) can be marked with a `*` in their type.
E.g. `A(value : int*)`
If you want to use these star-attributes with non-compartments, you need to enable a rust/cargo feature on compilation with `cargo run --features hybrid_att`.
This will slow down regular execution and should only be used when needed.

### Enums
Attributes can also have categories/enums as types.
For example:
```
A(attr:{phase1, phase2});
A(attr == "phase1") -> A(attr = "phase2") @ 1000[Hz];
```
Here `attr` is like a string, but it can only take those values defined in the type.
`A(attr = "phase3")` would give an error. This type is used to avoid typos for strings.

### Left-right correspondence
There is no need to explicitly name all attributes for a species on the right side of a reaction.
For example:
```
A(x : int);
B();

A(x==4) -> A + B @ ...;
```
Here, A on the right side will automatically correspond to the A on the left side, and x will be set to 4 as well.
```
A -> A + B@ ...;
```
This is also ok. Here, the A on the right side will automatically find its corresponding entity on the left and copy its attribute values.

If there are multiple species on the left or right side, you need to give them names explicitly using a colon:
```
A:a + A(x==3) -> A:a + B@ ...;
```

#### Guards
Sometimes, a reaction has a specific constraint on the amount of something.
You can use a guarding if.
Only when the `if` is true, the reaction will happen.
```
A -> B @ if #A > 12 then 32[Hz]; 
```
You can also use constants or reference attributes from the left side of the rule in the guard.


You can also chain multiple of these Expressions or use parenthesis:
```
A -> B @ if #A > 12 && (#A <= 30) then 32[Hz]; 
```
#### Max count

You can add a maximum reaction count to a rule via `A -{ max_count = 10 }-> B ...`

#### Time-dependent Expressions
The current simulation time can be accessed via `time()` within rate expressions or the guards of rates.
Of course, this time also has a unit of time.
Within the simulation, this time is not exact, but it will be evaluated at fixed steps.

### Functions
Custom functions, to simplify notation, can be defined using the `function` keyword:
```
function square(x) { x * x};
function times(a , b){ a*b};
```
There is also a small set of buildin functions:
 * `pi(), euler(), abs(x), exp(x), sin(x), cos(x), round(x), ceil(x), floor(x), RandomGauss(mu,var), RandomUniform(a,b), RandomPoisson(lambda)`

### Counting in subsolution
Sometimes, you need the count of a species that is not directly involved in the reaction's left side.
To do that, you can use a *global count*, denoted by two counting symbols (##).

You can use these counts in guards, rules, and other places where numbers are needed.
For example
```
A(v==2) -> B @ if ##A > 10 then 1[Hz];
```
means that an A with v Equal 2 will turn into a B only when the total number of As in the system (independent of their attribute) is Greater than 10.

If you want to count the number of entities in a particular compartment, use the *in* keyword.
Write:
```
C:c -> A  @ if #(A in c) > 3 then 0.4 [Hz]; 
```
If you want to count the total number of As in any C in the system, independent of the left side of the rule, you can use two `##`:
```
C:c -> A  @ if ##(A in C) > 3 then 0.4 [Hz];
```
The `in` keyword can be replaced by `anywhere_in` to denote a count arbitrarily deeply nested structures, like `C{ B{ A }}` (see [Dynamic structure](#Dynamic structure)) where the A is only counted for `anywhere_in C` and not for `in C`.

### Observations
If none are given, all species are observed into the csv file, using their expanded names.
To observe the amount of a specific entity add this line:
```
output ##A(f =="test");
```

You can also observe particular nested species like:
```
output ##(K(f == 4) in A);
```
This will count all the K that have f set to 4 and are in compartments of type A.

## Output
Various output options are implemented. This includes creating a plot of the defined observations. They are called with `--output-*`

## Usage
You can use the 'cargo run- release' command to run ML-Rules 3 from a command line tool.
If you want to use any flags, they need to be separated by a double dash `--`.
For example, to run the included predator-prey model:

```
cargo run --release -- benchmarkmodels/pp.mlr --simulate-until 10[day] --output-csv --num-observation 10
```

Other command line arguments are shown via `cargo run -- --help`

```
Options:
      --simulate-until <SIMULATE_UNTIL>
          Simulation end
      --num-observation <NUM_OBSERVATION>
          Number of simulation observations [default: 10]
      --dynamic
          allow dynamic restructuring/nesting
      --dynamic-silent
          allow dynamic restructuring/nesting but prints no output
      --print-every-step
          This debug function prints at every step and change, the structure and all reactions that are fired
      --no-cache-file
          Do not use a cache file. This is useful for some performance measures
      --tau-step <TAU_STEP>
          Tau leaping step
      --approx <APPROX>
          Approximation grade
      --code-gen
          generate optimized code for not dynamic models
      --num-reps <NUM_REPS>
          Number of parallel replications to be executed (in parallel) [default: 1]
      --expansion-steps <EXPANSION_STEPS>
          Maximum number of "Number of steps used for expansion" [default: 100]
      --output-prefix <OUTPUT_PREFIX>
          Output prefix
      --output-json
          give a json file output
      --output-json-zip
          give a json zip file output
      --output-flat-table
          give a flat table output
      --output-csv
          give a basic csv counts
      --output-rule-counts
          give rule counts
      --output-latex
          give latex version of input model
      --no-new-templates
          advanced option to generate new performance templates
      --switch-parenthesis
          switch parenthesis from [] and {} to {} and []
      --set-constant <SET_CONSTANT>
          set a constant to a different value
      --seed <SEED>
          (experimental) seed for simulation

```

If you want more information, you can enabel logging by setting `RUST_LOG=DEBUG` when calling cargo.


## Errors
Most errors will indicate where they came from by giving the line number (here, line 46):
```
thread 'main' panicked at '  --> 46:10
   |
46 | B -> A + C(Newcount = 1) @ 3.{Hz};
   |          ^
   |
   = The name 'Newcount' is not an attribute for species 'C''
```
But this functionality is not implemented for all errors yet.


## Performance Templates
Many rules and rate expressions fall into specific patterns.
The simulator can map these to specialized code, that can only compute this one expression. 
In practice this results in a 60-90% speedup for regular step cost.
The simulator already comes with many useful patterns. 
You can switch off by using the `--write-no-new-templates` flag. 
To switch off performance templates entirely, use `--no-default-features` on cargo during compilation.

## Example models
Several models that use many of the advanced features can be found under [ml-rules-3/benchmarkmodels](https://git.informatik.uni-rostock.de/mosi/ml-rules3-official/-/tree/main/benchmarkmodels).

